<?php

session_start();

if( !ini_get('safe_mode') ){ 
    set_time_limit(600); 
} 
ini_set('display_errors', '0');

if (empty($titletag)) {
	$titletag = "Professional BC Wedding Awards";
}

// DB Credentials
define('DBNAME', 'bcawards_db1');
define('DBUSER', 'root');
define('DBPWD', '');

// Base Virtual
define('_MY_BASE_',$_SERVER['DOCUMENT_ROOT']."/");	

// Base HTTP
define('_MY_HREF_', "http://www.bcweddingawards.com/");

// Submission files
define('_MY_FILE_H_',_MY_HREF_.'entryfiles/');
define('_MY_FILE_V_',_MY_BASE_.'entryfiles/');

// --------------------------------------------------------------------------------------
$conn = mysql_connect("localhost", "root", "");
if (!$conn) {
	echo "Could not establish connection to database";
	exit;
} 

// Reset this back to bcawards_db1 when go live
$db = mysql_select_db("bcawards_db1", $conn);
if (!$db) {
	echo "Could not establish connection to DB";
	exit;
}


// --------------------------------------------------------------------------------------
function displayCountries($country) {

	$sqlquery = "SELECT region, country FROM country ORDER BY regionorder, country";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			$region = stripslashes($row["region"]);
			
			if ($region!=$oldregion) {
				echo "</optgroup><optgroup label=\"".$region."\">";
			}

			if ($row["country"]== $country)	 {
				echo "<option value=\"".$row["country"]."\" selected>".stripslashes($row["country"])."</option>";
			} else {
				echo "<option value=\"".$row["country"]."\">".stripslashes($row["country"])."</option>";
			}
			
			$oldregion = $region;

		}
	}

}

// --------------------------------------------------------------------------------------
function getSubmissionYear($dt) {

	if (!empty($dt)) {
						
		$sql = "SELECT subyear FROM submissionyear WHERE '".$dt."' BETWEEN startdate AND enddate LIMIT 0,1 ";
		$result = mysql_query($sql) or die (mysql_error()."<br><br>".$f);
		$row = mysql_fetch_array($result) ;
		
		if ($row) {							
			return $row["subyear"];
		}
		
	}

	return "";
	
}

// --------------------------------------------------------------------------------------
function getField($tmpfield,$tmptable,$tmpcond) {

	// Only used in Header file
	if ($tmpcond=="") {
		$query = "SELECT (".$tmpfield.") AS SQLVal FROM $tmptable";
	} else {
		$query = "SELECT (".$tmpfield.") AS SQLVal FROM $tmptable WHERE $tmpcond";
	}
	
	//echo $query."<hr>";
	
	$result = mysql_query($query);

	// check for no return
	if ($result===false) return '';

	$row = mysql_fetch_array($result); //or die (mysql_error());

	if ($row) return $row['SQLVal'];
	return '';
}

// --------------------------------------------------------------------------------------
function createUploadFolder( $fullpath ) {
	
	if (!empty($fullpath)) {
		if (!is_dir($fullpath)) {

				// Create directory
				mkdir($fullpath) or die("Unable to create directory: ".$fullpath);
		   
				if (is_dir($fullpath)) {
		   		chmod($fullpath, 0755);
				}
		   
		} 
	}
	
}

// --------------------------------------------------------------------------------------
function displayGalaQty($q) {

	$seatsleft = getField("galaseatsavailable", "settings", "settingsid=1");
	$galamaxperperson = getField("galamaxperperson", "settings", "settingsid=1");
	
	if ($galamaxperperson > $seatsleft) {
		$maxnumber = $seatsleft;
	} else {
		$maxnumber = $galamaxperperson;
	}

	if ($maxnumber>0) {
		
		for ($n=0; $n<$maxnumber+1; $n++) {
			if ($n == $q)	 {
				echo "<option value=\"".$n."\" selected>".$n."</option>";
			} else {
				echo "<option value=\"".$n."\">".$n."</option>";
			}
		}	
		
	} else {
		echo "<option value=\"-1\">Sold Out - Sorry!</option>";
	}

}

// --------------------------------------------------------------------------------------
function displayRSVPQty($eligible, $currentcount, $q) {

	$maxnumber = $eligible-$currentcount;
	
	if ($maxnumber>0) {
		
		for ($n=0; $n<$maxnumber+1; $n++) {
			if ($n == $q)	 {
				echo "<option value=\"".$n."\" selected>".$n."</option>";
			} else {
				echo "<option value=\"".$n."\">".$n."</option>";
			}
		}	
		
	} else {
		echo "<option value=\"-1\">Sold Out - Sorry!</option>";
	}

}

// --------------------------------------------------------------------------------------
function getDiscountPrice($cd) {
	
	if (!empty($cd)) {
						
		$sql = "SELECT price FROM galacode WHERE code='".$cd."' AND isdeleted=0 AND expires > now()";
		$result = mysql_query($sql) or die (mysql_error()."<br><br>".$f);
		$row = mysql_fetch_array($result) ;
		
		if ($row) {							
			return $row["price"];
		}
		
	}

	return -1;
	
}

// --------------------------------------------------------------------------------------
function delete_directory($dir) {

	if ($handle = opendir($dir)) {
		$array = array();
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != "..") {
					if(is_dir($dir.$file))
					{
						if(!@rmdir($dir.$file)) // Empty directory? Remove it
						{
							delete_directory($dir.$file.'/'); // Not empty? Delete the files inside it
						}
					}
					else
					{
						@unlink($dir.$file);
					}
        }
    }
    closedir($handle);
		@rmdir($dir);
	}

}

// --------------------------------------------------------------------------------------
function createPermakey()
{
	return rand(10000000, 99999999); 
	
}

// --------------------------------------------------------------------------------------
function isPage($test_page, $current_page) {

	// Truncate current page
	$lastslash = strrpos($current_page, "/");
	if ($lastslash>=0) {
		$current = substr($current_page, $lastslash + 1, strlen($current_page));
	} else {
		$current = $current_page;
	}

	// Truncate test page you are comparing
	$lastslash2 = strrpos($test_page, "/");
	if ($lastslash2>0) {
		$test = substr($test_page, $lastslash2+1, strlen($test_page));
	} else {
		$test = $test_page;
	}

	//echo "Comparing $test with $current - $lastslash2";

	if (strtolower($test) == strtolower($current)) {
		return true;
	} else {
		return false;
	}


}

?>