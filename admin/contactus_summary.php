<? include("system/top.inc"); ?>

<a href="email_summary.php">Mailing List</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="contactus_summary.php">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="nomination_summary.php">Nominations</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="bridal_summary.php">Bridal</a>
<br><br>

<font class="subtitle">Contact Us</font>
<br><br>
The following messages have been submitted via the Contact Us form of the website
<br><br>

<?

include("system/searchbox.inc");

$query = "SELECT DATE_FORMAT(created, '%m/%d/%Y %h:%i %p') AS tmpCreated, contactid, name, email, message ";
$query.= "FROM form_contact ";
$query.= "WHERE isdeleted=0 ";
if (!empty($_POST['search'])) {
	$query.= "AND (email LIKE '%".$_POST['search']."%' OR name LIKE '%".$_POST['search']."%' OR message LIKE '%".$_POST['search']."%') ";
}
$query.= "ORDER BY created DESC";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"120px\"><b>Date</b></td>";
	echo "<td class=\"header\" width=\"150px\"><b>Name</b></td>";
	echo "<td class=\"header\" width=\"180px\"><b>Email</b></td>";
	echo "<td class=\"header\"><b>Message</b></td>";
	echo "<td class=\"header\" width=\"60px\">&nbsp;</td>";	
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
					
		echo "<td width=\"120px\">".$row["tmpCreated"]."</td>";
		echo "<td width=\"150px\">".stripslashes($row["name"])."</td>";
		echo "<td width=\"180px\">".stripslashes($row["email"])."</td>";
		echo "<td>".nl2br(stripslashes($row["message"]))."</td>";
		echo "<td width=\"60px\" align=\"center\"><a class=\"onwhite\" href=\"javascript:deleteRecord('form_contact', '".$row["contactid"]."', 'contactid', '"._MY_HREF_ADMIN_."contactus_summary.php');\">Delete</a></td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>