<?php
include("system/top.inc"); 

// Show total seats left
$seatsremaining = getField("galaseatsavailable", "settings", "settingsid=1");
$seatspurchased = getField("SUM(qty)", "form_gala", "isdeleted=0 AND isarchived=0 AND paypalconfirm=1");

?>

<a href="gala_summary.php">Gala Online Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="gala_summary_archive.php">Archived Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="rsvp_summary.php">RSVP Summary</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="guest_summary.php">Guest List</a>
<br><br>

<font class="subtitle">Guest List (<?php echo $seatspurchased; ?> seats purchased, <?php echo $seatsremaining; ?> seats remaining)</font>
<br><br>

<?php
	if (!empty($errortext)) echo "<center><div class=\"errorblock\">".$errortext."</div></center><br><br>";
?>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	
	<b>Name: </b>&nbsp;<input type="text" name="guestname" value="<?php echo $guestname; ?>">&nbsp;&nbsp;
	<b>Company: </b>&nbsp;<input type="text" name="companyname" value="<?php echo $companyname; ?>">&nbsp;&nbsp;
	<input type="submit" name="addguest" value="Add">
	
</form>

<br><br>

<?php

//$query = "SELECT attendeenames, companyname, rsvptotal AS qty, permakey, 0 AS guestid, 1 AS Type FROM rsvp WHERE isdeleted=0 AND isarchived=0
//	UNION ALL 
//	SELECT attendeenames, company AS companyname, qty, rsvppermakey AS permakey, 0 AS guestid, 2 AS Type FROM form_gala WHERE isdeleted=0 AND paypalconfirm=1 AND isarchived=0
//	UNION ALL
//	SELECT guestname AS attendeenames, companyname, 1 AS qty, 'GUEST' AS permakey, guestid, 3 AS Type FROM guest WHERE isdeleted=0 AND isarchived=0
//	ORDER BY companyname ";

$query = "SELECT a.attendeenames, f.company, SUM(f.qty) AS qty, f.rsvppermakey, 1 AS Type, 0 AS guestid
	FROM form_gala f
	LEFT OUTER JOIN gala_attendee a ON (f.rsvppermakey = a.rsvppermakey AND a.isdeleted=0)
	WHERE f.isdeleted=0 AND f.isarchived=0 AND f.paypalconfirm=1
	GROUP BY a.attendeenames, f.company, f.rsvppermakey
	ORDER BY f.company";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	
	// Get attendeenames into 2-dimensional array	
	$counter = 0;
		
	while ($row = mysql_fetch_array($result)) {
		
		$attendeenames = stripslashes($row["attendeenames"]);
		$names_arr = explode(";", $attendeenames);		
		$company = stripslashes($row["company"]);
		$qty = $row["qty"];
		$permakey = $row["rsvppermakey"];
		$guestid = $row["guestid"];
		$type = $row["Type"];
				
		for ($x=0; $x<$qty; $x++) {
			$guestlist_array[$counter][0] = $company;
			
			if (!empty($names_arr[$x])) {
				$guestlist_array[$counter][1] = $names_arr[$x];	
			} else {
				$guestlist_array[$counter][1] = "TBD";	
			}
			
			$guestlist_array[$counter][2] = $permakey;
			$guestlist_array[$counter][3] = $type;
			$guestlist_array[$counter][4] = $guestid;
			$counter++;
			
		}
		
	}
	
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"20px\"><b>#</b></td>";
	echo "<td class=\"header\"><b>Company</b></td>";
	echo "<td class=\"header\" width=\"120px\" align=\"center\"><b>Type</b></td>";
	echo "<td class=\"header\" width=\"200px\"><b>Guest</b></td>";
	echo "<td class=\"header\" width=\"50px\">&nbsp;</td>";
	echo "</tr>";
	
	for ($i=0; $i<$counter; $i++) {
		
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
		
		echo "<td width=\"20px\">".($i+1)."</td>";
		echo "<td>".$guestlist_array[$i][0]."</td>";
		echo "<td width=\"120px\" align=\"center\">";
		if ($guestlist_array[$i][3]=="1") {
			echo "Paid ticket";
		} else if ($guestlist_array[$i][3]=="3") {
			echo "Manually entered";
		}
		echo "</td>";
		echo "<td>".$guestlist_array[$i][1]."</td>";
		
		if ($guestlist_array[$i][2]!="GUEST" && !empty($guestlist_array[$i][2])) {
			echo "<td width=\"50px\" align=\"center\"><a target=\"new\" href=\""._MY_HREF_."tickets.php?code=".$guestlist_array[$i][2]."\">Link</a></td>";	
		} else if ($guestlist_array[$i][2]=="GUEST") {
			echo "<td width=\"50px\" align=\"center\"><a href=\"javascript:deleteGuest(".$guestlist_array[$i][4].")\">Delete</a></td>";	
		} else {
			echo "<td width=\"50px\" align=\"center\">&nbsp;</td>";
		}
				
		echo "</tr>";
		
	}
	
	echo "</table>";
	echo "<br><br>";
	echo "<b>Total attendees: ".$i."</b>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>