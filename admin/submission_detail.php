<?php 

if (empty($recordid)) $recordid = $_POST['recordid'];
if (empty($recordid)) $recordid = $_GET['id'];

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

// ----------------------------------------------
// Get position in collection, if exists
// ----------------------------------------------
if (empty($idcoll)) $idcoll = $_POST['idcoll'];
if (empty($idcoll)) $idcoll = $_GET['idcoll'];

if (!empty($idcoll)) {
	$idarr = explode("_", $idcoll);	
	$currentpos = array_search($recordid, $idarr);
		
	if ($currentpos==0) {
		$previd="";	
		$nextid = $idarr[$currentpos+1];
	} else {
		$previd = $idarr[$currentpos-1];
		$nextid = $idarr[$currentpos+1];
	}
		
	if ($currentpos==count($idarr)-1) {
		$previd = $idarr[$currentpos-1];
		$nextid="";	
	} else {
		$previd = $idarr[$currentpos-1];
		$nextid = $idarr[$currentpos+1];
	}	
	
}

include("system/top.inc"); 

if (!empty($recordid)) {
	include("get/submission_get.inc");	
	
} else {
	// Default values 
	$country = "Canada";
	$createddisplay = "Submitted on ".gmdate('m/d/Y h:i A');
	
	// Get file uploads
	$numimages = 12;
	include("get/submission_files_get.inc");
	
}

// Make sure they are allowed to edit this lead...
if ($_SESSION['s_role']=="Administrator") {
	$canedit = true;
}

if (!empty($action)) {
	
	if ($canedit) {
		include("include/submission_fields_edit.inc");	
		
	} else {
		include("include/submission_fields_read.inc");
	}
		
} else {
	include("include/submission_fields_read.inc");	
}

include("system/bottom.inc"); 

?>