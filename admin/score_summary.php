<? 
include("system/top.inc"); 	
include("system/getsearchvars_simple.inc");
?>

<font class="subtitle">Scores</font>
<br><br>

<?

include("system/searchbox.inc");

$query = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y') AS tmpCreated, ";
$query.= "s.companyname, s.contactperson, s.categoryid, s.status, s.phone, s.nominee, ";
$query.= "s.city, s.state, s.country, s.submissionid, s.subyear, cat.title AS category, ";
$query.= "AVG(sc.overallscore) AS AvgScore, COUNT(sc.scoreID) AS NumScores ";
$query.= "FROM submission s ";
$query.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$query.= "LEFT OUTER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.isdeleted=0) ";
$query.= "WHERE s.isdeleted=0 AND s.isarchived=0 ";
$query.= $sqlsearch;

$query.= "GROUP BY s.created, ";
$query.= "s.companyname, s.contactperson, s.categoryid, s.status, s.phone, s.nominee, ";
$query.= "s.city, s.state, s.country, s.submissionid, s.subyear, cat.title ";
$query.= "ORDER BY cat.title, AvgScore DESC ";
$query.= "LIMIT $start, $limit";

$sqlcount = "SELECT COUNT(s.submissionid) AS SQLCount ";
$sqlcount.= "FROM submission s ";
$sqlcount.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$sqlcount.= "WHERE s.isdeleted=0 AND s.isarchived=0 ";
$sqlcount.= $sqlsearch;

$t_result = mysql_query($sqlcount) or die(mysql_error()."<br><br>".$sqlcount);
$t_row = mysql_fetch_array($t_result);
$total_count = $t_row["SQLCount"];

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
		
	include ("system/nav.php");
	
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	//echo "<td class=\"header\" width=\"100px\"><b>Submitted</b></td>";	
	echo "<td class=\"header\" width=\"70px\" align=\"center\"><b>Year</b></td>";	
	echo "<td class=\"header\" width=\"250px\"><b>Category</b></td>";
	echo "<td class=\"header\" width=\"200px\"><b>By Company</b></td>";	
	echo "<td class=\"header\" width=\"70px\"><b>Status</b></td>";	
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b>Score</b></td>";	
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b># Scores</b></td>";	
	echo "<td class=\"header\" width=\"50px\" align=\"right\"><b>ID</b></td>";	
	echo "</tr>";
		
	$i = 1;
	$x = $start + 1;	
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		//echo "<td width=\"100px\">".$row["tmpCreated"]."</td>";		
		echo "<td width=\"70px\" align=\"center\">".$row["subyear"]."</td>";	
		echo "<td width=\"250px\">".stripslashes($row["category"])."</td>";
		echo "<td width=\"200px\">".stripslashes($row["companyname"])."</td>";
		echo "<td width=\"70px\">".stripslashes($row["status"])."</td>";
		echo "<td width=\"80px\" align=\"center\">";
		if (!empty($row["AvgScore"])) {
			echo number_format($row["AvgScore"],1)."%";	
		}
		echo "&nbsp;</td>";
		echo "<td width=\"80px\" align=\"center\">";
		if (!empty($row["NumScores"])) {
			echo $row["NumScores"];	
		} else {
			echo "0";
		}
		echo "</td>";
		echo "<td width=\"50px\" align=\"right\"><a href=\"submission_detail.php?id=".$row["submissionid"]."\">".$row["subyear"]."-".$row["submissionid"]."</a></td>";
		echo "</tr>";
		
		$i++;
		$x++;
		
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>