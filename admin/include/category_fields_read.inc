<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $title;
		
		if ($canedit) {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?id=".$recordid."&a=edit\">Edit</a>]</font>";
		}
		
	} else {
		echo "New Category";
	}

?>

</font><br><br>

<?
	
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Category Details:</td></tr>

<tr valign="top">
	<td class=label>Display Order:</td>
	<td class=field><? echo $sortweight; ?></td>
</tr>

<tr valign="top">
	<td class=label>Name:</td>
	<td class=field><? echo $title; ?></td>
</tr>

<tr valign="top">
	<td class=label>Code:</td>
	<td class=field><? echo $code; ?></td>
</tr>

<tr valign="top">
	<td class=label>Description:</td>
	<td class=field><? echo nl2br($comment); ?></td>
</tr>

<tr valign="top">
	<td class=label>Images Allowed:</td>
	<td class=field><? echo $numimages; ?></td>
</tr>

<tr valign="top">
	<td class=label>Instructions:</td>
	<td class=field><? echo nl2br($instructions); ?></td>
</tr>

<tr valign="top">
	<td class=label>Question 9:</td>
	<td class=field><? echo $question9; ?></td>
</tr>

<tr valign="top">
	<td class=label>Question 8:</td>
	<td class=field><? echo $excludeq8_display; ?></td>
</tr>

<tr valign="top">
	<td class=label>Question 9:</td>
	<td class=field><? echo $excludeq9_display; ?></td>
</tr>

<tr valign="top">
	<td class=label>Question 11:</td>
	<td class=field><? echo $excludeq11_display; ?></td>
</tr>

</table>