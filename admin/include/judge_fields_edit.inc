<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $fullname;
		if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"javascript:deleteForm('deletejudge');\">Delete</a>]</font>";
		}
		
	
	} else {
		echo "New Judge";
	}

?>

</font><br><br>

<?
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">

<span style="visibility:hidden"><input type="submit" name="deletejudge"></span>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">User Details:</td></tr>

<tr valign="top">
	<td class=m_label>First Name:</td>
	<td class=field>
		<input type="text" name="namefirst" value="<? echo $namefirst; ?>" size="20">
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Last Name:</td>
	<td class=field>
		<input type="text" name="namelast" value="<? echo $namelast; ?>" size="20">
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Email:</td>
	<td class=field>
		<input type="text" name="emailaddress" value="<? echo $emailaddress; ?>" size="40">
	</td>
</tr>	

<?
	if (!empty($recordid)) {
		$pwdlabel = "Change Password:";
		$labelclass = "label";
	} else {
		$pwdlabel = "Choose a Password:";
		$labelclass = "m_label";
	}
	
	if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) 
	{
	
?>

<tr valign="top">
	<td class=<? echo $labelclass; ?>><? echo $pwdlabel; ?></td>
	<td class=field>
		<input type="password" name="newpwd" value="" size="15">&nbsp;(min 6 chrs)
	</td>
</tr>

<tr valign="top">
	<td class=label>Confirm Password:</td>
	<td class=field>
		<input type="password" name="newpwd2" value="" size="15">
	</td>
</tr>		

<?
	}
?>
					
</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
			if (!empty($recordid)) {	
				echo "<button onClick=\"cancelURL('judge_summary.php');return false;\">Cancel</button>&nbsp;";
			} else {
				echo "<button onClick=\"cancelURL('judge_summary.php');return false;\">Cancel</button>&nbsp;";
			}
	?>	
	
	<input type="submit" name="savejudge" value="Save Changes">
</td>
</tr>
</table>
	
</form>
