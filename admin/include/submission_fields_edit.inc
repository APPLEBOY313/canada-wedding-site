<?
	// Prev/Next buttons
	if (!empty($previd) || !empty($nextid)) {
		echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-bottom:10px;\">";
		echo "<tr valign=\"top\">";
		if (!empty($previd)) echo "<td align=\"left\"><button id=\"previd\" onClick='document.location.href=\""._MY_HREF_."admin/submission_detail.php?a=edit&id=".$previd."&idcoll=".$idcoll."\"'><< Previous Submission</button></td>";
		if (!empty($nextid)) echo "<td align=\"right\"><button id=\"nextid\" onClick='document.location.href=\""._MY_HREF_."admin/submission_detail.php?a=edit&id=".$nextid."&idcoll=".$idcoll."\"'>Next Submission >></button></td>";	
		echo "</tr>";
		echo "</table>";
	} 
?>

<form name="theform" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $companyname;
		
		if ($_SESSION['s_role']=="Administrator") {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"javascript:deleteForm('deletesubmission');\">Delete</a>]</font>";
		}
		
	
	} else {
		echo "New Submission";
	}

?>

</font><br><br>

<?

	echo $createddisplay;
	echo "<br>";
		
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">
<input type="hidden" name="idcoll" value="<? echo $idcoll; ?>">
<input type="hidden" name="retnextid" value="<? echo $nextid; ?>">
<input type="hidden" name="retprevid" value="<? echo $previd; ?>">

<?
	echo "<input type=\"hidden\" name=\"returl\" value=\"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."\">";
?>

<span style="visibility:hidden"><input type="submit" name="deletesubmission"></span>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Submission Details:</td></tr>

<tr valign="top">
	<td class=label>ID:</td>
	<td class=field><? echo $uniqueid; ?></td>
</tr>

<tr valign="top">
	<td class=label>Status:</td>
	<td class=field>
		<select name="status">
		<? displayFields("submission_status", $status, "0"); ?>
		</select>
		&nbsp;&nbsp;&nbsp;<span id="confmessage"><button onClick="sendConfEmail('<? echo $recordid; ?>');return false;">Send Confirmation Email</button></span>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Category:</td>
	<td class=field>
		<select name="categoryid">
		<? displayCategories($categoryid); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=label>Company Name:</td>
	<td class=field><input type="text" name="companyname" maxlength="100" value="<? echo $companyname; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Contact Person:</td>
	<td class=field><input type="text" name="contactperson" maxlength="100" value="<? echo $contactperson; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Photographer Name:</td>
	<td class=field><input type="text" name="photographername" maxlength="100" value="<? echo $photographername; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Email:</td>
	<td class=field><input type="text" name="email" maxlength="150" value="<? echo $email; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Website:</td>
	<td class=field><input type="text" name="url" maxlength="150" value="<? echo $url; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Phone:</td>
	<td class=field>
		<input type="text" name="phone1" maxlength="3" value="<? echo $phone1; ?>" size="3">&nbsp;
		<input type="text" name="phone2" maxlength="3" value="<? echo $phone2; ?>" size="3">&nbsp;
		<input type="text" name="phone3" maxlength="4" value="<? echo $phone3; ?>" size="4">
	</td>
</tr>

<tr valign="top">
	<td class=label>Address:</td>
	<td class=field>
		<input type="text" name="address1" maxlength="75" value="<? echo $address1; ?>" size="20">&nbsp;
		<input type="text" name="address2" maxlength="75" value="<? echo $address2; ?>" size="15">
	</td>
</tr>

<tr valign="top">
	<td class=label>City:</td>
	<td class=field><input type="text" name="city" maxlength="75" value="<? echo $city; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>State/Province:</td>
	<td class=field><input type="text" name="state" maxlength="75" value="<? echo $state; ?>" size="30"></td>
</tr>


<tr valign="top">
	<td class=label>Postal Code:</td>
	<td class=field><input type="text" name="postalcode" maxlength="20" value="<? echo $postalcode; ?>" size="20"></td>
</tr>

<tr valign="top">
	<td class=label>Country:</td>
	<td class=field>
		<select name="country">
		<? displayCountries($country); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=label>How Long in Business:</td>
	<td class=field><input type="text" name="howlong" maxlength="150" value="<? echo $howlong; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Company Description:</td>
	<td class=field><textarea name="companydescription" rows="5" style="width:98%"><? echo $companydescription; ?></textarea></td>
</tr>

<tr valign="top">
	<td class=label>Other Information:</td>
	<td class=field><textarea name="otherinfo" rows="5" style="width:98%"><? echo $otherinfo; ?></textarea></td>
</tr>

<tr valign="top">
	<td class=label>Nominee:</td>
	<td class=field><input type="text" name="nominee" maxlength="75" value="<? echo $nominee; ?>" size="30"></td>
</tr>

<tr valign="top">
	<td class=label>Image Description:</td>
	<td class=field><textarea name="imagedescription" rows="5" style="width:98%"><? echo $imagedescription; ?></textarea></td>
</tr>

<tr valign="top">
	<td class=label>Vimeo Clip #:</td>
	<td class=field><input type="text" name="videourl" maxlength="20" value="<? echo $videourl; ?>" size="20"></td>
</tr>

<? echo $att_edit; ?>

</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
		if ($recordid>0) {
			echo "<button onClick=\"cancelURL('submission_detail.php?id=".$submissionid."&idcoll=".$idcoll."');return false;\">Cancel</button>&nbsp;";	
		} else {
			echo "<button onClick=\"cancelURL('index.php');return false;\">Cancel</button>&nbsp;";
		}
		
		echo "<input type=\"submit\" name=\"savesubmission\" value=\"Save Changes\">";
	?>	
	
	
</td>
</tr>
</table>
	
</form>
