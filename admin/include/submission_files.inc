<br><br>
<font class="subsubtitle">Images</font>
<br><br>

<?

// --------------------------------------------------------------
// Get List of files in folder
// --------------------------------------------------------------
$v_filefolder = _MY_FILE_V_.$submissionid."/";
$h_filefolder = _MY_FILE_H_.$submissionid."/";

// create a handler for the directory
$att_display = "";
$att_edit = "";
  
if (is_dir($v_filefolder)) {
	
	$handler = opendir($v_filefolder);

  // Read all files
  $counter = 1;
  
  while ($file = readdir($handler)) {
    if ($file != '.' && $file != '..') {       
    	
    	$escfile = str_replace("&", "%26", $file);
    	
    	if (!empty($att_display)) $att_display.="<br>";
    	if (!empty($att_edit)) $att_edit.="<br>";
    	
    	$att_display.= "<a target=\"tps_file\" title=\"Download file\" href=\"".$h_filefolder.$file."\">".$file."</a> (".number_format(filesize($v_filefolder.$file)/1024)." KB)";
    	$att_edit.= "<a target=\"tps_file\" title=\"Download file\" href=\"".$h_filefolder.$file."\">".$file."</a> (".number_format(filesize($v_filefolder.$file)/1024)." KB)";
    	$att_edit.= "&nbsp;&nbsp;<input type=\"checkbox\" name=\"delete_".$counter."\" value=\"".$file."\">&nbsp;Delete";
  	
  		$counter++;  	
    }
  }

	if (!empty($att_edit)) $att_edit.="<br><br>";
}

echo $att_display;

?>