<?
	// Prev/Next buttons
	if (!empty($previd) || !empty($nextid)) {
		echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-bottom:10px;\">";
		echo "<tr valign=\"top\">";
		if (!empty($previd)) echo "<td align=\"left\"><button id=\"previd\" onClick='document.location.href=\""._MY_HREF_."admin/scorejudge_detail.php?a=edit&submissionid=".$previd."&idcoll=".$idcoll."\"'><< Previous Submission</button></td>";
		if (!empty($nextid)) echo "<td align=\"right\"><button id=\"nextid\" onClick='document.location.href=\""._MY_HREF_."admin/scorejudge_detail.php?a=edit&submissionid=".$nextid."&idcoll=".$idcoll."\"'>Next Submission >></button></td>";	
		echo "</tr>";
		echo "</table>";
	} 
?>

<form name="theform" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle"><? echo $pagetitle; ?></font><br><br>

<?	
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="submissionid" value="<? echo $submissionid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">
<input type="hidden" name="idcoll" value="<? echo $idcoll; ?>">
<input type="hidden" name="retnextid" value="<? echo $nextid; ?>">
<input type="hidden" name="retprevid" value="<? echo $previd; ?>">

<?
	echo "<input type=\"hidden\" name=\"returl\" value=\"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."\">";
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Score Details:</td></tr>

<tr valign="top">
	<td class=m_label>Aesthetics:</td>
	<td class=field>
		<? 
			for ($i=1; $i<11; $i++) {
				$ischecked = "";
				if ($rating_aesthetics==$i) {
					$ischecked = "checked";
				}
				echo "<input type=\"radio\" name=\"rating_aesthetics\" value=\"".$i."\" ".$ischecked." />".$i."&nbsp;&nbsp;";
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Themes:</td>
	<td class=field>
		<? 
			for ($i=1; $i<11; $i++) {
				$ischecked = "";
				if ($rating_themes==$i) {
					$ischecked = "checked";
				}
				echo "<input type=\"radio\" name=\"rating_themes\" value=\"".$i."\" ".$ischecked." />".$i."&nbsp;&nbsp;";
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Originality:</td>
	<td class=field>
		<? 
			for ($i=1; $i<11; $i++) {
				$ischecked = "";
				if ($rating_originality==$i) {
					$ischecked = "checked";
				}
				echo "<input type=\"radio\" name=\"rating_originality\" value=\"".$i."\" ".$ischecked." />".$i."&nbsp;&nbsp;";
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Professionalism:</td>
	<td class=field>
		<? 
			for ($i=1; $i<11; $i++) {
				$ischecked = "";
				if ($rating_professionalism==$i) {
					$ischecked = "checked";
				}
				echo "<input type=\"radio\" name=\"rating_professionalism\" value=\"".$i."\" ".$ischecked." />".$i."&nbsp;&nbsp;";
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Overall Quality:</td>
	<td class=field>
		<? 
			for ($i=1; $i<11; $i++) {
				$ischecked = "";
				if ($rating_overallquality==$i) {
					$ischecked = "checked";
				}
				echo "<input type=\"radio\" name=\"rating_overallquality\" value=\"".$i."\" ".$ischecked." />".$i."&nbsp;&nbsp;";
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>&nbsp;</td>
	<td class=field><input type="checkbox" name="cannotjudge" <? echo $cannotjudge_checked; ?> onClick="cannotJudge();">&nbsp;I cannot judge this entry (e.g. does not meet criteria, not enough information, etc)</td>
</tr>

</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
		if ($clientid>0) {
			echo "<button onClick=\"cancelURL('index.php');return false;\">Cancel</button>&nbsp;";	
		} else {
			echo "<button onClick=\"cancelURL('index.php');return false;\">Cancel</button>&nbsp;";
		}
		
		echo "<input type=\"submit\" name=\"savescorejudge\" value=\"Save Changes and score next\">";
	?>	
	
	
</td>
</tr>
</table>

<br><br>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Submission Details:</td></tr>

<tr valign="top">
	<td class=label>ID:</td>
	<td class=field><? echo $uniqueid; ?></td>
</tr>

<tr valign="top">
	<td class=label>Category:</td>
	<td class=field><? echo $category; ?></td>
</tr>

<tr valign="top">
	<td class=label>Company Description:</td>
	<td class=field><? echo $companydescription; ?></td>
</tr>

<tr valign="top">
	<td class=label>Other Info:</td>
	<td class=field><? echo $otherinfo; ?></td>
</tr>

<tr valign="top">
	<td class=label>Image Description:</td>
	<td class=field><? echo $imagedescription; ?></td>
</tr>

<? 

	echo $att_display; 
	
	if (!empty($videourl)) {
?>
		<tr valign="top">
		<td class=label>Video:</td>
		<td class=field>
		
		<object width="100%" height="500">
			<param name="allowfullscreen" value="true" />
			<param name="allowscriptaccess" value="always" />
			<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" />
			<embed src="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="500"></embed>
		</object>
		
		</td>
		</tr>
		
<?
	}
?>

<tr valign="top">
	<td class=label>Videos:</td>
	<td class=field>
		
<?php
 	
	// Are there any videos in the EntryFiles?
	$videodir = _MY_FILE_V_.$submissionid."/video/";
	if (file_exists($videodir)) {
		$dir = new DirectoryIterator($videodir);
		$vidcount = 0;
		foreach ($dir as $fileinfo) {
		    if (!$fileinfo->isDot()) {
		        $videos[$vidcount][0] = $fileinfo->getFilename();
		     		$videos[$vidcount][1] = $fileinfo->getSize();  
		     		$vidcount++; 
		    }
		}
		
		if (count($videos)>0) {
			for ($x=0; $x<count($videos); $x++) {
				echo "<br><video id=\"video\" preload=\"auto\" src=\""._MY_FILE_H_.$submissionid."/video/".$videos[$x][0]."\" controls width=\"320px\"></video>";
				echo "<br><div id=\"bufferBar\"></div>";
			}
		}	
		
	}
	
?>

	</td>
</tr>

</table>

</form>

<?php
	include("system/video_buffer.inc");
?>