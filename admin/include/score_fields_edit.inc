<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $pagetitle;
		
		if ($_SESSION['s_role']=="Administrator") {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"javascript:deleteForm('deletescore');\">Delete</a>]</font>";
		}
		
	
	} else {
		echo "New Score";
	}

?>

</font><br><br>

<?		
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="submissionid" value="<? echo $submissionid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">

<?
	echo "<input type=\"hidden\" name=\"returl\" value=\"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."\">";
?>

<span style="visibility:hidden"><input type="submit" name="deletescore"></span>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Score Details:</td></tr>

<tr valign="top">
	<td class=m_label>Judge:</td>
	<td class=field>
		<select name="judgeid">
		<? displayJudges($judgeid); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Aesthetics:</td>
	<td class=field>
		<select name="rating_aesthetics">
		<? displayFields("score_rating", $rating_aesthetics, 0); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Themes:</td>
	<td class=field>
		<select name="rating_themes">
		<? displayFields("score_rating", $rating_themes, 0); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Originality:</td>
	<td class=field>
		<select name="rating_originality">
		<? displayFields("score_rating", $rating_originality, 0); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Professionalism:</td>
	<td class=field>
		<select name="rating_professionalism">
		<? displayFields("score_rating", $rating_professionalism, 0); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Overall Quality:</td>
	<td class=field>
		<select name="rating_overallquality">
		<? displayFields("score_rating", $rating_overallquality, 0); ?>
		</select>
	</td>
</tr>

</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
		if ($clientid>0) {
			echo "<button onClick=\"cancelURL('submission_detail.php?id=$submissionid');return false;\">Cancel</button>&nbsp;";	
		} else {
			echo "<button onClick=\"cancelURL('submission_detail.php?id=$submissionid');return false;\">Cancel</button>&nbsp;";
		}
		
		echo "<input type=\"submit\" name=\"savescore\" value=\"Save Changes\">";
	?>	
	
	
</td>
</tr>
</table>
	
</form>
