<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $fullname;
		
		if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
			if ($canedit) {
				echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?id=".$recordid."&a=edit\">Edit</a>]</font>";
			}
		}
		
	} else {
		echo "Untitled User";
	}

?>

</font><br><br>

<?
		
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>User Details:</td></tr>

<tr valign="top">
	<td class=label>Status:</td>
	<td class=field><? echo $isactviedisplay; ?></td>
</tr>

<tr valign="top">
	<td class=label>Name:</td>
	<td class=field><? echo $namefirst." ".$namelast; ?></td>
</tr>

<tr valign="top">
	<td class=label>Role:</td>
	<td class=field><? echo $role; ?></td>
</tr>

<tr valign="top">
	<td class=label>Email:</td>
	<td class=field><? echo $emailaddress; ?></td>
</tr>

<tr valign="top">
	<td class=label>Username:</td>
	<td class=field><? echo $loginid; ?></td>
</tr>

<tr valign="top">
	<td class=label>Last Login:</td>
	<td class=field><? echo $lastlogin; ?></td>
</tr>

</table>