<?
	// Prev/Next buttons
	if (!empty($previd) || !empty($nextid)) {
		echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-bottom:10px;\">";
		echo "<tr valign=\"top\">";
		if (!empty($previd)) echo "<td align=\"left\"><button id=\"previd\" onClick='document.location.href=\""._MY_HREF_."admin/submission_detail.php?id=".$previd."&idcoll=".$idcoll."\"'><< Previous Submission</button></td>";
		if (!empty($nextid)) echo "<td align=\"right\"><button id=\"nextid\" onClick='document.location.href=\""._MY_HREF_."admin/submission_detail.php?id=".$nextid."&idcoll=".$idcoll."\"'>Next Submission >></button></td>";	
		echo "</tr>";
		echo "</table>";
	} 
?>

<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $companyname;
		
		if ($canedit) {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?id=".$recordid."&idcoll=".$idcoll."&a=edit\">Edit</a>]</font>";
		}
		
	} else {
		echo "New Submission";
	}

?>

</font><br><br>

<?
		
	echo $createddisplay;
	echo "<br><br>";
		
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Submission Details:</td></tr>

<tr valign="top">
	<td class=label>ID:</td>
	<td class=field><? echo $uniqueid; ?></td>
</tr>

<?php
	if (!empty($paypaldesc)) {
?>

<tr valign="top">
	<td class=label>PayPal Payment:</td>
	<td class=field><? echo $paypaldesc; ?></td>
</tr>

<?php
}
?>

<tr valign="top">
	<td class=label>Status:</td>
	<td class=field><? echo $status.$emailsentdisplay; ?></td>
</tr>

<tr valign="top">
	<td class=label>Category:</td>
	<td class=field><? echo $category; ?></td>
</tr>

<tr valign="top">
	<td class=label>Company Name:</td>
	<td class=field><? echo $companyname; ?></td>
</tr>

<tr valign="top">
	<td class=label>Contact Person:</td>
	<td class=field><? echo $contactperson; ?></td>
</tr>

<?php
	if (!empty($photographername)) {
		echo "<tr valign=\"top\">";
		echo "<td class=label>Photographer Name:</td>";
		echo "<td class=field>".$photographername."</td>";
		echo "</tr>";
	}
?>

<tr valign="top">
	<td class=label>Email:</td>
	<td class=field><? echo $email; ?></td>
</tr>

<tr valign="top">
	<td class=label>Website:</td>
	<td class=field><? echo $url; ?></td>
</tr>

<tr valign="top">
	<td class=label>Phone:</td>
	<td class=field><? echo $phone_display; ?></td>
</tr>

<tr valign="top">
	<td class=label>Address:</td>
	<td class=field><? echo $address1." ".$address2; ?></td>
</tr>

<tr valign="top">
	<td class=label>City:</td>
	<td class=field><? echo $city; ?></td>
</tr>

<tr valign="top">
	<td class=label>State:</td>
	<td class=field><? echo $state; ?></td>
</tr>

<tr valign="top">
	<td class=label>Postal Code:</td>
	<td class=field><? echo $postalcode; ?></td>
</tr>

<tr valign="top">
	<td class=label>Country:</td>
	<td class=field><? echo $country; ?></td>
</tr>

<tr valign="top">
	<td class=label>How Long in Business:</td>
	<td class=field><? echo $howlong; ?></td>
</tr>

<tr valign="top">
	<td class=label>Company Description:</td>
	<td class=field><? echo nl2br($companydescription); ?></td>
</tr>

<tr valign="top">
	<td class=label>Other Information:</td>
	<td class=field><? echo nl2br($otherinfo); ?></td>
</tr>

<tr valign="top">
	<td class=label>Nominee:</td>
	<td class=field><? echo $nominee; ?></td>
</tr>

<tr valign="top">
	<td class=label>Image Description:</td>
	<td class=field><? echo $imagedescription; ?></td>
</tr>

<? 
	echo $att_display;

	if (!empty($videourl)) {
?>
		<tr valign="top">
		<td class=label>Video:</td>
		<td class=field>
		
		<object width="100%" height="500">
			<param name="allowfullscreen" value="true" />
			<param name="allowscriptaccess" value="always" />
			<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" />
			<embed src="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="500"></embed>
		</object>
		
		</td>
		</tr>
		
<?
	}
?>
	
	<tr valign="top">
		<td class=label>Videos:</td>
		<td class=field><a href="<?php echo _MY_HREF_; ?>videoupload.php?permakey=<?php echo $permakey; ?>&admin=1">Upload video</a>
		
<?php
 	
	// Are there any videos in the EntryFiles?
	$videodir = _MY_FILE_V_.$submissionid."/video/";
	if (file_exists($videodir)) {
		$dir = new DirectoryIterator($videodir);
		$vidcount = 0;
		foreach ($dir as $fileinfo) {
		    if (!$fileinfo->isDot()) {
		        $videos[$vidcount][0] = $fileinfo->getFilename();
		     		$videos[$vidcount][1] = $fileinfo->getSize();  
		     		$vidcount++; 
		    }
		}
		
//		if (count($videos)>0) {
//			echo "<ul>";
//			for ($x=0; $x<count($videos); $x++) {
//				echo "<li><a target=\"_blank\" href=\""._MY_FILE_H_.$submissionid."/video/".$videos[$x][0]."\">".$videos[$x][0]."</a>&nbsp;";
//				echo "(".number_format($videos[$x][1]/1024)." KB)</li>";
//			}
//			echo "</ul>";
//		}	
		
		if (count($videos)>0) {
			for ($x=0; $x<count($videos); $x++) {
				echo "<br><video preload='auto' src=\""._MY_FILE_H_.$submissionid."/video/".$videos[$x][0]."\" controls width=\"320px\"></video>";
				echo "<br><div id=\"bufferBar\"></div>";
			}
		}	
		
	}
	
?>

		</td>
	</tr>
</table>

<?
	include("submission_scores.inc");
?>