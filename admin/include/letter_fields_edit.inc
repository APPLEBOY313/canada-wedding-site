<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $title;
		
		if ($_SESSION['s_role']=="Administrator") {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"javascript:deleteForm('deleteletter');\">Delete</a>]</font>";
		}
		
	
	} else {
		echo "New Letter Template";
	}

?>

</font><br><br>

<?
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">

<span style="visibility:hidden"><input type="submit" name="deleteletter"></span>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Letter Template Details:</td></tr>

<tr valign="top">
	<td class=m_label>Name:</td>
	<td class=field>
		<input type="text" name="title" value="<? echo $title; ?>" style="width:95%">
	</td>
</tr>

<tr valign="top">
	<td class=m_label>Message:</td>
	<td class=field><? echo $intro; ?>
		<br><textarea name="body" rows="25" style="width:95%"><? echo $body; ?></textarea>
		<br>Place the string <b>[SNIPPET START - DO NOT REMOVE]</b> in the appropriate position where Snippets should be added
	</td>
</tr>
				
</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
			if (!empty($recordid)) {	
				echo "<button onClick=\"cancelURL('letter_summary.php');return false;\">Cancel</button>&nbsp;";
			} else {
				echo "<button onClick=\"cancelURL('letter_summary.php');return false;\">Cancel</button>&nbsp;";
			}
	?>	
	
	<input type="submit" name="saveletter" value="Save Changes">
</td>
</tr>
</table>
	
</form>