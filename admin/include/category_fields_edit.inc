<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $title;
		
		if ($_SESSION['s_role']=="Administrator") {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"javascript:deleteForm('deletecategory');\">Delete</a>]</font>";
		}
		
	
	} else {
		echo "New Category";
	}

?>

</font><br><br>

<?		
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">

<?
	echo "<input type=\"hidden\" name=\"returl\" value=\"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."\">";
?>

<span style="visibility:hidden"><input type="submit" name="deletecategory"></span>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Category Details:</td></tr>

<tr valign="top">
	<td class=label>Display Order:</td>
	<td class=field><input type="text" name="sortweight" maxlength="2" value="<? echo $sortweight; ?>" size="3"></td>
</tr>

<tr valign="top">
	<td class=m_label>Name:</td>
	<td class=field><input type="text" name="title" maxlength="75" value="<? echo $title; ?>" size="50"></td>
</tr>

<tr valign="top">
	<td class=m_label>Code:</td>
	<td class=field><input type="text" name="code" maxlength="5" value="<? echo $code; ?>" size="10"></td>
</tr>

<tr valign="top">
	<td class=label>Description:</td>
	<td class=field><textarea name="comment" rows="10" style="width:98%"><? echo $comment; ?></textarea></td>
</tr>

<tr valign="top">
	<td class=label>Images Allowed:</td>
	<td class=field>
		<select name="numimages">
		<? displayFields("category_numimages", $numimages); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td class=label>Instructions:</td>
	<td class=field><textarea name="instructions" rows="13" style="width:98%"><? echo $instructions; ?></textarea></td>
</tr>

<tr valign="top">
	<td class=label>Question 9:</td>
	<td class=field><input type="text" name="question9" maxlength="100" value="<? echo $question9; ?>" style="width:100%;"></td>
</tr>

<tr valign="top">
	<td class=label>Display Question 8:</td>
	<td class=field><input type="checkbox" name="excludeq8" <? echo $excludeq8_checked; ?>>&nbsp;Hide</td>
</tr>

<tr valign="top">
	<td class=label>Display Question 9:</td>
	<td class=field><input type="checkbox" name="excludeq9" <? echo $excludeq9_checked; ?>>&nbsp;Hide</td>
</tr>

<tr valign="top">
	<td class=label>Display Question 11:</td>
	<td class=field><input type="checkbox" name="excludeq11" <? echo $excludeq11_checked; ?>>&nbsp;Hide</td>
</tr>

</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
		if ($recordid>0) {
			echo "<button onClick=\"cancelURL('category_summary.php');return false;\">Cancel</button>&nbsp;";	
		} else {
			echo "<button onClick=\"cancelURL('category_summary.php');return false;\">Cancel</button>&nbsp;";
		}
		
		echo "<input type=\"submit\" name=\"savecategory\" value=\"Save Changes\">";
	?>	
	
	
</td>
</tr>
</table>
	
</form>
