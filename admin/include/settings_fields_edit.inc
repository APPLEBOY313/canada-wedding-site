<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">Application Settings</font><br><br>

<?
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="1">
<input type="hidden" name="action" value="<? echo $action; ?>">

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Settings:</td></tr>

<tr valign="top">
	<td class=label>Judge Message:</td>
	<td class=field><textarea name="message_judge" rows="5" style="width:98%"><? echo $message_judge; ?></textarea></td>
</tr>
			
<tr valign="top">
	<td class=label>Confirmation Subject:</td>
	<td class=field><input type="text" name="confsubject" style="width:98%" value="<? echo $confsubject; ?>"></td>
</tr>			
			
<tr valign="top">
	<td class=label>Confirmation Message:</td>
	<td class=field>
		Dear <i>Customer Name</i>
		<br><br>
		Regarding your recent submission into the <i>Category Name</i> category:
		<br><br>
		<textarea name="confbody" rows="8" style="width:98%"><? echo $confbody; ?></textarea>
		<br><br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>
			
<tr valign="top">
	<td class=label>RSVP Subject:</td>
	<td class=field><input type="text" name="rsvpsubject" style="width:98%" value="<? echo $rsvpsubject; ?>"></td>
</tr>			
			
<tr valign="top">
	<td class=label>RSVP Message:</td>
	<td class=field>
		Dear <i>Company Name</i>
		<br><br>
		<textarea name="rsvpbody" rows="8" style="width:98%"><? echo $rsvpbody; ?></textarea>
		<br><br>
		<i>Link to RSVP included here</i>
		<br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>			

<tr valign="top">
	<td class=label>RSVP Price per Seat: $</td>
	<td class=field><input type="text" name="rsvpprice" size="3" value="<? echo $rsvpprice; ?>"></td>
</tr>	

<tr valign="top">
	<td class=label>RSVP Confirmation Subject:</td>
	<td class=field><i>Qty</i> ticket(s) purchased for Awards Night</td>
</tr>			
			
<tr valign="top">
	<td class=label>RSVP Confirmation Message:</td>
	<td class=field>
		Dear <i>Company Name</i>
		<br><br>
		This email confirms your recent purchase of <i>qty</i> ticket(s) for our Awards Night on <i>Gala date</i>
		<br><br>
		<textarea name="rsvpconfbody" rows="8" style="width:98%"><? echo $rsvpconfbody; ?></textarea>
		<br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>			

<tr valign="top">
	<td class=label>Gala Date:</td>
	<td class=field><input type="text" name="galadate_display" size="20" maxlength="20" value="<? echo $galadate_display; ?>"></td>
</tr>

<tr valign="top">
	<td class=label>Gala Seats Available:</td>
	<td class=field><input type="text" name="galaseatsavailable" size="3" value="<? echo $galaseatsavailable; ?>"></td>
</tr>	

<tr valign="top">
	<td class=label>Gala Price per Seat: $</td>
	<td class=field><input type="text" name="galaprice" size="3" value="<? echo $galaprice; ?>"></td>
</tr>	

<tr valign="top">
	<td class=label>Max Gala Seats/Person:</td>
	<td class=field><input type="text" name="galamaxperperson" size="3" value="<? echo $galamaxperperson; ?>"></td>
</tr>

<tr valign="top">
	<td class=label>Guest Names:</td>
	<td class=field><input type="checkbox" name="canchangeguestnames" value="1" <?php echo $canchangeguestnames_checked; ?>>&nbsp;Guest names can be edited</td>
</tr>

<tr valign="top">
	<td class=label>Display Promo code:</td>
	<td class=field><input type="checkbox" name="displaypromocode" value="1" <?php echo $displaypromocode_checked; ?>>&nbsp;Users can enter a Promo code</td>
</tr>
				
</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
			if (!empty($recordid)) {	
				echo "<button onClick=\"cancelURL('settings_detail.php');return false;\">Cancel</button>&nbsp;";
			} else {
				echo "<button onClick=\"cancelURL('settings_detail.php');return false;\">Cancel</button>&nbsp;";
			}
	?>	
	
	<input type="submit" name="savesettings" value="Save Changes">
</td>
</tr>
</table>
	
</form>
