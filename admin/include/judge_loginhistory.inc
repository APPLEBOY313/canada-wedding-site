<br><br>
<font class="subsubtitle">Login History</font>
<br><br>
Most recent 50 shown
<br><br>

<?

$query = "SELECT logintime, ipaddress, DATE_FORMAT(logintime, '%m/%d/%Y %h:%i %p') AS tmpLoginTime ";
$query.= "FROM loginhistory_judge ";
$query.= "WHERE judgeid=".$judgeid;
$query.= " ORDER BY logintime DESC LIMIT 0,50";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\" width=\"150px\"><b>Login Time</b></td>";	
	echo "<td class=\"header\"><b>IP Address</b></td>";	
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";		
		echo "<td width=\"150px\">";	
		
		if ($row["logintime"]=="0000-00-00 12:00:00") {
			echo "Never";
		} else {
			
			$phptimestamp = mysql2timestamp($row["logintime"]);
			$daysago = TimeAgo($phptimestamp);
			$secondpos = strpos($daysago, "second");
			$minpos = strpos($daysago, "minute");
			$hourpos = strpos($daysago, "hour");
			
			if ($minpos===false && $hourpos===false && $secondpos===false) {
				echo $row["tmpLoginTime"];
				
			} else {
				echo $daysago;
			}
				
		}
				
		echo "&nbsp;</td>";		
		echo "<td>".showIP(stripslashes($row["ipaddress"]))."</td>";
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "Judge has not logged in yet";
	
}

?>