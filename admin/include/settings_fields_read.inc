<font class="subtitle">Application Settings
<?
		if ($canedit) {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?a=edit\">Edit</a>]</font>";
		}
?>

</font><br><br>

<?
		
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Settings:</td></tr>

<tr valign="top">
	<td class=label>Judge Message:</td>
	<td class=field><? echo nl2br($message_judge); ?></td>
</tr>

<tr valign="top">
	<td class=label>Confirmation Subject:</td>
	<td class=field><? echo $confsubject; ?></td>
</tr>

<tr valign="top">
	<td class=label>Confirmation Message:</td>
	<td class=field>
		Dear <i>Customer Name</i>
		<br><br>
		Regarding your recent submission into the <i>Category Name</i> category:
		<br><br>
		<? echo nl2br($confbody); ?>
		<br><br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>

<tr valign="top">
	<td class=label>RSVP Price/Seat:</td>
	<td class=field>$<? echo $rsvpprice; ?></td>
</tr>

<tr valign="top">
	<td class=label>RSVP Subject:</td>
	<td class=field><? echo $rsvpsubject; ?></td>
</tr>

<tr valign="top">
	<td class=label>RSVP Message:</td>
	<td class=field>
		Dear <i>Customer Name</i>
		<br><br>
		<? echo nl2br($rsvpbody); ?>
		<br><br>
		<i>Link to RSVP included here</i>
		<br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>

<tr valign="top">
	<td class=label>RSVP Confirmation Subject:</td>
	<td class=field><i>Qty</i> ticket(s) purchased for Awards Night</td>
</tr>

<tr valign="top">
	<td class=label>RSVP Confirmation Message:</td>
	<td class=field>
		Dear <i>Customer Name</i>
		<br><br>
		This email confirms that you have reserved <i>Number of tickets</i> tickets.
		This email confirms your recent purchase of <i>qty</i> ticket(s) for our Awards Night on <i>Gala date</i>
		<br><br>
		<? echo nl2br($rsvpconfbody); ?>
		<br>
		Professional BC Wedding Awards
		<br>http://www.bcweddingawards.com
	</td>
</tr>

<tr valign="top">
	<td class=label>Gala Date:</td>
	<td class=field><? echo $galadate_display; ?></td>
</tr>

<tr valign="top">
	<td class=label>Gala Seats Available:</td>
	<td class=field><? echo $galaseatsavailable; ?></td>
</tr>

<tr valign="top">
	<td class=label>Gala Price/Seat:</td>
	<td class=field>$<? echo $galaprice; ?></td>
</tr>

<tr valign="top">
	<td class=label>Max Gala Seats/Person:</td>
	<td class=field><? echo $galamaxperperson; ?></td>
</tr>

<tr valign="top">
	<td class=label>Guest Names:</td>
	<td class=field><? echo $canchangeguestnames_display; ?></td>
</tr>

<tr valign="top">
	<td class=label>Display Promo code:</td>
	<td class=field><? echo $displaypromocode_display; ?></td>
</tr>

</table>