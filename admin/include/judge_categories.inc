<br><br>
<font class="subsubtitle">Categories assigned to this judge</font>
<br>

<form method="POST" action="<?php echo _MY_HREF_ADMIN_; ?>judge_detail.php?id=<?php echo $recordid; ?>">
<input type="hidden" name="judgeid" value="<?php echo $recordid; ?>">

<b>Assign category:</b> <select name="categoryid"><?php displayCategories(); ?></select>

<input type="submit" name="assignjudgetocategory" value="Assign">

</form>

<br>

<?php

$query = "SELECT jc.ID, jc.categoryid, cat.title AS category, cat.code 
	FROM judges_category jc
	INNER JOIN category cat ON (cat.categoryid = jc.categoryid)
	WHERE jc.isdeleted=0 AND jc.judgeid=".$recordid."
	ORDER BY cat.code";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\" width=\"80px\"><b>Code</b></td>";	
	echo "<td class=\"header\"><b>Category</b></td>";	
	echo "<td class=\"header\" width=\"80px\">&nbsp;</td>";	
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";		
		echo "<td width=\"80px\">".$row["code"]."</td>";	
		echo "<td>".stripslashes($row["category"])."</td>";	
		echo "<td width=\"80px\"><a href=\"javascript:unassignJudgeCategory('".$row["ID"]."')\">Unassign</td>";	
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "Judge has not been assigned to any categories";
	
}

?>

<br><br>