<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $pagetitle;
		
		if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
			if ($canedit) {
				echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?id=".$recordid."&a=edit\">Edit</a>]</font>";
			}
		}
		
	} else {
		echo "Untitled Score";
	}

?>

</font><br><br>

<?
		
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Score Details:</td></tr>

<tr valign="top">
	<td class=label>Judge:</td>
	<td class=field>
		<?
			if ($_SESSION['s_role'] == "Administrator") {
				echo "<a href=\"judge_detail.php?id=".$judgeid."\">".$judge."</a>"; 	
			} else {
				echo $judge;
			}
		?>
	</td>
</tr>

<tr valign="top">
	<td class=label>Aesthetics:</td>
	<td class=field><? echo $rating_aesthetics; ?></td>
</tr>

<tr valign="top">
	<td class=label>Themes:</td>
	<td class=field><? echo $rating_themes; ?></td>
</tr>

<tr valign="top">
	<td class=label>Originality:</td>
	<td class=field><? echo $rating_originality; ?></td>
</tr>

<tr valign="top">
	<td class=label>Professionalism:</td>
	<td class=field><? echo $rating_professionalism; ?></td>
</tr>

<tr valign="top">
	<td class=label>Overall Quality:</td>
	<td class=field><? echo $rating_overallquality; ?></td>
</tr>

</table>