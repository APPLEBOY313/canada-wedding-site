<br><br>
<font class="subsubtitle">Scores</font>
<br><br>
<?
if ($_SESSION['s_role'] == "Administrator") {
	echo "<a href=\"score_detail.php?a=edit&submissionid=".$recordid."\" class=\"onwhite\">New Score</a>";
	echo "<br><br>";
}

$query = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, s.scoreid, ";
$query.= "s.rating_aesthetics, s.rating_themes, s.rating_originality, s.rating_professionalism, ";
$query.= "s.rating_overallquality, s.comment, s.overallscore, s.cannotjudge, j.namefirst, j.namelast ";
$query.= "FROM score s ";
$query.= "INNER JOIN judge j ON (j.judgeid = s.judgeid) ";
$query.= "WHERE s.submissionid=".$recordid." AND s.isdeleted=0 ";
$query.= "ORDER BY s.created DESC";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"150px\"><b>Date Scored</b></td>";	
	echo "<td class=\"header\"><b>Judge</b></td>";	
	echo "<td class=\"header\" width=\"75px\" align=\"center\">Aesthetics</td>";
	echo "<td class=\"header\" width=\"75px\" align=\"center\">Themes</td>";
	echo "<td class=\"header\" width=\"90px\" align=\"center\">Originality</td>";
	echo "<td class=\"header\" width=\"90px\" align=\"center\">Professionalism</td>";
	echo "<td class=\"header\" width=\"90px\" align=\"center\">Overall Quality</td>";
	echo "<td class=\"header\" width=\"90px\" align=\"center\">Overall Score</td>";
	echo "</tr>";
		
	$i = 1;
	$cannotjudge_count = 0;
		
	while ($row = mysql_fetch_array($result)) {
	
		if ($row["cannotdjudge"]=="1") {
			echo "<tr valign=\"top\" class=rowdata_cannotjudge>";
			$cannotjudge_count++;
			
		} else {
		
			if($i % 2) { 
				echo "<tr valign=\"top\" class=rowdata>";
			} else {
				echo "<tr valign=\"top\" class=rowdata_alt>";
			}
				
		}
			
		echo "<td width=\"150px\"><a href=\"score_detail.php?id=".$row["scoreid"]."\">".stripslashes($row["tmpCreated"])."</a></td>";		
		echo "<td>".stripslashes($row["namefirst"]." ".$row["namelast"])."</td>";
		echo "<td width=\"75px\" align=\"center\">".$row["rating_aesthetics"]."</td>";		
		echo "<td width=\"75px\" align=\"center\">".$row["rating_themes"]."</td>";		
		echo "<td width=\"90px\" align=\"center\">".$row["rating_originality"]."</td>";		
		echo "<td width=\"90px\" align=\"center\">".$row["rating_professionalism"]."</td>";		
		echo "<td width=\"90px\" align=\"center\">".$row["rating_overallquality"]."</td>";
		echo "<td width=\"90px\" align=\"center\">".$row["overallscore"]."%</td>";		
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

if ($cannotjudge_count>0) {
	echo "<div style=\"padding:2px;\">Legend: <span class=\"rowdata_cannotjudge\">Could not judge</span></div>";
}

?>