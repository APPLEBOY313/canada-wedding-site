<?
	// Prev/Next buttons
	if (!empty($previd) || !empty($nextid)) {
		echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-bottom:10px;\">";
		echo "<tr valign=\"top\">";
		if (!empty($previd)) echo "<td align=\"left\"><button id=\"previd\" onClick='document.location.href=\""._MY_HREF_."admin/scorejudge_detail.php?a=edit&submissionid=".$previd."&idcoll=".$idcoll."\"'><< Previous Submission</button></td>";
		if (!empty($nextid)) echo "<td align=\"right\"><button id=\"nextid\" onClick='document.location.href=\""._MY_HREF_."admin/scorejudge_detail.php?a=edit&submissionid=".$nextid."&idcoll=".$idcoll."\"'>Next Submission >></button></td>";	
		echo "</tr>";
		echo "</table>";
	} 
?>

<font class="subtitle">
<?
	if (!empty($recordid)) {
		
		echo $pagetitle;
		if ($canchangescore) {
			echo "&nbsp;&nbsp;<font style=\"font-size:10px\">[<a href=\"".$_SERVER['PHP_SELF']."?id=".$recordid."&a=edit\">Edit</a>]</font>";
		}
			
	} else {
		if ($canchangescore=="0") {
			echo "You are not permitted to judge this entry";
		} else {
			echo "Untitled Score";	
		}
	}
?>

</font><br><br>

<?
		
	if (!empty($emailerrortext)) {
		echo "<center><p class=\"errorblock\">".$emailerrortext."</p></center>";		
		
	}	else if (!empty($emailoktext)) {
		echo "<center><p class=\"okblock\">".$emailoktext."</p></center>";	
		
	}			
	
	if ($canchangescore=="1") {
	
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Scoring Details:</td></tr>

<tr valign="top">
	<td class=label>Aesthetics:</td>
	<td class=field><? echo $rating_aesthetics; ?></td>
</tr>

<tr valign="top">
	<td class=label>Themes:</td>
	<td class=field><? echo $rating_themes; ?></td>
</tr>

<tr valign="top">
	<td class=label>Originality:</td>
	<td class=field><? echo $rating_originality; ?></td>
</tr>

<tr valign="top">
	<td class=label>Professionalism:</td>
	<td class=field><? echo $rating_professionalism; ?></td>
</tr>

<tr valign="top">
	<td class=label>Overall Quality:</td>
	<td class=field><? echo $rating_overallquality; ?></td>
</tr>

</table>

<br><br>

<?
	}
?>

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class=header>Submission Details:</td></tr>

<tr valign="top">
	<td class=label>ID:</td>
	<td class=field><? echo $uniqueid; ?></td>
</tr>

<tr valign="top">
	<td class=label>Category:</td>
	<td class=field><? echo $category; ?></td>
</tr>

<tr valign="top">
	<td class=label>Company Description:</td>
	<td class=field><? echo $companydescription; ?></td>
</tr>

<tr valign="top">
	<td class=label>Other Info:</td>
	<td class=field><? echo $otherinfo; ?></td>
</tr>

<tr valign="top">
	<td class=label>Image Description:</td>
	<td class=field><? echo $imagedescription; ?></td>
</tr>

<? 

	echo $att_display; 
	
	if (!empty($videourl)) {
?>
		<tr valign="top">
		<td class=label>Video:</td>
		<td class=field>
		
		<object width="100%" height="500">
			<param name="allowfullscreen" value="true" />
			<param name="allowscriptaccess" value="always" />
			<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" />
			<embed src="http://vimeo.com/moogaloop.swf?clip_id=<? echo $videourl; ?>&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00adef&fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="500"></embed>
		</object>
		
		</td>
		</tr>
		
<?
	}
?>

<tr valign="top">
		<td class=label>Videos:</td>
		<td class=field>
		
<?php
 	
	// Are there any videos in the EntryFiles?
	$videodir = _MY_FILE_V_.$submissionid."/video/";
	if (file_exists($videodir)) {
		$dir = new DirectoryIterator($videodir);
		$vidcount = 0;
		foreach ($dir as $fileinfo) {
		    if (!$fileinfo->isDot()) {
		        $videos[$vidcount][0] = $fileinfo->getFilename();
		     		$videos[$vidcount][1] = $fileinfo->getSize();  
		     		$vidcount++; 
		    }
		}
		
		if (count($videos)>0) {
			for ($x=0; $x<count($videos); $x++) {
				echo "<br><video preload='auto' src=\""._MY_FILE_H_.$submissionid."/video/".$videos[$x][0]."\" controls width=\"320px\"></video>";
				echo "<br><div id=\"bufferBar\"></div>";
			}
		}	
		
	}
	
?>

		</td>
	</tr>
	
</table>