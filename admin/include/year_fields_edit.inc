<form name="theform" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">
<font class="subtitle">
<?
	if (!empty($recordid)) {
		echo "Submission Year: ".$subyear;			
	}

?>

</font><br><br>

<?
	if (!empty($errortext)) {
		echo "<center><p class=\"errorblock\">";
		echo $errortext;
		echo "</p></center>";
	}			
?>

<input type="hidden" name="recordid" value="<? echo $recordid; ?>">
<input type="hidden" name="action" value="<? echo $action; ?>">

<table width="100%" cellpadding="2" cellspacing="2" border="0" class="atable">	
<tr valign="top"><td colspan="4" class="header">Submission Year Details:</td></tr>

<tr valign="top">
	<td class=label>Year:</td>
	<td class=field><? echo $subyear; ?></td>
</tr>

<tr valign="top">
	<td class=label>Start:</td>
	<td class=field>
		<input name="startdate" id="cal1" type="text" value="<? echo $startdate_disp; ?>" size="10" maxlength="10" onclick="return showCalendar('cal1', 'mm/dd/y');">&nbsp;
	</td>
</tr>

<tr valign="top">
	<td class=label>End:</td>
	<td class=field>
		<input name="enddate" id="cal2" type="text" value="<? echo $enddate_disp; ?>" size="10" maxlength="10" onclick="return showCalendar('cal2', 'mm/dd/y');">&nbsp;
	</td>
</tr>

<tr valign="top">
	<td class=label>Judging Start:</td>
	<td class=field>
		<input name="judgingstart" id="cal3" type="text" value="<? echo $judgingstart_disp; ?>" size="10" maxlength="10" onclick="return showCalendar('cal3', 'mm/dd/y');">&nbsp;
	</td>
</tr>

<tr valign="top">
	<td class=label>Judging End:</td>
	<td class=field>
		<input name="judgingend" id="cal4" type="text" value="<? echo $judgingend_disp; ?>" size="10" maxlength="10" onclick="return showCalendar('cal4', 'mm/dd/y');">&nbsp;
	</td>
</tr>
			
</table>

<br><br>

<table width="100%" border="0">
<tr valign="top">
<td width="50%" align="left">
	
	<?
			if (!empty($recordid)) {	
				echo "<button onClick=\"cancelURL('year_summary.php');return false;\">Cancel</button>&nbsp;";
			} else {
				echo "<button onClick=\"cancelURL('year_summary.php');return false;\">Cancel</button>&nbsp;";
			}
	?>	
	
	<input type="submit" name="saveyear" value="Save Changes">
</td>
</tr>
</table>
	
</form>
