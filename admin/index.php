<?php 
include_once("system/auth.php");

if ($_SESSION['s_role'] == "Judge") {
	include("index_judge.php");	
	
} else if ($_SESSION['s_role'] == "Guest") {	
	include("index_guest.php");
	
} else if ($_SESSION['s_role'] == "Administrator") {
	include("index_admin.php");
}
?>

