<? include("system/top.inc"); ?>

<font class="subtitle">Categories</font>
<br><br>
<a href="category_detail.php?a=edit" class="onwhite">New Category</a>
<br><br>

<?

include("system/searchbox_category.inc");

$query = "SELECT cat.categoryid, cat.title, cat.code, cat.numimages, cat.sortweight, AVG(sc.overallscore) AS AvgScore, 
	COUNT(sc.scoreid) AS TotalScores, COUNT(s.submissionid) AS TotalSubmissions 
	FROM category cat 
	LEFT OUTER JOIN submission s ON (s.categoryid = cat.categoryid AND s.isdeleted=0 AND s.subyear='".$subyear."' AND s.status='Paid') ";
	
if (!empty($_POST['judgeid'])) {
	$query.= "INNER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.isdeleted=0 AND sc.judgeid=".$_POST['judgeid'].") ";
} else {
	$query.= "LEFT OUTER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.isdeleted=0) ";	
}

$query.= "WHERE cat.isdeleted=0 
	GROUP BY cat.categoryid, cat.title, cat.code, cat.numimages, cat.sortweight 
	ORDER BY cat.code";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	//echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\" width=\"50px\"><b>Code</b></td>";
	echo "<td class=\"header\"><b>Category</b></td>";	
	echo "<td class=\"header\" width=\"70px\" align=\"right\"><b># Images</b></td>";
	echo "<td class=\"header\" width=\"70px\" align=\"right\"><b>Disp. Order</b></td>";
	echo "<td class=\"header\" width=\"90px\" align=\"center\"><b># Submissions</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b># Scores</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b>Avg Score</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b># Judges</b></td>";
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		//echo "<td width=\"40px\">".$i."</td>";			
		echo "<td width=\"50px\">".stripslashes($row["code"])."</td>";
		echo "<td><a href=\"category_detail.php?id=".$row["categoryid"]."\">".stripslashes($row["title"])."</a></td>";
		echo "<td width=\"70px\" align=\"right\">".$row["numimages"]."</td>";
		echo "<td width=\"70px\" align=\"right\">".$row["sortweight"]."</td>";
		echo "<td width=\"90px\" align=\"center\">";
		if ($row["TotalSubmissions"]=="0") {
			echo "&nbsp;";
		} else {
			echo $row["TotalSubmissions"];
		}
		echo "</td>";
		echo "<td width=\"80px\" align=\"center\">";
		if ($row["TotalScores"]=="0") {
			echo "&nbsp;";
		} else {
			echo $row["TotalScores"];
		}
		echo "</td>";
		echo "<td width=\"80px\" align=\"center\">";
		if (!empty($row["AvgScore"])) {
			echo number_format($row["AvgScore"],1)."%";	
		}
		echo "&nbsp;</td>";
		
		// Judges count
		echo "<td width=\"80px\" align=\"right\">".GetCategoryJudgesCount($row["categoryid"])."</td>";
		
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>