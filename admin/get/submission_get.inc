<?php
	
$sql = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, ";
$sql.= "DATE_FORMAT(s.emailsent, '%m/%d/%Y %h:%i %p') AS tmpEmailSent, ";
$sql.= "cat.title AS category, cat.numimages, cat.code, s.* ";
$sql.= "FROM submission s ";
$sql.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$sql.= "WHERE s.submissionid='".$recordid."'";
	
$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
$row = mysql_fetch_array($result);

if ($row) {

	$submissionid = $recordid;
	$permakey = $row["permakey"];
	$datecreated = $row["tmpCreated"];		
	$emailsent = $row["tmpEmailSent"];		
	$categoryid = $row["categoryid"];
	$category = stripslashes($row["category"]);
	$status = stripslashes($row["status"]);
	$companyname = stripslashes($row["companyname"]);
	$contactperson = stripslashes($row["contactperson"]);
	$email = stripslashes($row["email"]);
	$url = stripslashes($row["url"]);
	$photographername = stripslashes($row["photographername"]);
	$permakey = $row["permakey"];

	$phone = stripslashes($row["phone"]);
	$phone1 = substr($phone, 0, 3);
	$phone2 = substr($phone, 3, 3);
	$phone3 = substr($phone, -4);
	
	if (!empty($phone)) {
		$phone_display = $phone1."-".$phone2."-".$phone3;
	}
	
	$address1 = stripslashes($row["address1"]);
	$address2 = stripslashes($row["address2"]);
	$city = stripslashes($row["city"]);
	$state = stripslashes($row["state"]);
	$postalcode = stripslashes($row["postalcode"]);
	$country = stripslashes($row["country"]);
	$howlong = stripslashes($row["howlong"]);
	$companydescription = stripslashes($row["companydescription"]);
	$otherinfo = stripslashes($row["otherinfo"]);
	$nominee = stripslashes($row["nominee"]);
	$imagedescription = stripslashes($row["imagedescription"]);
	$numimages = $row["numimages"];
	$code = $row["code"];
	$subyear = $row["subyear"];
	$uniqueid = $subyear."-".$submissionid;
	$videourl = stripslashes($row["videourl"]);
	
	// PayPal information
	$paypaldate = $row["paypaldate"];
	$paypaltransid = $row["transid"];
	$paypalamount = $row["amount"];
	$paypaldesc = "This submission is not found in PayPal";
	
	if (!empty($paypaltransid)) {
		$paypaldesc = "Paid $".$paypalamount." via PayPal on ".$paypaldate." [Transaction ID: ".$paypaltransid."]";
	}
	
	
	if ($emailsent=="00/00/0000 12:00 AM") {
		$emailsentdisplay = "";
	} else {
		$emailsentdisplay = "&nbsp;&nbsp;(Email sent on ".$emailsent.")";	
	}
		
	$createddisplay = "Submitted on ".$datecreated;
	
	// Attachments
	include("submission_files_get.inc");
	
			
}
		
?>