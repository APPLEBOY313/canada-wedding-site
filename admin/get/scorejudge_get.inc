<?

if (!empty($recordid)) {
		
	// Score details	
	$sql = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, s.scoreid, ";
	$sql.= "s.rating_aesthetics, s.rating_themes, s.rating_originality, s.rating_professionalism, ";
	$sql.= "s.rating_overallquality, s.comment, s.overallscore, s.submissionid, s.judgeid, sb.videourl, ";
	$sql.= "j.namefirst, j.namelast, sb.howlong, sb.companydescription, sb.otherinfo, sb.nominee, ";
	$sql.= "sb.imagedescription, sb.phone, sb.subyear, cat.title AS category, cat.numimages, cat.code, s.cannotjudge ";
	$sql.= "FROM score s ";
	$sql.= "INNER JOIN judge j ON (j.judgeid = s.judgeid) ";
	$sql.= "INNER JOIN submission sb ON (sb.submissionid = s.submissionid) ";
	$sql.= "INNER JOIN category cat ON (cat.categoryid = sb.categoryid) ";
	$sql.= "WHERE s.scoreid=".$recordid." AND s.isdeleted=0 AND sb.status='Paid' ";
	$sql.= "AND s.judgeid = ".$_SESSION['s_userid']." ";
	$sql.= "AND sb.categoryid IN (".$_SESSION['s_categories'].") ";
			
	$result = mysql_query($sql) or die ("Error 102<br>".mysql_error());
	$row = mysql_fetch_array($result);
		
	if ($row) {
	
		$scoreid = $recordid;
		$submissionid = $row["submissionid"];
		$judgeid = $row["judgeid"];	
		$judge = stripslashes($row["namefirst"]." ".$row["namelast"]);		
		$rating_aesthetics = $row["rating_aesthetics"];
		$rating_themes = $row["rating_themes"];
		$rating_originality = $row["rating_originality"];
		$rating_professionalism = $row["rating_professionalism"];
		$rating_overallquality = $row["rating_overallquality"];	
		$comment = stripslashes($row["comment"]);	
		$overallscore = $row["overallscore"];
		$category = stripslashes($row["category"]);
		
		// Sept 2011
		$cannotjudge = $row["cannotjudge"];
		if ($cannotjudge == "1")
		{
			$cannotjudge_checked = "checked";
		}
				
		$howlong = stripslashes($row["howlong"]);	
		$companydescription = stripslashes($row["companydescription"]);	
		$nominee = stripslashes($row["nominee"]);	
		$imagedescription = stripslashes($row["imagedescription"]);	
		
		// Can judge change this entry?
		$subyear = $row["subyear"];
		$canchangescore = canJudgeChangeScore($subyear); 
		$videourl = $row["videourl"];
		
		$phone = stripslashes($row["phone"]);
		$phone1 = substr($phone, 0, 3);
		$phone2 = substr($phone, 3, 3);
		$phone3 = substr($phone, -4);
		
		$numimages = $row["numimages"];
		$code = $row["code"];
		$subyear = $row["subyear"];
		$uniqueid = $subyear."-".$submissionid;	
			
		$pagetitle = "Your score for ".$uniqueid.": ".$overallscore."%";
				
		// Can this judge see this submission?
		$canview = true;
		
	}
		
} else if (!empty($submissionid)) {
		
	// New score - get Submission details
	$canchangescore = false;
	
	$sql = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, ";
	$sql.= "cat.title AS category, cat.numimages, cat.code, s.* ";
	$sql.= "FROM submission s ";
	$sql.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
	$sql.= "WHERE s.submissionid='".$submissionid."' AND s.status='Paid' ";
	$sql.= "AND s.categoryid IN (".$_SESSION['s_categories'].") ";
		
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);
	
	if ($row) {
		
		$datecreated = $row["tmpCreated"];		
		$categoryid = $row["categoryid"];
		$category = stripslashes($row["category"]);
		$status = stripslashes($row["status"]);
		$howlong = stripslashes($row["howlong"]);
		$companydescription = stripslashes($row["companydescription"]);
		$otherinfo = stripslashes($row["otherinfo"]);
		$nominee = stripslashes($row["nominee"]);
		$imagedescription = stripslashes($row["imagedescription"]);
		$subyear = $row["subyear"];
		$canchangescore = canJudgeChangeScore($subyear); 
				
		$phone = stripslashes($row["phone"]);
		$phone1 = substr($phone, 0, 3);
		$phone2 = substr($phone, 3, 3);
		$phone3 = substr($phone, -4);

		$numimages = $row["numimages"];
		$code = $row["code"];
		$subyear = $row["subyear"];
		$uniqueid = $subyear."-".$submissionid;	
		$videourl = $row["videourl"];

		$pagetitle = "New score for ".$uniqueid;
		
		// Can this judge see this submission?
		$canview = true;
						
	}
	
}

// Remove after testing
$canchangescore = true;

if ($submissionid>0) {
	// Attachments
	include("submission_files_get.inc");		
}
		
?>