<?
	
$sql = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, s.scoreid, ";
$sql.= "s.rating_aesthetics, s.rating_themes, s.rating_originality, s.rating_professionalism, ";
$sql.= "s.rating_overallquality, s.comment, s.overallscore, s.submissionid, s.judgeid, s.cannotjudge, ";
$sql.= "j.namefirst, j.namelast ";
$sql.= "FROM score s ";
$sql.= "INNER JOIN judge j ON (j.judgeid = s.judgeid) ";
$sql.= "WHERE s.scoreid=".$recordid." AND s.isdeleted=0 ";

$result = mysql_query($sql) or die ("Error 101".mysql_error()."<br>".$sql);
$row = mysql_fetch_array($result);

if ($row) {

	$scoreid = $recordid;
	$judgeid = $row["judgeid"];	
	$judge = stripslashes($row["namefirst"]." ".$row["namelast"]);		
	$submissionid = $row["submissionid"];
	$rating_aesthetics = $row["rating_aesthetics"];
	$rating_themes = $row["rating_themes"];
	$rating_originality = $row["rating_originality"];
	$rating_professionalism = $row["rating_professionalism"];
	$rating_overallquality = $row["rating_overallquality"];	
	$comment = stripslashes($row["comment"]);	
	$overallscore = $row["overallscore"];
	
	// Sept 2011
	$cannotjudge = $row["cannotjudge"];
	if ($cannotjudge == "1")
	{
		$cannotjudge_checked = "checked";
	}
	
	$pagetitle = "Score by ".$judge.": ".$overallscore."%";
	
}
		
?>