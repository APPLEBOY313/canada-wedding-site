<? 
	include("system/top.inc");
	if (!empty($_POST['nominationid'])) {
		include("set/sendnominationemails.inc");
	}
?>

<a href="email_summary.php">Mailing List</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="contactus_summary.php">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="nomination_summary.php">Nominations</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="bridal_summary.php">Bridal</a>
<br><br>

<font class="subtitle">Nominations</font>
<br><br>
The following nominations have been submitted via the Nomination form of the website
<br><br>

<?

include("system/searchbox.inc");

$query = "SELECT DATE_FORMAT(fn.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, ";
$query.= "DATE_FORMAT(fn.emailsenddate, '%m/%d/%Y %h:%i %p') AS tmpEmailSent, ";
$query.= "fn.nominationid, fn.name, fn.email, fn.companyname, fn.phone, ";
$query.= "fn.worktype, fn.emailcontact, fn.emailsenddate, fn.emailsendby,  ";
$query.= "au.namefirst ";
$query.= "FROM form_nomination fn ";
$query.= "LEFT JOIN adminuser au ON (au.adminuserid = fn.emailsendby) ";
$query.= "WHERE fn.isdeleted=0 ";
if (!empty($_POST['search'])) {
	$query.= "AND (fn.email LIKE '%".$_POST['search']."%' OR fn.name LIKE '%".$_POST['search']."%' OR fn.companyname LIKE '%".$_POST['search']."%' ";
	$query.= "OR fn.worktype LIKE '%".$_POST['search']."%' OR fn.emailcontact LIKE '%".$_POST['search']."%') ";
}
$query.= "ORDER BY fn.created DESC";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	
	echo "<form method=\"POST\" method=\"".$_SERVER['PHP_SELF']."\">";
	
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"20px\">&nbsp;</td>";
	echo "<td class=\"header\" width=\"120px\"><b>Date</b></td>";
	echo "<td class=\"header\" width=\"200px\"><b>Submitted By</b></td>";
	echo "<td class=\"header\" width=\"180px\"><b>Nominee</b></td>";
	echo "<td class=\"header\"><b>Type of Work</b></td>";
	echo "<td class=\"header\" width=\"120px\"><b>Email sent</b></td>";
	echo "<td class=\"header\" width=\"60px\">&nbsp;</td>";	
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
			
		echo "<td width=\"20px\"><input type=\"checkbox\" name=\"nominationid[]\" value=\"".$row["nominationid"]."\"></td>";	
		echo "<td width=\"120px\">".$row["tmpCreated"]."</td>";
		echo "<td width=\"200px\">".stripslashes($row["name"])."<br>".stripslashes($row["email"])."<br>".dispPhone($row["phone"])."</td>";
		echo "<td width=\"180px\">".stripslashes($row["companyname"])."<br>".stripslashes($row["emailcontact"])."</td>";
		echo "<td>".nl2br(stripslashes($row["worktype"]))."</td>";
		echo "<td width=\"120px\">";
		
		if ($row["emailsendby"]>0) {
			echo $row["tmpEmailSent"]."<br>By ".stripslashes($row["namefirst"]);
		}
		
		echo "&nbsp;</td>";
		echo "<td width=\"60px\" align=\"center\"><a class=\"onwhite\" href=\"javascript:deleteRecord('form_nomination', '".$row["nominationid"]."', 'nominationid', '"._MY_HREF_ADMIN_."nomination_summary.php');\">Delete</a></td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	echo "<br><br>";
	echo "<input type=\"submit\" name=\"sendemail\" value=\"Send Notification Email\">";
	echo "</form>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>