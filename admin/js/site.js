var thissite = "http://www.bcweddingawards.com/admin/";

// ------------------------------------------------------------------------------------
// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.sel.id == "sel1" || cal.sel.id == "sel3")
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// ------------------------------------------------------------------------------------
// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
}

// ------------------------------------------------------------------------------------
// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format) {
  var el = document.getElementById(id);
  if (calendar != null) {
    // we already have some calendar created
    calendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(false, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    calendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  calendar.setDateFormat(format);    // set the specified date format
  calendar.parseDate(el.value);      // try to parse the text in field
  calendar.sel = el;                 // inform it what input field we use
  calendar.showAtElement(el);        // show the calendar below it

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// ------------------------------------------------------------------------------------
// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

// ------------------------------------------------------------------------------------
function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

// ------------------------------------------------------------------------------------
function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(false, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("DD, M d");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}

// ----------------------------------------------------------
function deleteForm(buttonname) {

	if (confirm('Are you sure you wish to delete this record?')) {
		document.theform[buttonname].click();
	}

}

// ----------------------------------------------------------
function cancelForm(formname, permakey) {

	if (confirm('Are you sure you wish to cancel your changes?')) {
		document.location.href = thissite + formname + '?id=' + permakey;
	}

}

// ----------------------------------------------------------
function cancelNew(formname) {

	if (confirm('Are you sure you wish to cancel your changes?')) {
		document.location.href = thissite + formname.replace('detail', 'summary');
	}

}

// ----------------------------------------------------------
function cancelURL(turl) {

	if (confirm('Are you sure you wish to cancel your changes?')) {
		document.location.href = thissite + turl;
	}

}

// ----------------------------------------------------------
function showHideDiv(divname) {
	
	var divid = document.getElementById(divname);
					
	if (divid!="undefined") {
			
		if (divid.className == "Hide") {
			divid.className = "Show";
		} else {
			divid.className = "Hide";
		}
				
	}
}

// ----------------------------------------------------------
function expandCollapse(divname) {
	
	var divid = document.getElementById(divname);
	var divtext = document.getElementById(divname + '_exp');
	var expand = '[<a href=\"javascript:expandCollapse(\'' + divname + '\');\">Show</a>]';
	var collapse = '[<a href=\"javascript:expandCollapse(\'' + divname + '\');\">Hide</a>]';
	
						
	if (divid!="undefined" && divtext!='undefined') {
			
		if (divid.className == "Hide") {
			divid.className = "Show";
			divtext.innerHTML = collapse;			
			
		} else {
			divid.className = "Hide";
			divtext.innerHTML = expand;			
		}
				
	}
}

// ----------------------------------------------------------
function addOption(selectbox,text,value )
{
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
}

// ----------------------------------------------------------
function removeAllOptions(selectbox)
{
	var i;
	for(i=selectbox.options.length-1;i>=0;i--)
	{
		selectbox.remove(i);
	}
}


// ----------------------------------------------------------
function updateChrLimit(formname, fieldname, chrsname, l) 
{
	var f = document.forms[formname];
	var comments = f[fieldname].value;
	var chrsleft = document.getElementById(chrsname);
	
	if (comments.length > l)
	{
		f[fieldname].value = comments.substring(0, l);		
	} else {
		chrsleft.innerHTML = (l-comments.length-1);
	}
	
}

// ----------------------------------------------------------
function searchGetDateRange() {
	
	var f = document.forms['appsearchbox'];	
	var appdate = f.appdate.options[f.appdate.selectedIndex].value;
	var spanid1 = document.getElementById('searchdaterange1');				
	var spanid2 = document.getElementById('searchdaterange2');				
	
	if (appdate=='Date Range') {
		spanid1.className="Show";
		spanid2.className="Show";
	} else {
		spanid1.className="Hide";
		spanid2.className="Hide";
	}
	
	
}


// ----------------------------------------------------------
function checkFieldLength(fieldname, maxlength) {
	
	var f = document.forms[0];
	var tfield = f[fieldname].value;
	
	if (tfield.length>maxlength) {
		alert('You can only store ' + maxlength + ' characters here');
	}
	
}

// ----------------------------------------------------------
function dupAddress() {
		var f = document.forms['theform'];
		f.address1_ap.value = f.address1_p.value;
		f.address2_ap.value = f.address2_p.value;
		f.town_ap.value = f.town_p.value;
		f.countyid_ap.selectedIndex = f.countyid_p.selectedIndex;
		f.postcode_ap.value = f.postcode_p.value;
}

// ----------------------------------------------------------
function sendConfEmail(subid) {
	
	var f = document.forms['theform'];
		
	if (subid>0) {
		
		var confmessage = document.getElementById('confmessage');
		confmessage.innerHTML = "Sending...";
		var browser = navigator.appName; 
	
		if(browser == "Microsoft Internet Explorer"){
			var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}else{
			/* Create the object using other browser's method */
			var xmlhttp = new XMLHttpRequest();
		}
		
		var myURL = thissite + 'system/ajax_sendemail.php?submissionid=' + subid;
											
		xmlhttp.open("GET", myURL, true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) { 
				var response = xmlhttp.responseText;
										
				if (response=='OK') {
					confmessage.innerHTML = 'Email Sent';	
				} else {
					confmessage.innerHTML = 'Error sending email';
				}
												
			}
		}
	
		xmlhttp.send(null);
		
	}
		
}

// ----------------------------------------------------------
function sendConfEmailFromView(subid) {
		
	if (subid>0) {
		if (confirm('Are you sure you wish to set this submission to PAID?')) {
			
			var confmessage = document.getElementById('status_' + subid);
			confmessage.innerHTML = "Sending...";
			var browser = navigator.appName; 
		
			if(browser == "Microsoft Internet Explorer"){
				var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}else{
				/* Create the object using other browser's method */
				var xmlhttp = new XMLHttpRequest();
			}
		
			var myURL = thissite + 'system/ajax_setsubmissiontopaid.php?submissionid=' + subid;
														
			xmlhttp.open("GET", myURL, true);
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState == 4) { 
					var response = xmlhttp.responseText;
					if (response=='OK') {
						confmessage.innerHTML = 'Paid';	
					} else {
						confmessage.innerHTML = 'Error';
					}
													
				}
			}
		
			xmlhttp.send(null);
		
		}
		
	}
		
}

// ----------------------------------------------------------
function deleteRecord(tablename, recordid, pkfield, returl) {
	
	if (confirm('Are you sure you wish to delete this record?')) {
			
		var browser = navigator.appName; 	
		if(browser == "Microsoft Internet Explorer"){
			var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}else{
			/* Create the object using other browser's method */
			var xmlhttp = new XMLHttpRequest();
		}
		
		var myURL = thissite + 'system/ajax_deletedocument.php?tn=' + tablename + '&pkfield=' + pkfield + '&id=' + recordid + '&returl=' + escape(returl);
															
		xmlhttp.open("GET", myURL, true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) { 
				var response = xmlhttp.responseText;
										
				if (response=='OK') {
					window.location.reload();
				} else {
					alert('Error 6789: Could not delete record\n\n' + response);			
				}
												
			}
		}
	
		xmlhttp.send(null);
		
	}
		
}

// ----------------------------------------------------------
function SetAllCheckBoxes(FormName, FieldName)
{
	if(!document.forms[FormName])
		return;
		
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
		
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
		{
			if (objCheckBoxes[i].checked)
			{
				objCheckBoxes[i].checked = false;
			}
			else
			{
				objCheckBoxes[i].checked = true;
			}
			
		}
			
}

// ----------------------------------------------------------
function cannotJudge()
{
	var f = document.forms.theform;	
	
	if (f.cannotjudge.checked)
	{
		f.rating_aesthetics.disabled = true;	
		f.rating_themes.disabled = true;	
		f.rating_originality.disabled = true;	
		f.rating_professionalism.disabled = true;	
		f.rating_overallquality.disabled = true;	
	}
	else
	{
		f.rating_aesthetics.disabled = false;	
		f.rating_themes.disabled = false;	
		f.rating_originality.disabled = false;	
		f.rating_professionalism.disabled = false;	
		f.rating_overallquality.disabled = false;	
	}
	
}

// ----------------------------------------------------------
function unassignJudgeCategory(id) {
			
	var browser = navigator.appName; 	
	if(browser == "Microsoft Internet Explorer"){
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		/* Create the object using other browser's method */
		var xmlhttp = new XMLHttpRequest();
	}
	
	var myURL = thissite + 'system/ajax_deletejudgefromcategory.php?id=' + id;
														
	xmlhttp.open("GET", myURL, true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) { 
			var response = xmlhttp.responseText;
									
			if (response=='OK') {
				window.location.reload();
			} else {
				alert('Error 6789: Could not unassign judge from category\n\n' + response);			
			}
											
		}
	}

	xmlhttp.send(null);
	

		
}


// ----------------------------------------------------------
function deleteGuest(guestid) {
	
	if (confirm('Are you sure you wish to delete this guest?')) {
			
		var browser = navigator.appName; 	
		if(browser == "Microsoft Internet Explorer"){
			var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}else{
			/* Create the object using other browser's method */
			var xmlhttp = new XMLHttpRequest();
		}
		
		var myURL = thissite + 'system/ajax_deleteguest.php?guestid=' + guestid;
															
		xmlhttp.open("GET", myURL, true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) { 
				var response = xmlhttp.responseText;
										
				if (response=='OK') {
					window.location.reload();
				} else {
					alert('Error 6799: Could not delete record\n\n' + response);			
				}
												
			}
		}
	
		xmlhttp.send(null);
		
	}
		
}