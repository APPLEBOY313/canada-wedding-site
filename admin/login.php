<?
include("system/auth.php"); 

if ($_GET['err']=="2") {
	$accesserror = "You are not permitted to access this application from your current location.";
	
} else if ($_GET['err']=="3") {
	$accesserror = "You are not permitted to access this web page";
	
} else if ($_GET['err']=="4") {
	$oktext = "New password has been emailed";

}

if (!empty($loginerror)) {
	$forgotdivshowhide = "Show";
} else {
	$forgotdivshowhide = "Hide";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><? echo _COMPANYNAME_; ?></title>

<link href="<? echo _MY_HREF_ADMIN_; ?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<? echo _MY_HREF_ADMIN_; ?>css/style.css">	
<link rel="stylesheet" type="text/css" href="<? echo _MY_HREF_ADMIN_; ?>system/cal/cal2.css">
<script src="<? echo _MY_HREF_ADMIN_; ?>js/site.js" type="text/javascript"></script>

</head>

<body>

<div id="wrapper">

<img src="images/logo.png" border="0" style="border:0px solid #000;">

<center>	

<div style="margin-top:20px;margin-bottom:10px;">
	
	<form method="post" action="login.php">
	
		<table width=100%>
			<tr valign="top">
				<td>
					
					<center>
					<div class="login">
						
						<img src="<? echo _MY_IMG_ADMIN_H_; ?>login.gif" border="0" align="absmiddle">&nbsp;<font class="subtitle">Admin Login</font>
						
						<table width="100%" cellpadding="2" cellspacing="5">
						
						<tr valign="top">
							<td width="100" align="right">Username:</td>
							<td><input type="text" name="login_username" value="<? echo $_POST['login_username']; ?>" size=20 class=loginfield></td>
						</tr>
						
						<tr valign="top">
							<td width="100" align="right">Password:</td>
							<td><input type="password" name="login_pwd" size=20 class=loginfield></td>
						</tr>
						
						<tr valign="top">
							<td width="100" align="right">&nbsp;</td>
							<td><input type="submit" name="login" value="Login" class=loginfield></td>
						</tr>
						
						<tr valign="top">
							<td width="100" align="right">&nbsp;</td>
							<td><a href="judgelogin.php">Judge Login</a></td>
						</tr>
						
						</table>
						
						<? if (!empty($loginerror)) echo "<br><center><font class=\"errorblock\">".$loginerror."</font></center><br>"; ?>
						<? if (!empty($accesserror)) echo "<br><center><div class=\"errorblock\" style=\"width:90%\">".$accesserror."</div></center><br>"; ?>
										
					</div>	
					</center>
		
				</td>
			</tr>
		</table>

	</form>

</div>

<font style="font-size:11px;color:#666;">Copyright &copy; 2010 <? echo _COMPANYNAME_; ?>.  All rights reserved.</font>

</center>

</div>

</body>
</html>