<?php 
include("system/top.inc"); 	
include("system/getsearchvars_simple.inc");
?>

<font class="subtitle">Submissions</font>
<br><br>
<a href="submission_detail.php?a=edit" class="onwhite">New Submission</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="index_admin_archive.php" class="onwhite">Archived Submissions</a>
<br><br>

<?php

include("system/searchbox.inc");

$query = "SELECT s.submissionid, DATE_FORMAT(s.created, '%m/%d/%Y') AS tmpCreated, ";
$query.= "s.companyname, s.contactperson, s.categoryid, s.status, s.phone, s.nominee, ";
$query.= "s.city, s.state, s.country, s.submissionid, s.subyear, cat.title AS category, ";
$query.= "AVG(sc.overallscore) AS AvgScore, SUM(CASE WHEN sc.overallscore>0 THEN 1 ELSE 0 END) AS NumScores ";
$query.= "FROM submission s ";
$query.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$query.= "LEFT OUTER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.isdeleted=0 AND sc.overallscore>0) ";
$query.= "WHERE s.isdeleted=0 AND s.isarchived=0 ";
$query.= $sqlsearch;

$query.= "GROUP BY s.submissionid, s.created, ";
$query.= "s.companyname, s.contactperson, s.categoryid, s.status, s.phone, s.nominee, ";
$query.= "s.city, s.state, s.country, s.submissionid, s.subyear, cat.title ";
$query.= "ORDER BY s.created DESC ";
$query.= "LIMIT $start, $limit";

$sqlcount = "SELECT COUNT(s.submissionid) AS SQLCount ";
$sqlcount.= "FROM submission s ";
$sqlcount.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$sqlcount.= "WHERE s.isdeleted=0 AND s.isarchived=0 ";
$sqlcount.= $sqlsearch;

$t_result = mysql_query($sqlcount) or die(mysql_error()."<br><br>".$sqlcount);
$t_row = mysql_fetch_array($t_result);
$total_count = $t_row["SQLCount"];

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
		
	include ("system/nav.php");
	
	echo "<form action=\"index_admin.php\" method=\"POST\" name=\"theform\">";
	
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"10px\">";
	echo "<input type=\"checkbox\" onclick=\"SetAllCheckBoxes('theform','chkEntry[]')\">";
	echo "</td>";	
	echo "<td class=\"header\" width=\"90px\"><b>Submitted</b></td>";	
	echo "<td class=\"header\" width=\"70px\" align=\"center\"><b>Year</b></td>";	
	echo "<td class=\"header\" width=\"250px\"><b>Category</b></td>";
	echo "<td class=\"header\" width=\"200px\"><b>By Company</b></td>";	
	echo "<td class=\"header\" width=\"70px\"><b>Status</b></td>";	
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b>Score</b></td>";	
	echo "<td class=\"header\" width=\"80px\" align=\"center\"><b># Scores</b></td>";	
	echo "<td class=\"header\" width=\"80px\" align=\"right\"><b>ID</b></td>";	
	echo "</tr>";
		
	$n=0;
			
	while ($row = mysql_fetch_array($result)) {
	
		if ($n==0) {
			$idcoll = $row["submissionid"];
		} else {
			$idcoll.= "_".$row["submissionid"];
		}
	
		// Build Array
		$recarr[$n][0] = $row["submissionid"];
		$recarr[$n][1] = $row["scoreid"];
		$recarr[$n][2] = $row["tmpCreated"];
		$recarr[$n][3] = stripslashes($row["category"]);
		$recarr[$n][4] = stripslashes($row["country"]);
		$recarr[$n][5] = stripslashes($row["status"]);
		$recarr[$n][6] = $row["subyear"];
		$recarr[$n][7] = stripslashes($row["companyname"]);
		$recarr[$n][8] = $row["AvgScore"];
		$recarr[$n][9] = $row["NumScores"];	
	
		$n++;
		
	}
	
	rsort($recarr);
	
	for ($i=0; $i<count($recarr);$i++) {
		
			if($i % 2) { 
				echo "<tr valign=\"top\" class=rowdata>";
			} else {
				echo "<tr valign=\"top\" class=rowdata_alt>";
			}	
	
			echo "<td width=\"10px\"><input type=\"checkbox\" name=\"chkEntry[]\" value=\"".$recarr[$i][0]."\">";
			echo "<td width=\"90px\">".$recarr[$i][2]."</td>";		
			echo "<td width=\"70px\" align=\"center\">".$recarr[$i][6]."</td>";	
			echo "<td width=\"250px\">".$recarr[$i][3]."</td>";
			echo "<td width=\"200px\">".$recarr[$i][7]."</td>";
			echo "<td width=\"70px\"><span id=\"status_".$recarr[$i][0]."\">".$recarr[$i][5]."</span>";
			
			if ($recarr[$i][5]=="Pending") {
				echo "<br><a href=\"javascript:sendConfEmailFromView('".$recarr[$i][0]."');\">Set to Paid</a>";	
			}
			
			"</td>";
			echo "<td width=\"80px\" align=\"center\">";
			if (!empty($recarr[$i][8])) {
				echo number_format($recarr[$i][8],1)."%";	
			}
			echo "&nbsp;</td>";
			echo "<td width=\"80px\" align=\"center\">";
			if (!empty($recarr[$i][9])) {
				echo $recarr[$i][9];	
			} else {
				echo "0";
			}
			echo "</td>";
			echo "<td width=\"80px\" align=\"right\"><a href=\"submission_detail.php?id=".$recarr[$i][0]."&idcoll=".$idcoll."\">".$recarr[$i][6]."-".$recarr[$i][0]."</a></td>";
			echo "</tr>";
			
	}
	
	echo "</table>";
	echo "<br><br>";
	echo "<input type=\"submit\" name=\"btnArchive\" value=\"Archive\">&nbsp;";
	echo "<input type=\"submit\" name=\"btnSetPaid\" value=\"Set to Paid\">";
	echo "</form>";
	
} else {
	echo "No records found";
	
}

//echo $query;

include("system/bottom.inc"); 

?>