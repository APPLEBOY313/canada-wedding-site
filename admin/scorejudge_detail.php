<? 

if (empty($recordid)) $recordid = $_POST['recordid'];
if (empty($recordid)) $recordid = $_GET['id'];

if (empty($submissionid)) $submissionid = $_POST['submissionid'];
if (empty($submissionid)) $submissionid = $_GET['submissionid'];

// ----------------------------------------------
// Get position in collection, if exists
// ----------------------------------------------
if (empty($idcoll)) $idcoll = $_POST['idcoll'];
if (empty($idcoll)) $idcoll = $_GET['idcoll'];

if (!empty($idcoll)) {
	$idarr = explode("_", $idcoll);	
	$currentpos = array_search($submissionid, $idarr);
		
	if ($currentpos==0) {
		$previd="";	
		$nextid = $idarr[$currentpos+1];
	} else {
		$previd = $idarr[$currentpos-1];
		$nextid = $idarr[$currentpos+1];
	}
		
	if ($currentpos==count($idarr)-1) {
		$previd = $idarr[$currentpos-1];
		$nextid="";	
	} else {
		$previd = $idarr[$currentpos-1];
		$nextid = $idarr[$currentpos+1];
	}	
	
}

include("system/top.inc"); 

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

// Can this judge see this submission?
$canview = false;

// ----------------------------------------------
if (!empty($recordid) || !empty($submissionid)) {
	
	include("get/scorejudge_get.inc");	
	
	//if (!empty($submissionid)) {
	if (!empty($recordid)) {
		
		// Get Score details
		$sql = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, s.scoreid,
			s.rating_aesthetics, s.rating_themes, s.rating_originality, s.rating_professionalism, 
			s.rating_overallquality, s.comment, s.overallscore, s.submissionid, s.judgeid, s.cannotjudge, 
			j.namefirst, j.namelast, cat.title AS category 
			FROM score s 
			INNER JOIN judge j ON (j.judgeid = s.judgeid) 
			INNER JOIN submission sub ON (sub.submissionid = s.submissionid)
			INNER JOIN category cat ON (cat.categoryid = sub.categoryid)		
			WHERE s.scoreid=".$recordid." AND s.isdeleted=0 
			AND sub.categoryid IN (".$_SESSION['s_categories'].") ";
		
		$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
		$row = mysql_fetch_array($result);
		
		if ($row) {
		
			$rating_aesthetics = $row["rating_aesthetics"];
			$rating_themes = $row["rating_themes"];
			$rating_originality = $row["rating_originality"];
			$rating_professionalism = $row["rating_professionalism"];
			$rating_overallquality = $row["rating_overallquality"];	
			$comment = stripslashes($row["comment"]);	
			$overallscore = $row["overallscore"];
			$category = stripslashes($row["category"]);
			
			// Sept 2011
			$cannotjudge = $row["cannotjudge"];
			if ($cannotjudge == "1")
			{
				$cannotjudge_checked = "checked";
			}
						
		} else {
			
			$rating_aesthetics = "1";
			$rating_themes = "1";
			$rating_originality = "1";
			$rating_professionalism = "1";
			$rating_overallquality = "1";
			
		}
		
		
	}
	
	
} else {
	// Default values 
	$pagetitle = "Score this Submission";
	$createddisplay = "Created on ".gmdate('m/d/Y h:i A');
	
	$rating_aesthetics = "1";
	$rating_themes = "1";
	$rating_originality = "1";
	$rating_professionalism = "1";
	$rating_overallquality = "1";
	
}

// Make sure they are allowed to edit this lead...
if ($_SESSION['s_role']=="Judge") {
	
	if ($canchangescore=="1") {
		$canedit = true;	
	}
	
}


if (!empty($action) && $canview) {
	
	if ($canedit) {
		include("include/scorejudge_fields_edit.inc");	
		
	} else {
		include("include/scorejudge_fields_read.inc");
	}
		
} else if ($canview) {
	include("include/scorejudge_fields_read.inc");	
	
} else {
	echo "No entry found";
}

include("system/bottom.inc"); 

?>