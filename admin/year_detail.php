<? 
include("system/top.inc"); 

if (empty($recordid)) $recordid = $_POST['recordid'];
if (empty($recordid)) $recordid = $_GET['id'];

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

if (!empty($recordid)) {
	include("get/year_get.inc");	
} else {
	
	// Default values;
	
}

// Make sure they are allowed to edit this lead...
if ($_SESSION['s_role']=="Administrator") {
	$canedit = true;
	include("include/year_fields_edit.inc");	
}

include("system/bottom.inc"); 
?>