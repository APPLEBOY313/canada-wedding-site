<? 
include("system/top.inc"); 

if (empty($recordid)) $recordid = $_POST['recordid'];
if (empty($recordid)) $recordid = $_GET['id'];

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

if (empty($submissionid)) $submissionid = $_GET['submissionid'];


// ----------------------------------------------

if (!empty($recordid)) {
	include("get/score_get.inc");	
	
} else {
	// Default values 
	$createddisplay = "Created on ".gmdate('m/d/Y h:i A');
}

// Make sure they are allowed to edit this lead...
if ($_SESSION['s_role']=="Administrator") {
	$canedit = true;
}

if (!empty($action)) {
	
	if ($canedit) {
		include("include/score_fields_edit.inc");	
		
	} else {
		include("include/score_fields_read.inc");
	}
		
} else {
	include("include/score_fields_read.inc");	
}

include("system/bottom.inc"); 

?>