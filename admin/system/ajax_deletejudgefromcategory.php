<?php

include_once("auth.php");
$id = $_GET['id'];

if ($id>0) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE judges_category SET isdeleted=1 WHERE ID=:id";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':id', $id);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	echo "OK";
		
} else {
	
	echo "Error";	
	
}

?>