<?php
global $start;
global $limit;
global $referer;

$start = 0;
$limit = 50;
$qsort = "";
$qorder = "";

if (!empty($_GET['start'])) {
	$start = $_GET['start'];
}

if (!empty($_GET['limit'])) {
	$limit = $_GET['limit'];
}

if (!empty($_GET['sort'])) {
	$qsort = $_GET['sort'];
}

if (!empty($_GET['order'])) {
	$qorder = $_GET['order'];
}

// --------------------------------------------------------------------------------------
function getServerVar($var,$def='')
{
	// return actual if available, otherwise return default
	return (isset($_SERVER[$var]) ? trim($_SERVER[$var]) : $def);
}

// --------------------------------------------------------------------------------------
function getGetVar($var,$def='')
{
	// return actual if available, otherwise return default
	return (isset($_GET[$var]) ? trim($_GET[$var]) : $def);
}

// --------------------------------------------------------------------------------------
function getSessionVar($var,$def='')
{
	// return actual if available, otherwise return default
	return (isset($_SESSION[$var])?$_SESSION[$var]:$def);
}

// --------------------------------------------------------------------------------------
function safe($value){
   return mysql_real_escape_string($value);
} 

// --------------------------------------------------------------------------------------
function displayFields( $tmpkeyword, $current_value, $showblank ) {

	$found_current = false;
	$sqlquery="SELECT * FROM keyword WHERE keyword='$tmpkeyword'";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {
				
		while ($row = mysql_fetch_array($result)) {
			$values = $row["kwvalues"];
			$values_array = explode(";", $values);
			
			$array_count = count($values_array) + 1;

			if ($showblank=='1') echo "<option></option>\n";

			for ($i=0; $i < count($values_array); $i++) {
				if (trim($values_array[$i]!="")) {
					if (trim($values_array[$i])==trim($current_value))	 {
						echo "<option selected>".$values_array[$i]."</option>\n";
						$found_current = true;
					} else {
						echo "<option>".$values_array[$i]."</option>\n";
					}
				}
			}


			if (!$found_current && $current_value!="") echo "<option selected>".$current_value."</option>";

		}
	}

}

// --------------------------------------------------------------------------------------
function isPage($test_page, $current_page) {

	// Truncate current page
	$lastslash = strrpos($current_page, "/");
	if ($lastslash>=0) {
		$current = substr($current_page, $lastslash + 1, strlen($current_page));
	} else {
		$current = $current_page;
	}

	// Truncate test page you are comparing
	$lastslash2 = strrpos($test_page, "/");
	if ($lastslash2>0) {
		$test = substr($test_page, $lastslash2+1, strlen($test_page));
	} else {
		$test = $test_page;
	}

	//echo "Comparing $test with $current - $lastslash2";

	if (strtolower($test) == strtolower($current)) {
		return true;
	} else {
		return false;
	}


}

// --------------------------------------------------------------------------------------
function isAdminPage($current_page) {
	return (preg_match('|/admin/|',$current_page)?'Yes':'No');
}

// --------------------------------------------------------------------------------------
function getField($tmpfield,$tmptable,$tmpcond) {

	// Only used in Header file
	if ($tmpcond=="") {
		$query = "SELECT (".$tmpfield.") AS SQLVal FROM $tmptable";
	} else {
		$query = "SELECT (".$tmpfield.") AS SQLVal FROM $tmptable WHERE $tmpcond";
	}
	
	//echo $query."<hr>";
	
	$result = mysql_query($query);

	// check for no return
	if ($result===false) return '';

	$row = mysql_fetch_array($result); //or die (mysql_error());

	if ($row) return $row['SQLVal'];
	return '';
}



// --------------------------------------------------------------------------------------
function displayAdminUsers( $uid ) {

	$sqlquery="SELECT adminuserid, namefirst, namelast FROM adminuser WHERE isdeleted=0 AND isactive=1 ORDER BY namefirst, namelast";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["adminuserid"]== $uid)	 {
				echo "<option value=\"".$row["adminuserid"]."\" selected>".stripslashes($row["namefirst"]." ".$row["namelast"])."</option>";
			} else {
				echo "<option value=\"".$row["adminuserid"]."\">".stripslashes($row["namefirst"]." ".$row["namelast"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayCountries($country) {

	$sqlquery = "SELECT region, country FROM country ORDER BY regionorder, country";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			$region = stripslashes($row["region"]);
			
			if ($region!=$oldregion) {
				echo "</optgroup><optgroup label=\"".$region."\">";
			}

			if ($row["country"]== $country)	 {
				echo "<option value=\"".$row["country"]."\" selected>".stripslashes($row["country"])."</option>";
			} else {
				echo "<option value=\"".$row["country"]."\">".stripslashes($row["country"])."</option>";
			}
			
			$oldregion = $region;

		}
	}

}

// --------------------------------------------------------------------------------------
function displayCategories($catid) {

	$sqlquery = "SELECT categoryid, CONCAT(code,' ',title) AS title FROM category WHERE isdeleted=0 ORDER BY code";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["categoryid"]== $catid)	 {
				echo "<option value=\"".$row["categoryid"]."\" selected>".stripslashes($row["title"])."</option>";
			} else {
				echo "<option value=\"".$row["categoryid"]."\">".stripslashes($row["title"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayJudges($judgeid) {

	$sqlquery = "SELECT judgeid, CONCAT(namefirst, ' ', namelast) AS judge FROM judge WHERE isdeleted=0 ORDER BY namefirst";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["judgeid"]== $judgeid)	 {
				echo "<option value=\"".$row["judgeid"]."\" selected>".stripslashes($row["judge"])."</option>";
			} else {
				echo "<option value=\"".$row["judgeid"]."\">".stripslashes($row["judge"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayCounties( $cid ) {

	$sqlquery="SELECT countyid, county FROM county ORDER BY county";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["countyid"]== $cid)	 {
				echo "<option value=\"".$row["countyid"]."\" selected>".stripslashes($row["county"])."</option>";
			} else {
				echo "<option value=\"".$row["countyid"]."\">".stripslashes($row["county"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayCountiesInRegion( $r, $cid ) {

	if (!empty($r)) {
		$sqlquery="SELECT countyid, county FROM county WHERE region='$r' ORDER BY county";	
	} else {
		$sqlquery="SELECT countyid, county FROM county ORDER BY county";
	}
	
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["countyid"]== $cid)	 {
				echo "<option value=\"".$row["countyid"]."\" selected>".stripslashes($row["county"])."</option>";
			} else {
				echo "<option value=\"".$row["countyid"]."\">".stripslashes($row["county"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayRegions( $r ) {

	$sqlquery="SELECT region FROM county GROUP BY region ORDER BY region";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["region"]== $r)	 {
				echo "<option value=\"".$row["region"]."\" selected>".stripslashes($row["region"])."</option>";
			} else {
				echo "<option value=\"".$row["region"]."\">".stripslashes($row["region"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displayLetters( $lid ) {

	$sqlquery="SELECT lettertemplateid, title FROM lettertemplate WHERE isdeleted=0 ORDER BY title";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {

		echo "<option></option>";

		while ($row = mysql_fetch_array($result)) {

			if ($row["lettertemplateid"]== $lid)	 {
				echo "<option value=\"".$row["lettertemplateid"]."\" selected>".stripslashes($row["title"])."</option>";
			} else {
				echo "<option value=\"".$row["lettertemplateid"]."\">".stripslashes($row["title"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function displaySubYears( $yr ) {

	$sqlquery="SELECT subyear FROM submissionyear ORDER BY subyear";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {
		
		while ($row = mysql_fetch_array($result)) {
			
			if ($row["subyear"]== $yr)	 {
				echo "<option value=\"".$row["subyear"]."\" selected>".stripslashes($row["subyear"])."</option>";
			} else {
				echo "<option value=\"".$row["subyear"]."\">".stripslashes($row["subyear"])."</option>";
			}

		}
	}

}

// --------------------------------------------------------------------------------------
function showMessage( $tmptext, $tmptype, $imagepath ) {
	
	if (strtolower($tmptype)=="error") {
		$tmpclass = "warning";
	
	} else if (strtolower($tmptype)=="fyi") {
		$tmpclass = "";
	
	} else if (strtolower($tmptype)=="ok") {
		$tmpclass = "ok";
	
	}
	
	echo "<center><div class=\"messagebox\"><font class=\"".$tmpclass."\">".$tmptext."</font></div></center>";
	
}


// --------------------------------------------------------------------------------------
function isDuplicateUser($email, $currentuserid) {
	
	$dupsql = "SELECT userid ";
	$dupsql.= "FROM user ";
	$dupsql.= "WHERE email='".$email."' AND isdeleted=0 ";
	
	if (!empty($currentuserid)) {
		$dupsql.= "AND userid!=".$currentuserid;
	}
		
	$dupres = mysql_query($dupsql) or die("Error Code: 1040");
	$duprow = mysql_fetch_array($dupres);
	
	if ($duprow) {
		if ($duprow["userid"]>0) {
			return true;
		}
	}
	
	return false;
	
}



// --------------------------------------------------------------------------------------
function TimeAgo($datefrom,$dateto=-1)
{
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

if($datefrom<=0) { return "A long time ago"; }

// Uncomment for US
//if($dateto==-1) { $dateto = time(); }
if($dateto==-1) { $dateto = time() + 28800; }

// Calculate the difference in seconds betweeen
// the two timestamps

$difference = $dateto - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

if($difference < 60)
{
$interval = "s";
}

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
elseif($difference >= 60 && $difference<60*60)
{
$interval = "n";
}

// If difference is between 1 hour and 24 hours
// hours is a good interval
elseif($difference >= 60*60 && $difference<60*60*24)
{
$interval = "h";
}

// If difference is between 1 day and 7 days
// days is a good interval
elseif($difference >= 60*60*24 && $difference<60*60*24*7)
{
$interval = "d";
}

// If difference is between 1 week and 30 days
// weeks is a good interval
elseif($difference >= 60*60*24*7 && $difference <
60*60*24*30)
{
$interval = "ww";
}

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
elseif($difference >= 60*60*24*30 && $difference <
60*60*24*365)
{
$interval = "m";
}

// If difference is greater than or equal to 365
// days, return year. This will be incorrect if
// for example, you call the function on the 28th April
// 2008 passing in 29th April 2007. It will return
// 1 year ago when in actual fact (yawn!) not quite
// a year has gone by
elseif($difference >= 60*60*24*365)
{
$interval = "y";
}

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

switch($interval)
{
case "m":
$months_difference = floor($difference / 60 / 60 / 24 /
29);
while (mktime(date("H", $datefrom), date("i", $datefrom),
date("s", $datefrom), date("n", $datefrom)+($months_difference),
date("j", $dateto), date("Y", $datefrom)) < $dateto)
{
$months_difference++;
}
$datediff = $months_difference;

// We need this in here because it is possible
// to have an 'm' interval and a months
// difference of 12 because we are using 29 days
// in a month

if($datediff==12)
{
$datediff--;
}

$res = ($datediff==1) ? "$datediff month ago" : "$datediff
months ago";
break;

case "y":
$datediff = floor($difference / 60 / 60 / 24 / 365);
$res = ($datediff==1) ? "$datediff year ago" : "$datediff
years ago";
break;

case "d":
$datediff = floor($difference / 60 / 60 / 24);
$res = ($datediff==1) ? "$datediff day ago" : "$datediff
days ago";
break;

case "ww":
$datediff = floor($difference / 60 / 60 / 24 / 7);
$res = ($datediff==1) ? "$datediff week ago" : "$datediff
weeks ago";
break;

case "h":
$datediff = floor($difference / 60 / 60);
$res = ($datediff==1) ? "$datediff hour ago" : "$datediff
hours ago";
break;

case "n":
$datediff = floor($difference / 60);
$res = ($datediff==1) ? "$datediff minute ago" :
"$datediff minutes ago";
break;

case "s":
$datediff = $difference;
$res = ($datediff==1) ? "$datediff second ago" :
"$datediff seconds ago";
break;
}
return $res;
}

// --------------------------------------------------------------------------------------
function mysql2timestamp($datetime){
       $val = explode(" ",$datetime);
       $date = explode("-",$val[0]);
       $time = explode(":",$val[1]);
       return mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
}


// --------------------------------------------------------------------------------------
function displayFullName($f, $m, $l) {
	
	if (!empty($m)) {
		echo $f." ".$m." ".$l;
		
	} else if (empty($f) && empty($l)) {
		echo "<i>No Name</i>";
					
	} else {
		echo $f." ".$l;
	}

}

// --------------------------------------------------------------------------------------
function getFullName($f, $m, $l) {
	
	if (!empty($m)) {
		return $f." ".$m." ".$l;
		
	} else if (empty($f) && empty($l)) {
		return "";
					
	} else {
		return $f." ".$l;
	}

}

// --------------------------------------------------------------------------------------
function displayFormName($current_page) {

// Truncate current page
	$lastslash = strrpos($current_page, "/");
	if ($lastslash>=0) {
		$current = substr($current_page, $lastslash + 1, strlen($current_page));
	} else {
		$current = $current_page;
	}
	
	return $current;
	
}

// --------------------------------------------------------------------------------------
function createPermakey()
{
	// Size of Permakey
  $length=15; 
  $rand_id="";
  
  for($i=1; $i<=$length; $i++) {
		mt_srand((double)microtime() * 1000000);
   	$num = mt_rand(1,36);
   	$rand_id .= assign_rand_value($num);
	}
  
	return $rand_id;
	
}

// --------------------------------------------------------------------------------------
function createLoginID() {
	
	return rand(100000, 999999); 
	
} 

// --------------------------------------------------------------------------------------
function createPassCode() {
	
	return rand(100000, 999999); 
	
}

// --------------------------------------------------------------------------------------
function assign_rand_value($num)
{
// accepts 1 - 36
  switch($num)
  {
    case "1":
     $rand_value = "a";
    break;
    case "2":
     $rand_value = "b";
    break;
    case "3":
     $rand_value = "c";
    break;
    case "4":
     $rand_value = "d";
    break;
    case "5":
     $rand_value = "e";
    break;
    case "6":
     $rand_value = "f";
    break;
    case "7":
     $rand_value = "g";
    break;
    case "8":
     $rand_value = "h";
    break;
    case "9":
     $rand_value = "i";
    break;
    case "10":
     $rand_value = "j";
    break;
    case "11":
     $rand_value = "k";
    break;
    case "12":
     $rand_value = "l";
    break;
    case "13":
     $rand_value = "m";
    break;
    case "14":
     $rand_value = "n";
    break;
    case "15":
     $rand_value = "o";
    break;
    case "16":
     $rand_value = "p";
    break;
    case "17":
     $rand_value = "q";
    break;
    case "18":
     $rand_value = "r";
    break;
    case "19":
     $rand_value = "s";
    break;
    case "20":
     $rand_value = "t";
    break;
    case "21":
     $rand_value = "u";
    break;
    case "22":
     $rand_value = "v";
    break;
    case "23":
     $rand_value = "w";
    break;
    case "24":
     $rand_value = "x";
    break;
    case "25":
     $rand_value = "y";
    break;
    case "26":
     $rand_value = "z";
    break;
    case "27":
     $rand_value = "0";
    break;
    case "28":
     $rand_value = "1";
    break;
    case "29":
     $rand_value = "2";
    break;
    case "30":
     $rand_value = "3";
    break;
    case "31":
     $rand_value = "4";
    break;
    case "32":
     $rand_value = "5";
    break;
    case "33":
     $rand_value = "6";
    break;
    case "34":
     $rand_value = "7";
    break;
    case "35":
     $rand_value = "8";
    break;
    case "36":
     $rand_value = "9";
    break;
  }
return $rand_value;

}

// --------------------------------------------------------------------------------------
function isOddNumber($num) {
  return ($num%2) ? TRUE : FALSE;
}

// --------------------------------------------------------------------------------------
function getUserName($fid) {

	if ($fid>0) {
						
		$sql = "SELECT CONCAT(namefirst, ' ', namelast) AS tmpName FROM adminuser ";
		$sql.= "WHERE isdeleted=0 AND adminuserid=".$fid;
						
		$result = mysql_query($sql) or die (mysql_error()."<br><br>".$f);
		$row = mysql_fetch_array($result) ;
		
		if ($row) {							
			return stripslashes($row["tmpName"]);
		}
		
	}

	return "";
	
}

function showIP($ip) {
	// --------------------------------------------------------------------------------------
	
	if (substr($ip,0,3) == "141") {
		return "88.86.143.178";
		
	} else {
		return $ip;
	}
	
}

// --------------------------------------------------------------------------------------
function displayCheckboxFields( $tmpkeyword, $tmpname, $currentarr) {
	
	$current_array = explode(",", $currentarr);	
	$sqlquery="SELECT * FROM keyword WHERE keyword='$tmpkeyword'";	
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);
		
	if ($num_results>0) {
		
		echo "<TABLE width=\"75%\" cellpadding=0 cellspacing=0 border=0>";
		echo "<TR valign=\"top\">";
				
		while ($row = mysql_fetch_array($result)) {
			$values = $row["kwvalues"];		
			$values_array = explode(";", $values);
			$array_count = count($values_array) + 1;			
			
			for ($i=0; $i < count($values_array); $i++) {
				if (!empty($values_array[$i])) {
					$found_current = "";
					for ($p=0; $p < count($current_array); $p++) {
						if (trim($values_array[$i])==trim($current_array[$p]) && !empty($current_array[$p])) {
							$found_current = "Yes";
						}	
					}
					
					if($i == 2 || $i==4 || $i==6 || $i==8 || $i==10 || $i==12 || $i==14 || $i==16 || $i==18 || $i==20 || $i==22 || $i==24 || $i==26) echo "</TR><TR valign=\"top\">"; 
					
					if (!empty($values_array[$i])) {
						if ($found_current =="Yes") {
							echo "<TD style=\"line-height:1.8;\"><INPUT type=\"checkbox\" name=\"".$tmpname."[]\" value=\"".$values_array[$i]."\" checked>&nbsp;".trim($values_array[$i])."</TD>";
						} else {
							echo "<TD style=\"line-height:1.8;\"><INPUT type=\"checkbox\" name=\"".$tmpname."[]\" value=\"".$values_array[$i]."\">&nbsp;".trim($values_array[$i])."</TD>";
						}
					}
				
				}	
			}
		}
		
		echo "</TR></TABLE>";
	}

}

function cleanName($f, $m, $l) {
	// --------------------------------------------------------------------------------------
	
	if (empty($m)) {
		return stripslashes($f." ".$l);
	} else {
		return stripslashes($f." ".$m." ".$l);
	}
	
}

// --------------------------------------------------------------------------------------
function getSubmissionYear($dt) {

	if (!empty($dt)) {
						
		$sql = "SELECT subyear FROM submissionyear WHERE '".$dt."' BETWEEN startdate AND enddate LIMIT 0,1 ";
		$result = mysql_query($sql) or die (mysql_error()."<br><br>".$f);
		$row = mysql_fetch_array($result) ;
		
		if ($row) {							
			return $row["subyear"];
		}
		
	}

	return "";
	
}

// --------------------------------------------------------------------------------------
function createUploadFolder( $fullpath ) {
	
	if (!empty($fullpath)) {
		if (!is_dir($fullpath)) {

				// Create directory
				mkdir($fullpath) or die("Unable to create directory: ".$fullpath);
		   
				if (is_dir($fullpath)) {
		   		chmod($fullpath, 0755);
				}
		   
		} 
	}
	
}

// --------------------------------------------------------------------------------------
function canJudgeChangeScore($yr) {
	
	if (!empty($yr)) {
						
		$sql = "SELECT CASE WHEN DATE(NOW())>=judgingstart AND DATE(NOW())<=judgingend THEN 1 ELSE 0 END AS CanChange FROM submissionyear WHERE subyear='".$yr."' ";
		$result = mysql_query($sql) or die (mysql_error()."<br><br>".$f);
		$row = mysql_fetch_array($result) ;
		
		if ($row) {							
			if ($row["CanChange"]=="1") {
				return true;
			} else if ($row["CanChange"]=="0") {
				return false;
			}
		}
		
	}

	return false;
	
}

// --------------------------------------------------------------------------------------
function dispPhone( $ph ) {
	
	if (!empty($ph)) {
		
		$ph = cleanPhone($ph);
		
		$areacode = substr($ph,0,3);
		$middle = substr($ph,3,3);
		$end = substr($ph, 6, 4);
		return "(".$areacode.") ".$middle."-".$end;
		
	} else {
		return $ph;
	}
	
	
}

// --------------------------------------------------------------------------------------
function cleanPhone( $ph ) {
	
	$phone = str_replace("(", "", $ph);
	$phone = str_replace(")", "", $phone);
	$phone = str_replace("-", "", $phone);
	$phone = str_replace(".", "", $phone);
	$phone = str_replace(" ", "", $phone);

	return $phone;	
	
}

// --------------------------------------------------------------------------------------
function IsJudgeAssignToCategory($judgeid, $categoryid) {
	
	$sql = "SELECT ID FROM judges_category WHERE isdeleted=0 AND judgeid=".$judgeid." AND categoryid=".$categoryid;
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);
	
	if ($row) {
		if ($row["ID"]>0) {
			return true;
		}
	}
	
	return false;
	
}

// --------------------------------------------------------------------------------------
function GetCategoryJudgesCount($categoryid) {
	
	$sql = "SELECT COUNT(DISTINCT(judgeid)) AS SQLCount FROM judges_category WHERE isdeleted=0 AND categoryid=".$categoryid;
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);
	
	if ($row) {
		if ($row["SQLCount"]>0) {
			return $row["SQLCount"];
		}
	}
	
	return "0";
	
}

// --------------------------------------------------------------------------------------
function GetCategoriesForJudge($judgeid) {

	$cats = "";

	$sqlquery = "SELECT categoryid FROM judges_category WHERE isdeleted=0 AND judgeid=".$judgeid." ORDER BY categoryid";
	$result = mysql_query($sqlquery);
	$num_results = mysql_num_rows($result);

	if ($num_results>0) {
		
		while ($row = mysql_fetch_array($result)) {
			if (!empty($cats)) $cats.=",";
			$cats.=$row["categoryid"];
		}
	}
	
	return $cats;

}

?>