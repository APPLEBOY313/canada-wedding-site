<?php

include_once("auth.php");
$guestid = $_GET['guestid'];

if ($guestid>0) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE guest SET isdeleted=1 WHERE guestid=:guestid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':guestid', $guestid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
		
	// Add 1 back to Seats Available
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE settings SET galaseatsavailable=galaseatsavailable+1 WHERE settingsid=1";
	    
	    $sql = $dbh->prepare($q);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	echo "OK";
		
} else {
	
	echo "Error";	
	
}

?>