<?php include("auth.php"); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
<meta http-equiv="Pragma" content="no-cache">
<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><? echo _COMPANYNAME_ADMIN_; ?></title>

<link href="<? echo _MY_HREF_ADMIN_; ?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<? echo _MY_HREF_ADMIN_; ?>css/style.css">	
<link rel="stylesheet" type="text/css" href="<? echo _MY_HREF_ADMIN_; ?>system/cal/cal2.css">
	
<script type="text/javascript" src="<? echo _MY_HREF_ADMIN_; ?>system/cal/calendar.js"></script>
<script type="text/javascript" src="<? echo _MY_HREF_ADMIN_; ?>system/cal/lang/calendar-en.js"></script>
<script type="text/javascript" src="<? echo _MY_HREF_ADMIN_; ?>js/site.js"></script>
</head>

<body>

<div id="wrapper">
	
	<table width="100%" cellpadding="2" cellspacing="2" border="0">
	<tr valign="top">
	<td width="48px"><a href="<? echo _MY_HREF_ADMIN_; ?>"><img src="<? echo _MY_IMG_ADMIN_H_; ?>logo.png" width="200px" border="0" style="border:0px solid #000;"></a></td>
	<td align="right">
			<b><? echo _COMPANYNAME_ADMIN_; ?></b><br>
			<font style="font-size:11px;"><? echo $_SESSION['s_fullname']." logged in as ".$_SESSION['s_role']; ?></font>
				
			<?
				if (!isPage("image_preview.php", $_SERVER['PHP_SELF'])) {
					if ($_SESSION['s_role']=="Judge") {	 											
						if (!empty($_SESSION['s_message_judge'])) {
							echo "<div class=\"message_judge\">";
							echo nl2br($_SESSION['s_message_judge']);
							echo "</div>";
						}	
					}
				}
			?>
				
	</td>	
	</tr>
	</table>
	
	<table width="100%" cellpadding="2" cellspacing="2" border="0">

	<?
		if (!isPage("image_preview.php", $_SERVER['PHP_SELF'])) {
	?>
						
		<tr valign="top">
			<td colspan="2" style="padding-bottom:5px;padding-left:5px;border-bottom:1px solid #CCC;">
				
				<table width="100%" cellpadding="2" cellspacing="2" border="0">
				<tr valign="top">
					<td align="left">
											
						<?
							
							// Menu links
							echo "<span class=\"left\">";
							
							if ($_SESSION['s_role']=="Judge") {				
								echo "<a href=\""._MY_HREF_ADMIN_."index_judge.php?report=tobejudged\" class=\"onwhite\">Needs Judging</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."index_judge.php?report=judged\" class=\"onwhite\">Have Judged</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
							} else if ($_SESSION['s_role']=="Guest") {
								echo "<a href=\""._MY_HREF_ADMIN_."index_guest.php?report=tobejudged\" class=\"onwhite\">Needs Judging</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."index_guest.php?report=judged\" class=\"onwhite\">Have Judged</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
							} else {
								echo "<a href=\""._MY_HREF_ADMIN_."index.php\" class=\"onwhite\">Submissions</a>&nbsp;&nbsp;|&nbsp;&nbsp;";	
								echo "<a href=\""._MY_HREF_ADMIN_."score_summary.php\" class=\"onwhite\">Scores</a>&nbsp;&nbsp;|&nbsp;&nbsp;";	
							}
														
							echo "<a href=\""._MY_HREF_ADMIN_."index.php?action=logout\" class=\"onwhite\">Logout</a>"; 
							echo "</span>";
							
							// Show admin links
							echo "<span style=\"float:right;\">";			
							if ($_SESSION['s_role']=="Administrator") {				
								echo "<b>Admin:</b>&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."judge_summary.php\" class=\"onwhite\">Judges</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."category_summary.php\" class=\"onwhite\">Categories</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."nominee_summary.php\" class=\"onwhite\">Nominees</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."gala_summary.php\" class=\"onwhite\">Gala Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."discount_summary.php\" class=\"onwhite\">Discounts</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."year_summary.php\" class=\"onwhite\">Dates</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."email_summary.php\" class=\"onwhite\">Web Forms</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."settings_detail.php\" class=\"onwhite\">Settings</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
								echo "<a href=\""._MY_HREF_ADMIN_."adminuser_summary.php\" class=\"onwhite\">Users</a>";
								
//							} else if ($_SESSION['s_role']=="Judge") {	 								
//								
//								if (!empty($_SESSION['s_message_judge'])) {
//									echo "<div class=\"message_judge\">";
//									echo nl2br($_SESSION['s_message_judge']);
//									echo "</div>";
//								}
								
							}
							echo "</span>";
							
						?>
						
					</td>
				</tr>
				</table>
				
			</td>
		</tr>
		
		<?
			}	
		?>
		
		<tr valign="top">
			<td width="100%">
				
				<div style="margin-top:20px;">