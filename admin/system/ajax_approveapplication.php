<?php

include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/dbconnect.php");
include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/functions.php");

$status = $_GET['status'];
$qastatus = $_GET['qastatus'];
$uid = $_GET['uid'];
$appid = $_GET['appid'];

if (!empty($status) && $uid>0 && $appid>0) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE application SET status=:status, qastatus=:qastatus, reviewedadminuserid=:uid WHERE applicationid = :appid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':status', $status);
	    $sql->bindParam(':qastatus', $qastatus);
	    $sql->bindParam(':uid', $uid);
	    $sql->bindParam(':appid', $appid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	if ($status=="Approved") {
		$class = "green";	
		$message = "Application has been saved. Click below to create a custom follow-up letter.";
	} else if ($status=="Rejected") {
		$class = "red";
		$message = "Application Review has been ".$status;
	}
	
	echo "<font class=\"".$class."\">".$message."</font>";
	
} else {
	
	echo "0";	
		
}

?>
