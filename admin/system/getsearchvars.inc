<?

if (!empty($_POST['search'])) $search = $_POST['search'];
if (!empty($_POST['apptype'])) $apptype = $_POST['apptype'];
if (!empty($_POST['region'])) $region = $_POST['region'];
if (!empty($_POST['countyid'])) $countyid = $_POST['countyid'];
if (!empty($_POST['authorityid'])) $authorityid = $_POST['authorityid'];
if (!empty($_POST['apptype'])) $apptype = $_POST['apptype'];
if (!empty($_POST['appdate'])) $appdate = $_POST['appdate'];
if (!empty($_POST['startdate'])) $startdate = $_POST['startdate'];
if (!empty($_POST['enddate'])) $enddate = $_POST['enddate'];
if (!empty($_POST['adminuserid'])) $adminuserid= $_POST['adminuserid'];
if (!empty($_POST['status'])) $status = $_POST['status'];

if (!empty($_GET['region'])) $region = $_GET['region'];
if (!empty($_GET['countyid'])) $countyid = $_GET['countyid'];
if (!empty($_GET['authorityid'])) $authorityid = $_GET['authorityid'];
if (!empty($_GET['adminuserid'])) $adminuserid = $_GET['adminuserid'];
if (!empty($_GET['status'])) $status = $_GET['status'];

if (!empty($startdate)) {
	$date1 = explode("/", $_POST['startdate']);
	$mysqlstartdate = $date1[2]."-".$date1[1]."-".$date1[0];
}

if (!empty($enddate)) {
	$date1 = explode("/", $_POST['enddate']);
	$mysqlenddate = $date1[2]."-".$date1[1]."-".$date1[0];
}

if (isset($_POST['reset'])) {
	$search = "";
	$apptype = "";
	$appdate = "";
	$region = "";
	$countyid = "";
	$authorityid = "";
	$adminuserid = "";
	$startdate = "";
	$enddate = "";
	$mysqlstartdate = "";
	$mysqlenddate = "";
	$status = "";
}

$sqlsearch = "";

if (!empty($search)) {
	$sqlsearch.= " AND (ap.title LIKE '%".$search."%' OR ap.description LIKE '%".$search."%' OR ap.appnumber LIKE '%".$search."%') ";
}

if (!empty($status)) {
	$sqlsearch.= " AND (ap.status ='".$status."') ";
}

if (!empty($apptype)) {
	$sqlsearch.= " AND (ap.type_application ='".$apptype."') ";
}

if (!empty($region)) {
	$sqlsearch.= " AND (co.region ='".$region."') ";
}

if (!empty($countyid)) {
	$sqlsearch.= " AND (a.countyid ='".$countyid."') ";
}

if (!empty($authorityid)) {
	$sqlsearch.= " AND (a.authorityid ='".$authorityid."') ";
}

if (!empty($appdate)) {
	if ($appdate=="This week") {
		$sqlsearch.= " AND WEEK(ap.datesubmitted)=WEEK(NOW()) ";		
	} else if ($appdate=="Last 2 weeks") {
		$sqlsearch.= " AND ap.datesubmitted>=DATE_SUB(NOW(), INTERVAL 14 DAY) ";
	} else if ($appdate=="Last 3 weeks") {
		$sqlsearch.= " AND ap.datesubmitted>=DATE_SUB(NOW(), INTERVAL 21 DAY) ";
	} else if ($appdate=="Last 4 weeks") {
		$sqlsearch.= " AND ap.datesubmitted>=DATE_SUB(NOW(), INTERVAL 28 DAY) ";
	} else if ($appdate=="Date Range") {				
		if (!empty($startdate)) $sqlsearch.= " AND ap.datesubmitted>='".$mysqlstartdate."' ";
		if (!empty($enddate)) $sqlsearch.= " AND ap.datesubmitted<='".$mysqlenddate."' ";
	}
}

if (!empty($adminuserid)) {
	$sqlsearch.= " AND (ap.adminuserid=".$adminuserid.") ";
}

if ($status=="ToBeReviewed") {
	$sqlsearch.= " AND ap.status='Input' ";
	
} else if ($status=="Printed") {	
	$sqlsearch.= " AND ap.status='Printed' ";
}
	

?>