<?php

include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/dbconnect.php");
include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/functions.php");
include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/class.phpmailer.php");

$submissionid = $_GET['submissionid'];

if ($submissionid>0) {
	
	$subject = stripslashes(getField("confsubject", "settings", "settingsid=1"));
	$body = stripslashes(getField("confbody", "settings", "settingsid=1"));
		
	$sql = "SELECT s.email, s.contactperson, cat.title AS category ";
	$sql.= "FROM submission s ";
	$sql.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
	$sql.= "WHERE s.submissionid=".$submissionid;
		
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);
	
	if ($row) {
		
		$sendto = $row["email"];
		$contactperson = stripslashes($row["contactperson"]);
		if (empty($contactperson)) $contactperson = "Submitter";
		$category = stripslashes($row["category"]);
		
		$mailbody = "Dear ".$contactperson.",<br><br>Regarding your recent submission into the ".$category." category:<br><br>".$body;
		$mailbody.= "<br><br>Professional BC Wedding Awards<br>http://www.bcweddingawards.com";
		
		// ----------------------------------------------------------------		
		// Send Email
		// ----------------------------------------------------------------
		$mail = new PHPMailer();
		$mail->From = "submissions@bcweddingawards.com";
		$mail->FromName = "Professional BC Wedding Awards";
		$mail->Host = "localhost.localdomain";
		$mail->Mailer = "mail";
		$mail->Subject = $subject;
		$mail->Body = $mailbody;
		$mail->IsHTML(true); 
		$mail->AddAddress($sendto, $contactperson);	
		$mail->AddBCC("darryl.wing@gmail.com", "Darryl Wing");	
		$mail->Send();
		$mail->ClearAddresses();			
		
		try
		{ 
				$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		    
		    $q = "UPDATE submission SET emailsent=now() WHERE submissionid=:submissionid";
		    
		    $sql = $dbh->prepare($q);
		    $sql->bindParam(':submissionid', $submissionid);
		    $sql->execute();
		                 
		    $dbh = null;
		}
		catch(PDOException $e){
		  error_log('PDOException - ' . $e->getMessage(), 0);
		  http_response_code(500);
		  echo $e->getMessage();
		  die('Error establishing connection with database');
		}
		
		echo "OK";
		
	}
	
} else {
	
	echo "Error";	
		
}

?>