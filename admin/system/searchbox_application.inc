<form name="appsearchbox" action="<? echo $_SERVER['PHP_SELF']; ?>" method="POST">

<table width="100%" cellpadding="2" cellspacing="0" border="0" class="searchboxtable">
<tr valign="top">
	<td align="left">
	
		<table width="100%" cellpadding="2" cellspacing="0" border="0">
		<tr valign="top">
			<td width="100px" align="right"><b>Find:</b></td>
			<td width="400px" colspan="3"><input type="text" name="search" value="<? echo $search;?>" style="width:100%"></td>
			<td width="100px" align="right"><b>Type:</b></td>
			<td width="350px"><select name="apptype"><? displayFields("application_type", $apptype, "1"); ?></select>
			&nbsp;&nbsp;&nbsp;&nbsp;<b>Status:</b>&nbsp;<select name="status"><? displayFields("status", $status, "1"); ?></select>
			</td>
		</tr>
		<tr valign="top">
			<td width="100px" align="right"><b>Region:</b></td>
			<td width="150px"><select name="region" onchange="searchGetCountyListFromRegion();"><? displayRegions($region); ?></select></td>
			<td width="100px" align="right"><b>County:</b></td>
			<td width="150px"><select name="countyid" id="countyid" onchange="searchGetAuthorityListFromCounty();"><? displayCountiesInRegion($region, $countyid); ?></select></td>
			<td width="100px" align="right"><b>Authority:</b></td>
			<td width="350px"><select name="authorityid" id="authorityid"><? displayAuthoritesInCounty($region, $countyid, $authorityid); ?></select></td>
		</tr>
		<tr valign="top">
			<td width="100px" align="right"><b>Week:</b></td>
			<td width="150px"><select name="appdate" onchange="searchGetDateRange()"><? displayFields("application_datefilter", $appdate, "1"); ?></select></td>
			
			<?
				if (!empty($startdate) || !empty($enddate) || $appdate=="Date Range") {
					$drclass = "Show";
				} else {
					$drclass = "Hide";
				}
			?>
		
			<td width="100px" align="right" id="searchdaterange1" class="<? echo $drclass; ?>"><b>From:</b></td>
			<td width="400px" id="searchdaterange2" class="<? echo $drclass; ?>">
					<input name="startdate" id="cal1" type="text" value="<? echo $startdate; ?>" size=8 maxlength=10 onclick="return showCalendar('cal1', 'dd/mm/y');">&nbsp;			
					&nbsp;<b>To:</b>&nbsp;<input name="enddate" id="cal2" type="text" value="<? echo $enddate; ?>" size=8 maxlength=10 onclick="return showCalendar('cal2', 'dd/mm/y');">&nbsp;
			</td>
			<td width="100px" align="right"><b>Assigned:</b></td>
			<td width="150px"><select name="adminuserid"><? displayAdminUsers($adminuserid); ?></select></td>
		</tr>
		
		<tr valign="top">
			<td width="100px" align="right">&nbsp;</td>
			<td colspan="5" width="850px">
				<input type="submit" value="Search">&nbsp;<input type="submit" name="reset" value="Reset">
			</td>
		</tr>
		</table>		
		
	</td>
</tr>
</table>

</form>
<br>