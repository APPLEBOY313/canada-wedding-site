<?php

if (!empty($_POST['login_username']) && !empty($_POST['login_pwd'])) {
	
	try
	{ 
			$loginid = safe($_POST['login_username']);
			$loginpwd = safe($_POST['login_pwd']);
      
      $dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
      
      $q = "SELECT adminuserid, namefirst, namelast, email, role FROM adminuser WHERE isactive=1 AND isdeleted=0 AND loginid=:loginid AND loginpwd=:loginpwd";
      
      $sql = $dbh->prepare($q);
      $sql->bindParam(':loginid', $loginid);
      $sql->bindParam(':loginpwd', $loginpwd);
      
      $sql->execute();
      $login_row = $sql->fetch();
            
      if ($login_row) {
		
				$ipaddress = $_SERVER['REMOTE_ADDR'];
					
				// Verify security for user...
				$_SESSION['s_userid'] = $login_row["adminuserid"];
				$_SESSION['s_fullname'] = stripslashes($login_row["namefirst"]." ".$login_row["namelast"]);
				$_SESSION['s_email'] = $login_row["email"];
				$_SESSION['s_role'] = $login_row["role"];
																			
				// Update last login date/time
				$login_query = "UPDATE adminuser SET lastlogin=NOW(), ipaddress=:ipaddress WHERE adminuserid=:adminuserid ";
				$usql = $dbh->prepare($login_query);
      	$usql->bindParam(':adminuserid', $login_row["adminuserid"]);   
      	$usql->bindParam(':ipaddress', $ipaddress);   
      	$usql->execute();
				
				// Insert into history
				$history_query = "INSERT INTO loginhistory (created, adminuserid, logintime, ipaddress) VALUES (NOW(), :adminuserid, NOW(), :ipaddress)";
				$isql = $dbh->prepare($history_query);
      	$isql->bindParam(':adminuserid', $login_row["adminuserid"]);   
      	$isql->bindParam(':ipaddress', $ipaddress);   
      	$isql->execute();
						
				header("Location:index.php");
				exit();			
				
			} else {
				
				session_destroy();
				
			}
      
	                 
      $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    echo $e->getMessage();
    die('Error establishing connection with database');
  }
   		
	$loginerror = "Login failed";

}

?>