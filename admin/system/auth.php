<?php

// -----------------------------------------------------
// set some PHP configs
// -----------------------------------------------------
ini_set('display_errors', '1');

// -----------------------------------------------------
// Global settings
// -----------------------------------------------------
session_cache_limiter('none');
header('Cache-control: private'); // Fix for IE6
session_start();

// -----------------------------------------------------
// Include generic files
// -----------------------------------------------------
include_once('dbconnect.php');
include_once('functions.php');
include_once('class.phpmailer.php');

// -----------------------------------------------------
// Redirect to login page if session has expired
// -----------------------------------------------------
if (empty($_SESSION['s_userid'])) {	
	if (!isPage("login.php", $_SERVER['PHP_SELF']) && !isPage("judgelogin.php", $_SERVER['PHP_SELF'])) {
		header("Location:login.php");
		exit();
	}
}


// -----------------------------------------------------
$_SERVER['FULL_URL'] = 'http';
if(getServerVar('HTTPS')=='on'){$_SERVER['FULL_URL'] .=  's';}
$_SERVER['FULL_URL'] .=  '://';

// -----------------------------------------------------
define('_COMPANYNAME_','BC Wedding Awards');
define('_COMPANYNAME_ADMIN_','BC Wedding Awards');

// DB Credentials
define('DBNAME', 'bcawards_db1');
define('DBUSER', 'bcawards_bcadmin');
define('DBPWD', 'T+NBoH-3UtV7');

// Base Virtual
define('_MY_BASE_',$_SERVER['DOCUMENT_ROOT']."/");	
define('_MY_BASE_ADMIN_',$_SERVER['DOCUMENT_ROOT']."/admin/");

// Base HTTP
define('_MY_HREF_', "http://www.bcweddingawards.com/");
define('_MY_HREF_ADMIN_', _MY_HREF_."admin/");

// Root images
define('_MY_IMG_H_',_MY_HREF_.'images/');
define('_MY_IMG_V_',_MY_BASE_.'images/');

// Submission files
define('_MY_FILE_H_',_MY_HREF_.'entryfiles/');
define('_MY_FILE_V_',_MY_BASE_.'entryfiles/');

// Admin images
define('_MY_IMG_ADMIN_H_',_MY_HREF_ADMIN_.'/images/');
define('_MY_IMG_ADMIN_V_',_MY_BASE_ADMIN_.'/images/');

// -----------------------------------------------------
// Destroy session if Query has ?logout=true
// -----------------------------------------------------
if (isset($_GET['action'])&&($_GET['action'] == 'logout')) {
	
	if ($_SESSION['s_role']=="Judge") {				
		$loginpage = "judgelogin.php";
	} else {
		$loginpage = "login.php";
	}
	
	if (isset($_COOKIE[session_name()])) setcookie(session_name(), '', time()-42000, '/');
	session_destroy();
	header('Location:'.$loginpage);
	exit();
}

// -----------------------------------------------------
// Process Admin Login: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['login'])) {	
	include_once(_MY_BASE_ADMIN_."/system/processlogin.inc");	
}

// -----------------------------------------------------
// Process Judge Login: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['judgelogin'])) {	
	include_once(_MY_BASE_ADMIN_."/system/processlogin_judge.inc");	
}

// -----------------------------------------------------
// Save Admin User: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['saveadminuser'])) {
	include_once(_MY_BASE_ADMIN_."/set/adminuser_set.inc");	
}

// -----------------------------------------------------
// Delete User: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deleteadminuser'])) {
	include_once(_MY_BASE_ADMIN_."/set/adminuser_set.inc");	
}

// -----------------------------------------------------
// Save Settings: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savesettings'])) {
	include_once(_MY_BASE_ADMIN_."/set/settings_set.inc");	
}

// -----------------------------------------------------
// Save Keyword: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savekeyword'])) {
	include_once(_MY_BASE_ADMIN_."/set/keyword_set.inc");	
}

// -----------------------------------------------------
// Save Judge: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savejudge'])) {
	include_once(_MY_BASE_ADMIN_."/set/judge_set.inc");	
}

// -----------------------------------------------------
// Delete Judge: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deletejudge'])) {
	include_once(_MY_BASE_ADMIN_."/set/judge_set.inc");	
}

// -----------------------------------------------------
// Save Submission: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savesubmission'])) {
	include_once(_MY_BASE_ADMIN_."/set/submission_set.inc");	
}

// -----------------------------------------------------
// Delete Submission: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deletesubmission'])) {
	include_once(_MY_BASE_ADMIN_."/set/submission_set.inc");	
}

// -----------------------------------------------------
// Save Category: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savecategory'])) {
	include_once(_MY_BASE_ADMIN_."/set/category_set.inc");	
}

// -----------------------------------------------------
// Delete Category: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deletecategory'])) {
	include_once(_MY_BASE_ADMIN_."/set/category_set.inc");	
}

// -----------------------------------------------------
// Save Score: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savescore'])) {
	include_once(_MY_BASE_ADMIN_."/set/score_set.inc");	
}

// -----------------------------------------------------
// Delete Score: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deletescore'])) {
	include_once(_MY_BASE_ADMIN_."/set/score_set.inc");	
}

// -----------------------------------------------------
// Save Submission Year: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['saveyear'])) {
	include_once(_MY_BASE_ADMIN_."/set/year_set.inc");	
}

// -----------------------------------------------------
// Save Judge Score: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savescorejudge'])) {
	include_once(_MY_BASE_ADMIN_."/set/scorejudge_set.inc");	
}

// -----------------------------------------------------
// Save Discount Code: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['savediscount'])) {
	include_once(_MY_BASE_ADMIN_."/set/discount_set.inc");	
}

// -----------------------------------------------------
// Delete Discount Code: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['deletediscount'])) {
	include_once(_MY_BASE_ADMIN_."/set/discount_set.inc");	
}

// -----------------------------------------------------
// Process bulk Archive: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnArchive'])) {
	$bulk_archive = true;
	include_once(_MY_BASE_ADMIN_."/set/submission_bulkarchive_set.inc");	
}

// -----------------------------------------------------
// Process bulk Set To Paid: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnSetPaid'])) {
	$bulk_archive = true;
	include_once(_MY_BASE_ADMIN_."/set/submission_bulksetpaid_set.inc");	
}

// -----------------------------------------------------
// Process bulk Active: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnActive'])) {
	$bulk_archive = false;
	include_once(_MY_BASE_ADMIN_."/set/submission_bulkarchive_set.inc");	
}

// -----------------------------------------------------
// Process bulk Archive Gala: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnArchiveGala'])) {
	$bulk_archive = true;
	include_once(_MY_BASE_ADMIN_."/set/gala_bulkarchive_set.inc");	
}

// -----------------------------------------------------
// Process bulk Active: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnActiveGala'])) {
	$bulk_archive = false;
	include_once(_MY_BASE_ADMIN_."/set/gala_bulkarchive_set.inc");	
}

// -----------------------------------------------------
// Process bulk Send RSVP email: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['btnSendRSVP'])) {
	$bulk_archive = false;
	include_once(_MY_BASE_ADMIN_."/set/rsvp_sendemail_set.inc");	
}

// -----------------------------------------------------
// Assign judge to category: SQL Parametized: DONE
// -----------------------------------------------------
if (!empty($_POST['assignjudgetocategory'])) {
	include_once(_MY_BASE_ADMIN_."/set/judge_category_set.inc");	
}

// -----------------------------------------------------
// Add Guest name in RSVP list
// -----------------------------------------------------
if (!empty($_POST['addguest'])) {
	include_once(_MY_BASE_ADMIN_."/set/guest_set.inc");	
}

?>