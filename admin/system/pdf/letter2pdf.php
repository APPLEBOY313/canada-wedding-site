<?php
ini_set("display_errors", "Off");
define('_MY_BASE_',$_SERVER['DOCUMENT_ROOT']);
define('_PDFPATH_',$_SERVER['DOCUMENT_ROOT']."/system/pdf");

include(_PDFPATH_."/fpdf.php");
include_once(_MY_BASE_."/system/dbconnect.php");
include_once(_MY_BASE_."/system/functions.php");

if (empty($lettertemplateid)) {
	$lettertemplateid = $_GET['id']; 
}

if ($lettertemplateid>0) {
		
		// -------------------------------------------------
		// Get Invoice information
		// -------------------------------------------------
		$recordid = $lettertemplateid;
		include(_MY_BASE_."/get/letter_get.inc");				
		include(_PDFPATH_."/pdfclass.inc");
	
		$markercode = "[SNIPPET START - DO NOT REMOVE]";
		$body = str_replace($markercode, "", $body);
	
		// --------------------------------------------------------------
		//Instanciation of inherited class
		// --------------------------------------------------------------
		$pdf=new PDF('P', 'mm', 'A4');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		
		// Top Line (leave room for header)
		$pdf->Ln(40);
		
		$pdf->SetFont('Arial','',12);
		$pdf->MultiCell(190,6,$body,'','L',0,0);			
		$pdf->Ln(10);
					
		// --------------------------------------------------------------
		error_reporting(E_ALL ^ E_NOTICE);		
		$pdf->Output("FollowupLetter_".$invoicenumber.".pdf", "D");
		
		
}

?>
