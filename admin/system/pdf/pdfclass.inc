<?

class PDF extends FPDF {

	// Visibility...

	var $visibility='all';
	var $n_ocg_print;
	var $n_ocg_view;

	function SetVisibility($v)
	{
    	if($this->visibility!='all')
        		$this->_out('EMC');
    	if($v=='print')
        		$this->_out('/OC /OC1 BDC');
    	elseif($v=='screen')
        		$this->_out('/OC /OC2 BDC');
    	elseif($v!='all')
        		$this->Error('Incorrect visibility: '.$v);
    	$this->visibility=$v;
	}

	function _endpage()
	{
    		$this->SetVisibility('all');
    		parent::_endpage();
	}

	function _enddoc()
	{
    		if($this->PDFVersion<'1.5')
        		$this->PDFVersion='1.5';
    		parent::_enddoc();
	}

	function _putocg()
	{
    		$this->_newobj();
    		$this->n_ocg_print=$this->n;
    		$this->_out('<</Type /OCG /Name '.$this->_textstring('print'));
    		$this->_out('/Usage <</Print <</PrintState /ON>> /View <</ViewState /OFF>>>>>>');
    		$this->_out('endobj');
    		$this->_newobj();
    		$this->n_ocg_view=$this->n;
    		$this->_out('<</Type /OCG /Name '.$this->_textstring('view'));
    		$this->_out('/Usage <</Print <</PrintState /OFF>> /View <</ViewState /ON>>>>>>');
    		$this->_out('endobj');
	}

	function _putresources()
	{
    		$this->_putocg();
    		parent::_putresources();
	}

	function _putresourcedict()
	{
    		parent::_putresourcedict();
    		$this->_out('/Properties <</OC1 '.$this->n_ocg_print.' 0 R /OC2 '.$this->n_ocg_view.' 0 R>>');
	}

	function _putcatalog()
	{
    		parent::_putcatalog();
    		$p=$this->n_ocg_print.' 0 R';
    		$v=$this->n_ocg_view.' 0 R';
    		$as="<</Event /Print /OCGs [$p $v] /Category [/Print]>> <</Event /View /OCGs [$p $v] /Category [/View]>>";
    		$this->_out("/OCProperties <</OCGs [$p $v] /D <</ON [$p] /OFF [$v] /AS [$as]>>>>");
	}


	//Cell with horizontal scaling if text is too wide
	function CellFit($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='',$scale=0,$force=1)
	{
		//Get string width
		$str_width=$this->GetStringWidth($txt);

		//Calculate ratio to fit cell
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		if ($str_width>0) $ratio=($w-$this->cMargin*2)/$str_width;

		$fit=($ratio < 1 || ($ratio > 1 && $force == 1));
		if ($fit)
		{
			switch ($scale)
			{

				//Character spacing
				case 0:
					//Calculate character spacing in points
					$char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
					//Set character spacing
					$this->_out(sprintf('BT %.2f Tc ET',$char_space));
					break;

				//Horizontal scaling
				case 1:
					//Calculate horizontal scaling
					$horiz_scale=$ratio*100.0;
					//Set horizontal scaling
					$this->_out(sprintf('BT %.2f Tz ET',$horiz_scale));
					break;

			}
			//Override user alignment (since text will fill up cell)
			$align='';
		}

		//Pass on to Cell method
		$this->DataCell($w,$h,$txt,$border,$ln,$align,$fill,$link);

		//Reset character spacing/horizontal scaling
		if ($fit)
			$this->_out('BT '.($scale==0 ? '0 Tc' : '100 Tz').' ET');
	}

	//Cell with horizontal scaling only if necessary
	function CellFitScale($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
	{
		$this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,1,0);
	}

	//Cell with horizontal scaling always
	function CellFitScaleForce($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
	{
		$this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,1,1);
	}

	//Cell with character spacing only if necessary
	function CellFitSpace($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
	{
		$this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,0,0);
	}

	//Cell with character spacing always
	function CellFitSpaceForce($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
	{
		//Same as calling CellFit directly
		$this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,0,1);
	}

	//Patch to also work with CJK double-byte text
	function MBGetStringLength($s)
	{
		if($this->CurrentFont['type']=='Type0')
		{
			$len = 0;
			$nbbytes = strlen($s);
			for ($i = 0; $i < $nbbytes; $i++)
			{
				if (ord($s[$i])<128)
					$len++;
				else
				{
					$len++;
					$i++;
				}
			}
			return $len;
		}
		else
			return strlen($s);
	}



	// --------------------------------------------------------------------------
	function Header() {
		
		$pdf_header = "";
		//$this->SetFont('Arial','',12);
		//$this->SetY(10);
	  //$this->Cell(0,10,$pdf_header,0,0,'C');	
	  //$this->Ln(20);
				
	}


	// --------------------------------------------------------------------------
	function Footer() {
	
		$pdf_footer = "";
		//$this->SetY(-15);
		//$this->SetFont('Arial','',12);
		//$this->Cell(0,10,$pdf_footer,0,0,'C');
		
	}

	// --------------------------------------------------------------------------
	function PrintChapterTitle($label) {
    		$this->SetFont('Arial','',12);
    		$this->SetFillColor(0,0,0);
    		$this->SetTextColor(255,255,255);
    		$this->Cell(190,7,$label,0,1,'L',1);
    		$this->Ln(0);
    		$this->SetTextColor(0,0,0);
    		$this->SetFont('Arial','',9);
	}

	// --------------------------------------------------------------------------
	function SetDash($black=false,$white=false)
    {
        if($black and $white)
            $s=sprintf('[%.3f %.3f] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }

	// --------------------------------------------------------------------------
	function PrintRow($width,$data,$fontsize) {

		// $data[0] = Label
		// $data[1] = Data
		// $data[2] = Fill
		// $data[3] = Position;

    for($i=0;$i<count($data);$i++) {

			$this->SetFont('Arial','',$fontsize);

			if ($data[$i][2]=="Fill") {
				$this->SetFillColor(221,221,221);
			} else {
				$this->SetFillColor(255,255,255);
			}
			$position = $data[$i][3];

			$this->SetFont('Arial','B',$fontsize);

			if ($data[$i][0]=="") {
				$label_width = 1;
				$this->LabelCell($label_width,6,$data[$i][0],0,0,'$position',1);
			} else {
				$label_width = $this-> GetStringWidth($data[$i][0])+2;
				$this->LabelCell($label_width,6,$data[$i][0],0,0,'$position',1);
			}


			$this->SetFont('Arial','B',$fontsize);
			$data_width = $width[$i]-$label_width;
			$this->CellFitScale($data_width,6,$data[$i][1],0,0,'$position',1);

		}

		$this->Ln();

	}

	// --------------------------------------------------------------------------
	function PrintSummaryRow2($label,$data,$leftb) {

		if ($label!="") {
			$this->SetFont('Arial','B',9);
			$label_width = $this-> GetStringWidth($label)+2;
			$this->LabelCell($label_width,7,$label,0,0,$position,1);
		} else {
			$label_width=0;
		}

		$this->SetFont('Arial','',9);
		$data_width = 190-$label_width;
		$this->MultiCell($data_width,7,$data,1,'L',0,$leftb);

	}

	// --------------------------------------------------------------------------
	function PrintSummaryRow($label,$data,$leftb) {

		if ($label!="") {
			$this->SetFont('Arial','B',9);
			$this->Cell(190,6,$label,'LRT',0,'L');
		}

    $this->SetFont('Arial','',9);
		$this->MultiCell(190,6,$data,'LRB','L',0,0);

	}

	// --------------------------------------------------------------------------
	function WriteHTML($html)
	{
	    //HTML parser
	    $html=str_replace("\n",' ',$html);
	    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	    foreach($a as $i=>$e)
	    {
	        if($i%2==0)
	        {
	            //Text
	            if($this->HREF)
	                $this->PutLink($this->HREF,$e);
	            else
	                $this->Write(5,$e);
	        }
	        else
	        {
	            //Tag
	            if($e{0}=='/')
	                $this->CloseTag(strtoupper(substr($e,1)));
	            else
	            {
	                //Extract attributes
	                $a2=explode(' ',$e);
	                $tag=strtoupper(array_shift($a2));
	                $attr=array();
	                foreach($a2 as $v)
	                    if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
	                        $attr[strtoupper($a3[1])]=$a3[2];
	                $this->OpenTag($tag,$attr);
	            }
	        }
	    }
	}
	
	// --------------------------------------------------------------------------
	function OpenTag($tag,$attr)
	{
	    //Opening tag
	    if($tag=='B' or $tag=='I' or $tag=='U')
	        $this->SetStyle($tag,true);
	    if($tag=='A')
	        $this->HREF=$attr['HREF'];
	    if($tag=='BR')
	        $this->Ln(5);
	}
	
	// --------------------------------------------------------------------------
	function CloseTag($tag)
	{
	    //Closing tag
	    if($tag=='B' or $tag=='I' or $tag=='U')
	        $this->SetStyle($tag,false);
	    if($tag=='A')
	        $this->HREF='';
	}
	
	// --------------------------------------------------------------------------
	function SetStyle($tag,$enable)
	{
	    //Modify style and select corresponding font
	    $this->$tag+=($enable ? 1 : -1);
	    $style='';
	    foreach(array('B','I','U') as $s)
	        if($this->$s>0)
	            $style.=$s;
	    $this->SetFont('',$style);
	}
	
	// --------------------------------------------------------------------------
	function PutLink($URL,$txt)
	{
	    //Put a hyperlink
	    $this->SetTextColor(0,0,255);
	    $this->SetStyle('U',true);
	    $this->Write(5,$txt,$URL);
	    $this->SetStyle('U',false);
	    $this->SetTextColor(0);
	}

	// --------------------------------------------------------------------------
	

}

?>