<?php

if (empty($totalname)) $totalname = " records";
$navstring = "&sort=$sort&report=$report&categoryid=$categoryid";

echo "<table width=100% border=0 class=gentext><tr valign=\"top\"><td width=60%><span class=left>";

if ($start==0) {
	$dispstart = 1;
} else {
	$dispstart = $start+1;
}

if ($total_count < $limit) {
	echo $dispstart." to ".$total_count." of ".$total_count." ".$totalname;
} else {
	if ($start==1) {
		echo "1 to ".$limit." of ".$total_count." ".$totalname;
	} else {
		if ($num_results<$limit) {
			echo $dispstart." to ".($num_results+$start)." of ".$total_count." ".$totalname;
		} else {
			echo $dispstart." to ".($limit + $start)." of ".$total_count." ".$totalname;				
		}
	}
}

echo "</span></td><td class=right align=\"right\"><span class=\"right\"><font class=gentext>";

$prev = $start-$limit;
if ($prev<0) {
	$prev=0;
	echo "prev | ";
} else {
	echo "<a href=\"".$PHP_SELF."?start=$prev$navstring\" title=\"Previous\">prev</a> | ";
}

$next = $start+$limit;

if ($num_results<$limit) {
	echo "next";
} else {
	echo "<a href=\"".$PHP_SELF."?start=$next$navstring\" title=\"Next\">next</a>";
}


//  Build Jump To Page select

if ($total_count>$limit) {

	$x = $total_count/$limit;
	$r = round($x);
	$d = ($r-$x);
		
	if ($d>0.5){
		$x = ($r+1);
	}
			
	echo "&nbsp;&nbsp;<select onchange=\"document.location=this.options[this.selectedIndex].value\">";
	
	for ($c=0; $c<=$x; $c++) {
	
		$option_start = ((($c+1)*$limit)-$limit);
				
		if($option_start<=0) $option_start = 0;
		$option_value = $PHP_SELF."?start=".$option_start."$navstring";
				
		if (($start/$limit)<$c) {
			echo "<option value=\"".$option_value."\">Page ".($c+1)."</option>";
		} else {
			echo "<option value=\"".$option_value."\" selected>Page ".($c+1)."</option>";
		}
			
	}
	
	
	
	echo "</select>";
	
}


echo "</font></span></td></tr></table><br>";

?>