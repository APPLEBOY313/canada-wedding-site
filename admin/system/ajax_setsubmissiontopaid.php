<?php

include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/functions.php");

$submissionid = $_GET['submissionid'];

if (!empty($submissionid)) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE submission SET status='Paid' WHERE submissionid=:submissionid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':submissionid', $submissionid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	header("Location:ajax_sendemail.php?submissionid=".$submissionid);
	exit();
	
	echo "OK";
	
} else {
	
	echo "Error";	
		
}

?>