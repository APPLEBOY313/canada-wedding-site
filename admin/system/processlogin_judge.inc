<?php

if (!empty($_POST['login_username']) && !empty($_POST['login_pwd'])) {
	
	try
	{ 
			$email = safe($_POST['login_username']);
			$loginpwd = safe($_POST['login_pwd']);
      
      $dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
      
      $q = "SELECT judgeid, namefirst, namelast, email FROM judge WHERE email=:email AND loginpwd=:loginpwd";
      $sql = $dbh->prepare($q);      
      $sql->bindParam(':email', $loginid);
      $sql->bindParam(':loginpwd', $loginpwd);
      
      $sql->execute();
      $login_row = $sql->fetch();
            
      if ($login_row) {
		
				$ipaddress = $_SERVER['REMOTE_ADDR'];
					
				// Verify security for user...
				$_SESSION['s_userid'] = $login_row["judgeid"];
				$_SESSION['s_fullname'] = stripslashes($login_row["namefirst"]." ".$login_row["namelast"]);
				$_SESSION['s_email'] = $login_row["email"];
				$_SESSION['s_role'] = "Judge";
				$_SESSION['s_categories'] = GetCategoriesForJudge($login_row["judgeid"]);
																			
				// Update last login date/time
				$login_query = "UPDATE judge SET lastlogin=NOW(), ipaddress=:ipaddress WHERE judgeid=:judgeid";
				$usql = $dbh->prepare($login_query);
      	$usql->bindParam(':judgeid', $login_row["judgeid"]);   
      	$usql->bindParam(':ipaddress', $ipaddress);   
      	$usql->execute();
				
				// Insert into history
				$history_query = "INSERT INTO loginhistory_judge (created, judgeid, logintime, ipaddress) VALUES (NOW(), :judgeid, NOW(), :ipaddress)";
				$isql = $dbh->prepare($history_query);
      	$isql->bindParam(':judgeid', $login_row["judgeid"]);   
      	$isql->bindParam(':ipaddress', $ipaddress);   
      	$isql->execute();
						
				header("Location:index_judge.php?report=tobejudged");
				exit();				
				
			} else {
				
				session_destroy();
				
			}
      
	                 
      $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    echo $e->getMessage();
    die('Error establishing connection with database');
  }
  
	$loginerror = "Login failed";

}

?>