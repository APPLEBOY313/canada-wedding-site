<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deleteadminuser'])) {
	
	try
	{ 
			$loginid = safe($_POST['login_username']);
			$loginpwd = safe($_POST['login_pwd']);
      
      $dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
      
      $q = "UPDATE adminuser SET isdeleted=1 WHERE adminuserid=:adminuserid";
      
      $usql = $dbh->prepare($q);
      $usql->bindParam(':adminuserid', $recordid);    
      $usql->execute();
      
      header("Location:adminuser_summary.php?m=User%20deleted");
			exit();     
	                 
      $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    echo $e->getMessage();
    die('Error establishing connection with database');
  }
  	
} else {

	$namefirst = $_POST['namefirst'];
	$namelast = $_POST['namelast'];
	$emailaddress = $_POST['emailaddress'];
	$loginid = $_POST['loginid'];
	$newpwd = $_POST['newpwd'];
	$newpwd2 = $_POST['newpwd2'];	
	$role = $_POST['role'];
	$isactive = $_POST['isactive'];
	
	// These fields cannot be blank
	$mfields[0] = $namefirst;
	$mfields[1] = $namelast;
	$mfields[2] = $emailaddress;
	$mfields[3] = $loginid;
	
	if (empty($recordid)) {
		$mfields[4] = $newpwd;
	}
				
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	// Check for password change
	if (!empty($newpwd)) {
		
		if (strlen($newpwd)<6) {
			if (empty($errortext)) $errortext = "Password needs to be at least 6 characters";
			
		} else {
			if ($newpwd!=$newpwd2) {
				if (empty($errortext)) $errortext = "Passwords do not match";
			}
		}
		
	}
	
	if (empty($errortext)) {
		
		// Escape fields
		$namefirst = addslashes($namefirst);
		$namelast = addslashes($namelast);
		$emailaddress = addslashes($emailaddress);
		$loginid = addslashes($loginid);
		$company = addslashes($company);

		if (!empty($isactive)) {
			$isactive = 1;
		} else {
			$isactive = 0;
		}
				
		if (empty($recordid)) {
			
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		      
		      $q = "INSERT INTO adminuser (created, namefirst, namelast, email, loginid, loginpwd, role, isactive) 
		      	VALUES (NOW(), :namefirst, :namelast, :emailaddress, :loginid, :newpwd, :role, :isactive) ";
		      		      
		      $isql = $dbh->prepare($q);
		      $isql->bindParam(':namefirst', $namefirst);
		      $isql->bindParam(':namelast', $namelast);
		      $isql->bindParam(':emailaddress', $emailaddress);
		      $isql->bindParam(':loginid', $loginid);
		      $isql->bindParam(':newpwd', $newpwd);
		      $isql->bindParam(':role', $role);
		      $isql->bindParam(':isactive', $isactive);
		      
		      $isql->execute();
		      
		      header("Location:adminuser_summary.php?m=User%20added");
					exit();     
			                 
		      $dbh = null;
		  }
		  catch(PDOException $e){
		    error_log('PDOException - ' . $e->getMessage(), 0);
		    http_response_code(500);
		    echo $e->getMessage();
		    die('Error establishing connection with database');
		  }
			
			header("Location:adminuser_summary.php?m=User%20added");
			exit();
													
		} else {
			
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE adminuser SET namefirst=:namefirst, namelast=:namelast, email=:email, loginid=:loginid, 
			    	isactive=:isactive WHERE adminuserid=:adminuserid";
			    
			    if ($_SESSION['s_role']=="Administrator") {
						$q.= ", role=:role ";
					}
					
					if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
						if (!empty($newpwd)) {
							$q.= ", loginpwd=:newpwd ";
						}
					}
			    
			    $sql = $dbh->prepare($q);
			    $sql->bindParam(':namefirst', $namefirst);
		      $sql->bindParam(':namelast', $namelast);
		      $sql->bindParam(':email', $emailaddress);
		      $sql->bindParam(':loginid', $loginid);
		      $sql->bindParam(':isactive', $isactive);
		      $sql->bindParam(':adminuserid', $recordid);
		      
		      if ($_SESSION['s_role']=="Administrator") {
						$sql->bindParam(':role', $role);
					}
					
					if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
						if (!empty($newpwd)) {
							$sql->bindParam(':loginpwd', $newpwd);
						}
					}
		      
			    $sql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
			
			header("Location:adminuser_summary.php?m=User%20updated");
			exit();
			
		}
		
	}
	
}


?>