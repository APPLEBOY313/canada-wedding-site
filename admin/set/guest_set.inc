<?php

$guestname = $_POST['guestname'];
$companyname = $_POST['companyname'];

// These fields cannot be blank
$mfields[0] = $guestname;
$mfields[1] = $companyname;

for ($n=0; $n<count($mfields); $n++) {
	if ($mfields[$n]=="") {
		$errortext = "Please completed both name and company";
	}
}

// Check for duplicates in Guest table
if (empty($errortext)) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "SELECT guestid FROM guest WHERE guestname:guestname AND companyname=:companyname AND isdeleted=0 AND isarchived=0";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':guestname', $guestname);
	    $sql->bindParam(':companyname', $companyname);
	    $sql->execute();
	    
	    $row = $sql->fetch();
			if ($row) {
				if ($row["guestid"]>0) {
					$errortext = "That name is already in this Guest List";
				}
			}                 
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
}

// Check for duplicates in RSVP table
//if (empty($errortext)) {
//	$sql = "SELECT rsvpid FROM rsvp WHERE attendeenames LIKE '%".$guestname."%' AND companyname='$companyname' AND isdeleted=0 AND isarchived=0";
//	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
//	$row = mysql_fetch_array($result);
//	
//	if ($row) {
//		if ($row["rsvpid"]>0) {
//			$errortext = "That name is already in this Guest List";
//		}
//	}
//}

// Check for duplicates in Form_Gala table
//if (empty($errortext)) {
//	$sql = "SELECT galaid FROM form_gala WHERE attendeenames LIKE '%".$guestname."%' AND company='$companyname' AND isdeleted=0 AND paypalconfirm=1 AND isarchived=0";
//	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
//	$row = mysql_fetch_array($result);
//	
//	if ($row) {
//		if ($row["galaid"]>0) {
//			$errortext = "That name is already in this Guest List";
//		}
//	}
//}


if (empty($errortext)) {
			
	// Escape fields
	$guestname = addslashes($guestname);
	$companyname = addslashes($companyname);
						
	// Insert Guest
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "INSERT INTO guest (created, guestname, companyname) VALUES (NOW(), :guestname, :companyname) ";
	    
	    $isql = $dbh->prepare($q);
	    $isql->bindParam(':guestname', $guestname);
	    $isql->bindParam(':companyname', $companyname);
	    $isql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}

	// Remove one from Gala Seats Available
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE settings SET galaseatsavailable=galaseatsavailable-1 WHERE settingsid=1";
	    
	    $usql = $dbh->prepare($q);
	    $usql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
												
}

?>