<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deletejudge'])) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE judge SET isdeleted=1 WHERE judgeid=:recordid";
	    
	    $dsql = $dbh->prepare($q);
	    $dsql->bindParam(':recordid', $recordid);
	    $dsql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
			
	header("Location:judge_summary.php?m=User%20deleted");
	exit();
	
} else {

	$namefirst = $_POST['namefirst'];
	$namelast = $_POST['namelast'];
	$emailaddress = $_POST['emailaddress'];
	$newpwd = $_POST['newpwd'];
	$newpwd2 = $_POST['newpwd2'];	
	
	// These fields cannot be blank
	$mfields[0] = $namefirst;
	$mfields[1] = $namelast;
	$mfields[2] = $emailaddress;
		
	if (empty($recordid)) {
		$mfields[3] = $newpwd;
	}
			
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	// Check for password change
	if (!empty($newpwd)) {
		
		if (strlen($newpwd)<6) {
			if (empty($errortext)) $errortext = "Password needs to be at least 6 characters";
			
		} else {
			if ($newpwd!=$newpwd2) {
				if (empty($errortext)) $errortext = "Passwords do not match";
			}
		}
		
	}
	
	if (empty($errortext)) {
		
		// Escape fields
		$namefirst = addslashes($namefirst);
		$namelast = addslashes($namelast);
		$emailaddress = addslashes($emailaddress);
				
		if (empty($recordid)) {
			
			// Insert
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "INSERT INTO judge (created, namefirst, namelast, email, loginpwd) 
						VALUES (NOW(), :namefirst, :namelast, :emailaddress, :newpwd) ";
						    
			    $isql = $dbh->prepare($q);
			    $isql->bindParam(':namefirst', $namefirst);
			    $isql->bindParam(':namelast', $namelast);
			    $isql->bindParam(':emailaddress', $emailaddress);
			    $isql->bindParam(':newpwd', $newpwd);
			    $isql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
						
			header("Location:judge_summary.php?m=User%20added");
			exit();
													
		} else {
			
			// Update
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE judge SET namefirst=:namefirst, namelast=:namelast, email=:emailaddress ";
				
					if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
						if (!empty($newpwd)) {
							$sql.= ", loginpwd=:newpwd ";
						}
					}
					
					$q.= "WHERE judgeid=:recordid";
							    
			    $usql = $dbh->prepare($q);
			    $usql->bindParam(':namefirst', $namefirst);
			    $usql->bindParam(':namelast', $namelast);
			    $usql->bindParam(':emailaddress', $emailaddress);
			    $usql->bindParam(':recordid', $recordid);
			    
			    if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
						if (!empty($newpwd)) {
							$usql->bindParam(':newpwd', $newpwd);
						}
					}
			    
			    $usql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
						
			header("Location:judge_summary.php?m=User%20updated");
			exit();
			
		}
		
	}
	
}


?>