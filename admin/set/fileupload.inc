<?

if ($submissionid>0) {

	// Get Category Code
	$code = getField("code", "category", "categoryid=$categoryid");

	// Set foldername
	$uploadfolder = _MY_FILE_V_.$submissionid."/";
	$thumbfolder = $uploadfolder."thumbs/";

	// Handle Deletes
	if (!empty($_POST['delete_1'])) $delete_arr[count($delete_arr)] = $_POST['delete_1'];
	if (!empty($_POST['delete_2'])) $delete_arr[count($delete_arr)] = $_POST['delete_2'];
	if (!empty($_POST['delete_3'])) $delete_arr[count($delete_arr)] = $_POST['delete_3'];
	if (!empty($_POST['delete_4'])) $delete_arr[count($delete_arr)] = $_POST['delete_4'];
	if (!empty($_POST['delete_5'])) $delete_arr[count($delete_arr)] = $_POST['delete_5'];
	if (!empty($_POST['delete_6'])) $delete_arr[count($delete_arr)] = $_POST['delete_6'];
	if (!empty($_POST['delete_7'])) $delete_arr[count($delete_arr)] = $_POST['delete_7'];
	if (!empty($_POST['delete_8'])) $delete_arr[count($delete_arr)] = $_POST['delete_8'];
	if (!empty($_POST['delete_9'])) $delete_arr[count($delete_arr)] = $_POST['delete_9'];
	if (!empty($_POST['delete_10'])) $delete_arr[count($delete_arr)] = $_POST['delete_10'];
	if (!empty($_POST['delete_11'])) $delete_arr[count($delete_arr)] = $_POST['delete_11'];
	if (!empty($_POST['delete_12'])) $delete_arr[count($delete_arr)] = $_POST['delete_12'];
		
	// Delete any files?
	for ($i=0; $i<count($delete_arr);$i++) {
		
		$filetodelete1 = $uploadfolder.$delete_arr[$i];
		$filetodelete2 = $thumbfolder.str_replace(".jpg", "_thumb.jpg", strtolower($delete_arr[$i]));
		
		if (file_exists($filetodelete1)) unlink($filetodelete1); 
		if (file_exists($filetodelete2)) unlink($filetodelete2); 
		
	}
	
	/*** agreed width of the thumbnail ***/
	$thumbWidth = 250;
	
	/*** the maximum filesize from php.ini ***/
  $ini_max = str_replace('M', '', ini_get('upload_max_filesize'));
  $upload_max = $ini_max * 1048576;

	/*** maximum filesize allowed in bytes ***/
  $max_file_size  = 1048576;

	
	// Upload files
	for($i=0; $i<count($_FILES['fileupload']['tmp_name']);$i++) {
		
		// check if there is a file in the array
    if(!is_uploaded_file($_FILES['fileupload']['tmp_name'][$i]))
    {
        //$messages[] = 'No file uploaded';
    }
    /*** check if the file is less then the max php.ini size ***/
    elseif($_FILES['fileupload']['size'][$i] > $upload_max)
    {
    		$errortext.= "File #".($i+1)." size exceeds server limit<br />";
    }
    // check the file is less than the maximum file size
    elseif($_FILES['fileupload']['size'][$i] > $max_file_size)
    {
        $errortext.= "File #".($i+1)." size exceeds 1Mb limit<br />";
    }
    // check the file is JPG
    elseif($_FILES['fileupload']['type'][$i] != "image/pjpeg" && $_FILES['fileupload']['type'][$i] != "image/jpeg")
    {
        $errortext.= "File #".($i+1)." is not a JPG file<br />";
    }
    else
    {
        // Does directory exist?
				createUploadFolder($uploadfolder);
				createUploadFolder($thumbfolder);
						
				$tmp_name = $_FILES["fileupload"]["tmp_name"];
		    $name = $_FILES["fileupload"]["name"];
		    $filesize = $_FILES["fileupload"]["size"];
				$mimetype = $_FILES["fileupload"]["type"];
				
				if (!empty($code) && !empty($phone)) {
					$bcfilename = $code."_".substr($phone,-4)."_".($i+1).".jpg";
				} else {
					$bcfilename = "Image".($i+1).".jpg";
				}
				
				$bcthumbfilename = str_replace(".jpg", "_thumb.jpg", strtolower($bcfilename));		
								
				// Move original
		  	$uploadfilename = strtolower($uploadfolder.$bcfilename);		
		  	$thumbfilename = strtolower($thumbfolder.$bcthumbfilename);		
		  	
        if(@copy($_FILES['fileupload']['tmp_name'][$i],$uploadfilename))
        {                        
            // Success...Create thumbnail
            chmod($uploadfilename, 0777);
                    		    
            // load image and get image size
			      $img = imagecreatefromjpeg($uploadfilename);
			      $width = imagesx($img);
			      $height = imagesy($img);
			
			      // calculate thumbnail size
			      $new_width = $thumbWidth;
			      $new_height = floor($height * ($thumbWidth/$width));
			
			      // create a new temporary image
			      $tmp_img = imagecreatetruecolor($new_width, $new_height);
			
			      // copy and resize old image into new image 
			      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			
			      // save thumbnail into a file
			      imagejpeg($tmp_img, $thumbfilename);
			      chmod($thumbfilename, 0777);      
            
        }
        else
        {
            /*** an error message ***/
            $errortext.= "File #".($i+1)." failed to upload (unknown reason)";
        }
    }
    
	}

	
}

?>