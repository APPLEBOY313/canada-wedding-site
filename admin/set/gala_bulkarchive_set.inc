<?php

// Bulk process Gala Sales Archive or Active
$galaids = $_POST['chkEntry'];

for ($a=0; $a<count($galaids); $a++) {

	$gid = $galaids[$a];

	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    if ($bulk_archive) {
				$q = "UPDATE form_gala SET isarchived=1 WHERE galaid=:gid";
			} else if (!$bulk_archive) {
				$q = "UPDATE form_gala SET isarchived=0 WHERE galaid=:gid";
			}
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':gid', $gid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
}

?>