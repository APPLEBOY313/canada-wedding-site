<?php

// Bulk process submissions Archive or Active
$submissionids = $_POST['chkEntry'];

for ($a=0; $a<count($submissionids); $a++) {

	$subid = $submissionids[$a];
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	
			if ($bulk_archive) {
				$q = "UPDATE submission SET isarchived=1 WHERE submissionid=:subid";
			} else if (!$bulk_archive) {
				$q = "UPDATE submission SET isarchived=0 WHERE submissionid=:subid";
			}
			    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':subid', $subid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	
}

?>