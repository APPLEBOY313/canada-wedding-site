<?php

// Bulk process submissions Archive or Active
$submissionids = $_POST['chkEntry'];

// Load subject, body for confirmation email
$subject = stripslashes(getField("confsubject", "settings", "settingsid=1"));
$body = stripslashes(getField("confbody", "settings", "settingsid=1"));

for ($a=0; $a<count($submissionids); $a++) {

	$subid = $submissionids[$a];

	// Send confirmation email
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "SELECT s.email, s.contactperson, cat.title AS category 
				FROM submission s 
				INNER JOIN category cat ON (cat.categoryid = s.categoryid) 
				WHERE s.submissionid=:subid AND s.status='Pending' ";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':subid', $subid);
	    $sql->execute();
	    
	    $result = $sql->fetch();
	    
	    if ($result)
	    {
	    	// Update to Paid
				try
				{ 
						$dbh2 = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
						$dbh2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
				    
				    $q2 = "UPDATE submission SET status='Paid', emailsent=now() WHERE submissionid=:subid";
				    
				    $sql2 = $dbh2->prepare($q2);
				    $sql2->bindParam(':subid', $subid);
				    $sql2->execute();
				                 
				    $dbh2 = null;
				}
				catch(PDOException $e){
				  error_log('PDOException - ' . $e->getMessage(), 0);
				  http_response_code(500);
				  echo $e->getMessage();
				  die('Error establishing connection with database');
				}
								
				$sendto = $row["email"];
				$contactperson = stripslashes($row["contactperson"]);
				$category = stripslashes($row["category"]);
				
				$mailbody = "Dear ".$contactperson."<br><br>Regarding your recent submission into the ".$category." category:<br><br>".$body;
				$mailbody.= "<br><br>Professional BC Wedding Awards<br>http://www.bcweddingawards.com";
				
				// ----------------------------------------------------------------		
				// Send Email
				// ----------------------------------------------------------------
				$mail = new PHPMailer();
				$mail->From = "submissions@bcweddingawards.com";
				$mail->FromName = "Professional BC Wedding Awards";
				$mail->Host = "localhost.localdomain";
				$mail->Mailer = "mail";
				$mail->Subject = $subject;
				$mail->Body = $mailbody;
				$mail->IsHTML(true); 
				$mail->AddAddress($sendto, $contactperson);	
				$mail->AddBCC("darryl.wing@gmail.com", "Darryl Wing");	
				$mail->Send();
				$mail->ClearAddresses();			
	    }
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
}

?>