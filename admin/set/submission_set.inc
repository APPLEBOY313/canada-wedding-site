<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deletesubmission'])) {
			
	// Delete submission
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE submission SET isdeleted=1 WHERE submissionid=:recordid";
	    
	    $dsql = $dbh->prepare($q);
	    $dsql->bindParam(':recordid', $recordid);
	    $dsql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
		
	header("Location:index.php");
	exit();
	
} else {

	$status = $_POST['status'];
	$categoryid = $_POST['categoryid'];
	$companyname = $_POST['companyname'];
	$contactperson = $_POST['contactperson'];
	$email = $_POST['email'];
	$url = $_POST['url'];
	
	$phone1 = $_POST['phone1'];
	$phone2 = $_POST['phone2'];
	$phone3 = $_POST['phone3'];
	$phone = $phone1.$phone2.$phone3;
		
	$address1 = $_POST['address1'];
	$address2 = $_POST['address2'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$postalcode = $_POST['postalcode'];
	$country = $_POST['country'];
	$howlong = $_POST['howlong'];
	$companydescription = $_POST['companydescription'];
	$otherinfo = $_POST['otherinfo'];
	$nominee = $_POST['nominee'];
	$imagedescription = $_POST['imagedescription'];
	$videourl = $_POST['videourl'];
	$photographername = $_POST['photographername'];
					
	// These fields cannot be blank
	$mfields[0] = $categoryid;
				
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	if (empty($errortext)) {
				
		// Escape fields
		$companyname = addslashes($companyname);
		$contactperson = addslashes($contactperson);
		$email = addslashes($email);
		$url = addslashes($url);
		$address1 = addslashes($address1);
		$address2 = addslashes($address2);
		$city = addslashes($city);
		$state = addslashes($state);
		$postalcode = addslashes($postalcode);
		$country = addslashes($country);
		$howlong = addslashes($howlong);
		$companydescription = addslashes($companydescription);
		$otherinfo = addslashes($otherinfo);
		$nominee = addslashes($nominee);
		$imagedescription = addslashes($imagedescription);
		$videourl = addslashes($videourl);
		$photographername = addslashes($photographername);
						
		if (empty($recordid)) {

			// Set Submission Year
			$subyear = getSubmissionYear(date("Y-m-d"));
									
			// Insert
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "INSERT INTO submission (created, categoryid, status, companyname, contactperson, 
									email, url, phone, address1, address2, city, state, postalcode, country, howlong,
									companydescription, otherinfo, nominee, imagedescription, subyear, videourl, photographername) 
								VALUES (NOW(), 
									:categoryid, 
									:status, 
									:companyname, 
									:contactperson, 
									:email, 
									:url, 
									:phone, 
									:address1, 
									:address2, 
									:city, 
									:state, 
									:postalcode, 
									:country, 
									:howlong, 
									:companydescription, 
									:otherinfo, 
									:nominee, 
									:imagedescription, 
									:subyear, 
									:videourl, 
									:photographername) ";			
								
			    $sql = $dbh->prepare($q);
			    $sql->bindParam(':categoryid', $categoryid);
			    $sql->bindParam(':status', $status);
			    $sql->bindParam(':companyname', $companyname);
			    $sql->bindParam(':contactperson', $contactperson);
			    $sql->bindParam(':email', $email);
			    $sql->bindParam(':url', $url);
			    $sql->bindParam(':phone', $phone);
			    $sql->bindParam(':address1', $address1);
			    $sql->bindParam(':address2', $address2);
			    $sql->bindParam(':city', $city);
			    $sql->bindParam(':state', $state);
			    $sql->bindParam(':postalcode', $postalcode);
			    $sql->bindParam(':country', $country);
			    $sql->bindParam(':howlong', $howlong);
			    $sql->bindParam(':companydescription', $companydescription);
			    $sql->bindParam(':otherinfo', $otherinfo);
			    $sql->bindParam(':nominee', $nominee);
			    $sql->bindParam(':imagedescription', $imagedescription);
			    $sql->bindParam(':subyear', $subyear);
			    $sql->bindParam(':videourl', $videourl);
			    $sql->bindParam(':photographername', $photographername);
			    
			    $sql->execute();
					$dbh = null;
					
					$submissionid = $dbh->lastInsertId();
					
					// Handle file upload/deletes
					include(_MY_BASE_."set/fileupload.inc");
					
					if (empty($errortext)) {			
						header("Location:submission_detail.php?id=".$submissionid."&idcoll=".$idcoll);						
						exit();
					}
					  
			    
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
	
													
		} else {
			
			// Update
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE submission SET 
							status=:status, 
							categoryid=:categoryid, 
							companyname=:companyname, 
							contactperson=:contactperson, 
							email=:email, 
							url=:url, 
							phone=:phone, 
							address1=:address1, 
							address2=:address2, 
							city=:city, 
							state=:state, 
							postalcode=:postalcode, 
							country=:country, 			
							howlong=:howlong, 
							companydescription=:companydescription, 
							otherinfo=:otherinfo, 
							nominee=:nominee, 
							imagedescription=:imagedescription, 
							videourl=:videourl, 
							photographername=:photographername 
						WHERE submissionid=:recordid";			
			    
			    $sql = $dbh->prepare($q);
			    $sql->bindParam(':categoryid', $categoryid);
			    $sql->bindParam(':status', $status);
			    $sql->bindParam(':companyname', $companyname);
			    $sql->bindParam(':contactperson', $contactperson);
			    $sql->bindParam(':email', $email);
			    $sql->bindParam(':url', $url);
			    $sql->bindParam(':phone', $phone);
			    $sql->bindParam(':address1', $address1);
			    $sql->bindParam(':address2', $address2);
			    $sql->bindParam(':city', $city);
			    $sql->bindParam(':state', $state);
			    $sql->bindParam(':postalcode', $postalcode);
			    $sql->bindParam(':country', $country);
			    $sql->bindParam(':howlong', $howlong);
			    $sql->bindParam(':companydescription', $companydescription);
			    $sql->bindParam(':otherinfo', $otherinfo);
			    $sql->bindParam(':nominee', $nominee);
			    $sql->bindParam(':imagedescription', $imagedescription);
			    $sql->bindParam(':videourl', $videourl);
			    $sql->bindParam(':photographername', $photographername);
			    $sql->bindParam(':recordid', $recordid);
			    $sql->execute();
			                 
			    $dbh = null;
			    
			    // Handle file upload/deletes
					$submissionid = $recordid;
					include(_MY_BASE_."set/fileupload.inc");
						
					if (empty($errortext)) {			
						header("Location:submission_detail.php?id=".$recordid."&idcoll=".$idcoll);							
						exit();
					}
			    
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
			
		}
		
	}
	
}


?>