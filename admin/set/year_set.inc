<?

$recordid = $_POST['recordid'];
		
// Format Start Date
$date1 = explode("/", $_POST['startdate']);
$startdate = $date1[2]."-".$date1[0]."-".$date1[1];		

// Format End Date
$date1 = explode("/", $_POST['enddate']);
$enddate = $date1[2]."-".$date1[0]."-".$date1[1];		
		
// Format Judging Start Date
$date1 = explode("/", $_POST['judgingstart']);
$judgingstart = $date1[2]."-".$date1[0]."-".$date1[1];		

// Format Judging End Date
$date1 = explode("/", $_POST['judgingend']);
$judgingend = $date1[2]."-".$date1[0]."-".$date1[1];				
		
if (!empty($recordid)) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE submissionyear SET 
	    	startdate=:startdate, 
	    	enddate=:enddate, 
				judgingstart=:judgingstart, 
				judgingend=:judgingend 
				WHERE subyear=:recordid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':startdate', $startdate);
	    $sql->bindParam(':enddate', $enddate);
	    $sql->bindParam(':judgingstart', $judgingstart);
	    $sql->bindParam(':judgingend', $judgingend);
	    $sql->bindParam(':recordid', $recordid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	header("Location:year_summary.php?m=Year%20updated");
	exit();
	
}

?>