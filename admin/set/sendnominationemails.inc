<?

// Get subject and body
$subject = stripslashes(getField("nomsubject", "settings", "settingsid=1"));
$bodypart = stripslashes(getField("nombody", "settings", "settingsid=1"));
$sendfrom = "info@bcweddingawards.com";

$goodsendcounter = 0;
$badsendcounter = 0;

for ($i=0; $i<count($_POST['nominationid']); $i++) {
	
	$id = $_POST['nominationid'][$i];
	if ($id>0) {

		$companyname = stripslashes(getField("companyname", "form_nomination", "nominationid=".$id));
		$contactemail = stripslashes(getField("emailcontact", "form_nomination", "nominationid=".$id));

		if (!empty($companyname) && !empty($contactemail)) {
			
			$body = "Dear ".$companyname.",\n\n";
			$body.= $bodypart;
			$body.= "\n\nProfessional BC Wedding Awards\nhttp://www.bcweddingawards.com";
			
			$mail = new PHPMailer();
			$mail->From = $sendfrom;
			$mail->FromName = "BCWA Nominations";
			$mail->Host = "localhost.localdomain";
			$mail->Mailer = "mail";
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->IsHTML(false); 
			//$mail->AddAddress("darryl.wing@gmail.com");	
			$mail->AddAddress($contactemail);	
			$mail->Send();
			$mail->ClearAddresses();			
			
			// Update Nomination
			$usql = "UPDATE form_nomination SET emailsenddate=now(), emailsendby=".$_SESSION['s_userid'];
			$usql.= " WHERE nominationid=".$id;
			mysql_query($usql) or die(mysql_error()."<br><br>".$usql);
			
			$goodsendcounter++;
						
		} else {
			$badsendcounter++;
			
		}
				
	}
	
}

?>