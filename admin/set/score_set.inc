<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deletescore'])) {
			
	// Delete score
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE score SET isdeleted=1 WHERE scoreid=:recordid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':recordid', $recordid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
		
	header("Location:index.php");
	exit();
	
} else {

	$judgeid = $_POST['judgeid'];
	$submissionid = $_POST['submissionid'];
	$rating_aesthetics = $_POST['rating_aesthetics'];
	$rating_themes = $_POST['rating_themes'];
	$rating_originality = $_POST['rating_originality'];
	$rating_professionalism = $_POST['rating_professionalism'];
	$rating_overallquality = $_POST['rating_overallquality'];	
	$comment = $_POST['comment'];
					
	// These fields cannot be blank
	$mfields[0] = $judgeid;
	$mfields[1] = $submissionid;
	$mfields[2] = $rating_aesthetics;
	$mfields[3] = $rating_themes;
	$mfields[4] = $rating_originality;
	$mfields[5] = $rating_professionalism;
	$mfields[6] = $rating_overallquality;
					
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	if (empty($errortext)) {
				
		// Escape fields
		$comment = addslashes($comment);
		
		// Calculate final score
		$overallscore = ($rating_aesthetics + $rating_themes + $rating_originality + $rating_professionalism + $rating_overallquality)*2;
						
		if (empty($recordid)) {
									
			// Insert
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "INSERT INTO score (created, judgeid, submissionid, rating_aesthetics, rating_themes, 
						rating_originality, rating_professionalism, rating_overallquality, comment, overallscore) 
						VALUES (NOW(), :judgeid, :submissionid, :rating_aesthetics, :rating_themes, 
						:rating_originality, :rating_professionalism, :rating_overallquality, :comment, :overallscore) ";
			    
			    $isql = $dbh->prepare($q);
			    $isql->bindParam(':judgeid', $judgeid);
			    $isql->bindParam(':submissionid', $submissionid);
			    $isql->bindParam(':rating_aesthetics', $rating_aesthetics);
			    $isql->bindParam(':rating_themes', $rating_themes);
			    $isql->bindParam(':rating_originality', $rating_originality);
			    $isql->bindParam(':rating_professionalism', $rating_professionalism);
			    $isql->bindParam(':rating_overallquality', $rating_overallquality);
			    $isql->bindParam(':comment', $comment);
			    $isql->bindParam(':overallscore', $overallscore);			    
			    $isql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
	
			//$scoreid = mysql_insert_id();
						
			header("Location:submission_detail.php?id=".$submissionid);		
			exit();
													
		} else {
			
			// Update
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE score SET 
						judgeid=:judgeid, 
						rating_aesthetics=:rating_aesthetics, 
						rating_themes=:rating_themes, 
						rating_originality=:rating_originality, 
						rating_professionalism=:rating_professionalism, 
						rating_overallquality=:rating_overallquality, 
						comment=:comment, 
						overallscore=:overallscore 
						WHERE scoreid=:recordid";			
			    
			    $uql = $dbh->prepare($q);
			    $usql->bindParam(':judgeid', $judgeid);
			    $usql->bindParam(':rating_aesthetics', $rating_aesthetics);
			    $usql->bindParam(':rating_themes', $rating_themes);
			    $usql->bindParam(':rating_originality', $rating_originality);
			    $usql->bindParam(':rating_professionalism', $rating_professionalism);
			    $usql->bindParam(':rating_overallquality', $rating_overallquality);
			    $usql->bindParam(':comment', $comment);
			    $usql->bindParam(':overallscore', $overallscore);			
			    $usql->bindParam(':recordid', $recordid);			
			    $usql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
			
			header("Location:submission_detail.php?id=".$submissionid);							
			exit();
			
		}
		
	}
	
}


?>