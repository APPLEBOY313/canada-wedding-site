<?

// Get RSVP subject and body
$subject = stripslashes(getField("rsvpsubject", "settings", "settingsid=1"));
$bodypart = nl2br(stripslashes(getField("rsvpbody", "settings", "settingsid=1")));

$goodsendcounter = 0;
$badsendcounter = 0;

for ($i=0; $i<count($_POST['chkEntry']); $i++) {
	
	$rsvpid = $_POST['chkEntry'][$i];
	
	if ($rsvpid>0) {

		// Get company name, email, permakey
		$query = "SELECT companyname, email, permakey FROM rsvp WHERE rsvpid=".$rsvpid;
		$result = mysql_query($query);
		$row = mysql_fetch_array($result) or die (mysql_error());

		if ($row) {
			
			$companyname = stripslashes($row["companyname"]);
			$email = stripslashes($row["email"]);
			$permalink = _MY_HREF_."rsvp.php?code=".$row["permakey"];
			
			if (!empty($companyname) && !empty($email) && !empty($permalink)) {
			
				$body = "Dear ".$companyname.",";
				$body.= "<br><br>";
				$body.= str_replace("#rsvp#","<a href=\"".$permalink."\">Click here to register your RSVP</a>",$bodypart);
																
				$mail = new PHPMailer();
				$mail->From = "info@bcweddingawards.com";
				$mail->FromName = "Professional BC Wedding Awards";
				$mail->Host = "localhost.localdomain";
				$mail->Mailer = "mail";
				$mail->Subject = $subject;
				$mail->Body = $body;
				$mail->IsHTML(true); 
				$mail->AddAddress("info@bcweddingawards.com");	
				$mail->AddBCC("darryl.wing@gmail.com");	
				//$mail->AddAddress($email);	
				$mail->Send();
				$mail->ClearAddresses();			
				
				// Update Nomination
				$usql = "UPDATE rsvp SET emailsenddate=now() WHERE rsvpid=".$rsvpid;
				mysql_query($usql) or die(mysql_error()."<br><br>".$usql);
				
				$goodsendcounter++;
							
			} else {
				$badsendcounter++;
				
			}
			
			
		}
				
	}
	
}

?>