<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deletediscount'])) {
			
	// Delete Admin User
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE galacode SET isdeleted=1 WHERE galacodeid=:recordid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':recordid', $recordid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	header("Location:discount_summary.php?m=Code%20deleted");
	exit();
	
} else {

	$code = $_POST['code'];
	$price = $_POST['price'];
	$expires1 = $_POST['expires'];
	
	// These fields cannot be blank
	$mfields[0] = $code;
	$mfields[1] = $price;
	$mfields[2] = $expires1;
			
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	if (empty($errortext)) {
		
		// Date
		$date1 = explode("/", $expires1);
		$expires = $date1[2]."-".$date1[0]."-".$date1[1];
		
		// Escape fields
		$code = addslashes($code);
				
		if (empty($recordid)) {
			
			// Insert
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "INSERT INTO galacode (created, code, price, expires) VALUES (NOW(), :code, :price, :expires)";
			    
			    $sql = $dbh->prepare($q);
			    $sql->bindParam(':code', $code);
			    $sql->bindParam(':price', $price);
			    $sql->bindParam(':expires', $expires);
			    $sql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
			
			header("Location:discount_summary.php?m=Code%20added");
			exit();
													
		} else {
			
			// Update
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE galacode SET 
						code=:code, 
						price=:price, 
						expires=:expires 
						WHERE galacodeid=:recordid";
			    
			    $sql = $dbh->prepare($q);
			    $sql->bindParam(':code', $code);
			    $sql->bindParam(':price', $price);
			    $sql->bindParam(':expires', $expires);
			    $sql->bindParam(':recordid', $recordid);
			    $sql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
					
			header("Location:discount_summary.php?m=Code%20updated");
			exit();
			
		}
		
	}
	
}


?>