<?php

// Get RSVP subject and body
$subject = stripslashes(getField("rsvpsubject", "settings", "settingsid=1"));
$bodypart = nl2br(stripslashes(getField("rsvpbody", "settings", "settingsid=1")));

$goodsendcounter = 0;
$badsendcounter = 0;

for ($i=0; $i<count($_POST['chkEntry']); $i++) {
	
	$rsvpid = $_POST['chkEntry'][$i];
	
	if ($rsvpid>0) {

		// Get company name, email, permakey		
		try
		{ 
				$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		    
		    $q = "SELECT companyname, email, permakey FROM rsvp WHERE rsvpid=:rsvpid";
		    $sql = $dbh->prepare($q);
		    $sql->bindParam(':rsvpid', $rsvpid);
		    $sql->execute();
		    
		    $row = $sql->fetch();
		    
		    if ($row)
		    {
		    	$companyname = stripslashes($row["companyname"]);
					$email = stripslashes($row["email"]);
					$permalink_tickets = _MY_HREF_."tickets.php?code=".$row["permakey"];
					$permalink_norsvp = _MY_HREF_."norsvp.php?code=".$row["permakey"];
					
					if (!empty($companyname) && !empty($email) && !empty($row["permakey"])) {
					
						$body = "Dear ".$companyname.",";
						$body.= "<br><br>";
						$body.= str_replace("#notattending#","<a href=\"".$permalink_norsvp."\">click here if you will not be attending the Awards Night</a>",str_replace("#rsvp#","<a href=\"".$permalink_tickets."\">RSVP and purchase your tickets using this link</a>",$bodypart));
																		
						$mail = new PHPMailer();
						$mail->From = "info@bcweddingawards.com";
						$mail->FromName = "Professional BC Wedding Awards";
						$mail->Host = "localhost.localdomain";
						$mail->Mailer = "mail";
						$mail->Subject = $subject;
						$mail->Body = $body;
						$mail->IsHTML(true); 
						$mail->AddAddress($email);	
						$mail->AddBCC("info@bcweddingawards.com");	
						$mail->Send();
						$mail->ClearAddresses();			
						
						// Update Nomination
						try
						{ 
								$dbh2 = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
								$dbh2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
						    
						    $q2 = "UPDATE rsvp SET emailsenddate=now() WHERE rsvpid=:rsvpid";
						    
						    $sql2 = $dbh2->prepare($q2);
						    $sql2->bindParam(':rsvpid', $rsvpid);
						    $sql2->execute();
						                 
						    $dbh2 = null;
						}
						catch(PDOException $e){
						  error_log('PDOException - ' . $e->getMessage(), 0);
						  http_response_code(500);
						  echo $e->getMessage();
						  die('Error establishing connection with database');
						}
						
						$goodsendcounter++;
									
					} else {
						$badsendcounter++;
						
					}
		    }
		                 
		    $dbh = null;
		}
		catch(PDOException $e){
		  error_log('PDOException - ' . $e->getMessage(), 0);
		  http_response_code(500);
		  echo $e->getMessage();
		  die('Error establishing connection with database');
		}
			
	}
	
}

?>