<?php

$message_judge = $_POST['message_judge'];
$confsubject = $_POST['confsubject'];
$confbody = $_POST['confbody'];
$rsvpsubject = $_POST['rsvpsubject'];
$rsvpbody = $_POST['rsvpbody'];
$rsvpconfbody = $_POST['rsvpconfbody'];
$rsvpprice = $_POST['rsvpprice'];
$galaseatsavailable = $_POST["galaseatsavailable"];
$galaprice = $_POST["galaprice"];
$galamaxperperson = $_POST['galamaxperperson'];
$galadate_display = $_POST['galadate_display'];

$displaypromocode = $_POST['displaypromocode'];
if (!empty($displaypromocode)) {
	$displaypromocode_sql = 1;
} else {
	$displaypromocode_sql = 0;
}

$canchangeguestnames = $_POST['canchangeguestnames'];
if (!empty($canchangeguestnames)) {
	$canchangeguestnames_sql = 1;
} else {
	$canchangeguestnames_sql = 0;
}

try
{ 
		$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    
    
    
    $q = "UPDATE settings SET 
			message_judge=:message_judge, 
			confsubject=:confsubject, 
			confbody=:confbody, 
			rsvpsubject=:rsvpsubject, 
			rsvpbody=:rsvpbody, 
			rsvpconfbody=:rsvpconfbody, 
			galaseatsavailable=:galaseatsavailable, 
			galaprice=:galaprice, 
			rsvpprice=:rsvpprice, 
			galamaxperperson=:galamaxperperson, 
			galadate_display=:galadate_display, 
			canchangeguestnames=:canchangeguestnames_sql, 
			displaypromocode=:displaypromocode_sql 
			WHERE settingsid=1;";
    
    $sql = $dbh->prepare($q);
    $sql->bindParam(':message_judge', $message_judge, PDO::PARAM_STR);
    $sql->bindParam(':confsubject', $confsubject, PDO::PARAM_STR);
    $sql->bindParam(':confbody', $confbody, PDO::PARAM_STR);
    $sql->bindParam(':rsvpsubject', $rsvpsubject, PDO::PARAM_STR);
    $sql->bindParam(':rsvpbody', $rsvpbody, PDO::PARAM_STR);
    $sql->bindParam(':rsvpconfbody', $rsvpconfbody, PDO::PARAM_STR);
    $sql->bindParam(':galaseatsavailable', $galaseatsavailable, PDO::PARAM_STR);
    $sql->bindParam(':galaprice', $galaprice);
    $sql->bindParam(':rsvpprice', $rsvpprice);
    $sql->bindParam(':galamaxperperson', $galamaxperperson);
    $sql->bindParam(':galadate_display', $galadate_display, PDO::PARAM_STR);
    $sql->bindParam(':canchangeguestnames_sql', $canchangeguestnames_sql);
    $sql->bindParam(':displaypromocode_sql', $displaypromocode_sql);
    $sql->execute();
                 
    $dbh = null;
}
catch(PDOException $e){
  error_log('PDOException - ' . $e->getMessage(), 0);
  http_response_code(500);
  echo $e->getMessage();
  die('Error establishing connection with database');
}

header("Location:settings_detail.php");
exit();
	
?>