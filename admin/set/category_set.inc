<?php

$recordid = $_POST['recordid'];

if (!empty($_POST['deletecategory'])) {
			
	// Delete all submissions
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE submission SET isdeleted=1 WHERE categoryid=:recordid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':recordid', $recordid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
				
	// Delete category
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE category SET isdeleted=1 WHERE categoryid=:recordid";
	    
	    $sql = $dbh->prepare($q);
	    $sql->bindParam(':recordid', $recordid);
	    $sql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
		
	header("Location:index.php");
	exit();
	
} else {

	$title = $_POST['title'];
	$code = $_POST['code'];
	$comment = $_POST['comment'];
	$instructions = $_POST['instructions'];
	$numimages = $_POST['numimages'];
	$sortweight = $_POST['sortweight'];
	$question9 = $_POST['question9'];

	// Exclude questions (Sept 2011)
	$sql_excludeq8 = 0;
	$sql_excludeq9 = 0;
	$sql_excludeq11 = 0;
	
	$excludeq8 = $_POST['excludeq8'];
	$excludeq9 = $_POST['excludeq9'];
	$excludeq11 = $_POST['excludeq11'];
	
	if (!empty($excludeq8)) {
		$sql_excludeq8 = 1;
	}
	
	if (!empty($excludeq9)) {
		$sql_excludeq9 = 1;
	}
	
	if (!empty($excludeq11)) {
		$sql_excludeq11 = 1;
	}
					
	// These fields cannot be blank
	$mfields[0] = $title;
	$mfields[1] = $code;
					
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all required fields";
		}
	}
	
	if (empty($errortext)) {
				
		// Escape fields
		$title = addslashes($title);
		$code = addslashes($code);
		$comment = addslashes($comment);
		$instructions = addslashes($instructions);
		$question9 = addslashes($question9);
						
		if (empty($recordid)) {
									
			// Insert
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "INSERT INTO category (created, title, code, comment, numimages, instructions, question9, sortweight, excludeq8, excludeq9, excludeq11)
			    	VALUES (NOW(), :title, :code, :comment, :numimages, :instructions, :question9, :sortweight, :sql_excludeq8, :sql_excludeq9, :sql_excludeq11) ";
			   			    
			    $isql = $dbh->prepare($q);
			    $isql->bindParam(':title', $title);
		    	$isql->bindParam(':code', $code);
		    	$isql->bindParam(':comment', $comment);
		    	$isql->bindParam(':numimages', $numimages);
		    	$isql->bindParam(':instructions', $instructions);
		    	$isql->bindParam(':question9', $question9);
		    	$isql->bindParam(':sortweight', $sortweight);
		    	$isql->bindParam(':sql_excludeq8', $sql_excludeq8);
		    	$isql->bindParam(':sql_excludeq9', $sql_excludeq9);
		    	$isql->bindParam(':sql_excludeq11', $sql_excludeq11);
			    $isql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
	
			//$categoryid = mysql_insert_id();
						
			header("Location:category_summary.php");						
			exit();
													
		} else {
			
			// Update
			try
			{ 
					$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
					$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			    
			    $q = "UPDATE category SET 
							title=:title, 
							code=:code, 
							comment=:comment, 
							instructions=:instructions, 
							question9=:question9, 
							numimages=:numimages, 
							sortweight=:sortweight, 
							excludeq8=:sql_excludeq8, 
							excludeq9=:sql_excludeq9, 
							excludeq11=:sql_excludeq11 
							WHERE categoryid=:recordid";
			    
			    $usql = $dbh->prepare($q);
			    $usql->bindParam(':title', $title);
		    	$usql->bindParam(':code', $code);
		    	$usql->bindParam(':comment', $comment);
		    	$usql->bindParam(':numimages', $numimages);
		    	$usql->bindParam(':instructions', $instructions);
		    	$usql->bindParam(':question9', $question9);
		    	$usql->bindParam(':sortweight', $sortweight);
		    	$usql->bindParam(':sql_excludeq8', $sql_excludeq8);
		    	$usql->bindParam(':sql_excludeq9', $sql_excludeq9);
		    	$usql->bindParam(':sql_excludeq11', $sql_excludeq11);
		    	$usql->bindParam(':recordid', $recordid);
			    $usql->execute();
			                 
			    $dbh = null;
			}
			catch(PDOException $e){
			  error_log('PDOException - ' . $e->getMessage(), 0);
			  http_response_code(500);
			  echo $e->getMessage();
			  die('Error establishing connection with database');
			}
						
			header("Location:category_summary.php");							
			exit();
			
		}
		
	}
	
}


?>