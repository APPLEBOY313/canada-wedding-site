<?php

$recordid = $_POST['recordid'];
$submissionid = $_POST['submissionid'];
$idcoll = $_POST['idcoll'];
$retnextid = $_POST['retnextid'];
$retprevid = $_POST['retprevid'];

$judgeid = $_SESSION['s_userid'];
$rating_aesthetics = $_POST['rating_aesthetics'];
$rating_themes = $_POST['rating_themes'];
$rating_originality = $_POST['rating_originality'];
$rating_professionalism = $_POST['rating_professionalism'];
$rating_overallquality = $_POST['rating_overallquality'];	

// Sept 2011
$cannotjudge = $_POST['cannotjudge'];
if (!empty($cannotjudge)) {
	$sql_cannotjudge = 1;
	$rating_aesthetics = 0;
	$rating_themes = 0;
	$rating_originality = 0;
	$rating_professionalism = 0;
	$rating_overallquality = 0;
	
} else {

	$sql_cannotjudge = 0;

	// These fields cannot be blank
	$mfields[0] = $rating_aesthetics;
	$mfields[1] = $rating_themes;
	$mfields[2] = $rating_originality;
	$mfields[3] = $rating_professionalism;
	$mfields[4] = $rating_overallquality;
					
	for ($n=0; $n<count($mfields); $n++) {
		if ($mfields[$n]=="") {
			$errortext = "Please completed all rating fields";
		}
	}
	
}

if (empty($errortext)) {
			
	// Calculate final score
	$overallscore = ($rating_aesthetics + $rating_themes + $rating_originality + $rating_professionalism + $rating_overallquality)*2;	
	if (!empty($cannotjudge)) {
		$overallscore = 0;
	}
					
	if (empty($recordid)) {
								
		// Insert
		try
		{ 
				$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		    
		    $q = "INSERT INTO score (created, judgeid, submissionid, rating_aesthetics, rating_themes, 
							rating_originality, rating_professionalism, rating_overallquality, comment, overallscore, cannotjudge) 
							VALUES (NOW(), :judgeid, :submissionid, :rating_aesthetics, :rating_themes, 
							:rating_originality, :rating_professionalism, :rating_overallquality, :comment, 
							:overallscore, :sql_cannotjudge)";
		    
		    $isql = $dbh->prepare($q);
		    $isql->bindParam(':judgeid', $judgeid);
		    $isql->bindParam(':submissionid', $submissionid);
		    $isql->bindParam(':rating_aesthetics', $rating_aesthetics);
		    $isql->bindParam(':rating_themes', $rating_themes);
		    $isql->bindParam(':rating_originality', $rating_originality);
		    $isql->bindParam(':rating_professionalism', $rating_professionalism);
		    $isql->bindParam(':rating_overallquality', $rating_overallquality);
		    $isql->bindParam(':comment', $comment);
		    $isql->bindParam(':overallscore', $overallscore);
		    $isql->bindParam(':sql_cannotjudge', $sql_cannotjudge);
		    $isql->execute();
		                 
		    $dbh = null;
		    
		    if ($retnextid!=$submissionid) {
					header("Location:scorejudge_detail.php?a=edit&submissionid=".$retnextid."&idcoll=".$idcoll);		
				} else {
					header("Location:index_judge.php?report=tobejudged");			
				}
				
				exit();
		}
		catch(PDOException $e){
		  error_log('PDOException - ' . $e->getMessage(), 0);
		  http_response_code(500);
		  echo $e->getMessage();
		  die('Error establishing connection with database');
		}
		
		//$scoreid = mysql_insert_id();
												
	} else {
		
		// Update
		try
		{ 
				$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		    
		    $q = "UPDATE score SET 
					rating_aesthetics=:rating_aesthetics, 
					rating_themes=:rating_themes, 
					rating_originality=:rating_originality, 
					rating_professionalism=:rating_professionalism, 
					rating_overallquality=:rating_overallquality, 
					overallscore=:overallscore, 
					cannotjudge=:sql_cannotjudge 
					WHERE scoreid=:recordid";
		    
		    $usql = $dbh->prepare($q);
		    $usql->bindParam(':rating_aesthetics', $rating_aesthetics);
		    $usql->bindParam(':rating_themes', $rating_themes);
		    $usql->bindParam(':rating_originality', $rating_originality);
		    $usql->bindParam(':rating_professionalism', $rating_professionalism);
		    $usql->bindParam(':rating_overallquality', $rating_overallquality);
		    $usql->bindParam(':overallscore', $overallscore);
		    $usql->bindParam(':sql_cannotjudge', $sql_cannotjudge);
		    $usql->bindParam(':recordid', $recordid);
		    $usql->execute();
		                 
		    $dbh = null;
		    
		    if (!empty($retnextid)) {
					if ($retnextid!=$recordid) {
						header("Location:scorejudge_detail.php?a=edit&submissionid=".$retnextid."&idcoll=".$idcoll);		
					} else {
						header("Location:index_judge.php?report=tobejudged");			
					}
				} else {
					header("Location:index_judge.php?report=tobejudged");			
				}
				
				exit();
		    
		}
		catch(PDOException $e){
		  error_log('PDOException - ' . $e->getMessage(), 0);
		  http_response_code(500);
		  echo $e->getMessage();
		  die('Error establishing connection with database');
		}
		
	}
	
}


?>