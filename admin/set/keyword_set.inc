<?php

$recordid = $_POST['recordid'];
$kwvalues = addslashes($_POST['kwvalues']);
		
if (!empty($recordid)) {
	
	try
	{ 
			$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	    
	    $q = "UPDATE keyword SET kwvalues=:kwvalues WHERE keywordid=:recordid";
	    
	    $usql = $dbh->prepare($q);
	    $usql->bindParam(':kwvalues', $kwvalues);
	    $usql->bindParam(':recordid', $recordid);
	    $usql->execute();
	                 
	    $dbh = null;
	}
	catch(PDOException $e){
	  error_log('PDOException - ' . $e->getMessage(), 0);
	  http_response_code(500);
	  echo $e->getMessage();
	  die('Error establishing connection with database');
	}
	
	header("Location:keyword_summary.php?m=Keyword%20updated");
	exit();
	
}

?>