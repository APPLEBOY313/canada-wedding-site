<?php

$judgeid = $_POST['judgeid'];
$categoryid = $_POST['categoryid'];

if (!empty($judgeid) && !empty($categoryid)) {
	
	// Has this judge already been assign to this category?
	if (!IsJudgeAssignToCategory($judgeid, $categoryid)) {
		try
		{ 
				$dbh = new PDO('mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPWD);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		    
		    $q = "INSERT INTO judges_category (created, judgeid, categoryid) VALUES (NOW(), :judgeid, :categoryid)";
		    
		    $sql = $dbh->prepare($q);
		    $sql->bindParam(':judgeid', $judgeid);
		    $sql->bindParam(':categoryid', $categoryid);
		    $sql->execute();
		                 
		    $dbh = null;
		}
		catch(PDOException $e){
		  error_log('PDOException - ' . $e->getMessage(), 0);
		  http_response_code(500);
		  echo $e->getMessage();
		  die('Error establishing connection with database');
		}
	
	}
	
}

?>