<? 
include("system/top.inc"); 	
include("system/getsearchvars_simple.inc");

if (empty($report)) $report = $_GET['report'];
if (empty($report)) $report = $_POST['report'];

if ($report == "tobejudged") {
	$pagetitle = "Submissions not Judged";
} else {
	$pagetitle = "Submissions Judged";
}

?>

<font class="subtitle"><? echo $pagetitle; ?></font>
<br><br>

<?

include("system/searchbox.inc");

$query = "SELECT DATE_FORMAT(s.created, '%m/%d/%Y %h:%i %p') AS tmpCreated, ";
$query.= "s.categoryid, cat.title AS category, s.country, ";
$query.= "sc.scoreid, sc.overallscore, s.nominee, s.submissionid, s.subyear ";
$query.= "FROM submission s ";
$query.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$query.= "LEFT OUTER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.judgeid = ".$_SESSION['s_userid'].") ";
$query.= "WHERE s.isdeleted=0 AND s.status='Paid' ";
$query.= $sqlsearch;

if ($report == "tobejudged") {
	$query.= "AND sc.scoreid IS NULL ";
} else if ($report == "judged") {
	$query.= "AND sc.scoreid IS NOT NULL ";
}

$query.= "ORDER BY cat.title, s.created ASC ";
$query.= "LIMIT $start, $limit";

$sqlcount = "SELECT COUNT(s.submissionid) AS SQLCount ";
$sqlcount.= "FROM submission s ";
$sqlcount.= "INNER JOIN category cat ON (cat.categoryid = s.categoryid) ";
$sqlcount.= "LEFT OUTER JOIN score sc ON (sc.submissionid = s.submissionid AND sc.judgeid = ".$_SESSION['s_userid'].") ";
$sqlcount.= "WHERE s.isdeleted=0 AND s.status='Paid' ";
$sqlcount.= $sqlsearch;

if ($report == "tobejudged") {
	$sqlcount.= "AND sc.scoreid IS NULL ";
} else if ($report == "judged") {
	$sqlcount.= "AND sc.scoreid IS NOT NULL ";
}

$t_result = mysql_query($sqlcount) or die(mysql_error()."<br><br>".$sqlcount);
$t_row = mysql_fetch_array($t_result);
$total_count = $t_row["SQLCount"];

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
		
	include ("system/nav.php");
	
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"150px\"><b>Submitted</b></td>";	
	echo "<td class=\"header\" width=\"250px\"><b>Category</b></td>";
	echo "<td class=\"header\" width=\"200px\"><b>Country</b></td>";	
	echo "<td class=\"header\" width=\"120px\" align=\"center\"><b>Score</b></td>";	
	echo "<td class=\"header\" width=\"50px\" align=\"right\"><b>ID</b></td>";	
	echo "</tr>";
		
	$x = $start + 1;	
	$n=0;
					
	while ($row = mysql_fetch_array($result)) {
	
		if ($n==0) {
			$idcoll = $row["submissionid"];
		} else {
			$idcoll.= "_".$row["submissionid"];
		}
	
		// Build Array
		$recarr[$n][0] = $row["submissionid"];
		$recarr[$n][1] = $row["scoreid"];
		$recarr[$n][2] = $row["tmpCreated"];
		$recarr[$n][3] = stripslashes($row["category"]);
		$recarr[$n][4] = stripslashes($row["country"]);
		$recarr[$n][5] = $row["overallscore"];
		$recarr[$n][6] = $row["subyear"];
		
		$n++;

	}
		
	for ($i=0; $i<count($recarr);$i++) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"150px\">".$recarr[$i][2]."</td>";		
		echo "<td width=\"250px\">".$recarr[$i][3]."</td>";
		echo "<td width=\"200px\">".$recarr[$i][4]."</td>";
		echo "<td width=\"120px\" align=\"center\">";
		
		if (!empty($recarr[$i][5])) {
			echo $recarr[$i][5]."%";
		} else {
			echo "";
		} 
		
		echo "</td>";
		echo "<td width=\"50px\" align=\"right\"><a href=\"submission_detail.php?id=".$recarr[$i][0]."\">".$recarr[$i][6]."-".$recarr[$i][0]."</a></td>";
		echo "</tr>";
				
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>