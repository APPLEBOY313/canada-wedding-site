<? include("system/top.inc"); ?>

<font class="subtitle">Discount Codes</font>

<br><br>

<?

if ($_SESSION['s_role']=="Administrator") {
	echo "<a href=\"discount_detail.php?a=edit\" class=\"onwhite\">New Discount Code</a><br><br>";
}

$query = "SELECT DATE_FORMAT(expires, '%m/%d/%Y') AS tmpExpires, galacode.* ";
$query.= "FROM galacode WHERE isdeleted=0 ORDER BY code ";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\"><b>Code</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"right\"><b>Price</b></td>";
	echo "<td class=\"header\" width=\"150px\" align=\"right\"><b>Expires</b></td>";	
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
		
		echo "<td><a href=\"discount_detail.php?id=".$row["galacodeid"]."\">".stripslashes($row["code"])."</a></td>";
		echo "<td width=\"80px\" align=\"right\">$".$row["price"]."</td>";		
		echo "<td width=\"150px\" align=\"right\">".$row["tmpExpires"]."</td>";				
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>