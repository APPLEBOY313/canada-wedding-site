<? include("system/top.inc"); ?>

<a href="gala_summary.php">Gala Online Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="gala_summary_archive.php">Archived Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="rsvp_summary.php">RSVP Summary</a>
<br><br>

<font class="subtitle">Archived Gala Online sales</font>

<br><br>

<?

$query = "SELECT DATE_FORMAT(paypaldate, '%m/%d/%Y %h:%i %p') AS tmpPaid, form_gala.* ";
$query.= "FROM form_gala WHERE isdeleted=0 AND paypalconfirm>-1 AND isarchived=1 ORDER BY created DESC";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	
	echo "<form action=\"gala_summary_archive.php\" method=\"POST\" name=\"theform\">";
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"10px\">";
	echo "<input type=\"checkbox\" onclick=\"SetAllCheckBoxes('theform','chkEntry[]')\">";
	echo "</td>";	
	echo "<td class=\"header\" width=\"150px\"><b>Date Paid</b></td>";	
	echo "<td class=\"header\"><b>Name</b></td>";
	echo "<td class=\"header\"><b>Contact</b></td>";
	echo "<td class=\"header\" width=\"65px\" align=\"right\"><b># Tickets</b></td>";
	echo "<td class=\"header\" width=\"60px\" align=\"right\"><b>Code</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"right\"><b>Amount</b></td>";
	echo "<td class=\"header\" width=\"120px\" align=\"center\"><b>Result</b></td>";
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
		
		echo "<td width=\"10px\"><input type=\"checkbox\" name=\"chkEntry[]\" value=\"".$row["galaid"]."\">";		
		echo "<td width=\"150px\">".$row["tmpPaid"]."&nbsp;</td>";				
		echo "<td>".stripslashes($row["name"]);
		
		if (!empty($row["company"])) {
			echo "<br>".stripslashes($row["company"]);
		}
		
		echo "</td>";
		echo "<td>".stripslashes($row["email"])."<br>".dispPhone($row["phone"])."</td>";
		echo "<td width=\"65px\" align=\"right\">".$row["qty"]."</td>";
		echo "<td width=\"60px\" align=\"right\">".strtoupper($row["code"])."</td>";
		echo "<td width=\"80px\" align=\"right\">$".$row["amount"]."</td>";
		echo "<td width=\"120px\">";
		
		if ($row["paypalconfirm"]=="1") {
			echo "CONFIRMED<br>".$row["transid"];
		} else if ($row["paypalconfirm"]=="0") {
			echo "FAILED";
		}
		
		echo "</td>";
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	echo "<br><br>";
	echo "<input type=\"submit\" name=\"btnActiveGala\" value=\"Make Active\">";
	echo "</form>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>