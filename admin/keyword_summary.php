<? include("system/top.inc"); ?>

<font class="subtitle">Keywords</font>
<br><br>

<?
$query = "SELECT keywordid, keyword, kwvalues FROM keyword WHERE canchange=1 ORDER BY keyword ";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\" width=\"150px\"><b>Keyword</b></td>";	
	echo "<td class=\"header\"><b>Values</b></td>";
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";			
		echo "<td width=\"150px\"><a href=\"keyword_detail.php?id=".$row["keywordid"]."\">".stripslashes($row["keyword"])."</a></td>";
		echo "<td>".stripslashes($row["kwvalues"])."</td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>