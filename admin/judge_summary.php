<? include("system/top.inc"); ?>

<font class="subtitle">Judges</font>

<br><br>

<?

if ($_SESSION['s_role']=="Administrator") {
	echo "<a href=\"judge_detail.php?a=edit\" class=\"onwhite\">New Judge</a><br><br>";
}

$query = "SELECT * FROM judge WHERE isdeleted=0 ORDER BY namefirst, namelast ";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\"><b>Judge</b></td>";
	echo "<td class=\"header\" width=\"150px\"><b>Last Login</b></td>";	
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";		
		echo "<td><a href=\"judge_detail.php?id=".$row["judgeid"]."\">".stripslashes($row["namefirst"]." ".$row["namelast"])."</a></td>";
		echo "<td width=\"150px\">";	
		
		if ($row["lastlogin"]=="0000-00-00 00:00:00") {
			echo "Never";
		} else {
			
			$phptimestamp = mysql2timestamp($row["lastlogin"]);
			$daysago = TimeAgo($phptimestamp);
			$secondpos = strpos($daysago, "second");
			$minpos = strpos($daysago, "minute");
			$hourpos = strpos($daysago, "hour");
			
			if ($minpos===false && $hourpos===false && $secondpos===false) {
				echo $row["tmpLastLogin"];
				
			} else {
				echo $daysago;
			}
				
		}
				
		echo "&nbsp;</td>";		
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>