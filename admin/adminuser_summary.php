<? include("system/top.inc"); ?>

<font class="subtitle">Users</font>

<br><br>

<?

if ($_SESSION['s_role']=="Administrator") {
	echo "<a href=\"adminuser_detail.php?a=edit\" class=\"onwhite\">New User</a><br><br>";
}

$query = "SELECT au.adminuserid, au.namefirst, au.namelast, au.loginid, au.lastlogin, au.role, ";
$query.= "DATE_FORMAT(au.lastlogin, '%m/%d/%Y %h:%i %p') AS tmpLastLogin, au.isactive ";
$query.= "FROM adminuser au ";
$query.= "WHERE au.isdeleted=0 ";
$query.= "ORDER BY au.namefirst, au.namelast ";
$query.= "LIMIT $start, $limit ";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\"><b>Name</b></td>";
	echo "<td class=\"header\" width=\"120px\"><b>Role</b></td>";
	echo "<td class=\"header\" width=\"150px\"><b>Last Login</b></td>";	
	echo "</tr>";
		
	$i = 1;
		
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";		
		echo "<td><a href=\"adminuser_detail.php?id=".$row["adminuserid"]."\">".stripslashes($row["namefirst"]." ".$row["namelast"])."</a>";
		
		if ($row["isactive"]=="0") echo "&nbsp;&nbsp;&nbsp;<font class=\"red\">[Inactive]</font>";
		
		echo "</td>";
		echo "<td width=\"120px\">".stripslashes($row["role"])."</td>";
		echo "<td width=\"150px\">";	
		
		if ($row["lastlogin"]=="0000-00-00 00:00:00") {
			echo "Never";
		} else {
			
			$phptimestamp = mysql2timestamp($row["lastlogin"]);
			$daysago = TimeAgo($phptimestamp);
			$secondpos = strpos($daysago, "second");
			$minpos = strpos($daysago, "minute");
			$hourpos = strpos($daysago, "hour");
			
			if ($minpos===false && $hourpos===false && $secondpos===false) {
				echo $row["tmpLastLogin"];
				
			} else {
				echo $daysago;
			}
				
		}
				
		echo "&nbsp;</td>";		
		echo "</tr>";
		
		$i++;
				
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>