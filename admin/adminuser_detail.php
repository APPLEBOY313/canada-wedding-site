<? 
include("system/top.inc"); 

if (empty($recordid)) $recordid = $_POST['recordid'];
if (empty($recordid)) $recordid = $_GET['id'];

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

if (!empty($recordid)) {
	include("get/adminuser_get.inc");	
} else {
	
	// Default values;
	
}

// Make sure they are allowed to edit this lead...
if ($_SESSION['s_role']=="Administrator" || $recordid==$_SESSION['s_userid']) {
	$canedit = true;
}

if (!empty($action)) {
	
	if ($canedit) {
		include("include/adminuser_fields_edit.inc");	
		
	} else {
		include("include/adminuser_fields_read.inc");
	}
		
} else {
	include("include/adminuser_fields_read.inc");	
}

include("system/bottom.inc"); 

?>