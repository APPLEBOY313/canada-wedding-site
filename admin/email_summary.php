<? include("system/top.inc"); ?>

<a href="email_summary.php">Mailing List</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="contactus_summary.php">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="nomination_summary.php">Nominations</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="bridal_summary.php">Bridal</a>
<br><br>

<font class="subtitle">Mailing List</font>
<br><br>
The following email addresses have been submitted from the website
<br><br>

<?

include("system/searchbox.inc");

$query = "SELECT DATE_FORMAT(created, '%m/%d/%Y %h:%i %p') AS tmpCreated, mailingid, email ";
$query.= "FROM form_mailing ";
$query.= "WHERE isdeleted=0 ";
if (!empty($_POST['search'])) {
	$query.= "AND (email LIKE '%".$_POST['search']."%') ";
}
$query.= "ORDER BY created DESC";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"120px\"><b>Date</b></td>";
	echo "<td class=\"header\"><b>Email</b></td>";	
	echo "<td class=\"header\" width=\"60px\">&nbsp;</td>";		
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
					
		echo "<td width=\"120px\">".$row["tmpCreated"]."</td>";
		echo "<td>".stripslashes($row["email"])."</td>";
		echo "<td width=\"60px\" align=\"center\"><a class=\"onwhite\" href=\"javascript:deleteRecord('form_mailing', '".$row["mailingid"]."', 'mailingid', '"._MY_HREF_ADMIN_."email_summary.php');\">Delete</a></td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>