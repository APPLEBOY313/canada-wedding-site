<? include("system/top.inc"); ?>

<a href="gala_summary.php">Gala Online Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="gala_summary_archive.php">Archived Sales</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="rsvp_summary.php">RSVP Summary</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="guest_summary.php">Guest List</a>
<br><br>

<font class="subtitle">RSVP Summary</font>

<br><br>

<?

if (!empty($goodsendcounter) || !empty($badsendcounter)) {
	echo "<center><div style=\"margin-bottom:10px;background-color:yellow;border:1px solid #666;padding:5px;width:400px;text-align:center;font-weight:bold;\">";
	echo "Succeeded sending ".$goodsendcounter." emails.  Failed sending ".$badsendcounter." emails";
	echo "</div></center>";
}

$query = "SELECT DATE_FORMAT(rsvp.rsvpdate, '%m/%d/%Y') AS tmpResponded, 
	DATE_FORMAT(rsvp.emailsenddate, '%m/%d/%Y') AS tmpEmailSent, 
	rsvp.rsvpid,
	rsvp.isnotattending,
	rsvp.companyname,
	rsvp.status,
	rsvp.subyear,
	rsvp.numentries,
	rsvp.eligible,
	rsvp.permakey,
	SUM(fg.qty) AS rsvptotal
	FROM rsvp 
	LEFT OUTER JOIN form_gala fg ON (fg.rsvppermakey=rsvp.permakey AND fg.isdeleted=0 AND fg.paypalconfirm=1 AND fg.isarchived=0)
	WHERE rsvp.isdeleted=0 AND rsvp.isarchived=0 
	GROUP BY 
		rsvp.rsvpid,
		rsvp.rsvpdate,
		rsvp.isnotattending,
		rsvp.companyname,
		rsvp.status,
		rsvp.subyear,
		rsvp.numentries,
		rsvp.eligible,
		rsvp.permakey,
		rsvp.emailsenddate
	ORDER BY rsvp.companyname ";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	
	echo "<form action=\"rsvp_summary.php\" method=\"POST\" name=\"theform\">";
				
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"10px\"><input type=\"checkbox\" onclick=\"SetAllCheckBoxes('theform','chkEntry[]')\"></td>";	
	echo "<td class=\"header\"><b>Company</b></td>";
	echo "<td class=\"header\" width=\"100px\"><b>Status</b></td>";
	echo "<td class=\"header\" width=\"35px\" align=\"right\"><b>Year</b></td>";
	echo "<td class=\"header\" width=\"60px\" align=\"right\"><b># Entries</b></td>";
	echo "<td class=\"header\" width=\"60px\" align=\"right\"><b># Eligible</b></td>";
	echo "<td class=\"header\" width=\"75px\" align=\"right\"><b># Purchased</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"right\"><b>Date RSVP</b></td>";	
	echo "<td class=\"header\" width=\"120px\" align=\"center\"><b>&nbsp;</b></td>";
	echo "<td class=\"header\" width=\"80px\" align=\"right\"><b>Email Sent</b></td>";	
	echo "</tr>";
		
	$i = 1;
	$total_rsvp = 0;
		
	while ($row = mysql_fetch_array($result)) {
	
		$rowclass = "";
		if ($row["isnotattending"]=="1") {
			$rowclass = "notattending";
		}
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
		
		echo "<td width=\"10px\"><input type=\"checkbox\" name=\"chkEntry[]\" value=\"".$row["rsvpid"]."\">";
		echo "<td class=\"".$rowclass."\">".stripslashes($row["companyname"])."</td>";
		echo "<td width=\"100px\">".stripslashes($row["status"])."</td>";
		echo "<td width=\"35px\" align=\"right\">".$row["subyear"]."</td>";
		echo "<td width=\"60px\" align=\"right\">".$row["numentries"]."</td>";
		echo "<td width=\"60px\" align=\"right\">".$row["eligible"]."</td>";
		echo "<td width=\"75px\" align=\"right\">".$row["rsvptotal"]."</td>";
		echo "<td width=\"80px\" align=\"right\">";
		
		if ($row["tmpResponded"]=="00/00/0000") {
			echo "";
		} else  {
			echo $row["tmpResponded"];
		}
		
		echo "</td>";
		echo "<td align=\"center\" width=\"120px\">";
		echo "<a title=\"See RSVP link\" target=\"bcrsvp\" href=\""._MY_HREF_."tickets.php?code=".$row["permakey"]."\">Tickets</a> | ";
		
		if ($row["isnotattending"]=="1") {
			echo "<a title=\"Undo\" target=\"bcrsvp\" href=\""._MY_HREF_."norsvp.php?code=".$row["permakey"]."&undo=1\">Coming</a>";
		} else {
			echo "<a title=\"Not attending Awards Night\" target=\"bcrsvp\" href=\""._MY_HREF_."norsvp.php?code=".$row["permakey"]."\">Not coming</a>";	
		}
		
		echo "</td>";
		echo "<td width=\"80px\" align=\"right\">";
		
		if ($row["tmpEmailSent"]=="00/00/0000") {
			echo "";
		} else  {
			echo $row["tmpEmailSent"];
		}
		
		echo "</td>";
		echo "</tr>";
		
		$i++;
		$total_rsvp = $total_rsvp + $row["rsvptotal"];
				
	}
	
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" colspan=\"6\" align=\"right\"><b>Total:</b></td><td class=\"header\" align=\"right\"><b>".$total_rsvp."</b></td><td class=\"header\" colspan=\"3\">&nbsp;</td></tr>";
	echo "</tr>";
	echo "</table>";
	echo "<br>";
	echo "<input type=\"submit\" name=\"btnSendRSVP\" value=\"Send RSVP email\">";
	echo "</form>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 

?>