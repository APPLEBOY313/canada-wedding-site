<? include("system/top.inc"); ?>

<font class="subtitle">Nominees</font>
<br><br>

<?

include("system/searchbox_nominee.inc");

$query = "SELECT nominee, COUNT(`nominee`) AS TotalNominations ";
$query.= "FROM submission ";
$query.= "WHERE isdeleted=0 AND subyear='$subyear' ";
$query.= "GROUP BY `nominee` ";
$query.= "ORDER BY TotalNominations DESC";

$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"100%\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"40px\"><b>#</b></td>";
	echo "<td class=\"header\" width=\"50px\"><b>Year</b></td>";
	echo "<td class=\"header\"><b>Nominee</b></td>";	
	echo "<td class=\"header\" width=\"70px\" align=\"right\"><b>Total</b></td>";
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"40px\">".$i."</td>";			
		echo "<td width=\"50px\">".$subyear."</td>";
		echo "<td>".stripslashes($row["nominee"])."</td>";
		echo "<td width=\"70px\" align=\"right\">".$row["TotalNominations"]."</td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>