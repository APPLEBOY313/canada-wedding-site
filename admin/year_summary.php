<? include("system/top.inc"); ?>

<font class="subtitle">Permitted Submission Dates/Judging Dates</font>
<br><br>

<?
$query = "SELECT subyear, startdate, enddate, judgingstart, judgingend, ";
$query.= "DATE_FORMAT(startdate, '%m/%d/%Y') AS tmpStartDate, ";
$query.= "DATE_FORMAT(enddate, '%m/%d/%Y') AS tmpEndDate, ";
$query.= "DATE_FORMAT(judgingstart, '%m/%d/%Y') AS tmpJudgingStart, ";
$query.= "DATE_FORMAT(judgingend, '%m/%d/%Y') AS tmpJudgingEnd ";
$query.= "FROM submissionyear ORDER BY subyear";
$result = mysql_query($query) or die(mysql_error()."<br><br>".$query);
$num_results = mysql_num_rows($result);

if ($num_results>0) {
	echo "<table width=\"500px\" cellpadding=2 border=0 class=\"atable\">";
	echo "<tr valign=\"top\">";
	echo "<td class=\"header\" width=\"100px\"><b>Year</b></td>";	
	echo "<td class=\"header\" width=\"100px\"><b>Start Date</b></td>";
	echo "<td class=\"header\" width=\"100px\"><b>End Date</b></td>";
	echo "<td class=\"header\" width=\"100px\"><b>Judging Start</b></td>";
	echo "<td class=\"header\" width=\"100px\"><b>Judging End</b></td>";
	echo "</tr>";
		
	$i = 1;
			
	while ($row = mysql_fetch_array($result)) {
	
		if($i % 2) { 
			echo "<tr valign=\"top\" class=rowdata>";
		} else {
			echo "<tr valign=\"top\" class=rowdata_alt>";
		}	
				
		echo "<td width=\"100px\"><a href=\"year_detail.php?id=".$row["subyear"]."\">".stripslashes($row["subyear"])."</a></td>";
		echo "<td width=\"100px\">".stripslashes($row["tmpStartDate"])."</td>";
		echo "<td width=\"100px\">".stripslashes($row["tmpEndDate"])."</td>";
		echo "<td width=\"100px\">".stripslashes($row["tmpJudgingStart"])."</td>";
		echo "<td width=\"100px\">".stripslashes($row["tmpJudgingEnd"])."</td>";
		echo "</tr>";
		
		$i++;
			
	}
	
	echo "</table>";
	
} else {
	echo "No records found";
	
}

include("system/bottom.inc"); 
?>