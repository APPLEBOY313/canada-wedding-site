<? 
include("system/top.inc"); 
include("get/settings_get.inc");	

if (empty($action)) $action = $_POST['action'];
if (empty($action)) $action = $_GET['a'];

// Make sure they are allowed 
if ($_SESSION['s_role']=="Administrator") {
	$canedit = true;
}

if (!empty($action)) {
	
	if ($canedit) {
		include("include/settings_fields_edit.inc");	
		
	} else {
		include("include/settings_fields_read.inc");
	}
		
} else {
	include("include/settings_fields_read.inc");	
}

include("system/bottom.inc"); 

?>