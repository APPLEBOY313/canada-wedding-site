<?php
	ini_set('display_errors', '1');
	include("dbconnect.php");
	include("class.phpmailer.php");
	
	$code = $_GET['code'];
	if (empty($code)) {
		$code = $_POST['code'];
	}
	
	if (empty($code)) {
		$errortext = "Cannot locate your details.  Please follow the link in your email, or contact us if you are having problems.";
		
	} else {
		
		$query = "SELECT companyname, email, eligible, status, rsvptotal FROM rsvp WHERE permakey='".$code."' AND isarchived=0";
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
		$showform = true;
		
		if ($row) {
							
			if ($row["status"]=="Has RSVP") {
				if ($row["rsvptotal"]=="0") {
					$errortext = "Our records show you have already responded via RSVP.  PIf you wish to make any alterations please contact us.";
				} else {
					$errortext = "Our records show you have already responded via RSVP.  Please update your guest list below.  If you wish to make any alterations please contact us.";
				}
				$showform = false;
			}
			
			$rsvptotal	= $row["rsvptotal"];
			$companyname = stripslashes($row["companyname"]);
			$email = stripslashes($row["email"]);
			$eligible = $row["eligible"];
				
			if ($eligible == "1") {
				$elgible_display = "1 ticket";
			} else {
				$elgible_display = $eligible." tickets";
			}
			
			if ($rsvptotal>0) {
				if ($rsvptotal=="1") {
					$elgible_display.= " (1 has already been claimed)";
				} else {
					$elgible_display.= " (".$rsvptotal." have already been claimed)";
				}
			}
			
		} else {
			$errortext = "Cannot locate your details.  Please follow the link in your email, or contact us if you are having problems.";
			$showform = false;
						
		}
		
		if (!empty($_POST['submitrsvp']) && $_POST['rsvpnumber']!="0") {
			
			$email = $_POST['email'];
			$rsvpnumber = $_POST['rsvpnumber'];
			
			if ($rsvpnumber=="1") {
				$rsvpnumber_display = "1 ticket";
			} else {
				$rsvpnumber_display = $rsvpnumber." tickets";
			}
			
			$rsvptotal = $rsvptotal + $rsvpnumber;
			if ($rsvptotal>4) $rsvptotal=4;
			
			if ($rsvptotal == $eligible) {
				// Update Status to Has RSVP - no more allowed
				$usql = "UPDATE rsvp SET status='Has RSVP', rsvptotal=".$rsvptotal.", rsvpdate=now() WHERE permakey='".$code."'";
			} else {
				$usql = "UPDATE rsvp SET rsvptotal=".$rsvptotal.", rsvpdate=now() WHERE permakey='".$code."'";	
			}
					
			mysql_query($usql) or die("Oops - there was a problem submitting your RSVP");
			
			// Send confirmation email
			$subject = stripslashes(getField("rsvpconfsubject", "settings", "settingsid=1"));
			$bodypart = nl2br(stripslashes(getField("rsvpconfbody", "settings", "settingsid=1")));
			
			// Replace #rsvp# with dedicated link
			$bodypart = str_replace("#rsvp", "<a href=\"http://www.bcweddingawards.com/rsvp.php?code=".$code."\">this link</a>", $bodypart);
			
			$body = "Dear ".$companyname.",";
			$body.= "<br><br>";
			$body.= "This email confirms that you have reserved ".$rsvpnumber_display.".";
			$body.= "<br><br>";
			$body.= $bodypart;
			$body.= "<br><br>";
			$body.= "Professional BC Wedding Awards";
			$body.= "<br>";
			$body.= "http://www.bcweddingawards.com";
											
			$mail = new PHPMailer();
			$mail->From = "info@bcweddingawards.com";
			$mail->FromName = "Professional BC Wedding Awards";
			$mail->Host = "localhost.localdomain";
			$mail->Mailer = "mail";
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->IsHTML(true); 
			$mail->AddAddress($email);	
			$mail->AddBCC("info@bcweddingawards.com");	
			$mail->Send();
			$mail->ClearAddresses();			
			
			// Show success
			$oktext = "Your request has been received.  Thank you!";
						
		} else if ($_POST['rsvpnumber']=="0") {
			
			// Person is not attending
			$usql = "UPDATE rsvp SET status='Has RSVP', rsvptotal=0, rsvpdate=now() WHERE permakey='".$code."'";
			mysql_query($usql) or die("Oops - there was a problem submitting your RSVP");
			
			$showform = false;
			
			// Show success
			$oktext = "Your request has been received.  Thank you!";
			
		}	else if (!empty($_POST['submitnames'])) {
			
			// RSVP name list
			$attendeenames = "";
			$rsvp_name0 = trim($_POST['rsvp_name0']);
			$rsvp_name1 = trim($_POST['rsvp_name1']);
			$rsvp_name2 = trim($_POST['rsvp_name2']);
			$rsvp_name3 = trim($_POST['rsvp_name3']);
						
			if (!empty($rsvp_name0)) $attendeenames.=$rsvp_name0.";";			
			if (!empty($rsvp_name1)) $attendeenames.=$rsvp_name1.";";	
			if (!empty($rsvp_name2)) $attendeenames.=$rsvp_name2.";";		
			if (!empty($rsvp_name3)) $attendeenames.=$rsvp_name3.";";	
						
			$usql = "UPDATE rsvp SET attendeenames='".addslashes($attendeenames)."' WHERE permakey='$code'";
			mysql_query($usql) or die("Error #998<br>".$usql);
						
			// Purchased name list
			$attendeenames = "";
			$purchase_name0 = trim($_POST['purchase_name0']);
			$purchase_name1 = trim($_POST['purchase_name1']);
			$purchase_name2 = trim($_POST['purchase_name2']);
			$purchase_name3 = trim($_POST['purchase_name3']);
			$purchase_name4 = trim($_POST['purchase_name4']);
			$purchase_name5 = trim($_POST['purchase_name5']);
			
			$purchase_oldname_0 = trim($_POST['purchase_oldname_0']);
			$purchase_oldname_1 = trim($_POST['purchase_oldname_1']);
			$purchase_oldname_2 = trim($_POST['purchase_oldname_2']);
			$purchase_oldname_3 = trim($_POST['purchase_oldname_3']);
			$purchase_oldname_4 = trim($_POST['purchase_oldname_4']);
			$purchase_oldname_5 = trim($_POST['purchase_oldname_5']);
			
			$purchase_galaid_0 = trim($_POST['purchase_galaid_0']);
			$purchase_galaid_1 = trim($_POST['purchase_galaid_1']);
			$purchase_galaid_2 = trim($_POST['purchase_galaid_2']);
			$purchase_galaid_3 = trim($_POST['purchase_galaid_3']);
			$purchase_galaid_4 = trim($_POST['purchase_galaid_4']);
			$purchase_galaid_5 = trim($_POST['purchase_galaid_5']);
			
			// Perform for each update
			for ($i=0; $i<6; $i++) {
				
				// Get GalaID
				if ($i==0) $galaid = $purchase_galaid_0;
				if ($i==1) $galaid = $purchase_galaid_1;
				if ($i==2) $galaid = $purchase_galaid_2;
				if ($i==3) $galaid = $purchase_galaid_3;
				if ($i==4) $galaid = $purchase_galaid_4;
				if ($i==5) $galaid = $purchase_galaid_5;
				
				// Get Old Name
				if ($i==0) $galaoldname = $purchase_oldname_0;
				if ($i==1) $galaoldname = $purchase_oldname_1;
				if ($i==2) $galaoldname = $purchase_oldname_2;
				if ($i==3) $galaoldname = $purchase_oldname_3;
				if ($i==4) $galaoldname = $purchase_oldname_4;
				if ($i==5) $galaoldname = $purchase_oldname_5;
								
				// Get New Name
				if ($i==0) $galanewname = $purchase_name0;
				if ($i==1) $galanewname = $purchase_name1;
				if ($i==2) $galanewname = $purchase_name2;
				if ($i==3) $galanewname = $purchase_name3;
				if ($i==4) $galanewname = $purchase_name4;
				if ($i==5) $galanewname = $purchase_name5;
				
				if (!empty($galaid)) {
					
					$csql = "SELECT attendeenames FROM form_gala WHERE galaid=".$galaid;	
					$cresult = mysql_query($csql) or die (mysql_error()."<br>".$csql);
					$crow = mysql_fetch_array($cresult);
			
					if ($crow) {
						$galaoldnames = $crow["attendeenames"];					
						
						if (empty($galaoldnames)) {
							$usql = "UPDATE form_gala SET attendeenames='".addslashes($galanewname)."' WHERE galaid=".$galaid;
							mysql_query($usql) or die("Error #197<br>".$usql);	
							
						} else {
							$usql = "UPDATE form_gala SET attendeenames=REPLACE(attendeenames, '$galaoldname', '$galanewname') WHERE galaid=".$galaid;
							mysql_query($usql) or die("Error #198<br>".$usql);		
						}
						
					}	
						
				}
				
			}
			
			// All OK if you got here
			$names_ok = "OK";
			
		}
		
	}
	
	include("top.inc");
	include("forms/rsvp.inc"); 
	
	if ($rsvptotal>0) {	
		include("forms/guestlist.inc"); 
	}
	
	include("bottom.inc");
?>
