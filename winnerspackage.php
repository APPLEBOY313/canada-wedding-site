<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body">
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<a name="categories"></a>
	<div class="content">
		<div class="container">
			<div class="wrap winnerspack_con">
				<img src="siteimages/awards2014.jpg"><br><br>
				<h1>2016 Winners</h1>
				<p><br><br>Each entry that is received will be carefully reviewed and scored by the panel of <a href="http://www.bcweddingawards.com/judging.html"><b><big> judges</big></b></a> to determine the winners.</p>
				<p><b>2016 winners will receive the following:</b></p>
			    <span>
			    	- Elegant engraved glass trophy. Yes, it's that beautiful one you see above!<br>
			 		- Print coverage of the Awards in BC's largest Wedding magazine between December 2016 - December 2017.<br>
			 		- Full use of the awards logo online and in print.<br>
			 		- Recognition and presentation of your award at our <a href="http://www.bcweddingawards.com/events.html"><b><big> Annual Awards Gala </big></b></a><br>
			 		- Online listing for your company through the <a href="http://www.bcweddingawards.com"><b><big>Professional BC Wedding Awards</big></b></a> website.
			    </span>
			    <span>
			    	The Annual Awards Event will be an outstanding opportunity to network with a wide range of vendors in the BC Wedding Industry, as well as give everyone a fun industry night out!<br><br>
			    	If you wish to be notified of details as they emerge, take a moment to follow us on twitter, and become a friend on facebook!
			    </span>
			</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
</body>
</html>

