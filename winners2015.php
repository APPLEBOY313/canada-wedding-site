<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<a name="categories"></a>
	<div class="content">
		<div class="container">
			<div class="wrap winners2015_con">
				<h1>2015 Winners</h1>
				<span>Organizers of the 2015 Professional BC Wedding Awards handed out trophies on November 25, 2015 at The Hard Rock Casino Theatre in Coquitlam. The winners are those companies with the highest scoring entries. There is a maximum of 2 finalists per category, and they are listed here in alphabetical order.</span><br><br>
				<p>Please note that in most cases, only a small part of the entry material is shown publicly. Photographer credit was not known for all entries, it has been provided wherever possible.</p>
				<span class="winners_list">
					<a href='#1'>Best Wedding Cake</a>
					<a href='#2'>Best Bridal Bouquet</a>
					<a href='#3'>Best Wedding Florist - Overall</a>
					<a href='#4'>Best Wedding Make-up</a>
					<a href='#6'>Best Wedding Hair Style</a>
					<a href='#8'>Best Wedding Hair & Make-up - South Asian</a>
					<a href='#9'>Best Wedding Decor</a>
					<a href="#10">Best Wedding Stationery</a>
					<a href='#11'>Best Wedding Reception Venue</a>
					<a href='#12'>Best Wedding Reception Venue - Hotel or Banquet Hall</a>
					<a href='#13'>Best Wedding Ceremony Location</a>
					<a href='#14'>Best Wedding Officiant</a>
					<a href='#15'>Best Wedding Event Planning</a>
					<a href='#16'>Best Catered Wedding</a>
					<a href='#18'>Best Wedding DJ</a>
					<a href='#19'>Best Photo Booth - Photo Sequence</a>
					<a href='#20'>Best Candid/Photojournalism Photograph</a>
					<a href='#21'>Best Portrait- Bride and Groom Together</a>
					<a href='#22'>Best Wedding Detail Photograph</a>
					<a href='#23'>Best Wedding Group Photograph</a>
					<a href='#24'>Best Overall Wedding Photography</a>
					<a href='#25'>Best Wedding Videographer/Cinematographer </a>
					<a href='#27'>Best Edited Wedding Video</a>
					<a href='#28'>2015 Best Tasting Wedding Cake</a>
					<a href='#29'>2015 Tasters Choice - Best Hors D’Oeuvre</a>
					<a href='#31'>2015 Industry Achievement Award Winner</a>
					<p>Winners: The winners are those companies with the highest score.</p>
					<p>Finalists: In order to qualify as a finalist the score needs to be within 10% of the winning score.<br>There is a maximum of 2 finalists per category.</p>
				</span>
				<div class="winner">
					<h2><strong><a name="1"></a>Best Wedding Cake</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Anna Elizabeth Cakes</span><br/>
						<a href="http://annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a><br/><br/>
						<img src="winners2015/AnnaElizabeth_3171.jpg" width="467" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">The Cake & The Giraffe</span><br/>
							<a href="http://www.tcandtg.com" target="_blank">www.tcandtg.com</a></p>
							<p align="center"><img src="winners2015/CakeAndGiraffe_2842.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Whisk Cake Company</span><br />
							<a href="http://www.whiskcakes.com" target="_blank">www.whiskcakes.com</a></p>
							<p align="center"><img src="winners2015/WhiskCake_2879.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Dushan Flowers</span><br/>
						<a href="http://www.dushanflowers.com" target="_blank">www.dushanflowers.com</a><br/><br/>
						<img src="winners2015/Dushan_3368.jpg" width="420" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Chandelier Wedding & Event Design</span><br/>
							<a href="http://www.chandelierwedding.com" target="_blank">www.chandelierwedding.com</a></p>
							<p align="center"><img src="winners2015/Chandalier_3054.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Flower Factory</span><br />
							<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a></p>
							<p align="center"><img src="winners2015/Flower_Factory_2616.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Flower Factory</span><br/>
						<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br/><br/>
						<img src="winners2015/Flower_Factory_Overall_2642B.jpg" width="600" height="400"border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Sunflower Florist</span><br/>
							<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>
							<p align="center"><img src="winners2015/sunflower.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Verbena Floral Design</span><br />
							<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a></p>
							<p align="center"><img src="winners2015/Verbena_2688.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="4"></a>Best Wedding Make-up</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Nadia Albano Style Inc.</span><br/>
						<a href="http://www.nadiaalbano.com" target="_blank">www.nadiaalbano.com</a><br/><br/>
						<img src="winners2015/Nadia_Albano_2752.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">All Dolled Up Makeup & Hair</span><br/>
							<a href="http://www.alldolledupstudio.ca" target="_blank">www.alldolledupstudio.ca</a></p>
							<p align="center"><img src="winners2015/All_Dolled_Up_2796.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Katie Elwood Makeup Artistry</span><br />
							<a href="http://www.katieelwood.com" target="_blank">www.katieelwood.com</a></p>
							<p align="center"><img src="winners2015/Katie_Elwood_2759.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Hair Style</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Faye Smith Makeup & Hair</span><br/>
						<a href="http://www.fayesmithmakeup.com" target="_blank">www.fayesmithmakeup.com</a><br/><br/>
						<img src="winners2015/hs1_3552_5.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Nadia Albano Style Inc.</span><br/>
							<a href="http://www.nadiaalbano.com" target="_blank">www.nadiaalbano.com</a></p>
							<p align="center"><img src="winners2015/Nadia_Albano_HS_2752.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Red Carpet Ready by Christina</span><br />
							<a href="http://www.redcarpetreadybychristina.ca" target="_blank">www.redcarpetreadybychristina.ca</a></p>
							<p align="center"><img src="winners2015/RedCarpet_3124.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="8"></a>Best South Asian Bride – Hair & Makeup</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Salon Picasso Brial Studio</span><br/>
						<a href="http://www.salonpicasso.ca target="_blank">www.salonpicasso.ca</a><br/><br/>
						<img src="winners2015/Salon_Picasso_2633.jpg" width="400" height="600"" width="600" height="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Felicia Bromba</span><br/>
							<a href="http://www.makeup-by-felicia.com" target="_blank">www.makeup-by-felicia.com</a></p>
							<p align="center"><img src="winners2015/sa1_5505_3.jpg" width=width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Pink Orchid Studio</span><br />
							<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a></p>
							<p align="center"><img src="winners2015/Pink_Orchid_3139.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="9"></a>Best Wedding Decor</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Vintage Origami</span><br/>
						<a href="http://www.VintageOrigami.com" target="_blank">www.VintageOrigami.com</a><br/><br/>
						<img src="winners2015/VintageOrigami_2766.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Defining Decor</span><br/>
							<a href="http://www.definingdecor.com" target="_blank">www.definingdecor.com</a></p>
							<p align="center"><img src="winners2015/DefiningDecor_2900.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Petite Pearl Events</span><br />
							<a href="http://www.PetitePearlEvents.com" target="_blank">www.PetitePearlEvents.com</a></p>
							<p align="center"><img src="winners2015/Petite_Pearl_3128.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="10"></a>Best Wedding Stationery</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">I Said Yes Wedding Stationery and Design</span><br/>
						<a href="http://www.isaidyes.ca" target="_blank">www.isaidyes.ca</a><br/><br/>
						<img src="winners2015/I_Said_Yes_Wedding_Stationery_and_Design_3144.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Making Memories</span><br/>
							<a href="http://www.makingmemorieswithscrapbooking.com" target="_blank">www.makingmemorieswithscrapbooking.com</a></p>
							<p align="center"><img src="winners2015/Making_Memories_3032.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Stationery Bike Designs</span><br />
							<a href="http://www.stationerybikedesigns.com" target="_blank">www.stationerybikedesigns.com</a></p>
							<p align="center"><img src="winners2015/Stationery_Bike_Design_2934.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="11"></a>Best Wedding Reception Venue</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Loungeworks</span><br/>
						<a href="http://www.loungeworks.ca" target="_blank">www.loungeworks.ca</a><br/><br/>
						<img src="winners2015/LoungeWorks_3039.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brock House Restaurant</span><br/>
							<a href="http://www.brockhouserestaurant.com" target="_blank">www.brockhouserestaurant.com</a></p>
							<p align="center"><img src="winners2015/Brockhouse_Reception_2782.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Stanley Park Pavilion</span><br />
							<a href="http://www.stanleyparkpavilion.com" target="_blank">www.stanleyparkpavilion.com</a></p>
							<p align="center"><img src="winners2015/StanleyParkPavilion_2971.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Vancouver Club</span><br/>
						<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a><br/><br/>
						<img src="winners2015/Vancouver_Club_3360.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Inn at Laurel Point</span><br/>
							<a href="http://www.laurelpoint.com" target="_blank">www.laurelpoint.com</a></p>
							<p align="center"><img src="winners2015/Inn_At_Laurel_Point_3015.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Terminal City Club</span><br />
							<a href="http://www.tcclub.com" target="_blank">www.tcclub.com/plan</a></p>
							<p align="center"><img src="winners2015/Terminal_City_2983.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Vancouver Aquarium</span><br/>
						<a href="http://www.vanaqua.org/plan/weddings" target="_blank">www.vanaqua.org/plan/weddings</a><br/><br/>
						<img src="winners2015/VanAqua_2894.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brock House Restaurant</span><br/>
							<a href="http://www.brockhouserestaurant.com" target="_blank">www.brockhouserestaurant.com</a></p>
							<p align="center"><img src="winners2015/Brockhouse_Ceremony_2781.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">John M.S. Lecky UBC Boathouse</span><br />
							<a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>
							<p align="center"><img src="winners2015/UBC_BoatHouse_2884.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="14"></a>Best Wedding Officiant</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shawn Miller – Young, Hip & Married</span><br/>
						<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Lisa Hartley<br />Modern Celebrant</span><br/>
							<a href="http://www.moderncelebrant.com" target="_blank">www.moderncelebrant.com</a></p>							
						</div>
						<div class="other_pic">
							<p align="center"><br/><span class="company">MarryUs Custom Wedding Ceremonies</span><br />
							<a href="http://www.Marryus.ca" target="_blank">www.Marryus.ca</a></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="15"></a>Best Wedding Event Planning</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Filosophi Events</span><br/>
						<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a><br/><br/>
						<img src="winners2015/Filosophi_2774.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Fluent Productions</span><br/>
							<a href="http://www.fluent-productions.com" target="_blank">www.fluent-productions.com</a></p>
							<p align="center"><img src="winners2015/Fluent_Productions_3207.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Epic Events</span><br />
							<a href="http://http://www.epicevents.ca" target="_blank">http://www.epicevents.ca</a></p>
							<p align="center"><img src="winners2015/EpicEvents_3014.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="16"></a>Best Catered Wedding</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Savoury City Catering</span><br/>
						<a href="http://www.savourycity.com" target="_blank">www.savourycity.com</a><br/><br/>
						<img src="winners2015/SavouryCity_2707.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">SDetails Catering by Waterfront Wines</span><br/>
							<a href="http://www.waterfrontrestaurant.ca" target="_blank">www.waterfrontrestaurant.ca</a></p>
							<p align="center"><img src="winners2015/Details_2866.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Toque Catering</span><br />
							<a href="http://www.toquecatering.com" target="_blank">www.toquecatering.com</a></p>
							<p align="center"><img src="winners2015/Touque_2706.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="17"></a>Best Live Wedding Entertainment</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">SideOne</span><br/>
						<a href="http://www.sideone.ca" target="_blank">www.sideone.ca</a><br/><br/>
						<img src="winners2015/SideOne_3318A.jpg" width="600" height="426" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Colin Bullock</span><br/>
							<a href="http://www.colinbullock.com" target="_blank">www.colinbullock.com</a></p>
							<p align="center"><img src="winners2015/ColinBullock_2945.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Famous Players Band</span><br />
							<a href="http://www.famousplayersband.com" target="_blank">www.famousplayersband.com</a></p>
							<p align="center"><img src="winners2015/FamousPlayers_3025A.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="18"></a>Best Wedding DJ</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Decibel Entertainment</span><br/>
						<a href="http://www.decibelentertainment.com" target="_blank">www.decibelentertainment.com</a><br/><br/>
						<img src="winners2015/Decibel_3292.jpg" width="600" height="338" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">GirlOnWax Entertainment</span><br/>
							<a href="http://www.girlonwax.com" target="_blank">www.girlonwax.com</a></p>
							<p align="center"><img src="winners2015/GirlsOnWax_2891.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">X-Fusion Productions Inc.</span><br />
							<a href="http://www.xfusionroadshow.com" target="_blank">www.xfusionroadshow.com</a></p>
							<p align="center"><img src="winners2015/X_Fusion_2940.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="19"></a>Best Photo Booth - Photo Sequence</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Wink Photography</span><br/>
						<a href="http://www.winkphotography.ca" target="_blank">wwww.winkphotography.ca</a><br/><br/>
						<img src="winners2015/Wink_3222.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Four Frames Photo Booth</span><br/>
							<a href="www.FourFramesPhotoBooth.com" target="_blank">www.FourFramesPhotoBooth.com</a></p>
							<p align="center"><img src="winners2015/FourFrames_2711.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Vancouver Photo Booth</span><br />
							<a href="http://vancouverphotobooth.com" target="_blank">www.vancouverphotobooth.com</a></p>
							<p align="center"><img src="winners2015/VacouverPhotoBooth_3071.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="20"></a>Best Candid/Photojournalism Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Raymond & Jessie Photography</span><br/>
						<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a><br/><br/>
						<img src="winners2015/Raymond_Jessie_2986.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Justine Boulin Photography</span><br/>
							<a href="http://www.justineboulin.com" target="_blank">www.justineboulin.com</a></p>
							<p align="center"><img src="winners2015/JustineBoulin_PJ_2807.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Love Tree Photography</span><br />
							<a href="http://www.lovetreephotography.ca" target="_blank">www.lovetreephotography.ca</a></p>
							<p align="center"><img src="winners2015/LoveTree_2992.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="21"></a>Best Portrait - Bride and Groom together</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Apartment Photography</span><br/>
						<a href="http://www.theapartmentphotography.com" target="_blank">www.theapartmentphotography.com"</a><br/><br/>
						<img src="winners2015/The_Apartment_3259.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Love Tree Photography</span><br/>
							<a href="http://www.lovetreephotography.ca" target="_blank">www.lovetreephotography.ca</a></p>
							<p align="center"><img src="winners2015/LoveTree_BrideGroom_3286.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Mathias Fast Photography</span><br />
							<a href="http://www.mathiasfastphotography.com" target="_blank">www.mathiasfastphotography.com</a></p>
							<p align="center"><img src="winners2015/Mathias_3067.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="22"></a>Best Wedding Detail Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Jasalyn Thorne Photography</span><br/>
						<a href="http://www.jasalynthornephotography.com" target="_blank">www.jasalynthornephotography.com</a><br/><br/>
						<img src="winners2015/JasalynThorne_3011.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Shari + Mike Photographers</span><br/>
							<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
							<p align="center"><img src="winners2015/Shari_Mike_Detail_3166.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Yinger Fotokrafie Design</span><br />
							<a href="http://www.yingerdesign.com" target="_blank">www.yingerdesign.com</a></p>
							<p align="center"><img src="winners2015/YingerFotogkrafie_3195.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="23"></a>Best Wedding Group Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shari + Mike Photographers</span><br/>
						<a href="http://shariandmike.ca" target="_blank">www.shariandmike.ca</a><br/><br/>
						<img src="winners2015/Shari_Mike_Group_3164.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Ophelia Photography</span><br/>
							<a href="http://www.opheliaphotography.com" target="_blank">www.opheliaphotography.com</a></p>
							<p align="center"><img src="winners2015/Ophelia_3323.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Sakura Photography</span><br />
							<a href="http://www.sakuraphotography.com" target="_blank">www.sakuraphotography.com</a></p>
							<p align="center"><img src="winners2015/Sakura_3151.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="24"></a>Best Overall Wedding Photography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Povazan Photography</span><br/>
						<a href="http://www.povazanphotography.com" target="_blank">www.povazanphotography.com</a><br/><br/>
						<img src="winners2015/Povazan_2783D.jpg" width="600" height="400" border="0"><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Justine Boulin Photography</span><br/>
							<a href="http://www.justineboulin.com" target="_blank">www.justineboulin.com</a></p>
							<p align="center"><img src="winners2015/Justin_Boulin_Overall_2810.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Shari + Mike Photographers</span><br />
							<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
							<p align="center"><img src="winners2015/Shari_Mike_Overall_3371.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="25"></a>Best Wedding Videography/Cinematography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Hello Tomorrow Wedding Films</span><br/>
						<a href="http://www.hellotomorrow.com" target="_blank">www.hellotomorrow.com</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Memento Films</span><br/>
							<a href="http://www.mementofilms.ca" target="_blank">www.mementofilms.ca</a></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Premier Love</span><br />
							<a href="http://www.premierlove.ca" target="_blank">www.premierlove.ca</a></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="27"></a>Best Edited Wedding Video</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">VanWeddings Inc.</span><br/>
						<a href="http://www.vanweddings.com" target="_blank">www.vanweddings.com</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Memento Films</span><br/>
							<a href="http://www.MementoFilms.ca" target="_blank">www.MementoFilms.ca</a></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Modern Romance Productions</span><br />
							<a href="http://www.modernromance.ca" target="_blank">www.modernromance.ca</a></p>
						</div>
						<p align="right"><a href="winners2015.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="31"></a>Industry Achievement Award</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Gloria Cheung - Flower Factory</span><br/>
						<a href="http://www.flowerfactor.ca" target="_blank">www.flowerfactor.ca</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>					
				</div>
				<div class="final_list">
					<h2><strong><a name="28"></a>2015 Best Tasting Wedding Cake Award Winner</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company"></span><br/>
						<a href="http://www.annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<h2><strong><a name="29"></a>2015 Best Hors D’Oeuvre Award Winner</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">no fixed address catering</span><br/>
						<a href="http://nofixedaddresscatering.com" target="_blank">http://nofixedaddresscatering.com</a><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<h2><strong>2015 Judges Choice - Best Hors D’Oeuvre Award Winner</strong></h2><br><br>
  					<p>name<br/>
  					<a href="http://nofixedaddresscatering.com/" target="_blank">http://nofixedaddresscatering.com/</a><br><br>
  					<h2><strong>Best Hors D'Oeuvres PARTICIPANTS</strong></h2><br>
  					<p>Emelle's Catering, No Fixed Address Catering, Truffle's Fine Foods, Vij's Catering</p>
  				</div>
  				<span align="center">
  					<p>Our sincere thank-you to each of this years competing caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had.</p>
  					Special Thanks also to our Judges</b><br>Julian Bond of  <a href="http://www.picachef.com" target="_blank">Pacific Institute of Culinary Arts</a> <br><br>Mijune Pak of <a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br><br>Nathan Fong of <a href="http://www.fongonfood.com" target="_blank">FONG ON FOOD</a><br><br><br>
  					<p>Congratulations to all of the Winners and Finalists!</p>
  				</span>
			</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
</body>
</html>
