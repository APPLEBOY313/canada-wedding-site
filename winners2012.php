<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body">
<?php include("top.inc"); ?>
<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
<div class="wrap winners2012_con">
	<h1>2012 Winners</h1>
	<span>Organizers of the 2012 Professional BC Wedding Awards handed out 24 trophies on November 28, 2012 at the River Rock Theatre. The winners are those companies with the highest scoring entries. There were a maximum of 2 finalists per category.</span><br><br>
	<p><small>Please note that in some cases, only part of the entry material is shown. All entry text information has been omitted online. Photographer credit was not known for all entries, it has been provided wherever possible.</small></p>
	<div class="list_2column">
		<span class="winners_list left_justify">
			<a href='#1'>Best Wedding Cake</a>
			<a href='#2'>Best Bridal Bouquet</a>
			<a href='#3'>Best Wedding Florist - Overall</a>
			<a href='#4'>Best Wedding Make-up</a>
			<a href='#5'>Best Wedding Hair Style</a>
			<a href='#6'>Best Wedding Decor</a>
			<a href='#7'>Best Wedding Jewelry Design - Accessories</a>
			<a href='#8'>Best Wedding Jewelry Design - Rings</a>
			<a href='#9'>Best Wedding Transportation</a>
			<a href="#10">Best Wedding Stationery</a>
			<a href='#11'>Best Wedding Reception Venue - Alternative Location</a>
			<a href='#12'>Best Wedding Reception Venue - Hotel or Banquet Hall</a>
		</span>
		<span class="winners_list left_justify">
			<a href='#13'>Best Wedding Ceremony Location</a>
			<a href='#14'>Best Wedding Event Planning</a>
			<a href='#114'>Best Catered Wedding</a>
			<a href='#15'>Best Candid/Photojournalism Photograph</a>
			<a href='#16'>Best Portrait- Bride and Groom Together</a>
			<a href='#17'>Best Wedding Detail Photograph</a>
			<a href='#18'>Best Wedding Group Photograph</a>
			<a href='#19'>Best Overall Wedding Photography</a>
			<a href='#20'>Best Wedding Videographer/Cinematographer</a>
			<a href='#22'>2012 Tasters Choice - Best Hors D’Oeuvre Award Winner</a>
			<a href='#23'>2012 Judges Choice - Best Hors D’Oeuvre Award Winner</a>
			<a href='#21'>2012 Industry Achievement Award Winner</a>	
		</span>
	</div>
	<br><br>
	<span style="text-align: center;">
		<p><strong>Winners:</strong> The winners are those companies with the highest score.</p>
		<p><strong>Finalists:</strong> In order to qualify as a finalist the score needs to be within 10% of the winning score.  There is a maximum of 2 finalists per category.</p>
	</span>	
	<div class="winner">
		<h2><strong><a name="1"></a>Best Wedding Cake</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Anna Elizabeth Cakes</span><br/>
			<a href="http://www.annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a><br/><br/>
			<img src="winners2012/wc1.jpg" width="400" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Whisk Cake Company</span><br/>
				<a href="http://www.whiskcakes.com" target="_blank">www.whiskcakes.com</a></p>
				<p align="center"><img src="winners2012/wc2.jpg" width="266" height="400" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Luscious Creations</span><br />
				<a href="http://www.lusciouscreations.ca" target="_blank">www.lusciouscreations.ca</a></p>
				<p align="center"><img src="winners2012/wc3.jpg" width="266" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">The Flower Factory</span><br/>
			<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br/><br/>
			<img src="winners2012/bbb1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Bliss Event + Design</span><br/>
				<a href="http://www.blissevent.ca" target="_blank">www.blissevent.ca</a></p>
				<p align="center"><img src="winners2012/bbb2.jpg" width="400" height="266"  border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Verbena Floral Design</span><br />
				<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a></p>
				<p align="center"><img src="winners2012/bbb3.jpg" width="400" height="266" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">The Flower Factory</span><br/>
			<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br/><br/>
			<img src="winners2012/wf1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Verbena Floral Design</span><br/>
				<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a></p>
				<p align="center"><img src="winners2012/wf3.jpg" width="400" height="266" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Sunflower Florist</span><br />
				<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>
				<p align="center"><img src="winners2012/wf2.jpg" width="400" height="266" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="4"></a>Best Wedding Make-up</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Pink Orchid Studio</span><br/>
			<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a><br/><br/>
			<img src="winners2012/ma1.jpg" width="458" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">ma-luxe</span><br/>
				<a href="http://www.ma-luxe.com" target="_blank">www.ma-luxe.com</a></p>
				<p align="center"><img src="winners2012/ma2.jpg" width="350" height="350" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Mink Makeup + Hair</span><br />
				<a href="http://www.minkmakeupart.com" target="_blank">www.minkmakeupart.com</a></p>
				<p align="center"><img src="winners2012/ma3.jpg" width="350" height="350" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="5"></a>Best Wedding Hair Style</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Mink Makeup + Hair</span><br/>
			<a href="http://www.minkmakeupart.com" target="_blank">www.minkmakeupart.com</a><br/><br/>
			<img src="winners2012/hs1.jpg" width="400" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Felicia Bromba</span><br/>
				<a href="http://www.brides-by-felicia.com" target="_blank">www.brides-by-felicia.com</a></p>
				<p align="center"><img src="winners2012/hs2.jpg" width="267" height="400" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Pink Orchid Studio</span><br />
				<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a></p>
				<p align="center"><img src="winners2012/hs3.jpg" width="267" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="6"></a>Best Wedding Decor</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Art of The Party</span><br/>
			<a href="http://www.artoftheparty.ca" target="_blank">www.artoftheparty.ca</a><br/><br/>
			<img src="winners2012/wd1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>						
	</div>
	<div class="winner">
		<h2><strong><a name="7"></a>Best Wedding Jewelry Design - Accessories</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Whisper Jewels</span><br/>
			<a href="http://www.whisperjewels.com" target="_blank">www.whisperjewels.com</a><br/><br/>
			<img src="winners2012/jd1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Elsa Corsi  |  Beautiful Jewellery</span><br/>
				<a href="http://www.elsacorsi.com" target="_blank">www.elsacorsi.com</a></p>
				<p align="center"><img src="winners2012/jd2.jpg" width="400" height="264" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">The Sonja Picard Collection</span><br/>
				<a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com</a></p>
				<p align="center"><img src="winners2012/jd3.jpg" width="400" height="264" border="0"><br/></p>
			</div>
		</div>					
		<p align="right"><a href="winners2012.php" >return to category listing</a></p>		
	</div>
	<div class="winner">
		<h2><strong><a name="8"></a>Best Wedding Jewelry Design - Rings</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Jewellery Artists 3D</span><br/>
			<a href="http://www.ja3d.com" target="_blank">www.ja3d.com</a><br/><br/>
			<img src="winners2012/jdr1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic" style="width: 100%">
				<p align="center"><span class="company">The Sonja Picard Collection</span><br/>
				<a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com</a></p>
				<p align="center"><img src="winners2012/jdr2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a></p>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="9"></a>Best Wedding Transportation</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Ultimate Limousine</span><br/>
			<a href="http://www.ultimatelimo4you.com" target="_blank">www.ultimatelimo4you.com</a><br/><br/>
			<img src="winners2012/wt1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>			
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="10"></a>Best Wedding Stationery</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Love by Phoebe</span><br/>
			<a href="http://www.lovebyphoebe.com" target="_blank">www.lovebyphoebe.com</a><br/><br/>
			<img src="winners2012/sd1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Sunlit Letter Press</span><br/>
				<a href="http://www.sunlit-letterpress.com" target="_blank">www.sunlit-letterpress.com</a></p>
				<p align="center"><img src="winners2012/sd2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Uniquity Invitations</span><br />
				<a href="http://www.uniquityinvitations.com" target="_blank">www.uniquityinvitations.com</a></p>
				<p align="center"><img src="winners2012/sd3.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="11"></a>Best Wedding Reception Venue - Alternative Location</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Capilano Suspension Bridge</span><br/>
			<a href="http://www.capbridge.com" target="_blank">www.capbridge.com</a><br/><br/>
			<img src="winners2012/rva1.jpg" width="400" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">The Wedding Yacht</span><br/>
				<a href="http://www.theweddingyacht.com" target="_blank">www.theweddingyacht.com</a></p>
				<p align="center"><img src="winners2012/rva3.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">John M.S. Lecky UBC Boathouse</span><br />
				<a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>
				<p align="center"><img src="winners2012/rva2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">The Vancouver Club</span><br/>
			<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a><br/><br/>
			<img src="winners2012/rv1.jpg" width="600" height="386" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Pinnacle Hotel at the Pier</span><br/>
				<a href="http://www.pinnaclepierhotel.com" target="_blank">www.pinnaclepierhotel.com</a></p>
				<p align="center"><img src="winners2012/rv2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Terminal City Club</span><br />
				<a href="http://www.tcclub.com" target="_blank">www.tcclub.com/plan</a></p>
				<p align="center"><img src="winners2012/rv3.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Rockwater Secret Cove Resort</span><br/>
			<a href="http://www.rockwatersecretcoveresort.com" target="_blank">www.rockwatersecretcoveresort.com</a><br/><br/>
			<img src="winners2012/cl1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Pinnacle Hotel at the Pier</span><br/>
				<a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>
				<p align="center"><img src="winners2012/cl2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">The Chapel Group - Minoru Chapel - Minoru Chapel</span><br />
				<a href="http://www.thechapels.ca" target="_blank">www.thechapels.ca</a></p>
				<p align="center"><img src="winners2012/cl3.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="14"></a>Best Wedding Event Planning</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">White Dahlia Weddings</span><br/>
			<a href="http://www.whitedahliaweddings.com" target="_blank">www.whitedahliaweddings.com</a><br/><br/>
			<img src="winners2012/ep1.jpg" width="400" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Filosophi Events<</span><br/>
				<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a></p>
				<p align="center"><img src="winners2012/ep2.jpg" width="261" height="400" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Lori L Fraser Signature Events</span><br />
				<a href="http://www.lorilfrasersignatureevents.com" target="_blank">www.lorilfrasersignatureevents.com</a></p>
				<p align="center"><img src="winners2012/ep3.jpg" width="267" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="114"></a>Best Catered Wedding</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Emelle's Catering Ltd.</span><br/>
			<a href="http://www.emelles.com" target="_blank">www.emelles.com</a><br/><br/>
			<img src="winners2012/cw.jpg" width="266" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="15"></a>Best Candid/Photojournalism Photograph</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Leanne Pedersen Photographers Ltd.</span><br/>
			<a href="http://www.leannepedersen.com" target="_blank">www.leannepedersen.com</a><br/><br/>
			<img src="winners2012/pj1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">StonePhoto</span><br/>
				<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a></p>
				<p align="center"><img src="winners2012/pj2.jpg" width="400" height="266" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Michael Wachniak</span><br />
				<a href="http://www.michaelwachniak.com" target="_blank">www.michaelwachniak.com</a></p>
				<p align="center"><img src="winners2012/pj3.jpg" width="400" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="16"></a>Best portrait: Bride and Groom Together</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Pardeep Singh Photography</span><br/>
			<a href="http://www.pardeepsingh.ca" target="_blank">www.pardeepsingh.ca"</a><br/><br/>
			<img src="winners2012/bg1.jpg" width="400" height="600" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Jen Steele Photography</span><br/>
				<a href="http://www.jensteele.com" target="_blank">www.jensteele.com</a></p>
				<p align="center"><img src="winners2012/bg2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Randal Kurt Photography</span><br />
				<a href="http://www.randalkurt.com" target="_blank">www.randalkurt.com</a></p>
				<p align="center"><img src="winners2012/bg3.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="17"></a>Photography &#8211; Best Wedding Detail Photograph</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">John Bello Photographer</span><br/>
			<a href="http://www.bohnjello.com" target="_blank">www.bohnjello.com</a><br/><br/>
			<img src="winners2012/d1.jpg" width="600" height="400" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Butter Studios</span><br/>
				<a href="http://www.butterstudios.ca" target="_blank">http://www.butterstudios.ca</a></p>
				<p align="center"><img src="winners2012/d2.jpg" width="300" height="400" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Brellow Productions</span><br />
				<a href="http://www.brellowproductions.com" target="_blank">www.brellowproductions.com</a></p>
				<p align="center"><img src="winners2012/d3.jpg" width="267" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="18"></a>Photography &#8211; Best Wedding Group Photograph</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Erin Wallis Photography</span><br/>
			<a href="http://www.erinwallis.com" target="_blank">www.erinwallis.com</a><br/><br/>
			<img src="winners2012/g1.jpg" width="600" height="400"" border="0">
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Abby Photography</span><br/>
				<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a></p>
				<p align="center"><img src="winners2012/g2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Raymond & Jessie Photography</span><br />
				<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
				<p align="center"><img src="winners2012/g3.jpg" width="400" height="223" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="19"></a>Photography &#8211; Best Overall Wedding Photography</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">JONETSU STUDIOS</span><br/>
			<a href="http://www.jonetsuphotography.com" target="_blank">www.jonetsuphotography.com</a><br/><br/>
			<img src="winners2012/o1.jpg" width="529" height="600" border="0"><br/><br/>
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Shari + Mike Photographers</span><br/>
				<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
				<p align="center"><img src="winners2012/o2.jpg" width="400" height="267" border="0"><br/></p>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Daniela Ciuffa Photography</span><br />
				<a href="http://www.ciuffaphotography.com" target="_blank">www.ciuffaphotography.com</a></p>
				<p align="center"><img src="winners2012/o3.jpg" width="286" height="400" border="0"><br/></p>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>						
	</div>
	<div class="winner">
		<h2><strong><a name="20"></a>Best Wedding Videography/Cinematography</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Modern Romance Productions</span><br/>
			<a href="http://www.modernromance.ca" target="_blank">www.modernromance.ca</a><br/><br/>
			<iframe src="http://player.vimeo.com/video/50351368" width="60%" min-width="500" height="382" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe><br />
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>
		<div>
			<div class="other_pic">
				<p align="center"><span class="company">Perfect Pair Media</span><br/>
				<a href="http://www.perfectpairmedia.ca" target="_blank">www.perfectpairmedia.ca</a></p>
				<iframe src="http://player.vimeo.com/video/54001492" width="90%" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe><br>
			</div>
			<div class="other_pic">
				<p align="center"><span class="company">Glimmer Films Inc.</span><br />
				<a href="http://www.glimmerfilms.ca" target="_blank">www.glimmerfilms.ca</a></p>
				<iframe src="http://player.vimeo.com/video/51266853" width="90%" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
			</div>
			<p align="right"><a href="winners2012.php" >return to category listing</a></p>
		</div>							
	</div>
	<div class="winner">
		<h2><strong><a name="21"></a>Industry Achievement Award</strong></h2>
		<div class="winner_pic">
			<a href="http://www.flower-z.com" target="_blank">
			<img src="winners2012/evan.jpg" border="0" ></a><br />
			<span class="company">Evan Orion - Flowerz</span><br />
			<a href="http://www.flower-z.com" target="_blank">www.flower-z.com</a>
			<p align="center" class="company">Other Notable Individuals who were voted for an Industry Achievement Award<br />
			(Random)</p>
			<p align="center" class="company">
			Alicia Keats<br>
			Colin Upright<br>
			Devon Hird<br>
			Lisa Gratton<br>
			Soha Lavin<br>
			Erin Bishop<br>
			Corrine Colledge
			<p align="center">&nbsp;</p>
		</div>
		<div>
			<p align="right"><a href="winners2012.php" >return to category listing</a>
		</div>
	</div>
	<div class="winner">
		<h2><strong><a name="22"></a>2012 Tasters Choice - Best Hors D’Oeuvre Award Winner</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Truffles Fine Foods</span><br/>
			<a href="http://www.trufflesfinefoods.com" target="_blank">www.trufflesfinefoods.com</a><br/><br/>
			<p>Poached Quail Egg Served on an Indian Candy Rosti with Herb Foam and Caviar</p>
			<p align="center"><img src="winners2012/trufflesfinefoods.jpg" width="600" height="300" border="0" alt="photo credit"></p><br />
			<p><img src="winners2011/divider.png"></p>
			<p class="finalist"><em>Finalists</em></p>
		</div>						
	</div>
	<div class="winner">
		<h2><strong><a name="23"></a>2012 Judges Choice - Best Hors D’Oeuvre Award Winner</strong></h2>
		<div class="winner_pic">
			<p>
			<span class="company">Emmelle's</span><br/>
			<a href="http://www.emelles.com" target="_blank">www.emelles.com</a><br/><br/>
			<p>Blue Cheese & Walnut Dusted Ruby & Golden Beet Duet Nestled on a Parmesean Tuille & Bed of Arugula Chiffonade.</p>
			<p align="center"><img src="winners2012/emelles.jpg" width="453" height="600" border="0" alt="photo credit"></p><br />
		</div>						
	</div>
	<div class="winner">
		<h2><strong>PARTICIPANTS</strong></h2>
		<div class="column_3">
			<div class="winner_pic">
				<div>
					<span class="company">Kale and Nori</span><br>
					<a href="http://www.kaleandnori.com/" target="_blank">www.kaleandnori.com</a><br/><br>
					<p>Korean Beef ’meatloaf’ Served with Crispy Bourbon Barrel Aged Fish Sauce and Sweet and Hot Okanagan Chili Pepper Kim Chi</p>
				</div><br>				
				<p align="center"><img src="http://www.bcweddingawards.com/winners2012/kaleandnori.jpg" width="300" height="451" border="0"></p>
			</div>	
			<div class="winner_pic">
				<div>
					<span class="company">Edgeceptional Catering</span><br>
					<a href="http://edgecatering.ca" target="_blank">edgecatering.ca</a><br/><br/>
					<p>Edge Made Duck Proscuitto on a Phyllo Spoon with Fig Compote & Orange Glaze, served with microgreens</p>
				</div><br>			
				<p align="center"><img src="winners2012/edgeceptional.jpg" width="300" height="451" border="0" alt="photo credit"></p>
			</div>	
			<div class="winner_pic">
				<div>
					<span class="company">River Rock Catering</span><br/>
					<a href="http://www.riverrock.com" target="_blank">www.riverrock.com</a><br/><br/>
					<p>Qualicum Beach Scallop Tartar with Spinach & Black Sesame Cone - Wasabi  Mayonnaise Flying Fish Rose, Kaiware Sprouts</p>
				</div><br>				
				<p align="center"><img src="winners2012/rr.jpg" width="300" height="451" border="0" alt="photo credit"></p>
			</div>	
		</div>
							
	</div>
	<div class="final_list">		
		<span align="center">
			<p>Our sincere thank-you to each of this years competing caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had. The bar has been raised once again!</p>
			<br><img src="winners2012/cateringjudges2012.jpg" border="0" align="center" ><br>
			<b>Special Thanks also to our Judges - (l to r)</b><br>Mijune Pak of <a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br>Julian Bond of  <a href="www.picachef.com/" target="_blank"> Pacific Institute of Culinary Arts</a><br>Lindsay Anderson of 
			<a href="http://www.365daysofdining.com/" target="_blank">365 Days of Dining!</a><br>
			<p>Our most sincere congratulations to all of the winners and finalists, well done!</p>
			<a href="pastwinners.php"><img src="siteimages/pastwinnersheader.png" width="250" height="75" border="0"></a>
		</span>
	</div>
</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>

</body>
</html>
