<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
<script type="text/javascript">
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}
function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}
var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		home_over = newImage("../menuimages/home-over.jpg");
		blog_over = newImage("../menuimages/blog-over.jpg");
		categories_over = newImage("../menuimages/categories-over.jpg");
		judging_over = newImage("../menuimages/judging-over.jpg");
		faq_over = newImage("../menuimages/faq-over.jpg");
		events_over = newImage("../menuimages/events-over.jpg");
		contact_over = newImage("../menuimages/contact-over.jpg");
		preloadFlag = true;
	}
}
</script>
</head>
<body onLoad="preloadImages();">
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<a name="categories"></a>
	<div class="content">
		<div class="container">
			<div class="wrap pastwin_con">
				<h1>Award Recipients</h1>
				<p>Here are the Winners and Finalists for each year of the Professional BC Wedding Awards. We are proud to showcase their winning entries and hope that this will be a useful reference page for Wedding Professionals and the general public.</p>
				<span>
					<a href="winners2015.php">
						<h1>2015</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
				<span>
					<a href="winners2014.php">
						<h1>2014</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
				<span>
					<a href="winners2013.php">
						<h1>2013</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
				<span>
					<a href="winners2012.php">
						<h1>2012</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
				<span>
					<a href="winners2011.php">
						<h1>2011</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
				<span>
					<a href="winners2010.php">
						<h1>2010</h1>
						<h3>WINNERS & FINALISTS</h3>
					</a>
				</span>
			</div>
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
</body>
</html>

