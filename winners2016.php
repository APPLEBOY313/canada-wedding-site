<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
<script type="text/javascript">
	function newImage(arg) {
		if (document.images) {
			rslt = new Image();
			rslt.src = arg;
			return rslt;
		}
	}
	function changeImages() {
		if (document.images && (preloadFlag == true)) {
			for (var i=0; i<changeImages.arguments.length; i+=2) {
				document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
			}
		}
	}
	var preloadFlag = false;
	function preloadImages() {
		if (document.images) {
			home_over = newImage("menuimages/home-over.jpg");
			blog_over = newImage("menuimages/blog-over.jpg");
			categories_over = newImage("menuimages/categories-over.jpg");
			judging_over = newImage("menuimages/judging-over.jpg");
			faq_over = newImage("menuimages/faq-over.jpg");
			events_over = newImage("menuimages/events-over.jpg");
			contact_over = newImage("menuimages/contact-over.jpg");
			preloadFlag = true;
		}
	}
</script>
</head>
<body onLoad="preloadImages();">
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<a name="categories"></a>
	<div class="content">
		<div class="container">
			<div class="wrap winners2016_con">
				<h1>2016 Winners</h1>
				<span>Organizers of the 2016 Professional BC Wedding Awards handed out trophies on November 23, 2016 at The Hard Rock Molson Canadian Casino Theatre in Coquitlam. The winners are those companies with the highest scoring entries. There is a maximum of 2 finalists per category, and they are listed here in alphabetical order. </span><br><br>
				<p>Please note that in most cases, only a small part of the entry material is shown publicly. Photographer credit was not known for all entries, it has been provided wherever possible.</p>
				<span class="winners_list">
					<a href='#1'>Best Wedding Cake</a>
					<a href='#2'>Best Bridal Bouquet</a>
					<a href='#3'>Best Wedding Florist - Overall</a>
					<a href='#4'>Best Wedding Make-up</a>
					<a href='#6'>Best Wedding Hair Style</a>
					<a href='#8'>Best Wedding Hair & Make-up - South Asian</a>
					<a href='#9'>Best Wedding Decor</a>
					<a href="#10">Best Wedding Stationery</a>
					<a href='#11'>Best Wedding Reception Venue</a>
					<a href='#12'>Best Wedding Reception Venue - Hotel or Banquet Hall</a>
					<a href='#13'>Best Wedding Ceremony Location</a>
					<a href='#14'>Best Wedding Officiant</a>
					<a href='#15'>Best Wedding Event Planning</a>
					<a href='#16'>Best Catered Wedding</a>
					<a href='#18'>Best Wedding DJ</a>
					<a href='#19'>Best Photo Booth - Photo Sequence</a>
					<a href='#20'>Best Candid/Photojournalism Photograph</a>
					<a href='#21'>Best Portrait- Bride and Groom Together</a>
					<a href='#22'>Best Wedding Detail Photograph</a>
					<a href='#23'>Best Wedding Group Photograph</a>
					<a href='#24'>Best Overall Wedding Photography</a>
					<a href='#25'>Best Wedding Videographer/Cinematographer </a>
					<a href='#27'>Best Edited Wedding Video</a>
					<a href='#28'>2016 Best Tasting Wedding Cake</a>
					<a href='#29'>2016 Tasters Choice - Best Hors D’Oeuvre</a>
					<a href='#31'>2016 Industry Achievement Award Winner</a>
					<p>Winners: The winners are those companies with the highest score.</p>
					<p>Finalists: In order to qualify as a finalist the score needs to be within 10% of the winning score.<br>There is a maximum of 2 finalists per category.</p>
				</span>
				<div class="winner">
					<h2><strong><a name="1"></a>Best Wedding Cake</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Momo Chen Cakes</span><br/>
						<a href="http://www.momochencakes.com" target="_blank">www.momochencakes.com</a><br/><br/>
						<img src="winners2016/images/wc1_6596_2.jpg" width="467" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">A. Elizabeth Cakes</span><br/>
							<a href="http://www.aelizabethcakes.com" target="_blank">www.aelizabethcakes.com</a></p>
							<p align="center"><img src="winners2016/images/wc1_4589_1.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Whisk Cake Company</span><br />
							<a href="http://www.whiskcakes.com" target="_blank">www.whiskcakes.com</a></p>
							<p align="center"><img src="winners2016/images/wc1_5678_1.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Verbena Floral Design</span><br/>
						<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a><br/><br/>
						<img src="winners2016/images/bb1_8390_2.jpg" width="420" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Crocus Floral Design</span><br/>
							<a href="http://crocusfloraldesign.com" target="_blank">www.crocusfloraldesign.com</a></p>
							<p align="center"><img src="winners2016/images/bb1_2244_1.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Flower Factory</span><br />
							<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a></p>
							<p align="center"><img src="winners2016/images/bb1_0003_1crop.jpg" width="298" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Verbena Floral Design</span><br/>
						<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a><br/><br/>
						<img src="winners2016/images/wf1_8390_12.jpg" width="600" height="400"border="0">
						<img src="winners2016/images/BOFWinnerCollage.jpg" width="600" height="240" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Crocus Floral Design</span><br/>
							<a href="http://www.crocusfloraldesign.com" target="_blank">www.crocusfloraldesign.com</a></p>
							<p align="center"><img src="winners2016/images/wf1_2244_5.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Flower Factory</span><br />
							<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a></p>
							<p align="center"><img src="winners2016/images/wf1_0003_6.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="4"></a>Best Wedding Make-up</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Denise Elliott Beauty Co.</span><br/>
						<a href="http://www.deniseelliott.ca" target="_blank">www.deniseelliott.ca</a><br/><br/>
						<img src="winners2016/images/ma1_9272_1.jpg" width="447" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Blue Jasmine Beauty Group</span><br/>
							<a href="http://www.jasminehoffman.com" target="_blank">www.jasminehoffman.com</a></p>
							<p align="center"><img src="winners2016/images/ma1_3832_5crop.jpg" width="400" height="276" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Felicia Bromba - Makeup and Hair</span><br />
							<a href="http://www.makeup-by-felicia.com" target="_blank">www.makeup-by-felicia.com</a></p>
							<p align="center"><img src="winners2016/images/ma1_5505_2crop.jpg" width="400" height="276" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Hair Style</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">All Dolled Up Makeup & Hair Studio</span><br/>
						<a href="http://www.alldolledupstudio.ca" target="_blank">www.alldolledupstudio.ca</a><br/><br/>
						<img src="winners2016/images/hs1_4628_4.jpg" width="407" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Denise Elliott Beauty Co.</span><br/>
							<a href="http://www.deniseelliott.ca" target="_blank">www.deniseelliott.ca</a></p>
							<p align="center"><img src="winners2016/images/hs1_9272_4crop.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Nadia Albano Style Inc.</span><br />
							<a href="http://www.nadiaalbano.com" target="_blank">www.nadiaalbano.com</a></p>
							<p align="center"><img src="winners2016/images/hs1_3016_1.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="8"></a>Best South Asian Bride – Hair & Makeup</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Pink Orchid Studio</span><br/>
						<a href="http://pinkorchidstudio.com target="_blank">pinkorchidstudio.com</a><br/><br/>
						<img src="winners2016/images/sa1_6242_1.jpg" width="600" height="400" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Girlfriendz Studio 7</span><br/>
							<a href="http://girlfriendzstudio7.com" target="_blank">www.girlfriendzstudio7.com</a></p>
							<p align="center"><img src="winners2016/images/sa1_5993_1.jpg" width=266 height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Salon Picasso Bridal Studio</span><br />
							<a href="http://salonpicasso.ca" target="_blank">www.salonpicasso.ca</a></p>
							<p align="center"><img src="winners2016/images/sa1_3712_4.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="9"></a>Best Wedding Decor</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Cahoots Creative</span><br/>
						<a href="http://www.cahootscreative.ca" target="_blank">www.cahootscreative.ca</a><br/><br/>
						<img src="winners2016/images/wd1_2080_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Bespoke Decor</span><br/>
							<a href="http://bespokedecor.ca" target="_blank">www.bespokedecor.ca</a></p>
							<p align="center"><img src="winners2016/images/wd01_bespoke.jpg" width="300" height="425" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Vintage Origami</span><br />
							<a href="http://www.vintageorigami.com" target="_blank">www.vintageorigami.com</a></p>
							<p align="center"><img src="winners2016/images/wd1_0070_9.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="10"></a>Best Wedding Stationery</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Love by Phoebe</span><br/>
						<a href="http://www.lovebyphoebe.com" target="_blank">www.lovebyphoebe.com</a><br/><br/>
						<img src="winners2016/images/sd1_8884_3.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Ginger Creative</span><br/>
							<a href="http://www.gingercreative.ca" target="_blank">www.gingercreative.ca</a></p>
							<p align="center"><img src="winners2016/images/sd1_0159_3.jpg" width="400" height="277" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Stationery Bike Designs</span><br />
							<a href="http://www.stationerybikedesigns.com" target="_blank">www.stationerybikedesigns.com</a></p>
							<p align="center"><img src="winners2016/images/sd1_3760_5.jpg" width="300" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="11"></a>Best Wedding Reception Venue</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Bird's Eye Cove Farm</span><br/>
						<a href="http://www.birdseyecovefarm.com" target="_blank">www.birdseyecovefarm.com</a><br/><br/>
						<img src="winners2016/images/rv2_6379_6.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">John M.S. Lecky UBC Boathouse</span><br/>
							<a href="http://www.ubcboathouse.com" target="_blank">www.aelizabethcakes.comwww.ubcboathouse.com</a></p>
							<p align="center"><img src="winners2016/images/rv2_2011_1.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Rustic Wedding</span><br />
							<a href="http://www.rusticwedding.ca" target="_blank">www.rusticwedding.ca</a></p>
							<p align="center"><img src="winners2016/images/rv2_0752_4.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Fairmont Hotel Vancouver</span><br/>
						<a href="http://www.fairmont.com/hotel-vancouver/meetings-weddings/weddings/" target="_blank">www.fairmont.com</a><br/><br/>
						<img src="winners2016/images/rv1_1811_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Inn at Laurel Point</span><br/>
							<a href="http://www.laurelpoint.com" target="_blank">www.laurelpoint.com</a></p>
							<p align="center"><img src="winners2016/images/rv1_6752_5.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Vancouver Club</span><br />
							<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a></p>
							<p align="center"><img src="winners2016/images/rv1_7008_3.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Bird's Eye Cove Farm</span><br/>
						<a href="http://www.birdseyecovefarm.com" target="_blank">www.birdseyecovefarm.com</a><br/><br/>
						<img src="winners2016/images/cl1_6379_1.jpg" width="400" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brock House Restaurant</span><br/>
							<a href="http://www.brockhouserestaurant.com" target="_blank">www.brockhouserestaurant.com</a></p>
							<p align="center"><img src="winners2016/images/cl1_3317_7.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Stanley Park Pavilion</span><br />
							<a href="http://www.stanleyparkpavilion.com" target="_blank">www.stanleyparkpavilion.com</a></p>
							<p align="center"><img src="winners2016/images/cl1_7474_8.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="14"></a>Best Wedding Officiant</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shawn Miller - Young, Hip & Married</span><br/>
						<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a><br/><br/>
						<img src="winners2016/images/wo1_2100_1.jpg" width="400" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Celebrant Lisa Hartley<br />Simply Ceremony</span><br/>
							<a href="http://www.lisahartley.com" target="_blank">www.lisahartley.com</a></p>
							<p align="center"><img src="winners2016/images/wo1_9105_1crop.jpg" width="290" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><br/><span class="company">Randy Hamm - Young, Hip & Married</span><br />
							<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a></p>
							<p align="center"><img src="winners2016/images/wo1_2449_2crop.jpg" width="300" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="15"></a>Best Wedding Event Planning</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Filosophi Events</span><br/>
						<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a><br/><br/>
						<img src="winners2016/images/ep1_3746_1.jpg" width="400" height="600" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Alicia Keats Weddings and Events</span><br/>
							<a href="http://www.aliciakeats.com" target="_blank">www.aliciakeats.com</a></p>
							<p align="center"><img src="winners2016/images/ep1_2487_4.jpg" width="300" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Smitten Events</span><br />
							<a href="http://www.smittenevents.ca" target="_blank">www.smittenevents.ca</a></p>
							<p align="center"><img src="winners2016/images/ep1_8824_1.jpg" width="300" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="16"></a>Best Catered Wedding</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Toque Catering</span><br/>
						<a href="http://www.toquecatering.com" target="_blank">www.toquecatering.com</a><br/><br/>
						<img src="winners2016/images/fc1_5256_3.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Savoury City Catering + Events</span><br/>
							<a href="http://www.savourycity.com" target="_blank">www.savourycity.com</a></p>
							<p align="center"><img src="winners2016/images/fc1_8484_8.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Truffles Fine Foods</span><br />
							<a href="http://www.trufflesfinefoods.com" target="_blank">www.trufflesfinefoods.com</a></p>
							<p align="center"><img src="winners2016/images/fc1_5945_4crop.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="18"></a>Best Wedding DJ</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">X-Fusion Roadshow</span><br/>
						<a href="http://www.xfusionroadshow.com" target="_blank">www.xfusionroadshow.com</a><br/><br/>
						<img src="winners2016/images/dj1_9645_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Decibel Entertainment</span><br/>
							<a href="http://www.decibelentertainment.com" target="_blank">www.decibelentertainment.com</a></p>
							<p align="center"><img src="winners2016/images/dj1_2202_3sharp.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Dynamic Weddings</span><br />
							<a href="http://www.dynamicweddings.ca" target="_blank">www.dynamicweddings.ca</a></p>
							<p align="center"><img src="winners2016/images/dj1_dynamicWed_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="19"></a>Best Photo Booth - Photo Sequence</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Adam and Kev Weddings</span><br/>
						<a href="http://wwww.adamandkevweddings.com" target="_blank">wwww.adamandkevweddings.com</a><br/><br/>
						<img src="winners2016/images/adamandkev.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Butter Studios</span><br/>
							<a href="http://www.butterstudios.ca" target="_blank">www.butterstudios.ca</a></p>
							<p align="center"><img src="winners2016/images/pb1_5333_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Four Frames Photo Booth</span><br />
							<a href="http://www.FourFramesPhotoBooth.com" target="_blank">www.FourFramesPhotoBooth.com</a></p>
							<p align="center"><img src="winners2016/images/fourFrames.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="20"></a>Best Candid/Photojournalism Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Caroline Ross Photography</span><br/>
						<a href="http://www.carolinephotography.ca" target="_blank">www.carolinephotography.ca</a><br/><br/>
						<img src="winners2016/images/bp2_5312_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Justine Boulin Photography</span><br/>
							<a href="http://www.justineboulin.com" target="_blank">www.justineboulin.com</a></p>
							<p align="center"><img src="winners2016/images/bp2_1342_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Shari + Mike Photographers</span><br />
							<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
							<p align="center"><img src="winners2016/images/rv1_7008_3.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="21"></a>Best Portrait - Bride and Groom together</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Jelger and Tanja Photographers - Jelger Vitt</span><br/>
						<a href="http://jelgerandtanja.com" target="_blank">www.jelgerandtanja.com</a><br/><br/>
						<img src="winners2016/images/bp3_5512_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">BAKEPHOTOGRAPHY - Melissa Baker</span><br/>
							<a href="http://www.bakephotography.com" target="_blank">www.bakephotography.com</a></p>
							<p align="center"><img src="winners2016/images/bp3_6345_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Apartment Photography - Jeff Chang</span><br />
							<a href="http://www.theapartmentphotography.com" target="_blank">www.theapartmentphotography.com</a></p>
							<p align="center"><img src="winners2016/images/bp3_0911_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="22"></a>Best Wedding Detail Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Koyo Photography - Lindsay Naito</span><br/>
						<a href="http://www.koyophotography.com" target="_blank">www.koyophotography.com</a><br/><br/>
						<img src="winners2016/images/bp5_0767_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Jasalyn Thorne Photography</span><br/>
							<a href="http://www.jasalynthornephotography.com" target="_blank">www.jasalynthornephotography.com</a></p>
							<p align="center"><img src="winners2016/images/bp5_6598_1.jpg" width="400" height="230" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Apartment Photography</span><br />
							<a href="http://www.theapartmentphotography.com" target="_blank">www.theapartmentphotography.com</a></p>
							<p align="center"><img src="winners2016/images/bp5_0911_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="23"></a>Best Wedding Group Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Adam and Kev Weddings - Kev Holloway</span><br/>
						<a href="http://adamandkevweddings.com" target="_blank">www.adamandkevweddings.com</a><br/><br/>
						<img src="winners2016/images/bp4_4131_1.jpg" width="600" height="400" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Blush Photography - Angela Waterberg</span><br/>
							<a href="http://photosbyblush.com" target="_blank">www.photosbyblush.com</a></p>
							<p align="center"><img src="winners2016/images/bp4_8630_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Shari + Mike Photographers - Shari Vallely</span><br />
							<a href="http://shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
							<p align="center"><img src="winners2016/images/bp4_6645_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="24"></a>Best Overall Wedding Photography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shari + Mike Photographers</span><br/>
						<a href="http://shariandmike.ca" target="_blank">www.shariandmike.ca</a><br/><br/>
						<img src="winners2016/images/bp1_6645_2.jpg" width="600" height="400" border="0"><br/><br/>
						<img src="winners2016/images/BOWPWinnerCollage.jpg" width="800" height="396" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Justine Boulin Photography</span><br/>
							<a href="http://www.justineboulin.com" target="_blank">www.justineboulin.com</a></p>
							<p align="center"><img src="winners2016/images/bp1_1342_12.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Rivkah Photography - Rebecca Carroll</span><br />
							<a href="http://www.rivkahphotography.ca" target="_blank">www.rivkahphotography.ca</a></p>
							<p align="center"><img src="winners2016/images/bp1_3686_10.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="25"></a>Best Wedding Videography/Cinematography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Hello Tomorrow Wedding Films</span><br/>
						<a href="http://www.hellotomorrow.com" target="_blank">www.hellotomorrow.com</a><br/><br/>
						<iframe src="https://player.vimeo.com/video/193139207" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brellow</span><br/>
							<a href="http://brellow.com" target="_blank">www.brellow.com</a></p>
							<iframe src="https://player.vimeo.com/video/193138136" width="500" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Memento Films</span><br />
							<a href="http://www.mementofilms.ca" target="_blank">www.mementofilms.ca</a></p>
							<iframe src="https://player.vimeo.com/video/193138323" width="500" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="27"></a>Best Edited Wedding Video</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Hello Tomorrow Wedding Films</span><br/>
						<a href="www.hellotomorrow.com" target="_blank">www.hellotomorrow.com</a><br/><br/>
						<iframe src="https://player.vimeo.com/video/193136654" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Memento Films</span><br/>
							<a href="http://www.MementoFilms.ca" target="_blank">www.MementoFilms.ca</a></p>
							<iframe src="https://player.vimeo.com/video/193137041" width="500" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">VanWeddings Inc.</span><br />
							<a href="http://www.vanweddings.com" target="_blank">www.vanweddings.com</a></p>
							<iframe src="https://player.vimeo.com/video/193137642" width="500" height="220" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>
						<p align="right"><a href="winners2016.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="31"></a>Industry Achievement Award</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shawn Miller - Young, Hip & Married</span><br/>
						<a href="http://younghipandmarried.com" target="_blank">www.younghipandmarried.com</a><br/><br/>
						<img src="winners2016/images/shawnMiller.jpg" width="246" height="200" border="0">
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>					
				</div>
				<div class="final_list">
					<h2><strong><a name="28"></a>2016 Best Tasting Wedding Cake Award Winner</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">When Pigs Fly Pastries</span><br/>
						<a href="http://whenpigsflypastries.com" target="_blank">www.whenpigsflypastries.com</a><br/><br/>
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="winner_pic">
							<p align="center"><span class="company">The Cake and the Giraffe</span><br/>
							<a href="http://www.tcandtg.com" target="_blank">www.tcandtg.com</a></p>
						</div>
					</div>
					<h2><strong><a name="29"></a>2016 Best Hors D’Oeuvre Award Winner</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Boy with a Knife Catering</span><br/>
						<a href="http://boywithaknife.ca" target="_blank">www.boywithaknife.ca</a><br/><br/>
						<p><img src="winners2016/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="winner_pic">
							<p align="center"><span class="company">Truffles Fine Foods</span><br/>
						</div>
					</div>
					<h2><strong><a name="29"></a>Best Hors D'Oeuvres PARTICIPANTS</strong></h2>						
				</div>
			</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
</body>
</html>
