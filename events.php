<html>
<head>
<title>The Professional BC Wedding Awards</title>
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
    		<div class="wrap faq_con">
    			<img src="siteimages/EventsPageWeb2016.jpg">
				<h1>Events</h1>
				<span>
					<article class="event_art">
						<p class="sansserif" align="justify">
						<big><b>2016 Annual Awards Night</b></big><br><br>
						The Professional BC Wedding Awards evening is planned annually to celebrate the finest of BC’s wedding industry. The event last year included an awards presentation, entertainment, a  catering competition, as well as an opportunity to network with the finest folks in the industry. All Winners and Finalists will be announced Wednesday, November 23rd, 2016 at the 7th Annual Awards Night that will be held at the Hard Rock Casino Theatre in Coquitlam.</p>
					</article>
					<article class="event_art">
						<p>
							<br><b>Date and Time:</b>  Wednesday, November 23rd, 2016 - Doors open at 6:30pm - 11:30pm (be prompt! - competition hors d'oeuvre tasting at 7pm).<br><br>
							<b>Venue:</b> Hard Rock Casino Theatre - 2080 United Boulevard Coquitlam, BC V3K 6W3 <br><br>
							<b>Dress:</b> Evening Wear/Cocktail Attire<br><br>
							<b>Tickets:</b> Opportunities to RSVP for the event will first be given to submitting companies starting October 16th. Any remaining tickets available as of October 26th will be available for purchase by BC wedding industry vendors.<br><br>
							<b>Transportation:</b> There is plenty of parking available at the venue.  Transit is easy, just take Sky Train station is Braid Station and then the 159 or 791 bus direct to the casino.  
						</p>
					</article>
					<article class="event_art">
						<p>
							<br><b>More Details:</b><br><br>
							&#149; All Winners and Finalists for 2016 will be announced that night!<br><br>
							&#149; Photos and Videos from the winning entries will be shown on screens either side of the main stage.<br><br>
							&#149; The ever popular catering competition is back once again so get your taste buds ready, you will be voting on your favourite!<br><br>
							&#149; Live Entertainment, DJ, Tasty Nibbles, Cash Bar, the event will run until 11:30.<br><br>
							&#149; Coat check will be provided by donation.  100% of proceeds will go to a deserving local charity.<br><br>
							See you there!<br><br/>
						</p>
					</article>
					<article class="event_art">
						<p>
							<br><b>Our Emcee for the 2016 Awards Night:</b><br>	
						</p>
						<p>
							<img src="siteimages/dawnChubai300px.jpg" align="left">
							<b>Dawn Chubai</b><br><br>
							For over a decade, before your coffee is even poured, Dawn Chubai has been starting your morning off with a cheerful smile as Host of Breakfast Television on City. Not up early? Keep an eye on the big screen as well, where Dawn can been seen in several film and television productions as your friendly neighbourhood "Reporter". Love a little night music? An independent Jazz Recording Artist, Dawn Chubai also fronts her own jazz groups, performing at corporate events and recently started her own company, <a href="https://www.facebook.com/KingWillowManagement" target="_blank">King Willow Management</a> with her husband, which provides emcee and music services and specializes in media preparation for chefs, fitness professionals, realtors, winemakers and lifestyle brands.<br><br>
						</p>
					</article>
				</span>
				<div class="partner">
					<img SRC="siteimages/sponsorbar2016.png" ALT="" BORDER=0 usemap="#Map" >
					<map name="Map" id="Map">
					     <area shape="rect" coords="132,63,360,138" href="http://www.realweddings.ca/" target="_blank" alt="Real Weddings Magazine" />
					     <area shape="rect" coords="401,60,491,138" href="http://www.beauphoto.com/" target="_blank" alt="Beau Photo" />
					     <area shape="rect" coords="537,69,740,135" href="http://hyperfocus.ca/" target="_blank" alt="hyperfocus photography" />
					     <area shape="rect" coords="256,160,644,247" href="http://www.hardrockcasinovancouver.com/" target="_blank" alt="Molson Canadian TheatreHard Rock Casino" />
					     <area shape="rect" coords="37,257,251,313" href="http://kingwillowmanagement.com/" target="_blank" />
					     <area shape="rect" coords="292,263,445,329" href="http://pixstarphotobooth.com/" target="_blank" />
					     <area shape="rect" coords="481,257,622,337" href="http://www.koncepteventdesign.com/" target="_blank" />
					     <area shape="rect" coords="661,252,844,311" href="https://audioedge.ca" target="_blank" />
					     <area shape="rect" coords="68,352,225,442" href="http://trufflesfinefoods.com/" target="_blank" />
					     <area shape="rect" coords="241,360,425,444" href="http://fiestacreativecatering.com/" target="_blank" />
					     <area shape="rect" coords="446,359,606,452" href="http://boywithaknife.ca" target="_blank" />
					     <area shape="rect" coords="618,353,833,457" href="http://whitetablecatering.com/" target="_blank" />
					     <area shape="rect" coords="172,469,386,541" href="http://www.mementofilms.ca/" target="_blank" />
					     <area shape="rect" coords="538,472,741,547" href="https://impressionsliveart.com/" target="_blank" />
						 <area shape="rect" coords="294,552,387,677" href="http://www.tcandtg.com/" target="_blank" />
					     <area shape="rect" coords="85,566,254,663" href="http://aelizabethcakes.com/" target="_blank" />
						 <area shape="rect" coords="444,557,580,682" href="http://pinkribbonbakery.ca/" target="_blank" />
					     <area shape="rect" coords="589,566,779,662" href="http://whenpigsflypastries.com/" target="_blank" />
					     <area shape="rect" coords="198,695,341,774" href="http://www.amoriskunawedding.com/" target="_blank" />
					     <area shape="rect" coords="369,691,523,782" href="https://thetuxstore.com/" target="_blank" />
					     <area shape="rect" coords="539,690,684,774" href="http://www.sourcedancecompany.com/" target="_blank" />
					</map>
				</div>
			</div>			
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
	</section>
<?php include("bottom2.inc"); ?>