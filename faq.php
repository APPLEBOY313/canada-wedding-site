<html>
<head>
<title>The Professional BC Wedding Awards</title>
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
    		<div class="wrap faq_con">
    			<img src="siteimages/FAQ2016.jpg">
				<h1>FAQ</h1>
				<span>
					<article>
						<p class="question">When do the submissions open, and when is the deadline for entries?</p>
						<p class="answer">Submissions will open early September 2016.  Work submitted to be judged for the 2016 Professional BC Wedding Awards must be for a wedding that falls between October 1st, 2015 and September 30th, 2016.</p>
					</article>
					<article>
						<p class="question">Who Can Enter?</p>
						<p class="answer">Submissions for the awards is open to wedding related businesses based within the Province of British Columbia that have been in business for a minimum of 2 years as of October 15th, 2016. Each submission requires a submission fee of $68.</p>
					</article>
					<article>
						<p class="question">Do I need to be nominated by someone?</p>
						<p class="answer">No, companies or individuals submit their own work - the main awards are based on judging criteria from our judges, not nominations.</p>
					</article>
					<article>
						<p class="question">Can I submit more than once?</p>
						<p class="answer">Yes, if you would like to submit more than once for a category, or for multiple applicable categories, that's fine. Each submission must be done separately with its own completed form and the submission fee paid.</p>
					</article>
					<article>
						<p class="question">I am making multiple entries, do I need to fill out the form every time?</p>
						<p class="answer">No, all of the form data that you enter with your company details will be held in a cookie on your computer.  As long as you use the same web browser and do not clear out your cookies, your company data will populate the form.</p>
					</article>
					<article>
						<p class="question">I am a wedding industry professional, but I don't see my  category listed, what can I do?</p>
						<p class="answer">We are currently considering new categories. We would love to hear from you with your suggestions. Please get in touch to let us know your thoughts.</p>
					</article>
					<article>
						<p class="question">How do the judges score the entries?</p>
						<p class="answer">The judging panel will be judging each of the entries and awarding scores for several areas based on a range of criteria.  The judges will not have access to the names of the companies for each submission to avoid any bias scoring.</p>
					</article>
					<article>
						<p class="question">Will I be able to find out my score?</p>
						<p class="answer">No, entrants will not be able have access to their score from the judges. However, the winner and finalists for each category will be made public.</p>
					</article>
					<article>
						<p class="question">How do I enter?</p>
						<p class="answer">To enter your company, simply visit the categories page on our website, click on the category that applies to your company and then fill out the appropriate details.</p>
					</article>
					<article>
						<p class="question">Can anyone go to the Awards night?</p>
						<p class="answer">The Awards Night will be open to companies who submit for an award. For the last awards night, we were at capacity. If the venue capacity becomes a problem for the number of attendees then priority will be given to those people who are receiving an award.</p>
					</article>
					<article>
						<p class="question">Am I guaranteed an Awards Night ticket with my submission?</p>
						<p class="answer">For each entry that a company makes, they will be offered a special nominal ticket price of $15. Companies will be contacted by email for an RSVP closer to the event night. Tickets will be available on a first come first served basis up to event capacity.  Any remaining tickets will go on sale at the end of October.</p>
					</article>
					<article>
						<p class="question">Can I submit an entry for the industry achievement award?</p>
						<p class="answer">No, the Industry Achievement award is the only award that is based on nominations from companies and individuals submitting for all the other categories. On the entry form for each of the categories the entrant will be asked to nominate someone in the industry for the Industry Achievement Award, the person with the most nominations will win.</p>
					</article>
					<article>
						<p class="question">I notice that my company is required to have operated for a minimum of 2 years but our company has recently re-branded  and I believe that I should be exempt from this requirement, what should I do?</p>
						<p class="answer">If your business has recently changed names and therefore does not meet the minimum time requirements then please send us an email explaining your situation and provide any supporting information that you can.  Exemptions to the 2 year rule will be evaluated on a case by case basis.</p>
					</article>
					<article>
						<p class="question">My company has done work in British Columbia, but we are based out of Province, can we enter?</p>
						<p class="answer">No, the awards are open to BC based businesses only.</p>
					</article>
					<article>
						<p class="question">I am interested in knowing exactly how the scoring is being done, is this information available?</p>
						<p class="answer">Scoring by the judges will be based on five separate areas; aesthetics, themes, originality, professionalism and quality.  Each of the judges will review all of the information provided from a company (except for their name and contact details) and then score the submission in each of the five separate areas.</p>
					</article>
					<article>
						<p class="question">My category entry requires photographs and we don't have and good photos of our work, what can I do?</p>
						<p class="answer">Photographs will be an essential part of the judging process for most of the categories.  Get in touch with the	photographer who covered the wedding you were involved with and see if you can make  arrangements to use some of their photographs. Alternatively you could have a photographer photograph your work (if possible). Photographs submitted should be sharp, well exposed and clearly show your work.</p>
					</article>
					<article>
						<p class="question">My question is not listed here, how can i get in touch with you?</p>
						<p class="answer">Please use the <a href="contact1.php">contact us form</a> and we will be happy to address any concerns or questions you have.</p>
					</article>
				</span>
			</div>			
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
	</section>
<?php include("bottom2.inc"); ?>