<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body">
<?php include("top.inc"); ?>
<section class="slide fade kenBurns">
	<a name="categories"></a>
	<div class="content">
		<div class="container">
			<div class="wrap winners2014_con">
				<h1>2014 Winners</h1>
				<span>Organizers of the 2014 Professional BC Wedding Awards handed out trophies on November 26, 2014 at The Kay Meek Theatre in West Vancouver. The winners are those companies with the highest scoring entries. There is a maximum of 2 finalists per category, and they are listed here in alphabetical order.</span><br><br>
				<p>Please note that in most cases, only a small part of the entry material is shown publicly. Photographer credit was not known for all entries, it has been provided wherever possible.</p>
				<span class="winners_list">
					<a href='#1'>Best Wedding Cake</a>
					<a href='#2'>Best Bridal Bouquet</a>
					<a href='#3'>Best Wedding Florist - Overall</a>
					<a href='#4'>Best Wedding Make-up</a>
					<a href='#6'>Best Wedding Hair Style</a>
					<a href='#8'>Best Wedding Hair & Make-up - South Asian</a>
					<a href='#9'>Best Wedding Decor</a>
					<a href="#10">Best Wedding Stationery</a>
					<a href='#11'>Best Wedding Reception Venue</a>
					<a href='#12'>Best Wedding Reception Venue - Hotel or Banquet Hall</a>
					<a href='#13'>Best Wedding Ceremony Location</a>
					<a href='#14'>Best Wedding Officiant</a>
					<a href='#15'>Best Wedding Event Planning</a>
					<a href='#16'>Best Catered Wedding</a>
					<a href='#17'>Best Live Wedding Entertainment</a>
					<a href='#18'>Best Wedding DJ</a>
					<a href='#19'>Best Photo Booth - Photo Sequence</a>
					<a href='#20'>Best Candid/Photojournalism Photograph</a>
					<a href='#21'>Best Portrait- Bride and Groom Together</a>
					<a href='#22'>Best Wedding Detail Photograph</a>
					<a href='#23'>Best Wedding Group Photograph</a>
					<a href='#24'>Best Overall Wedding Photography</a>
					<a href='#25'>Best Wedding Videographer/Cinematographer - BC Wedding </a>
					<a href='#27'>Best Edited Wedding Video</a>
					<a href='#28'>2014 Tasters Choice - Best Hors D’Oeuvre</a>
					<a href='#29'>2014 Judges Choice - Best Hors D'Oeuvre</a>
					<a href='#30'>2014 Industry Achievement Award Winner</a>
					<p>Winners: The winners are those companies with the highest score.</p>
					<p>Finalists: In order to qualify as a finalist the score needs to be within 10% of the winning score.<br>There is a maximum of 2 finalists per category.</p>
				</span>
				<div class="winner">
					<h2><strong><a name="1"></a>Best Wedding Cake</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Anna Elizabeth Cakes</span><br/>
						<a href="http://annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a><br/><br/>
						<img src="winners2014/2001.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Coastal Cake Company</span><br/>
							<a href="http://www.costalcakecompany.com" target="_blank">www.costalcakecompany.com</a></p>
							<p align="center"><img src="winners2014/2181.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">When Pigs Fly Pastries</span><br />
							<a href="http://www.whenpigsflypastries.com" target="_blank">www.whenpigsflypastries.com</a></p>
							<p align="center"><img src="winners2014/2063.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Flower Factory</span><br/>
						<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br/><br/>
						<img src="winners2014/1959.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Dushan Flowers</span><br/>
							<a href="http://www.dushanflowers.com" target="_blank">www.dushanflowers.com</a></p>
							<p align="center"><img src="winners2014/bb1_3733_2.jpg" width="400" height="266"  border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">FLEURtacious by Lynda Marie</span><br />
							<a href="http://www.lyndamarie.ca" target="_blank">www.lyndamarie.ca</a></p>
							<p align="center"><img src="winners2014/2228.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Verbena Floral Design</span><br/>
						<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a><br/><br/>
						<img src="winners2014/2111.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">FLEURtacious by Lynda Marie</span><br/>
							<a href="http://www.lyndamarie.ca" target="_blank">www.lyndamarie.ca</a></p>
							<p align="center"><img src="winners2014/2211.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Flower Factory</span><br />
							<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a></p>
							<p align="center"><img src="winners2014/2099.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="4"></a>Best Wedding Make-up</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Sharon Rai Hair & Makeup Artistry</span><br/>
						<a href="http://www.sharonrai.com" target="_blank">www.sharonrai.com</a><br/><br/>
						<img src="winners2014/hs1_8155_2.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Jasmine Hoffman Makeup Artist</span><br/>
							<a href="http://www.jasminehoffman.com" target="_blank">www.jasminehoffman.com</a></p>
							<p align="center"><img src="winners2014/2427.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Denise Elliot Make-up</span><br />
							<a href="http://www.makeupbydenise.ca" target="_blank">www.makeupbydenise.ca</a></p>
							<p align="center"><img src="winners2014/2458.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Hair Style</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Nadia Albano Style Inc.</span><br/>
						<a href="http://www.nadiaalbano.com" target="_blank">www.nadiaalbano.com</a><br/><br/>
						<img src="winners2014/2296.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Sharon Rai Hair & Makeup Artistry</span><br/>
							<a href="http://www.sharonrai.com" target="_blank">www.sharonrai.com</a></p>
							<p align="center"><img src="winners2014/hs1_8155_4.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Margaret Lai Makeup and Hair</span><br />
							<a href="http://www.margaretlai.com" target="_blank">www.margaretlai.com</a></p>
							<p align="center"><img src="winners2014/2262.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="7"></a>Best Wedding Hair Style - South Asian</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Pink Orchid Studio</span><br/>
						<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a><br/><br/>
						<img src="winners2014/2289.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">BeautyCall</span><br/>
							<a href="http://www.Beautycall.ca" target="_blank">www.Beautycall.ca</a></p>
							<p align="center"><img src="winners2014/2484.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Contoured Studios</span><br />
							<a href="http://www.contouredstudios.com" target="_blank">www.contouredstudios.com</a></p>
							<p align="center"><img src="winners2014/1999.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="8"></a>Best South Asian Bride – Hair & Makeup</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Pink Orchid Studio</span><br/>
						<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a><br/><br/>
						<img src="winners2014/2288.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Jayna Marie Makeup + Hair</span><br/>
							<a href="http://www.jaynamarie.com" target="_blank">www.jaynamarie.com</a></p>
							<p align="center"><img src="winners2014/2368.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Sharon Rai Hair & Makeup Artistry</span><br />
							<a href="http://www.sharonrai.com" target="_blank">www.sharonrai.com</a></p>
							<p align="center"><img src="winners2014/2492.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="9"></a>Best Wedding Decor</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Petite Pearl Events</span><br/>
						<a href="http://www.PetitePearlEvents.com" target="_blank">www.PetitePearlEvents.com</a><br/><br/>
						<img src="winners2014/2015.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Greenscape Design & Décor</span><br/>
							<a href="http://www.greenscapedecor.com" target="_blank">www.greenscapedecor.com</a></p>
							<p align="center"><img src="winners2014/2542.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Vintage Origami</span><br />
							<a href="http://www.VintageOrigami.com" target="_blank">www.VintageOrigami.com</a></p>
							<p align="center"><img src="winners2014/1988.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="10"></a>Best Wedding Stationery</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Love by Phoebe</span><br/>
						<a href="http://www.lovebyphoebe.com" target="_blank">www.lovebyphoebe.com</a><br/><br/>
						<img src="winners2014/2552.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Making Memories with Scrapbooking</span><br/>
							<a href="http://www.makingmemorieswithscrapbooking.com" target="_blank">www.makingmemorieswithscrapbooking.com</a></p>
							<p align="center"><img src="winners2014/2363.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Sunlit Letterpress</span><br />
							<a href="http://www.sunlit-letterpress.com" target="_blank">www.sunlit-letterpress.com</a></p>
							<p align="center"><img src="winners2014/2129.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="11"></a>Best Wedding Reception Venue</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Cecil Green Park House</span><br/>
						<a href="http://www.cecilgreenpark.ubc.ca" target="_blank">www.cecilgreenpark.ubc.ca</a><br/><br/>
						<img src="winners2014/rv1_6289_4.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">cience World British Columbia</span><br/>
							<a href="http://www.scienceworld.ca" target="_blank">www.scienceworld.ca</a></p>
							<p align="center"><img src="winners2014/1992.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Loft at Earls Yaletown</span><br />
							<a href="http://www.earls.ca" target="_blank">www.earls.ca</a></p>
							<p align="center"><img src="winners2014/2553.jpg" width="400" height="267" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Fairmont Pacific Rim</span><br/>
						<a href="http://www.fairmont.com/pacificrim" target="_blank">www.fairmont.com/pacificrim</a><br/><br/>
						<img src="winners2014/2329.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Inn at Laurel Point</span><br/>
							<a href="http://www.laurelpoint.com" target="_blank">www.laurelpoint.com</a></p>
							<p align="center"><img src="winners2014/2339.jpg" width="400" height="266" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Terminal City Club</span><br />
							<a href="http://www.tcclub.com" target="_blank">www.tcclub.com/plan</a></p>
							<p align="center"><img src="winners2014/2087.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Sparkling Hill Resort</span><br/>
						<a href="http://www.SparklingHillResort.com" target="_blank">www.SparklingHillResort.com</a><br/><br/>
						<img src="winners2014/2339.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brock House Restaurant</span><br/>
							<a href="http://www.brockhouserestaurant.com" target="_blank">www.brockhouserestaurant.com</a></p>
							<p align="center"><img src="winners2014/2139.jpg" width="400" height="266"" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Cecil Green Park House</span><br />
							<a href="http://www.cecilgreenpark.ubc.ca" target="_blank">www.cecilgreenpark.ubc.ca</a></p>
							<p align="center"><img src="winners2014/2079.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="14"></a>Best Wedding Officiant</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shawn Miller – Young, Hip & Married</span><br/>
						<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a><br/><br/>
						<img src="winners2014/2334.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Michele Davidson<br />Modern Celebrant</span><br/>
							<a href="http://www.moderncelebrant.com" target="_blank">www.moderncelebrant.com</a></p>
							<p align="center"><img src="winners2014/2167.jpg" width="400" height="266" border="0"></p>
						</div>
						<div class="other_pic">
							<p align="center"><br/><span class="company">MarryUs <br />Young Hip& Married</span><br />
							<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a></p>
							<p align="center"><img src="winners2014/2539.jpg" width="400" height="266" border="0"></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="15"></a>Best Wedding Event Planning</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Filosophi Events</span><br/>
						<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a><br/><br/>
						<img src="winners2014/2103.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">DreamGroup Productions: planner Sandy Pandher</span><br/>
							<a href="http://www.DreamGroup.ca" target="_blank">www.DreamGroup.ca</a></p>
							<p align="center"><img src="winners2014/2449.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">SpotLight Event Planning & Design</span><br />
							<a href="http://www.spotlightevent.ca" target="_blank">http://www.spotlightevent.ca</a></p>
							<p align="center"><img src="winners2014/2236.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="16"></a>Best Catered Wedding</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Truffles Fine Foods</span><br/>
						<a href="http://www.trufflesfinefoods.com" target="_blank">www.trufflesfinefoods.com</a><br/><br/>
						<img src="winners2014/2330.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Bespoke Events</span><br/>
							<a href="http://www.detailscatering.ca" target="_blank">www.detailscatering.ca</a></p>
							<p align="center"><img src="winners2014/2185.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Emelle's Catering</span><br />
							<a href="http://www.emelles.com" target="_blank">www.emelles.com</a></p>
							<p align="center"><img src="winners2014/2333.jpg" width="300" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="17"></a>Best Live Wedding Entertainment</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">SideOne</span><br/>
						<a href="http://www.sideone.ca" target="_blank">www.sideone.ca</a><br/><br/>
						<img src="winners2014/2127.jpg" width="600" height="426" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Butter Studios</span><br/>
							<a href="http://www.butterstudios.ca" target="_blank">www.butterstudios.ca</a></p>
							<p align="center"><img src="winners2014/2124.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Modern Romance Productions</span><br />
							<a href="http://www.slowmoboothvancouver.com" target="_blank">www.slowmoboothvancouver.com</a></p>
							<p align="center"><img src="winners2014/6270.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="18"></a>Best Wedding DJ</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">DJ Khanvict – Decibel Entertainment</span><br/>
						<a href="http://www.Khanvict.ca" target="_blank">www.Khanvict.ca</a><br/><br/>
						<img src="winners2014/2203.jpg" width="600" height="338" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">A Day to Remember Events</span><br/>
							<a href="http://www.adaytoremember.ca" target="_blank">www.adaytoremember.ca</a></p>
							<p align="center"><img src="winners2014/2047.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">DJ BFAD</span><br />
							<a href="http://www.djbfad.com" target="_blank">www.djbfad.com</a></p>
							<p align="center"><img src="winners2014/1966.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="19"></a>Best Photo Booth - Photo Sequence</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Four Frames Photo Booth</span><br/>
						<a href="http://www.FourFramesPhotoBooth.com" target="_blank">www.FourFramesPhotoBooth.com</a><br/><br/>
						<img src="winners2014/2032.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Butter Studios</span><br/>
							<a href="http://www.butterstudios.ca" target="_blank">www.butterstudios.ca</a></p>
							<p align="center"><img src="winners2014/2121.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Love Tree Photography</span><br />
							<a href="http://www.lovetreephotography.ca" target="_blank">www.lovetreephotography.ca</a></p>
							<p align="center"><img src="winners2014/2570.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="20"></a>Best Candid/Photojournalism Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">JONETSU STUDIOS</span><br/>
						<a href="http://www.jonetsuphotography.com" target="_blank">www.jonetsuphotography.com</a><br/><br/>
						<img src="winners2014/2321.jpg" width="600" height="563" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Amber Hughes Photography</span><br/>
							<a href="http://www.amberhughes.ca" target="_blank">www.amberhughes.ca</a></p>
							<p align="center"><img src="winners2014/2132.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Michael Wachniak Photographer</span><br />
							<a href="http://www.michaelwachniak.com" target="_blank">www.michaelwachniak.com</a></p>
							<p align="center"><img src="winners2014/2526.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="21"></a>Best Portrait - Bride and Groom together</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shari + Mike Photographers</span><br/>
						<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca"</a><br/><br/>
						<img src="winners2014/2122.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Daniela Ciuffa Photography</span><br/>
							<a href="http://www.ciuffaphotography.com" target="_blank">www.ciuffaphotography.com</a></p>
							<p align="center"><img src="winners2014/2354.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Leanne Pederson</span><br />
							<a href="http://www.leannepedersen.com" target="_blank">www.leannepedersen.com</a></p>
							<p align="center"><img src="winners2014/0388.jpg" width="400" height="219" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="22"></a>Best Wedding Detail Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Jasalyn Thorne Photography</span><br/>
						<a href="http://www.jasalynthornephotography.com" target="_blank">www.jasalynthornephotography.com</a><br/><br/>
						<img src="winners2014/2527.jpg" width="600" height="317" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Butter Studios</span><br/>
							<a href="http://www.butterstudios.ca" target="_blank">www.butterstudios.ca</a></p>
							<p align="center"><img src="winners2014/2206.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Michael Wachniak Photographer</span><br />
							<a href="http://www.michaelwachniak.com" target="_blank">www.michaelwachniak.com</a></p>
							<p align="center"><img src="winners2014/2547.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="23"></a>Best Wedding Group Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Fran Chelico Photography</span><br/>
						<a href="http://www.franchelico.com" target="_blank">www.franchelico.com</a><br/><br/>
						<img src="winners2014/2208.jpg" width="600" height="400"" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Family Creative Imagery</span><br/>
							<a href="http://www.familycreativeimagery.com" target="_blank">www.familycreativeimagery.com</a></p>
							<p align="center"><img src="winners2014/2210.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Nickersons</span><br />
							<a href="http://www.thenickersons.ca" target="_blank">www.thenickersons.ca</a></p>
							<p align="center"><img src="winners2014/2396.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="24"></a>Best Overall Wedding Photography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Jenna & Tristan</span><br/>
						<a href="http://jennaandtristan.com" target="_blank">jennaandtristan.com</a><br/><br/>
						<img src="winners2014/2265.jpg" width="600" height="400" border="0"><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Fran Chelico Photography</span><br/>
							<a href="http://www.franchelico.com" target="_blank">www.franchelico.com</a></p>
							<p align="center"><img src="winners2014/2245.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">KGOODPHOTO</span><br />
							<a href="http://www.kgoodphoto.com" target="_blank">www.kgoodphoto.com</a></p>
							<p align="center"><img src="winners2014/2198.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="25"></a>Best Wedding Videography/Cinematography - BC Wedding:</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">VanWeddings</span><br/>
						<a href="http://www.vanweddings.com" target="_blank">www.vanweddings.com</a><br/><br/>
						<iframe src="//player.vimeo.com/video/109343414" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br />
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Beautiful Wedding Films</span><br/>
							<a href="http://www.facebook.com/BeautifulWeddingFilms" target="_blank">BeautifulWeddingFilms</a></p>
							<iframe src="//player.vimeo.com/video/109195834" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Rosette Films</span><br />
							<a href="http://www.rosettefilms.ca" target="_blank">www.rosettefilms.ca</a></p>
							<iframe src="//player.vimeo.com/video/109210302" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="26"></a>Best Wedding Videography/Cinematography - Destination Wedding:</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Kismet Creative</span><br/>
						<a href="http://www.kismetcreative.ca" target="_blank">www.kismetcreative.ca</a><br/><br/>
						<iframe src="//player.vimeo.com/video/109255763" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br />
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Hello Tomorrow Wedding Films</span><br/>
							<a href="http://www.hellotomorrow.com" target="_blank">www.hellotomorrow.com</a></p>
							<iframe src="//player.vimeo.com/video/109358014" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">VanWeddings</span><br />
							<a href="http://www.vanweddings.com" target="_blank">www.vanweddings.com</a></p>
							<iframe src="//player.vimeo.com/video/109344085" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="27"></a>Best Edited Wedding Video</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Memento Films</span><br/>
						<a href="http://www.MementoFilms.ca" target="_blank">www.MementoFilms.ca</a><br/><br/>
						<iframe src="//player.vimeo.com/video/109155510" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Hello Tomorrow Wedding Films</span><br/>
							<a href="http://www.hellotomorrow.com" target="_blank">www.hellotomorrow.com</a></p>
							<iframe src="//player.vimeo.com/video/109358015" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Limelight Studios</span><br />
							<a href="http://www.limelightstudios.com" target="_blank">www.limelightstudios.com</a></p>
							<iframe src="//player.vimeo.com/video/109096113" width="400" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="30"></a>Industry Achievement Award</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Yuriko Larson - Vintage Origami</span><br/>
						<a href="http://www.vintageorigami.com" target="_blank">www.vintageorigami.com</a><br/><br/>
						<p align="right"><a href="winners2014.php" >return to category listing</a>
					</div>					
				</div>
				<div class="final_list">		
					<h2><strong><a name="29"></a>2014 Best Taster Choice - Best Hors D’Oeuvre Award Winner</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">no fixed address catering</span><br/>
						<a href="http://nofixedaddresscatering.com" target="_blank">http://nofixedaddresscatering.com</a><br/><br/>
						<img src="winners2014/NoFixedJudgesChoice.jpg" width="700" height="466" border="0">
					</div>
					<h2><strong>2014 Judges Choice - Best Hors D’Oeuvre Award Winner</strong></h2><br><br>
  					<p>no fixed address catering</p><br/>
  					<a href="http://nofixedaddresscatering.com/" target="_blank">http://nofixedaddresscatering.com/</a><br><br>
  					<img src="winners2014/NoFixedJudgesChoice.jpg" width="700" height="466" border="0"><br><br>
  					<h2><strong>Best Hors D'Oeuvres PARTICIPANTS</strong></h2><br>
  					<p>Emelle's Catering, No Fixed Address Catering, Truffle's Fine Foods, Vij's Catering</p>
  				</div>
  				<span align="center">
  					<p>No FIxed Address Catering  <a href="http://nofixedaddresscatering.com/" target="_blank">www.nofixedaddresscatering.com</a></p>
  					
  					<p>Emelle's  <a href="http://www.emelles.com/" target="_blank">www.emelles.com</a></p>
  					<p>Edgeceptional Catering  <a href="http://edgecatering.ca/" target="_blank">www.edgecatering.ca</a></p>
  					<p>Truffles Fine Foods  <a href="http://trufflesfinefoods.com/" target="_blank">www.trufflesfinefoods.com</a></p>
  				</span>
  				<span align="center">
  					<p>Our sincere thank-you to each of this years competing caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had.</p>
  					Special Thanks also to our Judges</b><br>Julian Bond of  <a href="http://www.picachef.com" target="_blank">Pacific Institute of Culinary Arts</a> <br><br>Mijune Pak of  <a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br><br>Nathan Fong of  <a href="http://www.fongonfood.com" target="_blank">FONG ON FOOD</a><br><br><br>
  					<p>Congratulations to all of the Winners and Finalists!</p>
  				</span>
			</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
</body>
</html>
