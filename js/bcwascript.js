var thissite = "http://www.bcweddingawards.com/";

// -------------------------------------------------------------------------------
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

// -------------------------------------------------------------------------------
function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

// -------------------------------------------------------------------------------
var preloadFlag = false;

// -------------------------------------------------------------------------------
function preloadImages() {
	if (document.images) {
		home_over = newImage("menuimages/home-over.jpg");
		blog_over = newImage("menuimages/blog-over.jpg");
		categories_over = newImage("menuimages/categories-over.jpg");
		judging_over = newImage("menuimages/judging-over.jpg");
		faq_over = newImage("menuimages/faq-over.jpg");
		events_over = newImage("menuimages/events-over.jpg");
		contact_over = newImage("menuimages/contact-over.jpg");
		preloadFlag = true;
	}
}

// -------------------------------------------------------------------------------
function Trim(str)
{
    while (str.substring(0,1) == ' ') // check for white spaces from beginning
    {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length-1, str.length) == ' ') // check white space from end
    {
        str = str.substring(0,str.length-1);
    }  
    return str;
}

// -------------------------------------------------------------------------------
function autoTab(element, nextElement) {
    if (element.value.length == element.maxLength && nextElement != null) {
        element.form.elements[nextElement].focus();
    }
}

// -------------------------------------------------------------------------------
function IsValidEmail(e)
{
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	
	if (filter.test(e))
		return true;
	else{
		return false;
	}
	
}

// -------------------------------------------------------------------------------
function IsValidPhone(p1, p2, p3)
{
			
	if (p1.length<3) return false;
	if (p2.length<3) return false;
	if (p3.length<4) return false;
	
	if (isNaN(p1)) return false;	
	if (isNaN(p2)) return false;	
	if (isNaN(p3)) return false;	
	
	return true;
			
} 

// -------------------------------------------------------------------------------
function validateForm()
{
		
	var f = document.forms['BCWA'];
	var email = f.email.value;
	var categorycode = f.categorycode.value;
	var phone1 = f.phone1.value;
	var phone2 = f.phone2.value;
	var phone3 = f.phone3.value;
	
	if (typeof(f.companydescription) != 'undefined')
	{
		var companydescription = f.companydescription.value;	
	}
	
	// Added 30th Aug 2013
	if (typeof(f.photographername) != 'undefined')
	{
		var photographername = f.photographername.value;	
	}
	
	if (typeof(f.otherinfo) != 'undefined')
	{
		var otherinfo = f.otherinfo.value;	
	}
		
	
	var btn = document.getElementById("btnSubmit");
	var errortext = '';
	
	if (Trim(email).length==0)
	{
		errortext = errortext + 'Email Address\n';
		
	} else {
		
		if (!IsValidEmail(Trim(email)))
		{
			errortext = errortext + 'Enter a valid email address\n';
		}
		
	}
	
	if (Trim(phone1).length==0 || Trim(phone2).length==0 || Trim(phone3).length==0)
	{		
		if(!IsValidPhone(Trim(phone1), Trim(phone2), Trim(phone3) ))
   	{
   		errortext = errortext + 'Enter a valid phone number\n';
   		
   	} else {
   		errortext = errortext + 'Enter a phone number\n';
   	}
	}
	
	if (typeof(f.companydescription) != 'undefined')	
	{
		if (companydescription.indexOf("http:")>0)
		{
			errortext = errortext + 'Remove all HTML links in Question 8\n';
		}
	}
	
	if (typeof(f.otherinfo) != 'undefined')
	{
		if (otherinfo.indexOf("http:")>0)
		{
			errortext = errortext + 'Remove all HTML links in Question 9\n';
		}
	}
	
	// Added 30th Aug 2013
	if (Left(categorycode,2)=="BP")
	{
		if (Trim(photographername).length==0)
		{
			errortext = errortext + 'Photographer Name\n';
			
		}
	}
	
	if (errortext.length>0)
	{
		alert('Please complete the following information:\n\n' + errortext);
				
	} else {
		
		document.all.pleasewaitScreen.style.visibility="visible";	
		f.finalPost.value = "1";
		f.submit();
		
	}
		
}

// ------------------------------------------------------------------------------------
function showHideDiv(tdiv) {
	
	var thediv = document.getElementById(tdiv);
	if (typeof(thediv)!='undefined') {
		
		if (thediv.className == "Hide") {
			thediv.className = "Show";
		} else {
			thediv.className = "Hide";
		}
		
	}
	
}

// ------------------------------------------------------------------------------------
function updateChrLimit(fieldName, l, displayName) 
{
	var f = document.forms['BCWA'];
	var commentField = f[fieldName].value;
	//var chrsleft = document.getElementById(displayName);
	
	if (commentField.length > l)
	{
		alert('You have exceeded the maximum of ' + l + ' characters.  Only the first ' + l + ' characters will be submitted');
		f[fieldName].value = commentField.substring(0, l);
	}
	
	//chrsleft.innerHTML = (l-commentField.length-1);
	
}

// ------------------------------------------------------------------------------------
function Left(str, n){
  if (n <= 0)
    return "";
  else if (n > String(str).length)
    return str;
  else
    return String(str).substring(0,n);
}

// ----------------------------------------------------------
