<?php include("top.inc"); ?>

<center>
<br><br>
<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="295"><p class="sansserif" align="justify">
			<br>The Professional BC Wedding Awards is the longest running wedding industry Awards program in Canada.  We are an independent, unbiased and dedicated to finding and awarding the best of British Columbia's dynamic and creative wedding industry. These awards recognize some of the amazing creative talent, companies and individuals that make up British Columbia's wedding professionals. 
			<br><br>
			<!-- Each November we also hold our annual Gala Awards Night, an incredible evening of celebration with the largest wedding industry event in Western Canada.<br><br>-->
			
			The awards are a useful tool to help connect the best of the wedding industry with brides, planners and other industry vendors. We are proud of all the winners and finalists, they truly deserve the recognition they've received and we are happy to hear of the incredible positive effects that winning an award has had on their businesses.
 		</td>

		<td>
			<IMG SRC="http://www.bcweddingawards.com/siteimages/middle.png" WIDTH=263 HEIGHT=317 BORDER=0 ALT="" USEMAP="#middle_Map">
		</td>

		<td>
			<img src="siteimages/BCWA_awardsIndex.jpg" width="321" height="305" border="0">
			<br><br>
		</td>
	</tr>
</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="center"><img src="siteimages/AwardsNightBanner2016.jpg"></td></tr>
    	<tr>
   	    <td align="center" valign="middle">&nbsp;</td></tr>
	<tr><td><img src="siteimages/dots.jpg" width="879" height="9" alt="" /></td></tr>
</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	<td><IMG SRC="siteimages/sponsorbar2015.png" BORDER=0 ALT="" ></td>
</tr>
</table>

</center>
			
<?php include("bottom.inc"); ?>