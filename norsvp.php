<?php
ini_set('display_errors', '1');
include("dbconnect.php");

if (empty($code)) {
	$code = $_GET['code'];
}

if (empty($undo)) {
	$undo = $_GET['undo'];
}

if (empty($_SESSION['sessioncode'])) {
	$_SESSION['sessioncode']="SESSIONCODE:".mt_rand(1000000000000, 9999999999999);
}

if (!empty($code)) {
	
	if ($undo=="1") {
		
		$usql = "UPDATE rsvp SET isnotattending=0, isnotattending_date=now() WHERE permakey='".$code."' AND isarchived=0";
		mysql_query($usql) or die(mysql_error());
		$oktext = "Thank you, so glad you're coming!";		
		
	} else {
			
		// How many tickets has this person bought already?	
		$ticketcount = getField("SUM(qty)", "form_gala", "rsvppermakey='$code' AND paypalconfirm=1");
		$ticketcount_rsvp = getField("SUM(rsvpqty)", "form_gala", "rsvppermakey='$code' AND paypalconfirm=1");
		
		// ------------------------------------------------------------------------------------------------------
		// Retrieve all records for this code
		// ------------------------------------------------------------------------------------------------------
		$query = "SELECT companyname, email, eligible, status, eligible FROM rsvp WHERE permakey='".$code."' AND isarchived=0";
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
			
		if ($row) {		
			$company = stripslashes($row["companyname"]);
				
			// How many tickets has this person bought already?
			$ticketcount = getField("SUM(qty)", "form_gala", "rsvppermakey='$code' AND paypalconfirm=1");
			$ticketcount_rsvp = getField("SUM(rsvpqty)", "form_gala", "rsvppermakey='$code' AND paypalconfirm=1");
			
			if (empty($ticketcount)) $ticketcount=0;		
			if (empty($ticketcount_rsvp)) $ticketcount_rsvp=0;		
							
			if ($ticketcount + $ticketcount_rsvp>0) {	
				// If they bought tickets then present message to contact Justin
				$errortext = "We have detected that you have already purchased tickets for this event.  Please contact info@bcweddingawards.com.";	
			} else {
				// Set to NoRSVP	
				
				$usql = "UPDATE rsvp SET isnotattending=1, isnotattending_date=now() WHERE permakey='".$code."' AND isarchived=0";
				mysql_query($usql) or die(mysql_error());
				
				$oktext = "Thank you, sorry you won't be joining us this time!";
			}	
			
		} else {
			$errortext = "Cannot locate your details.  Please follow the link in your email, or contact us if you are having problems.";	
		}
				
	}
	
} else {
	$errortext = "Cannot locate your details.  Please follow the link in your email, or contact us if you are having problems.";
	$showform = false;
	$foundcode = false;			
}
	
include("top.inc");

?>

<div style="color:#FFF;text-align:left;width:800px;margin-top:20px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p><b><big>Awards Night <?php echo date("Y"); ?>&nbsp;<?php if (!empty($company)) echo " - ".$company; ?></big></b></p>

<?php

if (!empty($errortext)) {
	echo "<center><p style=\"color:red;margin-bottom:10px;\">".$errortext."</p></center>";
}			

if (!empty($oktext)) {
	echo "<center><p style=\"color:green;margin-bottom:10px;\">".$oktext."</p></center>";
}

?>

</div>

<?php	
	include("bottom.inc");
?>