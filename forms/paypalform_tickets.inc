<form action="https://www.paypal.com/cgi-bin/webscr"  method="post">

<!-- the cmd parameter is set to _xclick for a Buy Now button -->
<input type="hidden" name="cmd" value="_xclick">

<input type="hidden" name="business" value="info@bcweddingawards.com">
<input type="hidden" name="item_name" value="Awards Night Tickets">
<input type="hidden" name="item_number" value="AwardNight16">
<input type="hidden" name="amount" value="<? echo $amount; ?>">
<input type="hidden" name="quantity" value="1">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="currency_code" value="CAD">
<input type="hidden" name="lc" value="US">
<input type="hidden" name="bn" value="PP-BuyNowBF">
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">

<!-- If you have turned on auto return on your paypal profile the next line shows where they will go after they have paid by paypal and they click return to site -->
<input type="hidden" name="return" value="http://www.bcweddingawards.com/galathankyou.php"> 

 <!-- and this is where they will go if they click cancel -->
<input type="hidden" name="cancel_return" value="http://www.bcweddingawards.com/gala.php">

<!-- notes about the following parameter rm.  If your return_url page is a static web page and if Instant Payment Notification is on within the profile, you must to set rm = 1 in the button code so that the return_url page can be called through a GET. If you do not set it, your users will encounter HTTP 405 errors when they try to go to the return page because a static web page cant accept a POST. So make your return_url a .php file if you have rm=2-->
<input type="hidden" name="rm" value="2">          <!--Auto return must be off if rm=2     -->

<!--Next line is where paypal should send the ipn to. we will have set a global value in our paypal account but we can re specify it here  -->
<input type="hidden" name="notify_url" value="http://www.bcweddingawards.com/set/paypal_ipncheck.php" /> 
<input type="hidden" name="custom" value="<? echo session_id(); ?>">     

</form>

<!--<form action="https://www.sandbox.paypal.com/cgi-bin/webscr"  method="post">-->
<!--<form action="https://www.paypal.com/cgi-bin/webscr" method="post">-->
