<div style="color:#FFF;text-align:left;width:840px;margin-top:5px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p class="sansserif" align="justify">
<b><big>Nominate a Wedding Vendor</big></b>
<br><br>
Do you know a Wedding vendor in British Columbia who you really think is truly outstanding? If so we would love to hear about it. Simply fill out the nomination details in the form below and we will contact that company regarding their submission for an award.
<br><br>
IMPORTANT: Nominations are used to help us contact companies who you think should be submitting for an award. The number of nominations a company receives has no effect on the score that company will receive, judges rate only the information supplied from the company itself.
<br><br>
Please note that your personal information will not be given to the company you nominate and we will not share your personal contact information with any third party. 
</p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">

<?
	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;\">".$errortext."</p></center>";
	}			
	
	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;\">".$oktext."</p></center>";
	}
?>


<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>Email:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>Phone Number:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<input id="phone1" name="phone1" value="<? echo $phone1; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone2" name="phone2" value="<? echo $phone2; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone3" name="phone3" value="<? echo $phone3; ?>" size="2" maxlength="4" type="text" />
		<br>
		###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;####
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>Company You Wish to Nominate:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="companyname" name="companyname" value="<? echo $companyname; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>What type of work does this Company do?</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="worktype" name="worktype" value="<? echo $worktype; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="350px"><font face="Verdana" size="2" color="#ebebeb"><b>Email Contact for the Nominated Company:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="emailcontact" name="emailcontact" value="<? echo $emailcontact; ?>" size="40" type="text" />
		<br><br>
		<input  value="Submit" type="submit" />
	</td>
</tr>

</table>
</form>

</div>