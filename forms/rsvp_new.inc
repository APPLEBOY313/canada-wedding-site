<div style="color:#FFF;text-align:left;width:800px;margin-top:20px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p><b><big>Awards Night RSVP for <?php echo $companyname; ?></big></b></p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>?code=<? echo $code; ?>">
<input type="hidden" name="code" value="<?php echo $code; ?>">
<input type="hidden" name="email" value="<?php echo $email; ?>">

<?php

	echo "<big>".$welcometext."</big>";

	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;margin-bottom:10px;\">".$errortext."</p></center>";
	}			
	
	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;margin-bottom:10px;\">".$oktext."</p></center>";
	}
	
	if ($showform && ($eligible-$rsvptotal>0)) {
		
		echo "<big>You are eligible for <b>".$elgible_display."</b></big>";
		echo "<br><br>";
		echo "<big>How many tickets would you like? ";
		echo "<select name=\"rsvpnumber\">";
		echo "<option value=\"0\">Will not attend</option>";	
		
		for ($n=1; $n<($eligible-$rsvptotal)+1; $n++) {
			
			if ($n==($eligible-$rsvptotal)) { 
				echo "<option selected value=\"".$n."\">".$n."</option>";
			} else {
				echo "<option value=\"".$n."\">".$n."</option>";	
			}
			
		}
		
		echo "</select>";
		echo "</big>&nbsp;&nbsp;";		
				
		echo "<input value=\"Submit\" type=\"submit\" name=\"submitrsvp\" />";
	}
	
?>

</form>

</div>