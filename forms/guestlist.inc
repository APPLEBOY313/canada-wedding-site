<div style="color:#FFF;text-align:left;width:800px;margin-top:40px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
<p><hr style="color:#FFF;"></p>
<p><b><big>Attendee List</big></b></p>

<p class="sansserif" align="justify">

<?php
	// Can guest change names?
	$canchangeguestnames = getField("canchangeguestnames", "settings", "settingsid=1");
	if ($canchangeguestnames=="1") {
		$canchange = true;
		echo "<big>Please enter the guest names below.  You can alter them at any time by returning to this page.</big>";
	} else {
		$canchange = false;
		echo "<big>Changes to the guest list are not possible at this time.</big>";
	}
	
?>

<br><br></p>

<form name="guestlist" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>?code=<? echo $code; ?>">
<input type="hidden" name="code" value="<?php echo $code; ?>">

<?php

	// Get list of attendees for this company (RSVP)
	$asql = "SELECT attendeenames FROM rsvp WHERE permakey='$code' AND isarchived=0";
	$aresult = mysql_query($asql) or die(mysql_error()."<br><br>".$asql);
	$arow = mysql_fetch_array($aresult);

	if ($arow) {
		
		$names_rsvp = array();		
		$anames = explode(";",stripslashes($arow["attendeenames"]));
			
		for ($p=0;$p<count($anames);$p++) {
			if (!empty($anames[$p])) {
				array_push($names_rsvp, $anames[$p]);
			}
		}
			
	}
		
	// Get list of attendees for this company (Purchase)
	$asql = "SELECT attendeenames, qty, galaid FROM form_gala WHERE rsvppermakey='$code' AND isdeleted=0 AND paypalconfirm=1 AND isarchived=0";
	$aresult = mysql_query($asql) or die(mysql_error()."<br><br>".$asql);
	$anum_results = mysql_num_rows($aresult);
	$qtypurchased = 0;
	
	if ($anum_results>0) {
		while ($arow = mysql_fetch_array($aresult)) {
			$anames = explode(";",stripslashes($arow["attendeenames"]));
						
			for ($p=0;$p<count($anames);$p++) {
				if (!empty($anames[$p])) {
					array_push($names_rsvp, $anames[$p]."~".$arow["galaid"]);
				}
			}
			
			$qtypurchased = $qtypurchased + $arow["qty"];
						
		}
	}

	// List the names out
	echo "<table width=\"700px\" cellspacing=\"2\" border=0>";
	
	// List RSVP names
	for ($t=0; $t<$rsvptotal; $t++) {
		
		if (!empty($names_rsvp[$t])) {
			$rsvpname_tmp = $names_rsvp[$t];
		} else {
			$rsvpname_tmp = "";
		}
		
		echo "<tr valign=\"top\">";
		echo "<td align=\"left\" width=\"70px\" style=\"color:#FFF;\">Guest ".($t+1).":</td>";
		
		if (!empty($rsvpname_tmp)) {
			echo "<td style=\"color:#FFF;font-weight:bold;\" width=\"250px\">".$rsvpname_tmp."</td>";
			
			if ($canchange) {
				echo "<td style=\"color:#FFF;font-size:11px;\"><span style=\"padding-top:3px;\">Change Name:</a>&nbsp;&nbsp;<input type=\"text\" name=\"rsvp_name".$t."\" value=\"".$names_rsvp[$t]."\"></td>";	
				echo "<td style=\"padding-top:4px;\"><a style=\"color:#d2ac67;\" href=\"javascript:deleteGuest('".addslashes($rsvpname_tmp)."','$code');\">Remove Guest</a></td>";
			}
			
		} else {
			if ($canchange) {
				echo "<td style=\"color:#FFF;font-size:11px;\" width=\"250px\"><input type=\"text\" name=\"rsvp_name".$t."\" value=\"".$names_rsvp[$t]."\"></td>";	
			}
		}
		
		echo "</tr>";			
	}
	
	// List Purchased names
	for ($t2=0; $t2<$qtypurchased; $t2++) {
		
		$tmparray = split("~", $names_rsvp[$t]);
		$purchased_name = $tmparray[0];
		$purchased_galaid = $tmparray[1];
				
		if (!empty($purchased_name)) {
			$purchase_tmp = $purchased_name;
		} else {
			$purchase_tmp = "";
		}
		
		echo "<tr valign=\"top\">";
		echo "<td align=\"left\" width=\"60px\" style=\"color:#FFF;\">Guest ".($t+1).":</td>";
		
		if (!empty($purchase_tmp)) {
			echo "<td style=\"color:#FFF;font-weight:bold;\" width=\"250px\">".$purchase_tmp."</td>";
			echo "<td style=\"color:#FFF;font-size:11px;\">";
			echo "<input type=\"hidden\" name=\"purchase_galaid_".$t2."\" value=\"".$purchased_galaid."\">";
			echo "<input type=\"hidden\" name=\"purchase_oldname_".$t2."\" value=\"".$purchase_tmp."\">";
			if ($canchange) {
				echo "<span style=\"padding-top:3px;\">Change Name:</a>&nbsp;&nbsp;<input type=\"text\" name=\"purchase_name".$t2."\" value=\"".$purchase_tmp."\"></td>";	
			}
			echo "<td style=\"padding-top:4px;color:#d2ac67;font-size:11px;\">Purchased via PayPal</td>";
						
		} else {
			echo "<td style=\"color:#FFF;font-size:11px;\" width=\"250px\">";
			if ($canchange) {
				echo "<input type=\"hidden\" name=\"purchase_galaid_".$t2."\" value=\"".$purchased_galaid."\">";
				echo "<input type=\"text\" name=\"purchase_name".$t2."\" value=\"".$names_rsvp[$t2]."\">";
			}
			echo "</td>";	
		}
		
		echo "</tr>";			
		
		$t++;
		
	}
	
	echo "</table>";		
	
//	echo "<pre>";
//	print_r($names_rsvp);
//	echo "</pre>";
//	
	echo "<br><br>";
	if ($canchange) {
		echo "<input value=\"Update Guest Names\" type=\"submit\" name=\"submitnames\" />";	
	}
	
	if (!empty($names_ok)) {
		echo "&nbsp;&nbsp;<span style=\"color:green;\">Guest names saved</span>";
	}
		
?>

</form>

</div>

<?php

	if (($eligible-$rsvptotal>0)) {
		echo "<b><font style=\"color:#FFF;font-size:16px\">Before purchasing additional tickets, please claim all of your allocated tickets</font></b>";	
	} else {
		echo "<b><a style=\"font-size:16px;\" href=\"gala.php?code=".$code."\">Purchase additional tickets</a></b>";		
	}

?>
