<div style="color:#FFF;text-align:left;width:840px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
<p><hr style="color:#FFF;"></p>
<b><big>Attendee List</big></b>

<?php
	// Can guest change names?
	$canchangeguestnames = getField("canchangeguestnames", "settings", "settingsid=1");
	if ($canchangeguestnames=="1") {
		$canchange = true;
		echo "<p class=\"sansserif\" align=\"justify\">Please enter the guest names below.  You can alter them at any time by returning to this page.</p>";
	} else {
		$canchange = false;
		echo "<p class=\"sansserif\" align=\"justify\">Changes to the guest list are no longer possible</p>";
	}
	
	// Get list of attendees for this company (Purchase)
	$asql = "SELECT attendeenames FROM gala_attendee WHERE rsvppermakey='$code' AND isdeleted=0";
	$aresult = mysql_query($asql) or die(mysql_error()."<br><br>".$asql);
	$anum_results = mysql_num_rows($aresult);
		
	if ($anum_results>0) {
		while ($arow = mysql_fetch_array($aresult)) {
			$anames = explode(";",stripslashes($arow["attendeenames"]));						
		}
	}

	echo "<table style=\"text-align:left;\" cellpadding=\"4\" cellspacing=\"2\" border=\"0\" bgcolor=\"transparent\" width=\"100%\">";
	
	// List RSVP names
	for ($t=0; $t<$ticketcount; $t++) {	
		echo "<tr valign=\"top\">";
		echo "<td align=\"right\" width=\"175px\"><font face=\"Verdana\" size=\"2\" color=\"#ebebeb\"><b>Guest ".($t+1).":</font></td>";
		echo "<td style=\"color:#FFF;font-size:11px;\"><input type=\"text\" name=\"guestname".($t+1)."\" value=\"".$anames[$t]."\"></td>";	
		echo "<td width=\"150px\" align=\"right\"></td>";
		echo "</tr>";			
	}
	
	echo "<tr valign=\"top\">";
	echo "<td colspan=\"3\"><p class=\"sansserif\" align=\"justify\">";
	
	if ($canchange) {
		echo "<input value=\"Update Guest Names\" type=\"submit\" name=\"submitnames\" />";	
		if (!empty($names_message)) echo $names_message;
	}
	
	echo "</p></td>";	
	echo "</tr>";		
	
	echo "</table>";		
		
?>

<?php

	//if (($eligible-$rsvptotal>0)) {
	//	echo "<b><font style=\"color:#FFF;font-size:16px\">Before purchasing additional tickets, please claim all of your allocated tickets</font></b>";	
	//} else {
	//	echo "<b><a style=\"font-size:16px;\" href=\"gala.php?code=".$code."\">Purchase additional tickets</a></b>";		
	//}

?>
<br><br>
<p><hr style="color:#FFF;"></p>
<big><b>Purchase additional tickets</b></big>
</div>