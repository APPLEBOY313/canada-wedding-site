<?
	// Functions
	
	// ---------------------------------------------------------------------
	function displayMonths($m) {
		
		for ($i=1;$i<13;$i++) {
			
			if ($m==$i) {
				echo "<option selected>".$i."</option>";
			} else {
				echo "<option>".$i."</option>";
			}
		}
			
	}
	
	// ---------------------------------------------------------------------
	function displayDays($d) {
		
		for ($i=1;$i<32;$i++) {
			
			if ($d==$i) {
				echo "<option selected>".$i."</option>";
			} else {
				echo "<option>".$i."</option>";
			}
		}
			
	}
	
	// ---------------------------------------------------------------------
	function displayYears($yy) {
		
		$y = date("Y");
				
		for ($i=$y;$i<$y+10;$i++) {
			
			if ($yy==$i) {
				echo "<option selected>".$i."</option>";
			} else {
				echo "<option>".$i."</option>";
			}
		}
			
	}
	
?>

<div style="color:#FFF;text-align:left;width:840px;margin-top:5px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p class="sansserif" align="justify">
<b><big>Sign up for our Newsletter</big></b>
<br><br>
Please use the form below to sign up for our news letter.
<br><br>
Our monthly Bridal newsletter will begin in January 2011 and will feature lots of useful tips and information from all of our award winners and sponsors. Recieving this newsletter will give you the inside scoop on the industries hottest information. 
<br><br>
</p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">

<?
	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;\">".$errortext."</p></center>";
	}			
	
	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;\">".$oktext."</p></center>";
	}
?>


<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Email:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Wedding Date:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<p class="sansserif">
			<select name="wedding_month"><? displayMonths($wedding_month); ?></select>
			&nbsp;/&nbsp;
			<select name="wedding_day"><? displayDays($wedding_day); ?></select>
			&nbsp;/&nbsp;
			<select name="wedding_year"><? displayYears($wedding_year); ?></select>
			<br>
			MM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YYYY
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Phone Number:</b></font></td>
	<td><p class="sansserif">
		<input id="phone1" name="phone1" value="<? echo $phone1; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone2" name="phone2" value="<? echo $phone2; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone3" name="phone3" value="<? echo $phone3; ?>" size="2" maxlength="4" type="text" />
		<br>
		###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;####
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Message</b></font></td>
	<td>
		<textarea id="body" name="body" rows="5" style="width:98%"><? echo $body; ?></textarea>
		<br><br>
		<input  value="Submit" type="submit" />
	</td>
</tr>

</table>
</form>

</div>