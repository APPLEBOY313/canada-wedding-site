<div style="color:#FFF;text-align:left;width:840px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
<p class="sansserif" align="justify">
Please use the form below to purchase tickets for the Awards Night on <?php echo $galadate_display; ?>. 
</p>

<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
	<td width="150px" align="right"></td>
</tr>

<?php
if (!empty($code) && $foundcode) {
?>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Email:</b></font></td>
	<td>
		<font face="Verdana" size="2" color="#ebebeb"><? echo $email; ?></font>
		<input type="hidden" name="email" value="<? echo $email; ?>">
	</td>
	<td width="150px" align="right"></td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Company Name:</b></font></td>
	<td>
		<font face="Verdana" size="2" color="#ebebeb"><? echo $company; ?></font>
		<input type="hidden" name="company" value="<? echo $company; ?>">
	</td>
	<td width="150px" align="right"></td>
</tr>

<?php
} else {
?>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Email:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
	<td width="150px" align="right"></td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Company Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="company" name="company" value="<? echo $company; ?>" size="40" type="text" />
	</td>
	<td width="150px" align="right"></td>
</tr>

<?php
}
?>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Phone Number:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<input id="phone1" name="phone1" value="<? echo $phone1; ?>" style="width:40px" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone2" name="phone2" value="<? echo $phone2; ?>" style="width:40px" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone3" name="phone3" value="<? echo $phone3; ?>" style="width:50px" maxlength="4" type="text" />
		</p>
	</td>
	<td width="150px" align="right"><p class="sansserif"><b>Total</b></p></td>
</tr>

<?php
	// Hide if they have purchased the maximum amount of discounted tickets
	if ($eligible-$ticketcount_rsvp>0) {
?>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Number of Discounted Tickets:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<select name="rsvpqty" onchange="recalcCart();">
		<? displayRSVPQty($eligible, $ticketcount_rsvp, $rsvpqty); ?>
		</select>&nbsp;@&nbsp;$<span id="rsvpprice_display"><?php echo $rsvpprice; ?></span> each</p>
	</td>
	<td width="150px" align="right"><p class="sansserif"><span id="rsvptotal_display"><?php echo $rsvptotal_display; ?></span></p></td>
</tr>

<?php
}
?>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Number of Regular Tickets:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<select name="galaqty" onchange="recalcCart();">
		<? displayGalaQty($galaqty); ?>
		</select>&nbsp;@&nbsp;$<span id="galaprice_display"><?php echo $galaprice; ?></span> each</p>
	</td>
	<td width="150px" align="right"><p class="sansserif"><span id="galatotal_display"><?php echo $galatotal_display; ?></span></span></p></td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb">
<?php
	if (!empty($promodisplay)) {
?>	
	<b>Promo Code:</b>
<?php
}
?>	
	</font></td>
	<td><p class="sansserif">
	
<?php
	if (!empty($promodisplay)) {
?>	
		<input id="name" name="discountcode" value="<? echo $discountcode; ?>" size="6" type="text" />&nbsp;&nbsp;
		<input id="dccheck" type="button" value="Apply" onclick="checkDiscountCode();" />&nbsp;&nbsp;<span id="discountcodemessage"></span>
		<br><br>
<?php
}
?>

		<input type="submit" name="galacontinue" value="Continue" />
		</p>
	</td>
	<td width="150px" align="right"><p class="sansserif"><b><span id="amount_display"><?php echo $amount_display; ?></span></span></b></p></td>
</tr>


</table>

<input type="hidden" name="code" value="<?php echo $code; ?>">
<input type="hidden" name="rsvpprice" value="<?php echo $rsvpprice; ?>">
<input type="hidden" name="galaprice" value="<?php echo $galaprice; ?>">
<input type="hidden" name="amount">

</div>