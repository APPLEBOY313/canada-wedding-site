<?php

$g1_class = "Show";
$g2_class = "Hide";
$g3_class = "Hide";
$g4_class = "Hide";
$g5_class = "Hide";
$g6_class = "Hide";

$guestname1 = "";
$guestname2 = "";
$guestname3 = "";
$guestname4 = "";
$guestname5 = "";
$guestname6 = "";

if (!empty($_GET['id'])) {
	$sql = "SELECT * FROM form_gala WHERE sessid='".$_GET['id']."' AND isdeleted=0 AND paypalconfirm=-1";	
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);

	if ($row) {
		$name = stripslashes($row["name"]);
		$company = stripslashes($row["company"]);
		$email = $row["email"];	
		$code = strtoupper($row["code"]);
		$qty = $row["qty"];								
		$phone = $row["phone"];
		$phone1 = substr($phone, 0, 3);
		$phone2 = substr($phone, 3, 3);
		$phone3 = substr($phone, 6, 4);		
		
		$anames = explode(";",stripslashes($row["attendeenames"]));		
		if (!empty($anames[0])) $guestname1=$anames[0];
		if (!empty($anames[1])) $guestname2=$anames[1];
		if (!empty($anames[2])) $guestname3=$anames[2];
		if (!empty($anames[3])) $guestname4=$anames[3];
		if (!empty($anames[4])) $guestname5=$anames[4];
		if (!empty($anames[5])) $guestname6=$anames[5];		

		if ($qty=="1") {
			$g2_class = "Hide";
			$g3_class = "Hide";
			$g4_class = "Hide";
			$g5_class = "Hide";
			$g6_class = "Hide";				
		} else if ($qty=="2") {
			$g2_class = "Show";
			$g3_class = "Hide";
			$g4_class = "Hide";
			$g5_class = "Hide";
			$g6_class = "Hide";				
		} else if ($qty=="3") {
			$g2_class = "Show";
			$g3_class = "Show";
			$g4_class = "Hide";
			$g5_class = "Hide";
			$g6_class = "Hide";				
		} else if ($qty=="4") {
			$g2_class = "Show";
			$g3_class = "Show";
			$g4_class = "Show";
			$g5_class = "Hide";
			$g6_class = "Hide";				
		} else if ($qty=="5") {
			$g2_class = "Show";
			$g3_class = "Show";
			$g4_class = "Show";
			$g5_class = "Show";
			$g6_class = "Hide";				
		} else if ($qty=="6") {
			$g2_class = "Show";
			$g3_class = "Show";
			$g4_class = "Show";
			$g5_class = "Show";
			$g6_class = "Show";				
		}
									
	}

}

?>

<div style="color:#FFF;text-align:left;width:840px;margin-top:5px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p class="sansserif" align="justify">
<b><big>Awards Night</big></b>
<br><br>
Please use the form below to purchase tickets for the Awards Night on November 25th, 2015. 
<br><br>
Tickets are $<?php echo $galaprice; ?><br><br>
</p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="rsvppermakey" value="<?php echo $rsvppermakey; ?>">

<?
	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;\">".$errortext."</p></center>";
	}			

	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;\">".$oktext."</p></center>";
	}
?>

<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Company Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="company" value="<? echo $company; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Email:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Phone Number:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<input id="phone1" name="phone1" value="<? echo $phone1; ?>" size="3" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone2" name="phone2" value="<? echo $phone2; ?>" size="3" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone3" name="phone3" value="<? echo $phone3; ?>" size="4" maxlength="4" type="text" />
		<br>
		###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;####
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Number of Tickets:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<select name="qty" onchange="showHideNames(this)">
		<? displayGalaQty($qty); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px">&nbsp;</td>
	<td>
		<p class="sansserif">
		<table width="100%" cellpadding="2" cellspacing="2">
		<tr valign="top">
			<td>
				<div id="g1" style="color:#FFF;font-size:11px;" class="<?php echo $g1_class;?>">Guest Name 1:&nbsp;&nbsp;<input name="guestname1" value="<? echo $guestname1; ?>" size="20" maxlength="50" type="text" /></div>
				<div id="g2" style="color:#FFF;font-size:11px;" class="<?php echo $g2_class;?>">Guest Name 2:&nbsp;&nbsp;<input name="guestname2" value="<? echo $guestname2; ?>" size="20" maxlength="50" type="text" /></div>
				<div id="g3" style="color:#FFF;font-size:11px;" class="<?php echo $g3_class;?>">Guest Name 3:&nbsp;&nbsp;<input name="guestname3" value="<? echo $guestname3; ?>" size="20" maxlength="50" type="text" /></div>
				<div id="g4" style="color:#FFF;font-size:11px;" class="<?php echo $g4_class;?>">Guest Name 4:&nbsp;&nbsp;<input name="guestname4" value="<? echo $guestname4; ?>" size="20" maxlength="50" type="text" /></div>
				<div id="g5" style="color:#FFF;font-size:11px;" class="<?php echo $g5_class;?>">Guest Name 5:&nbsp;&nbsp;<input name="guestname5" value="<? echo $guestname5; ?>" size="20" maxlength="50" type="text" /></div>
				<div id="g6" style="color:#FFF;font-size:11px;" class="<?php echo $g6_class;?>">Guest Name 6:&nbsp;&nbsp;<input name="guestname6" value="<? echo $guestname6; ?>" size="20" maxlength="50" type="text" /></div>
			</td>
		</tr>
		</table>
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Discount Code:</b></font></td>
	<td>
		<input id="name" name="code" value="<? echo $code; ?>" size="6" type="text" />
		<br><br>
		<input type="submit" name="galacontinue" value="Continue" />
	</td>
</tr>

</table>

</form>

</div>