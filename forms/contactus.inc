<div style="color:#FFF;text-align:left;width:800px;margin-top:20px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p class="sansserif" align="justify">
<b><big>Contact Us</big></b>
<br><br>

If you have any questions about BC Wedding Awards please complete the following form and we will respond promptly.
</p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">

<?
	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;\">".$errortext."</p></center>";
	}			
	
	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;\">".$oktext."</p></center>";
	}
?>


<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Name</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Email</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Message</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<textarea id="body" name="body" rows="5" style="width:98%"><? echo $body; ?></textarea>
		<br><br>
		<input  value="Submit" type="submit" />
	</td>
</tr>

</table>
</form>

</div>