<?
if (!empty($_GET['id'])) {
	
	$sql = "SELECT * FROM form_gala WHERE sessid='".$_GET['id']."' AND isdeleted=0 AND paypalconfirm=-1";	
	$result = mysql_query($sql) or die (mysql_error()."<br>".$sql);
	$row = mysql_fetch_array($result);

	if ($row) {
	
		$name = stripslashes($row["name"]);
		$company = stripslashes($row["company"]);
		$email = $row["email"];	
		$code = strtoupper($row["code"]);
		$qty = $row["qty"];
								
		$phone = $row["phone"];
		$phone1 = substr($phone, 0, 3);
		$phone2 = substr($phone, 3, 3);
		$phone3 = substr($phone, 6, 4);
										
	}
	
}
?>

<div style="color:#FFF;text-align:left;width:840px;margin-top:5px;margin-bottom:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">

<p class="sansserif" align="justify">
<b><big>Gala Night</big></b>
<br><br>
Please use the form below to purchase tickets for our Gala Night. Tickets are $38.
<br><br>
If you submitted for an award, each of your submissions included a ticket and a spot on the guest list. You have been emailed further details. <br><br>
</p>

<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">

<?
	if (!empty($errortext)) {
		echo "<center><p style=\"color:red;\">".$errortext."</p></center>";
	}			
	
	if (!empty($oktext)) {
		echo "<center><p style=\"color:green;\">".$oktext."</p></center>";
	}
?>


<table style="text-align:left;" cellpadding="4" cellspacing="2" border="0" bgcolor="transparent" width="100%">

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Your Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="name" value="<? echo $name; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Company Name:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="name" name="company" value="<? echo $company; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Email:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<input id="email" name="email" value="<? echo $email; ?>" size="40" type="text" />
	</td>
</tr>

<tr valign="top">
	<td align="right" width="250px"><font face="Verdana" size="2" color="#ebebeb"><b>Phone Number:</b></font><span style="color:red;"><small>*</small></span></td>
	<td><p class="sansserif">
		<input id="phone1" name="phone1" value="<? echo $phone1; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone2" name="phone2" value="<? echo $phone2; ?>" size="1" maxlength="3" type="text" />&nbsp;-&nbsp;
		<input id="phone3" name="phone3" value="<? echo $phone3; ?>" size="2" maxlength="4" type="text" />
		<br>
		###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;###&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;####
		</p>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Number of Tickets:</b></font><span style="color:red;"><small>*</small></span></td>
	<td>
		<select name="qty">
		<? displayGalaQty($qty); ?>
		</select>
	</td>
</tr>

<tr valign="top">
	<td align="right" width="80px"><font face="Verdana" size="2" color="#ebebeb"><b>Discount Code:</b></font></td>
	<td>
		<input id="name" name="code" value="<? echo $code; ?>" size="6" type="text" />
		<br><br>
		<input type="submit" name="galacontinue" value="Continue" />
	</td>
</tr>

</table>
</form>

</div>