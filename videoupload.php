<html>
<head>
<title>The Professional BC Wedding Awards</title>
</head>
<body>
<?php 
include_once("dbconnect.php");


$permakey = $_GET['permakey'];
$isadmin = $_GET['admin'];
$issuccess = false;

if (empty($permakey)) $permakey=$_POST['permakey'];
if (empty($isadmin)) $isadmin=$_POST['isadmin'];

$submissionid = getField("submissionid", "submission", "permakey=$permakey");
$categoryid = getField("categoryid", "submission", "permakey=$permakey");

if(!empty($_FILES)) {	
						
	if (is_uploaded_file($_FILES['userVideo']['tmp_name'])) {
							
		// Create the videos folder
		$uploadfolder = _MY_FILE_V_.$submissionid."/";
		createUploadFolder($uploadfolder);
		$uploadfolder = _MY_FILE_V_.$submissionid."/video/";
		createUploadFolder($uploadfolder);
		
		// Set destination paths
		$sourcePath = $_FILES['userVideo']['tmp_name'];
		$videoid = createPermakey();
		
		$path = $_FILES['userVideo']['tmp_name'];
		$ext = end(explode('.', $_FILES['userVideo']['name']));
		$targetPath = $uploadfolder.$videoid.".".$ext;
		
		// Upload the file
		if(move_uploaded_file($sourcePath,$targetPath)) {
			echo "<div id=\"loader-success\" style=\"display:none;font-family:Arial;color:green;\">";
			echo "Video uploaded successfully!<br><br>";
			if ($isadmin=="1") {
				echo "<b><a href=\""._MY_HREF_."admin/submission_detail.php?id=".$submissionid."\">Back to Submission</a></b>";
			} else {
				echo "<b><a href=\"thankyou_submission.php?permakey=".$permakey."\">Continue to payment</a></b>";
			}
			$issuccess = true;
		}
	}
}	else {
	include("top.inc"); 	
?>
<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
			<div class="wrap">
				<p>Video Upload</p>			
				<form id="uploadForm" action="videoupload.php?permakey=<?php echo $permakey; ?>" method="post">
					<input type="hidden" name="permakey" value="<?php echo $permakey; ?>">
					<input type="hidden" name="isadmin" value="<?php echo $isadmin; ?>">
				    <div class="input_bar">
				        <label style="font-family:Arial;">Upload Video File:</label>
				        <input name="userVideo" id="userVideo" type="file" class="demoInputBox" />&nbsp;&nbsp;
				        <input type="submit" id="btnSubmit" value="Upload" class="btnSubmit"/>&nbsp;Max: 250Mb        
				    </div>
				    <div class="media_type">
				    	<p>Allowed file types: MP4, WEBM and OGG</p>	        
				        <div id="videoerror" class="errorblock hide"></div>
				    </div>
				    <div id="progress-div">
				    <div id="progress-bar"></div>
				    </div>
				    <div id="targetLayer"></div>  
				</form>
					
				<?php
			  	if (($categoryid==29 || $categoryid==28) && !$issuccess) {
			  		echo "<form id=\"continueForm\" action=\"thankyou_submission.php?permakey=".$permakey."\" method=\"post\">";
		  			echo "<input type=\"submit\" value=\"Continue to payment\">";
						echo "</form>";
			  	}
			    ?>
			<?php 
				include("bottom.inc");
			}
			?>
			</div>
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
