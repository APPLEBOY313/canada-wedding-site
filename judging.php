<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
			<div class="wrap">			
				<div class="fix-12-12 left">
					<h1 class="margin-bottom-5 ae-1 fromLeft">Judges</h1>
					<p class="sansserif" align="justify">Each entry that is received will be carefully reviewed and scored by the panel of judges. Each of the  judges are carefully chosen to represent different fields within the wedding industry. Each judge has been selected for their overall understanding and experience within the industry.</p>
					<div class="judge_art">
						<div class="col_pan">
							<article>
								<img src="images/GreenTeaPhotography.jpg" width="135" height="90" align="left">
								<p><b>Matt + Ann</b>
								<br>Better than cheesecake on a Wednesday, Matt and Ann of Green Tea Photography are internationally acclaimed wedding photographers with a zest for life and a passion for really, really, really HOT sauce!  Specializing in both creative fine-art as well as photojournalistic style photography, they do their best to meld these two worlds together to tell stories in the most emotionally impactful way possible.</p>
								<a href="http://greenteaphotography.com/" target="_blank">greenteaphotography.com</a>
							</article>
							<article>
								<img src="images/DQ.jpg" width="135" height="90" align="left">
								<p><b>Dave & Quin Cheung</b>
								<br>Dave & Quin Cheung are the visual artists behind DQ Studios and creators of retouching innovation Motibodo.  This husband and wife team are internationally awarded image and album makers and educators in business, lighting and client relationship.  Their focus on great imagery and uncommon client experience has allowed their business to thrive and grow amidst the ever-growing competition.  They make their home with their two boys in Calgary, Alberta, Canada.</p>
								<a href="http://www.dqstudios.com/" target="_blank">www.dqstudios.com</a> | <a href="http://www.motibodo.com/" target="_blank">www.motibodo.com</a>
							</article>
						</div>
						<div class="col_pan">
							<article>
								<img src="images/cindy.jpg" width="90" height="94" align="left">
								<p><b>Cindy Johnson</b>
								<br>Cindy Johnson continues to raise the bar in luxury wedding planning and design. Cindy has spent the last decade refining her skills and hand-picking the best suppliers in the industry.
								Cindy regularly appears as a destination wedding expert on programs such as Breakfast Television, Roger�s Daytime, Real Life, CH Morning Live and more and has been featured in magazines and online outlets, including WedLuxe, Wedding Style Magazine, Weddingbells and StyleatHome.com to name just a few.
								</p>
								<a href="http://www.platinumeventsgroup.com/" target="_blank">Platinum Events Group</a>
							</article>
							<article>
								<img src="siteimages/ArianeSteshaWeb.jpg" width="90" height="90" align="left">
								<p><b>Stesha and Ariane</b>
								<br>Real Weddings art director Stesha Ho has brought the magazine to new heights with fresh and elegant designs that capture the B.C. wedding industry at its best. With four years in Melbourne, Australia working in bridal fashion, she&rsquo;s first to notice upcoming trends. Ariane Fleischmann joined the Real Weddings team as editor in 2016. Her eye for detail and publishing expertise has helped transform the magazine into a necessary and inspiring tool for couples planning their weddings in B.C. Together, they&rsquo;re excited to see this years entries for the awards and to recognize the talent amongst the many vendors in the wedding industry.</p>
								<a href="http://blog.realweddings.ca" target="_blank">Real Weddings</a>
							</article>
						</div>
						<div class="col_pan">
							<article>
								<img src="http://www.bcweddingawards.com/images/belles.jpg" width="90" height="94" align="left">
								<p><b>Sarah and Aubrey</b>
								<br>Sarah and Aubrey are best known for hosting Shaw TV�s Wedding Belles and AMPIA award winning TV series The Proposal. They were Leo Award winners for Hosting in 2014. Along with television they emcee numerous live events including the BC Wedding Awards, My Dream Wedding Show, Wedding Soiree, the BC Wedding Awards and the Leo Awards Celebration Gala as well as contribute to Real Weddings Magazine, Wedding Ring VI magazine, MsInTheBiz and Heirloom Magazine.
								</p>
								<a href="http://www.thebellesperfectproposals.com/" target="_blank">The Proposal</a>
							</article>
							<article>
								<img src="http://www.bcweddingawards.com/images/wong.jpg" width="90" height="90" align="left">
								<p><b>Michael Y. Wong (MYW)</b>
								<br>Internationally acclaimed & award winning event-cinematographer and educator, MYW is renown for producing sophisticated, heartfelt and commercial-grade wedding films + commercials.  Recognized for his non-nonsense teaching approach to filmmaking � his lectures have made appearances world-wide.  Recently his new, sold-out MYW education series of classes in Toronto have already attracted students from as far away as Japan, Brazil, and Vancouver. His commercial works include Sheraton Hotels, Airmiles, Tassimo, Costco to name a few.</p>
								<a href="http://michaelywong.com/" target="_blank"> MichaelWong.com</a>
							</article>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
	</section>

<?php include("bottom2.inc"); ?>