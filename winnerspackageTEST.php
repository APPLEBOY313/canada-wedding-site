<html>

<head>

<title>The Professional BC Wedding Awards </title>

<link rel=stylesheet type="text/css" href="bcwastyle.css">









<script type="text/javascript">

<!--



function newImage(arg) {

	if (document.images) {

		rslt = new Image();

		rslt.src = arg;

		return rslt;

	}

}



function changeImages() {

	if (document.images && (preloadFlag == true)) {

		for (var i=0; i<changeImages.arguments.length; i+=2) {

			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];

		}

	}

}



var preloadFlag = false;

function preloadImages() {

	if (document.images) {

		home_over = newImage("menuimages/home-over.jpg");

		blog_over = newImage("menuimages/blog-over.jpg");

		categories_over = newImage("menuimages/categories-over.jpg");

		judging_over = newImage("menuimages/judging-over.jpg");

		faq_over = newImage("menuimages/faq-over.jpg");

		events_over = newImage("menuimages/events-over.jpg");

		contact_over = newImage("menuimages/contact-over.jpg");

		preloadFlag = true;

	}

}



// -->

</script>









</head>





<body onLoad="preloadImages();">

<center>

<table width="900" border="0" cellpadding="0" cellspacing="0">

	<tr>

	<td><br><a href="index.php"><img src="siteimages/logo.png" align="left"/ hspace="10" border="0" ></a>

		<br><br><br><br><br><br><br><br><br><br><br>

	</td>



	<td align="right"><br><br><br><br><br><br><br><br><!--<button type="button" onClick="window.location.href='mailing.php';">Sign Up for Our Mailing List</button>-->

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<br><br>
<img src="http://www.bcweddingawards.com/siteimages/links.jpg" width="227" height="25" border="0" USEMAP="#links_Map">
&nbsp;&nbsp;&nbsp;&nbsp;

	</td>

	</tr>

</table>




<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">

	<tr>

		<td colspan="15">

			<img src="menuimages/menu_01.jpg" width="879" height="4" alt="" /></td>

	</tr>

	<tr>

		<td>

			<img src="menuimages/menu_02.jpg" width="38" height="21" alt="" /></td>

		<td>

			<a href="index.php"

				onmouseover="changeImages('home', 'menuimages/home-over.jpg'); return true;"

				onmouseout="changeImages('home', 'menuimages/home.jpg'); return true;">

				<img name="home" src="menuimages/home.jpg" width="68" height="21" border="0" alt="" /></a></td>

		<!--<td>

			<img src="menuimages/menu_04.jpg" width="30" height="21" alt="" /></td>

		<td>

			<a href="http://bcweddingawards.com/blog/" target="_blank"

				onmouseover="changeImages('blog', 'menuimages/blog-over.jpg'); return true;"

				onmouseout="changeImages('blog', 'menuimages/blog.jpg'); return true;">

				<img name="blog" src="menuimages/blog.jpg" width="67" height="21" border="0" alt="" /></a></td>-->

		<td>

			<img src="menuimages/menu_06.jpg" width="25" height="21" alt="" /></td>

		<td>

			<a href="http://www.bcweddingawards.com/categories.php"

				onmouseover="changeImages('categories', 'menuimages/categories-over.jpg'); return true;"

				onmouseout="changeImages('categories', 'menuimages/categories.jpg'); return true;">

				<img name="categories" src="menuimages/categories.jpg" width="162" height="21" border="0" alt="" /align="right"></a></td>

		<td>

			<img src="menuimages/menu_08.jpg" width="40" height="21" alt="" /align="right"></td>

		<td>

			<a href="judging.php"

				onmouseover="changeImages('judging', 'menuimages/judging-over.jpg'); return true;"

				onmouseout="changeImages('judging', 'menuimages/judging.jpg'); return true;">

				<img name="judging" src="menuimages/judging.jpg" width="111" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="menuimages/menu_10.jpg" width="34" height="21" alt="" /></td>

		<td>

			<a href="faq.php"

				onmouseover="changeImages('faq', 'menuimages/faq-over.jpg'); return true;"

				onmouseout="changeImages('faq', 'menuimages/faq.jpg'); return true;">

				<img name="faq" src="menuimages/faq.jpg" width="51" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="menuimages/menu_12.jpg" width="35" height="21" alt="" /></td>

		<td>

			<a href="events.php"

				onmouseover="changeImages('events', 'menuimages/events-over.jpg'); return true;"

				onmouseout="changeImages('events', 'menuimages/events.jpg'); return true;">

				<img name="events" src="menuimages/events.jpg" width="91" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="menuimages/menu_14.jpg" width="31" height="21" alt="" /></td>

		<td>

			<a href="contact.php"

				onmouseover="changeImages('contact', 'menuimages/contact-over.jpg'); return true;"

				onmouseout="changeImages('contact', 'menuimages/contact.jpg'); return true;">

				<img name="contact" src="menuimages/contact.jpg" width="112" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="menuimages/menu_16.jpg" width="39" height="21" alt="" /></td>

	</tr>

	<tr>

		<td colspan="15">

			<img src="menuimages/menu_17.jpg" width="879" height="4" alt="" /></td>

	</tr>

</table>



<table width="879" border="0" cellpadding="10" cellspacing="0" align="center">

	<tr>



<td width="440"><br><br>

<img src="siteimages/awards2014.jpg"><br>

<img src="siteimages/2015winnersheader.jpg" width="250" height="75">


<p class="sansserif" align="justify">


<br><br>
<br><br>Each entry that is received will be carefully reviewed and scored by the panel of <a href="http://www.bcweddingawards.com/judging.html"><b><big> judges</big></b></a> to determine the winners.
<br><br>


<b>2015 winners will receive the following:</b>
<br>
<br>

 - Print coverage of the Awards in BC's largest Wedding magazine between December 2015 - December 2016.
<br><br>

 - Full use of the awards logo online and in print.  <br><br>
 - Elegant engraved glass trophy.<br><br>
 - Recognition and presentation of your award at our <a href="http://www.bcweddingawards.com/events.html"><b><big> Annual Awards Gala </big></b></a>
 <br><br> 
 - Online listing for your company through the <a href="http://www.bcweddingawards.com"><b><big>Professional BC Wedding Awards</big></b></a> website. <br><br>

<br><br><br><br><br>

The Annual Awards Event will be an outstanding opportunity to network with a wide range of vendors in the BC Wedding Industry, as well as give everyone a fun industry night out!
<br><br>
If you wish to be notified of details as they emerge, take a moment to follow us on twitter, and become a friend on facebook!
<br><br><center>

</center>

<br><br><br><br>

<!--<img src="images/judge.jpg" width="90" height="90" align="left" hspace="10">







<b>Judge</b>

<br>

Judge bio here<br><br>

</td>

<td>



<td width="439"><p class="sansserif" align="justify">





<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<img src="images/judge.jpg" width="90" height="90" align="left" hspace="10">



<b>Judge</b>

<br>

Judge bio here<br><br>

</td>





</tr>

<!--<tr>

<td colspan="4"><img src="siteimages/dots.jpg" width="879" height="9" alt="" /><p class="sansserif" align="justify">If you’re a vendor within the Wedding industry then get ready!  We are looking for outstanding companies and we want you to submit your best work.  Being a finalist in the awards will give you serious credentials and publicity, forever changing your business opportunities and professional network capabilities.

</td></tr>-->

	</tr>

</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>

<td><img src="siteimages/dots.jpg" width="879" height="9" alt="" /></td>

	</tr>

	</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>



   <td style="padding-right:5px;padding-left:5px;" align="left">



     <a href="index.php" >HOME</a>

   &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->


    <a href="http://www.bcweddingawards.com/categories.php">CATEGORIES</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="judging.php">JUDGING</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="faq.php">FAQ</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="events.php">EVENTS</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="contact.php">CONTACT</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

    </td>




	</tr>

</table>









<MAP NAME="links_Map">

<AREA SHAPE="rect" ALT="" COORDS="202,0,224,25" HREF="http://twitter.com/BCweddingAwards" TARGET="_blank">

<AREA SHAPE="rect" ALT="" COORDS="177,0,200,25" HREF="http://www.facebook.com/pages/Professional-BC-Wedding-Awards/137667716271526?created" TARGET="_blank">

</MAP>














</body>



</html>

