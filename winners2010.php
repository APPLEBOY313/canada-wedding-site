<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body">
<?php include("top.inc"); ?>
<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
			<div class="wrap winners2010_con">
				<h1>2010 Winners</h1>
				<span>Organizers of the 2010 Professional BC Wedding Awards handed out 20 trophies on December 1, 2010 in Vancouver BC at District 319.
				The ../winners are those companies with the highest scoring entries. In order to qualify as a finalist the score needs to be within 5% of the winning score.  There were a maximum of 2 runners up per category.</span><br><br>
				<p><small>Please note that in some cases, the text that appears is abridged. Photographer credit was not known for all entries, it has been provided wherever possible.</small></p>
				<div class="list_2column">
					<span class="winners_list left_justify">
						<a href='#1'>Best Wedding Reception Venue</a>
						<a href='#2'>Best Wedding Ceremony Location</a>
						<a href='#3'>Best Wedding Cake</a>
						<a href='#4'>Best Catered Wedding</a>
						<a href='#5'>Best Bridal Bouquet</a>
						<a href='#6'>Best Wedding Florist - Overall</a>
						<a href='#7'>Best Wedding Decor</a>
						<a href='#8'>Best Wedding Event Planning</a>
						<a href='#9'>Best Wedding Make-up</a>
						<a href="#10">Best Wedding Hair Style</a>
					</span>
					<span class="winners_list left_justify">
						<a href='#11'>Best Wedding Stationery</a>
						<a href='#12'>Best Wedding Videographer/ Cinematographer</a>
						<a href='#13'>Best Wedding Jewelry Design - Rings</a>
						<a href='#14'>Best Wedding Jewelry Design - Accessories</a>
						<a href='#15'>Best Wedding Transportation</a>
						<a href='#16'>Photography - Best Overall Wedding Photography</a>
						<a href='#17'>Photography - Best Candid/Photojournalism photograph</a>
						<a href='#18'>Photography - Best portrait: Bride and Groom together</a>
						<a href='#19'>Photography - Best Wedding Detail Photograph</a>
						<a href='#20'>Photography - Best Wedding Group Photograph</a>
					</span>
				</div>
				<br><br>
				<span style="text-align: center;">
					<p>2010 Industry Achievement Award Winner</p>
				</span>	
				<div class="winner">
					<h2><strong><a name="1"></a>Best Wedding Reception Venue:<br>Vancouver Aquarium</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.visitvanaqua.org" target="_blank">www.visitvanaqua.org</a><br/><br/>
						<i>"A unique event facility space, where guests can explore our habitats and displays while enjoying food and beverage in our amazing galleries. All proceeds from events go towards the education, conservation and research initiative of our programs."
						</i><br></p>
						<img src="winners/photos/rv1_3786_2.jpg "><br><br>
						<img src="winners/photos/rv1_3786_3.jpg "><br><br>
						<img src="winners/photos/rv1_3786_4.jpg "><br><br>
						<img src="winners/photos/rv1_3786_5.jpg "><br><br>
						<img src="winners/photos/rv1_3786_6.jpg "><br><br>
						<img src="winners/photos/rv1_3786_8.jpg "><br>
						<small>Photo Credit: Sweet Pea Photography</small>
					</div>
					<p>RUNNER-UP</p>
					<p>UBC Boathouse</p>
					<p><a href="winners2010.php" >return to category listing</a></p>
				</div>
				<div class="winner">
					<h2><strong><a name="2"></a>Best Wedding Ceremony Location:<br>Celebration Pavilion - Queen Elizabeth Park</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.thechapels.ca" target="_blank">www.thechapels.ca</a><br/><br/>
						<i>"Our goal is to provide all couples with an unique and memorable wedding experience. Our venue has capacity for 125 guests, a raised alter with a unique glass wall which opens to create a natural outdoor backdrop, Beautiful silk flower arrangements in ivory or red included with facility rental, Bride's room which includes a separate washroom for the bride and her attendants, Grand Piano, Covered Walkway. Located in the heart of Vancouver at the highest elevation, the view is spectacular! No need to relocate for your photos as manicured gardens, fountains and winding walkways surround the area."
						</i><br></p>
						<img src="winners/photos/cl1_0701_5.jpg "><br><br>
						<img src="winners/photos/cl1_0701_6.jpg "><br><br>
						<img src="winners/photos/cl1_0701_8.jpg "><br><br>
						<img src="winners/photos/cl1_0701_2.jpg "><br><br>
					</div>		
					<p>RUNNER-UP</p>
					<p>UBC Boathouse</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="3"></a>Best Wedding Cake:<br>Ganache Pâtisserie</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.ganacheyaletown.com" target="_blank">www.ganacheyaletown.com</a><br/><br/>
						<i>"With a nod to the City of Love, this wedding cake draws inspiration from the romantic and timeless beauty of Paris with some modern flair in its arrangement, indvidually designed tiers and a non-traditional flower on top, a handmade sugar gardenia."
						</i><br></p>
						<img src="winners/photos/wc1_1098_1.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Culinary Cakes</p>
					<p><a href="winners2010.php" >return to category listing</a></p>					
				</div>
				<div class="winner">
					<h2><strong><a name="4"></a>Best Catered Wedding:<br>Culinary Capers Catering</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.culinarycapers.com" target="_blank">www.culinarycapers.com</a><br/><br/>
						<i>"It was key to focus on seasonal late summer ingredients, highlighting unique finishes, textures and presentation. Also working with ingredients from regional farms and using sustainable seafood products."
						<br><br>
						"The passed hors d'oeuvre included the bride's mother's favourite: crispy mac and cheese croquette made with BC Farmhouse aged cheddar with a spicy tomato dip; a toasted crisp brioche spoon topped with duck, ginger and kumquat barbecue sauce finished with sour cream and smoked salt crystals; and crepe purses filled with wild porcini mushrooms and fresh herbs"
						<br><br>
						"Each station featured chefs preparing and presenting complete small plates adding energy to the reception's dining experience. The lamb carving station featured Moroccan rack of lamb stuffed with anchovies and mint, scented with traditional spices and preserved lemon. It was served with an apricot almond coucous, vegetables jewels and finished with a caramelized lemon sauce.
						<br><br>
						"Other stations included a chef's antipasto station featuring fresh local spot prawns with crispy spiced pork belly in a nasturtium vinaigrette; marinated albacore tuna and organic BC artisan charcuterie and cheeses. A beef station featured a grilled rib-eye steak and servers passed a complete mini meal that featured a miso maple glazed Spring salmon finished with a yazu gastrique."
						<br><br>
						"The bride and groom were huge dessert lovers so the piéce de résistance of the evening was the dessert station featuring 16 miniature desserts. Our pastry chef brainstormed with our bride to create a French patisserie inspired table. Desserts were presented on a variety of different white and clear ceramic dessert stands and on customized white wooden tiering to give visual layering to the station. Favourites included; banana cake pops dipped in chocolate and rolled in house made toffee, dolce de leche cheesecakes, traditional meyer lemon bars, chocolate turtle shots and warm apple and ginger crisp. Something to satisfy every sweet tooth.
						<br><br>
						"The white 4-tiered wedding cake featured hand sculpted sugar hydrangea. Two tiers were a lemon raspberry cake. A vanilla butter cake with layers of zesty lemon curd, fluffy vanilla bean buttercream, embedded with fresh raspberries. And two tiers were a Neopolitan cake which was layers of milk chocolate cake with chocolate, strawberry and vanilla buttercream and rich dolce de leche finished with a fresh strawberry coulis syrup."
						<br><br>
						"A take home gift was a selection of house made gourmet cookies presented in old fashioned candy jars. Guests were encouraged to take a bag and fill them with their favourites."
						</i><br></p>
						<img src="winners/photos/fc1_0123_1.jpg"><br><br>
						<img src="winners/photos/fc1_0123_2.jpg"><br><br>
						<img src="winners/photos/fc1_0123_4.jpg"><br><br>
						<img src="winners/photos/fc1_0123_5.jpg"><br><br>
						<img src="winners/photos/fc1_0123_6.jpg"><br><br>
						<img src="winners/photos/fc1_0123_7.jpg"><br><br>
						<img src="winners/photos/fc1_0123_8.jpg"><br><br>
						<img src="winners/photos/fc1_0123_11.jpg"><br><br>
						<img src="winners/photos/fc1_0123_12.jpg"><br>
						<small>Photo Credit: Foodie Photography</small>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="5"></a>Best Bridal Bouquet:<br>A Sea of Bloom Floral Designs</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.aseaofbloom.com" target="_blank">www.aseaofbloom.com</a><br/><br/>
						<i>"My company began as a small cutting garden to leasing a farm and growing and selling cutflowers to local florist and farmers markets"<br><br>"I am known for local flowers and I like the unusal ones too. That's fun! My customers love it!"
						</i><br></p>
						<img src="winners/photos/bb1_3952_1.jpg"><br><br>
						<img src="winners/photos/bb1_3952_2.jpg"><br><br>
						<img src="winners/photos/bb1_3952_3.jpg"><br><br>
						<img src="winners/photos/bb1_3952_4.jpg"><br>
						<small>Photo Credit: Ophelia Photography.</small>
					</div>
					<p>RUNNER-UP</p>
					<p>The Flowerbox Inc.<br>ROA Floral and Event Design</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Reception Venue:<br>Vancouver Aquarium</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.visitvanaqua.org" target="_blank">www.visitvanaqua.org</a><br/><br/>
						<i>"A unique event facility space, where guests can explore our habitats and displays while enjoying food and beverage in our amazing galleries. All proceeds from events go towards the education, conservation and research initiative of our programs."
						</i><br></p>
						<img src="winners/photos/rv1_3786_2.jpg "><br><br>
						<img src="winners/photos/rv1_3786_3.jpg "><br><br>
						<img src="winners/photos/rv1_3786_4.jpg "><br><br>
						<img src="winners/photos/rv1_3786_5.jpg "><br><br>
						<img src="winners/photos/rv1_3786_6.jpg "><br><br>
						<img src="winners/photos/rv1_3786_8.jpg "><br>
						<small>Photo Credit: Sweet Pea Photography</small>
						<p align="right"><a href="winners2010.php" >return to category listing</a></p>
					</div>
					<p>RUNNER-UP</p>
					<p>UBC Boathouse</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Florist - Overall:<br>Simply Sweet Floral Designs</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.visitvanaqua.org" target="_blank">www.visitvanaqua.org</a><br/><br/>
						<i>"This wedding is unique because the couple trusted us to design their wedding flowers almost entirely up to us."<br><br>"The colors are very bold, jewel tones and really gave a statement that went well with their attire and venues."
						</i><br></p>
						<img src="winners/photos/wf1_2055_1.jpg"><br><br>
						<img src="winners/photos/wf1_2055_2.jpg"><br><br>
						<img src="winners/photos/wf1_2055_6.jpg"><br><br>
						<img src="winners/photos/wf1_2055_8.jpg"><br><br>
						<img src="winners/photos/wf1_2055_10.jpg"><br><br>
						<img src="winners/photos/wf1_2055_11.jpg"><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Flowerz</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="7"></a>Best Wedding Decor:<br>Tie The Knot Events</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.tietheknotvictoria.com" target="_blank">www.tietheknotvictoria.com</a><br/><br/>
						<i>"Instead of doing the classic beach theme wedding which tends to be on the casual side we wanted to create a wedding with an upscale waterfront feel through the use of sand, shells, mirrors, interesting vases and glassware. Our centerpieces ran the length of the table with reflecting mirrors and many unique pieces. Tables were full of glassware, vases and flowers that made each point of view different. We added white chair covers and cream taffeta ties with one simple stem of ivy. Guests had views of the ocean and we wanted this to be reflected in the decor. After the formal dinner guests were invited into the lounge we had created for dancing. This room was a plain meeting room which we transformed into an intimate club feel. We created this atmosphere by draping the entire room from floor to ceiling with white sheer fabric, soft up lighting and white lounge furniture. We also hung many large mirrors around the room as well as adding mirrors to the existing light fixtures creating a chandelier effect and we topped it all off with a gleaming white dance floor."
						</i><br></p>
						<img src="winners/photos/wd1_1200_1.jpg"><br><br>
						<img src="winners/photos/wd1_1200_2.jpg"><br><br>
						<img src="winners/photos/wd1_1200_3.jpg"><br><br>
						<img src="winners/photos/wd1_1200_5.jpg"><br><br>
						<img src="winners/photos/wd1_1200_6.jpg"><br><br>
						<img src="winners/photos/wd1_1200_12.jpg"><br>
						<img src="winners/photos/wd1_1200_10.jpg"><br>
					</div>			
				</div>
				<div class="winner">
					<h2><strong><a name="8"></a>Best Wedding Event Planning:<br>Lori L Fraser Signature Events</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.lorilfrasersignatureevents.com" target="_blank">www.lorilfrasersignatureevents.com</a><br/><br/>
						<i>"My couple wanted a unique, over the top wedding for 300 plus guests w/ their own ministers and a church that would hold 300, the reception at their chosen venue a new hotel.&quot;
						</i><br></p>
						<img src="winners/photos/ep1_1722_1.jpg"><br><br>
						<img src="winners/photos/ep1_1722_3.jpg"><br><br>
						<img src="winners/photos/ep1_1722_4.jpg"><br><br>
						<img src="winners/photos/ep1_1722_7.jpg"><br><br>
						<img src="winners/photos/ep1_1722_9.jpg"><br><br>
						<img src="winners/photos/ep1_1722_11.jpg"><br>
						<img src="winners/photos/ep1_1722_10.jpg"><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Elements Modern Weddings</p>
					<p><a href="winners2010.php" >return to category listing</a></p>
				</div>
				<div class="winner">
					<h2><strong><a name="9"></a>Best Wedding Make-up:<br>Felicia Bromba  |  Make-up Artist</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.makeup-by-felicia.com" target="_blank">www.makeup-by-felicia.com</a><br/><br/>
						<i>"Medical grade and Non-medical levels of Makeup applications for those suffering from Skin conditions including such areas as Facial Psoriasis, Rosacea, Melasma and more."
						</i><br></p>
						<img src="winners/photos/ma1_5505_1.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Jayna Marie Make-Up + Hair</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="10"></a>Best Wedding Hair Style:<br>Mink Makeup + Hair</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.visitvanaqua.org" target="_blank">www.visitvanaqua.org</a><br/><br/>
						<i>"Amber wanted a stylish, contemporary hair style that nonetheless didn't appear too "stuffy," but somewhat loose and romantic, while still having all her hair up. I developed an a-symmetrical style with a braid on one side of her head and a loose, curly bun on the other side. This hair style was achieved using extensions for her straight shoulder-length hair, curling all her hair, and by employing hair sewing as a technique to create the upstyle. Hair sewing is a relatively new concept in hair styling and uses upholstery thread to literally sew the hair into a very secure, yet comfortable, style for the bride."
						</i><br></p>
						<img src="winners/photos/hs1_9901_1.jpg"><br><br>
						<img src="winners/photos/hs1_9901_2.jpg"><br><br>
						<img src="winners/photos/hs1_9901_4.jpg"><br><br>
						<img src="winners/photos/hs1_9901_5.jpg"><br><br>
						<small>Photo Credit: Gloria Bell - Island Expressions Photography</small>
					</div>
					<p>RUNNER-UP</p>
					<p>UBC Boathouse</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="11"></a>Best Wedding Stationery:<br>Uniquity Invitations</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.uniquityinvitations.com" target="_blank">www.uniquityinvitations.com</a><br/><br/></p>
						<img src="winners/photos/sd1_0426_1.jpg"><br><br>
						<img src="winners/photos/sd1_0426_2.jpg"><br><br>
						<img src="winners/photos/sd1_0426_3.jpg"><br><br>
						<img src="winners/photos/sd1_0426_4.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>I said Yes Wedding Stationery and Design</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="12"></a>Best Wedding Videography/Cinematography:<br>Love Story Media</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.lovestorymedia.com" target="_blank">www.lovestorymedia.com</a><br/><br/>
						</p>
						<iframe src="http://player.vimeo.com/video/10521920" width="600" height="340" frameborder="0"></iframe><br>
					</div>
					<p>RUNNER-UP</p>
					<p>RF Productions<br>Hello Tomorrow Productions</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="13"></a>Best Wedding Jewelry Design - Rings:<br>Jewellery Artists 3D</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.ja3d.com" target="_blank">www.ja3d.com</a><br/><br/>
						<i>"These rings were created for a Vancouver couple who wanted to express their unique personal experiences and values in two rings. The Brides Platinum ring, with 28 diamonds and 12 saphires, reflects experiences of the two Canadians travels in India, and is inspired by the shape and colors of a lotus flower blossom. The Groom's ring is designed around the themes of uniqueness and the unexpected. The shape of his ring is square rather than round. Even the metal is special and unusual. It is composed of Palladium another pure precious metal, and has been finished with a hammered and sandblasted texture to contrast the highly polished centre which holds three diamonds. The bridal couple wished for only two rings representing their relationship."
						</i><br></p>
						<img src="winners/photos/jd1_8821_1.jpg"><br><br>
						<img src="winners/photos/jd1_8821_2.jpg"><br><br>
						<img src="winners/photos/jd1_8821_3.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Britton Diamonds</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="14"></a>Best Wedding Jewelry Design - Accessories:<br>Elsa Corsi  |  Beautiful Jewellery</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.elsacorsi.com" target="_blank">www.elsacorsi.com</a><br/><br/>
						<i>"This suite consists of earrings, necklace and a "convertible" hair brooch.<br><br>It was created specifically using the inspirations brought to us by the bride.<br><br>The hairpiece has a customized adapter so that after the wedding the piece may be worn as a brooch.<br><br>The earrings and necklace are considered to be one of a kind. They have not been made before and will not be made again. Our brides appreciate that their pieces are not available by the hundreds (or even dozens) and that they are just as unique as every wedding detail.<br><br>The crystals used are all Swarovski, the highest quality of crystal available.<br><br>The technique is that of vintage jewellery-jewellery popularized in the 1950's. Often when clients see our jewellery, the proclaim that their grandmother had a piece "just like it'...we are thrilled to be creating jewellery that has the same longevity in style and quality for that can be added to the family jewellery box that impart their own story."
						</i><br></p>
						<img src="winners/photos/jd2_5577_1.jpg"><br><br>
						<img src="winners/photos/jd2_5577_2.jpg"><br><br>
						<img src="winners/photos/jd2_5577_3.jpg"><br><br>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="15"></a>Best Wedding Transportation:<br>Ultimate Limousine</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.ultimatelimo4you.com" target="_blank">www.ultimatelimo4you.com</a><br/><br/>
						<i>"Our fleet is elegant with exclusive vehicles that will make your big day even more glamorous, accommodating anywhere from 4 to 22 passengers."<br><br>"...We provide complimentary decorations, ice. bottled water, cold champagne and red carpet service at no charge to our clients. That is also one of the contributing that factors in the majority of wedding reservations that we get year after year. As you can see in the pictures all of our limousines are decorated with the color theme that our clients provide us with. We place flowers on the door handles, ribbon on the hood and a bow on the grill of the limousine."
						</i><br></p>
						<img src="winners/photos/wt1_8888_2.jpg "><br><br>
						<img src="winners/photos/wt1_8888_3.jpg "><br><br>
						<img src="winners/photos/wt1_8888_5.jpg "><br><br>
						<img src="winners/photos/wt1_8888_6.jpg "><br><br>
						<img src="winners/photos/wt1_8888_7.jpg "><br><br>
						<img src="winners/photos/wt1_8888_8.jpg "><br><br>
						<img src="winners/photos/wt1_8888_9.jpg "><br><br>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="16"></a>Photography - Best Overall Wedding Photography:<br>Bebb Studios</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.bebbstudios.com" target="_blank">www.bebbstudios.com</a><br/><br/></p>
						<img src="winners/photos/bp1_4221_1.jpg"><br><br>
						<img src="winners/photos/bp1_4221_2.jpg"><br><br>
						<img src="winners/photos/bp1_4221_3.jpg"><br><br>
						<img src="winners/photos/bp1_4221_4.jpg"><br><br>
						<img src="winners/photos/bp1_4221_5.jpg"><br><br>
						<img src="winners/photos/bp1_4221_6.jpg"><br><br>
						<img src="winners/photos/bp1_4221_7.jpg"><br><br>
						<img src="winners/photos/bp1_4221_8.jpg"><br><br>
						<img src="winners/photos/bp1_4221_9.jpg"><br><br>
						<img src="winners/photos/bp1_4221_10.jpg"><br><br>
						<img src="winners/photos/bp1_4221_11.jpg"><br><br>
						<img src="winners/photos/bp1_4221_12.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Jonetsu Studios</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="17"></a>Photography - Best Candid/Photojournalism Photograph:<br>StonePhoto</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a><br/><br/></p>
						<img src="winners/photos/bp2_3049_1.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Randal Kurt Photography<br>Pardeep Singh Photography</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="18"></a>Photography - Best portrait: Bride and Groom Together:<br>Bebb Studios</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.bebbstudios.com" target="_blank">www.bebbstudios.com</a><br/><br/></p>
						<img src="winners/photos/bp3_4221_1.jpg"><br><br>
					</div>
					<p>RUNNER-UP</p>
					<p>Daniela Ciuffa Photography</p>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="19"></a>Photography - Best Wedding Detail Photograph:<br>StonePhoto</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a><br/><br/></p>
						<img src="winners/photos/bp3_5447_1.jpg"><br><br>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>
				<div class="winner">
					<h2><strong><a name="20"></a>Photography - Best Wedding Group Photograph:<br>Ophelia Photography</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.opheliaphotography.com" target="_blank">www.opheliaphotography.com</a><br/><br/></p>
						<img src="winners/photos/bp3_3404_1.jpg"><br><br>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>				
				</div>

				<div class="winner">
					<h2><strong><a name="21"></a>Industry Achievement Award:<br>Genève McNally - DreamGroup Productions</strong></h2>
					<div class="winner_pic">
						<p>
						<a href="http://www.dreamgroup.ca" target="_blank">www.dreamgroup.ca</a><br/><br/></p>
						<p>
							Top Scoring Individuals for Industry Achievement Award<br>
							(In Random Order)<br>
							Colin Upright<br>
							Tracey Heppner<br>
							Angela Girard<br>
							Geneve McNally<br>
							Kathy Marliss<br>
							Evan Orion<br>
							Tanya Peters<br>
							Sarah Shore<br>
						</p>
					</div>
					<p><a href="winners2010.php" >return to category listing</a></p>
					<span>
						We are very excited to be supporting the Intersections Film Club with our Gala Event.Intersections Media Opportunities For Youth Society is a non-profit society which offers a variety of skills development programs for at-risk youth in the lower mainland who face multiple barriers to employment.<br><br>Intersections' mandate is to transition at-risk youth into continued education and/or sustainable employment, while reinforcing healthy life-style choices and positive life skills. Intersections provides workshop and project-based programs that focus on film, video, and the visual arts as a means of teaching fundamental life and employment skills to its participants.<br><br>
					</span>	
					<a href="http://intersectionsfilmclub.tumblr.com/" target="_blank"><img src="siteimages/IntsxFilmClub_Logo-small.jpg" width="300" height="134" alt=""  border="0"/ align="center"></a><br><br>
				</div>
			</div>
		</div>
	</div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>

</body>
</html>
