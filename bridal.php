<html>
<head>
<title>The Professional BC Wedding Awards</title>
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
    		<div class="wrap contact_con">
    			<h1>Contact Us</h1>
    			<p>If you have any questions about BC Wedding Awards please complete the following form and we will respond promptly.</p>
				<form id="theform" method="post" action="<? echo $_SERVER['PHP_SELF']; ?>">
					<span>
						<label>NAME<b>*</b></label>
						<input type="text" name="name">
					</span>
					<span>
						<label>Email<b>*</b></label>
						<input type="text" name="email">
					</span>
					<span class="msg_span">
						<label>Message<b>*</b></label>
						<textarea rows="5" name="msg"></textarea>
					</span>	
					<button type="submit">Submit</button>
				</form>
				<div class="us_info">
					<div class="us_row">
						<span>
							<img src="images/JustinBCWAbio.png">
							<p>Justin Eckersall<br>Producer<br>justin(at)bcweddingawards.com</p>
						</span>
						<span>
							<img src="images/SteffBCWAbioPic.jpg">
							<p>Steff Kobialka<br>Managing Partner<br>					info(at)bcweddingawards.com</p>
						</span>
					</div>
					<div class="us_row">
						<span>
							<img src="images/sachaChinBCWAbioPic.jpg">
							<p>Sacha Chin<br>Event Coordinator<br>
							sacha(at)thefunteam.com</p>
						</span>
						<span>
							<img src="images/TiffanyBCWAbioPic.jpg">
							<p>Tiffany Elsner<br>Event Volunteer Coordinator<br>
							tiffany(at)thefunteam.com</p>
						</span>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>
<?php include("bottom2.inc"); ?>