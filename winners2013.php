<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body">
<?php include("top.inc"); ?>
<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
			<div class="wrap winners2013_con">
				<h1>2013 Winners</h1>
				<span>Organizers of the 2013 Professional BC Wedding Awards handed out 26 trophies on November 27, 2013 at The Pinnacle Hotel at the Pier in North Vancouver. The winners are those companies with the highest scoring entries. There is a maximum of 2 finalists per category, and they are listed here in alphabetical order.</span><br><br>
				<p><small>Please note that in most cases, only a small part of the entry material is shown publicly. Photographer credit was not known for all entries, it has been provided wherever possible.</small></p>
				<div class="list_2column">
					<span class="winners_list left_justify">
						<a href='#1'>Best Wedding Cake</a>
						<a href='#2'>Best Bridal Bouquet</a>
						<a href='#3'>Best Wedding Florist - Overall</a>
						<a href='#4'>Best Wedding Make-up</a>
						<a href='#5'>Best Wedding Hair Style</a>
						<a href='#6'>Best Wedding Decor</a>
						<a href='#7'>Best Wedding Jewelry Design - Accessories</a>
						<a href='#8'>Best Wedding Jewelry Design - Rings</a>
						<a href='#9'>Best Wedding Transportation</a>
						<a href="#10">Best Wedding Stationery</a>
						<a href='#11'>Best Wedding Reception Venue - Alternative Location</a>
						<a href='#12'>Best Wedding Reception Venue - Hotel or Banquet Hall</a>
						<a href='#13'>Best Wedding Ceremony Location</a>
					</span>
					<span class="winners_list left_justify">
						<a href='#113'>Best Wedding Officiant</a>
						<a href='#14'>Best Wedding Event Planning</a>
						<a href='#114'>Best Catered Wedding</a>
						<a href='#115'>Best Live Wedding Entertainment</a>
						<a href='#15'>Best Candid/Photojournalism Photograph</a>
						<a href='#16'>Best Portrait- Bride and Groom Together</a>
						<a href='#17'>Best Wedding Detail Photograph</a>
						<a href='#18'>Best Wedding Group Photograph</a>
						<a href='#19'>Best Overall Wedding Photography</a>
						<a href='#20'>Best Wedding Videographer/Cinematographer</a>
						<a href='#21'>Best Edited Wedding Video</a>
						<a href='#22'>2013 Tasters Choice - Best Hors D’Oeuvre</a>
						<a href='#23'>2013 Judges Choice - Best Hors D'Oeuvre</a>
					</span>
				</div>
				<div class="winner">
					<h2><strong><a name="1"></a>Best Wedding Cake</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Whisk Cake Company</span><br/>
						<a href="http://www.whiskcakes.com" target="_blank">www.whiskcakes.com</a><br/><br/>
						<img src="winners2013/Whisk_wc1_7913_3.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Aeyra Cakes</span><br/>
							<a href="http://www.aeyracakes.ca" target="_blank">www.aeyracakes.ca</a></p>
							<p align="center"><img src="winners2013/Aeyra_wc1_3584_3.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">When Pigs Fly Pastries</span><br />
							<a href="http://www.whenpigsflypastries.com" target="_blank">www.whenpigsflypastries.com</a></p>
							<p align="center"><img src="winners2013/AnnaEliz2_wc1_4589_1.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Flower Factory</span><br/>
						<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br/><br/>
						<img src="winners2013/FlowerFactory_bb1_0003_1.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Just Fresh Concepts</span><br/>
							<a href="http://www.justfreshconcepts.ca" target="_blank">www.justfreshconcepts.ca</a></p>
							<p align="center"><img src="winners2013/JustFresh_bb1_0357_1.jpg" width="266" height="400"  border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Sunflower Florist</span><br />
							<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>
							<p align="center"><img src="winners2013/Sunflower_bb1_7677_2.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Verbena Floral Design</span><br/>
						<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a><br/><br/>
						<img src="winners2013/VerbenaOverall1.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Just Fresh Concepts</span><br/>
							<a href="http://www.justfreshconcepts.ca" target="_blank">www.justfreshconcepts.ca</a></p>
							<p align="center"><img src="winners2013/JustFreshOverall.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Sunflower Florist</span><br />
							<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>
							<p align="center"><img src="winners2013/SunflowerOver1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="4"></a>Best Wedding Make-up</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Denise Elliott Makeup Artist</span><br/>
						<a href="http://www.makeupbydenise.ca" target="_blank">www.makeupbydenise.ca</a><br/><br/>
						<img src="winners2013/DeniseElliot2.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Pink Orchid Studio</span><br/>
							<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a></p>
							<p align="center"><img src="winners2013/PinkOrchidMakeup.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Studio 26 by Racquel Lacson</span><br />
							<a href="http://www.26RL.com" target="_blank">www.26RL.com</a></p>
							<p align="center"><img src="winners2013/Studio26Makeup.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="5"></a>Best Wedding Hair Style</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Pink Orchid Studio</span><br/>
						<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a><br/><br/>
						<img src="winners2013/PinkOrchid.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Sharon Rai Hair & Makeup Artistry</span><br/>
							<a href="http://www.sharonrai.com" target="_blank">www.sharonrai.com</a></p>
							<p align="center"><img src="winners2013/SharonRai_hs1_8155_2.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Studio 26 by Racquel Lacson</span><br />
							<a href="http://www.26RL.com" target="_blank">www.26RL.com</a></p>
							<p align="center"><img src="winners2013/Studio26_hs1_4884_3.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="6"></a>Best Wedding Decor</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Vintage Origami</span><br/>
						<a href="http://www.vintageorigami.com" target="_blank">www.vintageorigami.com</a><br/><br/>
						<img src="winners2013/Vintage2.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Day to Remember Weddings & Events</span><br/>
							<a href="http://www.adaytoremember.ca" target="_blank">www.adaytoremember.ca</a></p>
							<p align="center"><img src="winners2013/TV-DayToRemember_wd1_1153_3.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">ROA Floral and Event Designs Inc.</span><br />
							<a href="http://www.RoaDesigns.com" target="_blank">www.RoaDesigns.com</a></p>
							<p align="center"><img src="winners2013/ROA_wd1_5465_1.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="7"></a>Best Wedding Jewelry Design - Accessories</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Rebekah-Anne-Designs</span><br/>
						<a href="http://www.rebekah-anne-designs.com" target="_blank">www.rebekah-anne-designs.com</a><br/><br/>
						<img src="winners2013/Rebekah.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic" style="width: 100%">
							<p align="center"><span class="company">Whisper Jewels</span><br/>
							<a href="http://www.whisperjewels.com" target="_blank">www.whisperjewels.com</a></p>
							<p align="center"><img src="winners2013/Whisper_jd2_6202_6.jpg" width="400" height="266" border="0"><br/></p>
						</div>
					</div>					
					<p align="right"><a href="winners2013.php" >return to category listing</a></p>		
				</div>
				<div class="winner">
					<h2><strong><a name="8"></a>Best Wedding Jewelry Design - Rings</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Sonja Picard Collection</span><br/>
						<a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com</a><br/><br/>
						<img src="winners2013/SonjaP.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic" style="width: 100%">
							<p align="center"><span class="company">Secret Sapphire Luxury Jewellery</span><br/>
							<a href="http://secretsapphire.ca" target="_blank">secretsapphire.ca</a></p>
							<p align="center"><img src="winners2013/SecretSaphireA.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a></p>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="9"></a>Best Wedding Transportation</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Ultimate Limousine</span><br/>
						<a href="http://www.ultimatelimo4you.com" target="_blank">www.ultimatelimo4you.com</a><br/><br/>
						<img src="winners2013/Ultimate.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic" style="width: 100%">
							<p align="center"><span class="company">Boss Limousine Service</span><br/>
							<a href="http://www.bosslimos.ca" target="_blank">www.bosslimos.ca</a></p>
							<p align="center"><img src="winners2013/Boss_wt1_2677_7.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="10"></a>Best Wedding Stationery</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Uniquity Invitations</span><br/>
						<a href="http://www.uniquityinvitations.com" target="_blank">www.uniquityinvitations.com</a><br/><br/>
						<img src="winners2013/Uniquity.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Sunlit Letterpress</span><br/>
							<a href="http://www.sunlit-letterpress.com" target="_blank">www.sunlit-letterpress.com</a></p>
							<p align="center"><img src="winners2013/Sunlit_sd1_3143_2.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Yeates Creative</span><br />
							<a href="http://www.yeatescreative.ca" target="_blank">www.yeatescreative.ca</a></p>
							<p align="center"><img src="winners2013/Yeates_sd1_4343_2.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="11"></a>Best Wedding Reception Venue - Alternative Location</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Rockwater Secret Cove Resort</span><br/>
						<a href="http://www.rockwatersecretcoveresort.com" target="_blank">www.rockwatersecretcoveresort.com</a><br/><br/>
						<img src="winners2013/Rockwater.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Science World</span><br/>
							<a href="http://www.scienceworld.ca/rentus" target="_blank">www.scienceworld.ca/rentus</a></p>
							<p align="center"><img src="winners2013/TV-Science2.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Vancouver Aquarium</span><br />
							<a href="http://www.vanaqua.org/plan" target="_blank">www.vanaqua.org/plan</a></p>
							<p align="center"><img src="winners2013/TV-Aquarium.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Vancouver Club</span><br/>
						<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a><br/><br/>
						<img src="winners2013/VanClub2.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Cecil Green Park House</span><br/>
							<a href="http://www.cecilgreenpark.ubc.ca" target="_blank">www.cecilgreenpark.ubc.ca</a></p>
							<p align="center"><img src="winners2013/Cecil_rv1_6289_3.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Terminal City Club</span><br />
							<a href="http://www.tcclub.com" target="_blank">www.tcclub.com/plan</a></p>
							<p align="center"><img src="winners2013/TerminalCity_rv1_8640_5.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Wedding Yacht</span><br/>
						<a href="http://www.theweddingyacht.com" target="_blank">www.theweddingyacht.com</a><br/><br/>
						<img src="winners2013/WeddingYacht_cl1_2224_5.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">John M.S. Lecky UBC Boathouse</span><br/>
							<a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>
							<p align="center"><img src="winners2013/UBCBoathouse_cl1_2951_2.jpg" width="400" height="266"="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">The Chapel Group – The Chapel at Minoru Park</span><br />
							<a href="http://www.thechapels.ca" target="_blank">www.thechapels.ca</a></p>
							<p align="center"><img src="winners2013/Minerou_cl1_0770_6A.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="113"></a>Best Wedding Officiant</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Michele Davidson - Modern Celebrant</span><br/>
						<a href="http://www.moderncelebrant.ca" target="_blank">www.moderncelebrant.ca</a><br/><br/>
						<img src="winners2013/MicheleD_wo1_4217_2.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Roxanne Thornton<br />MarryUs Custom Wedding Ceremonies</span><br/>
							<a href="http://www.Marryus.ca" target="_blank">www.Marryus.ca</a></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Shawn Miller <br />Young Hip & Married</span><br />
							<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="14"></a>Best Wedding Event Planning</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Filosophi Events</span><br/>
						<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a><br/><br/>
						<img src="winners2013/Filosophi_ep1_3746_7.jpg" width="400" height="600" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Petite Pearl Events<</span><br/>
							<a href="http://petitepearlevents.com" target="_blank">petitepearlevents.com</a></p>
							<p align="center"><img src="winners2013/Petite Pearl Events_ep1_9392_11.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Smitten Events</span><br />
							<a href="http://smittenevents.ca" target="_blank">http://smittenevents.ca</a></p>
							<p align="center"><img src="winners2013/Smitten_ep1_7502_8.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="114"></a>Best Catered Wedding</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Savoury City Catering & Events</span><br/>
						<a href="http://www.savourycity.com" target="_blank">www.savourycity.com</a><br/><br/>
						<img src="winners2013/tv-savoury.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Emelle's Catering</span><br/>
							<a href="http://www.emelles.com" target="_blank">www.emelles.com</a></p>
							<p align="center"><img src="winners2013/Emelles_fc1_6551_5.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Louis Gervais Fine Foods & Catering</span><br />
							<a href="http://www.louisgervais.com" target="_blank">www.louisgervais.com</a></p>
							<p align="center"><img src="winners2013/TV-Louis_fc1_7720_3.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="115"></a>Best Live Wedding Entertainment</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">SideOne</span><br/>
						<a href="http://www.sideone.ca" target="_blank">www.sideone.ca</a><br/><br/>
						<img src="winners2013/SideOne_le1_0902_2.jpg" width="700" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Musical Occasions</span><br/>
							<a href="http://www.musicaloccasions.ca" target="_blank">www.musicaloccasions.ca</a></p>
							<p align="center"><img src="winners2013/Musical.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Butter Photobooth</span><br />
							<a href="http://www.butterphotobooth.ca" target="_blank">www.butterphotobooth.ca</a></p>
							<p align="center"><img src="winners2013/ButterPhotobooth_le1_5333_4.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="15"></a>Best Candid/Photojournalism Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Abby Photography</span><br/>
						<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a><br/><br/>
						<img src="winners2013/AbbyCandid.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">John Bello Photographer</span><br/>
							<a href="http://www.bohnjello.com" target="_blank">www.bohnjello.com</a></p>
							<p align="center"><img src="winners2013/JohnBello_bp2_0487_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Raymond & Jessie Photography</span><br />
							<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
							<p align="center"><img src="winners2013/Raymond_bp2_1909_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="16"></a>Best Portrait : Bride and Groom together</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Abby Photography</span><br/>
						<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca"</a><br/><br/>
						<img src="winners2013/AbbyBandG.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Dallas Kolotylo Photography</span><br/>
							<a href="http://www.ciuffaphotography.com" target="_blank">dallaskolotylo.com</a></p>
							<p align="center"><img src="winners2013/Dallas.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">StonePhoto</span><br />
							<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a></p>
							<p align="center"><img src="winners2013/StonPhoto_bp3_5447_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="17"></a>Best Wedding Detail Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">The Apartment Wedding Photography</span><br/>
						<a href="http://www.theapartmentphotography.com" target="_blank">www.theapartmentphotography.com</a><br/><br/>
						<img src="winners2013/Apartment.jpg" width="600" height="400" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Blush Wedding Photography</span><br/>
							<a href="http://blushweddingphotography.com" target="_blank">http://blushweddingphotography.com</a></p>
							<p align="center"><img src="winners2013/Blush_bp5_1836_1.jpg" width="266" height="400" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Daniela Ciuffa Photography</span><br />
							<a href="http://www.ciuffaphotography.com" target="_blank">www.ciuffaphotography.com</a></p>
							<p align="center"><img src="winners2013/Daniela_bp5_3202_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="18"></a>Best Wedding Group Photograph</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Shari + Mike Photographers</span><br/>
						<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a><br/><br/>
						<img src="winners2013/ShariMikeGroup_bp4_6645_1.jpg" width="600" height="400"" border="0">
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Abby Photography</span><br/>
							<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a></p>
							<p align="center"><img src="winners2013/AbbyGroup_bp4_0797_1.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Raymond & Jessie Photography</span><br />
							<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
							<p align="center"><img src="winners2013/RaymondGroup.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="19"></a>Best Overall Wedding Photography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">JONETSU STUDIOS</span><br/>
						<a href="http://www.jonetsuphotography.com" target="_blank">www.jonetsuphotography.com</a><br/><br/>
						<img src="winners2013/JONETSU2.jpg" width="600" height="482" border="0"><br/><br/>
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Abby Photography</span><br/>
							<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a></p>
							<p align="center"><img src="winners2013/AbbyOverall.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Leanne Pedersen Photographers</span><br />
							<a href="http://www.leannepedersen.com" target="_blank">www.leannepedersen.com</a></p>
							<p align="center"><img src="winners2013/LeanneP.jpg" width="400" height="266" border="0"><br/></p>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>						
				</div>
				<div class="winner">
					<h2><strong><a name="20"></a>Best Wedding Videography/Cinematography</strong></h2>
					<div class="winner_pic">
						<p>
						<span class="company">Hello Tomorrow Wedding Films</span><br/>
						<a href="http://www.hellotomorrow.ca" target="_blank">www.hellotomorrow.ca</a><br/><br/>
						<iframe src="http://player.vimeo.com/video/76829145" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe><br />
						<p><img src="winners2011/divider.png"></p>
						<p class="finalist"><em>Finalists</em></p>
					</div>
					<div>
						<div class="other_pic">
							<p align="center"><span class="company">Brellow</span><br/>
							<a href="http://www.brellow.com" target="_blank">www.brellow.com</a></p>
							<iframe src="http://player.vimeo.com/video/76778838" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe><br>
						</div>
						<div class="other_pic">
							<p align="center"><span class="company">Solo Lite Productions</span><br />
							<a href="http://mementofilms.ca" target="_blank">mementofilms.ca</a></p>
							<iframe src="http://player.vimeo.com/video/76769006" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
						</div>
						<p align="right"><a href="winners2013.php" >return to category listing</a></p>
					</div>							
				</div>
				<div class="winner">
					<h2><strong><a name="21"></a>Industry Achievement Award</strong></h2>
					<div class="winner_pic">
						<a href="http://www.filosophi.com" target="_blank">
						<img src="winners2013/erinbishop.jpg" border="0" ></a><br />
						<span class="company">Erin Bishop - Filosophi Events</span><br />
						<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a>
					</div>
					<div>
						<p align="right"><a href="winners2013.php" >return to category listing</a>
					</div>
				</div>
				<div class="final_list">		
					<span align="center">
						<p>Our sincere thank-you to each of this years competing caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had. The bar has been raised once again!</p>
						<br><img src="siteimages/catersponsorbar2013.jpg" width="879" height="125" border="0" align="center" ><br>
						<b>Special Thanks also to our Judges</b><br>Mijune Pak of<a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br>David Roberston of<a href="http://www.dirtyapron.com" target="_blank"> The Dirty Apron</a><br><br><br>
						<p>Congratulations to all of the Winners and Finalists!</p>
						<a href="pastwinners.php"><img src="siteimages/pastwinnersheader.png" width="250" height="75" border="0"></a>
					</span>
				</div>
			</div>
    	</div>
    </div>
    <div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
</section>

</body>
</html>
