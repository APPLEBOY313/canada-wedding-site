<?php include("top.inc"); ?>

<p class="sansserif" align="justify">
	Judging for the Professional BC Wedding Awards </b>
	<br><br>
	Judges will review and score entries on a point based system. Judges will not have access to the company name or contact information in the judging process in order to keep judging as unbiased as possible. If the judges wish, they are permitted to look (without scoring) through all of the submissions for a particular category before they begin scoring, this is allowed so that a judge can get an overall impression regarding the quality of the submissions in a particular category first.
	<br><br>
	Scoring will be done on a scale of 1-10 for various criteria. The judging criteria will include the following areas: <br>
	<br>
	-
	aesthetics<br>
	-
	themes<br>
	-
	originality<br>
	-
	professionalism<br>
	-
	overall quality<br>
	<br>
	Scoring from all of the judges will then be combined to produce an overall score following which the winner and up to 2 runners up will be contacted. <br>
	<br>
	
	<a href="categories.php"><big><b>Return to the CATEGORIES page</b></big></a>
	
	<br><br><br><br><br><br>
</p>

<?php include("bottom.inc"); ?>