<html>
<head>
<title>The Professional BC Wedding Awards</title>
<link rel=stylesheet type="text/css" href="css/custom.css">
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
    		<div class="wrap home_con">
				<div class="home_top">
					<span class="dcr_txt">
						<p class="sansserif" align="justify">
						<br>The Professional BC Wedding Awards is the longest running wedding industry Awards program in Canada.  We are an independent, unbiased and dedicated to finding and awarding the best of British Columbia's dynamic and creative wedding industry. These awards recognize some of the amazing creative talent, companies and individuals that make up British Columbia's wedding professionals. 
						<br><br>
						The awards are a useful tool to help connect the best of the wedding industry with brides, planners and other industry vendors. We are proud of all the winners and finalists, they truly deserve the recognition they've received and we are happy to hear of the incredible positive effects that winning an award has had on their businesses.</p>
					</span>
					<span class="winner_txt">
						<a href="winners2016.php">
							<div>
								<h2>2016</h2>
								<h3>WINNERS</h3>
							</div>
						</a>						
						<a href="winnerspackage.php">
							<div>
								<h2>WINNERS</h2>
								<h3>PACKAGE</h3>
							</div>
						</a>						
						<a href="pastwinners.php">
							<div>
								<h2>PAST</h2>
								<h3>WINNERS</h3>
							</div>
						</a>						
					</span>
					<img src="./siteimages/BCWA_awardsIndex.jpg">
				</div>
				<div class="video_pan">
					<iframe src="https://player.vimeo.com/video/194595011" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<span>
						<h2>------ NOVEMBER 23RD WAS A BIG DEAL ------</h2>
						<h3>THANK YOU FOR JOINING US AT THE BIGGIEST INDUSTRY NIGHT OF THE YEAR!!</h3>
					</span>
				</div>
				<div class="partner">
					<img SRC="siteimages/sponsorbar2016.png" ALT="" BORDER=0 usemap="#Map" >
					<map name="Map" id="Map">
					     <area shape="rect" coords="132,63,360,138" href="http://www.realweddings.ca/" target="_blank" alt="Real Weddings Magazine" />
					     <area shape="rect" coords="401,60,491,138" href="http://www.beauphoto.com/" target="_blank" alt="Beau Photo" />
					     <area shape="rect" coords="537,69,740,135" href="http://hyperfocus.ca/" target="_blank" alt="hyperfocus photography" />
					     <area shape="rect" coords="256,160,644,247" href="http://www.hardrockcasinovancouver.com/" target="_blank" alt="Molson Canadian TheatreHard Rock Casino" />
					     <area shape="rect" coords="37,257,251,313" href="http://kingwillowmanagement.com/" target="_blank" />
					     <area shape="rect" coords="292,263,445,329" href="http://pixstarphotobooth.com/" target="_blank" />
					     <area shape="rect" coords="481,257,622,337" href="http://www.koncepteventdesign.com/" target="_blank" />
					     <area shape="rect" coords="661,252,844,311" href="https://audioedge.ca" target="_blank" />
					     <area shape="rect" coords="68,352,225,442" href="http://trufflesfinefoods.com/" target="_blank" />
					     <area shape="rect" coords="241,360,425,444" href="http://fiestacreativecatering.com/" target="_blank" />
					     <area shape="rect" coords="446,359,606,452" href="http://boywithaknife.ca" target="_blank" />
					     <area shape="rect" coords="618,353,833,457" href="http://whitetablecatering.com/" target="_blank" />
					     <area shape="rect" coords="172,469,386,541" href="http://www.mementofilms.ca/" target="_blank" />
					     <area shape="rect" coords="538,472,741,547" href="https://impressionsliveart.com/" target="_blank" />
						 <area shape="rect" coords="294,552,387,677" href="http://www.tcandtg.com/" target="_blank" />
					     <area shape="rect" coords="85,566,254,663" href="http://aelizabethcakes.com/" target="_blank" />
						 <area shape="rect" coords="444,557,580,682" href="http://pinkribbonbakery.ca/" target="_blank" />
					     <area shape="rect" coords="589,566,779,662" href="http://whenpigsflypastries.com/" target="_blank" />
					     <area shape="rect" coords="198,695,341,774" href="http://www.amoriskunawedding.com/" target="_blank" />
					     <area shape="rect" coords="369,691,523,782" href="https://thetuxstore.com/" target="_blank" />
					     <area shape="rect" coords="539,690,684,774" href="http://www.sourcedancecompany.com/" target="_blank" />
					</map>
				</div>
			</div>			
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
	</section>
<?php include("bottom2.inc"); ?>