<html>

<head>

<title>The Professional BC Wedding Awards</title>

<link rel=stylesheet type="text/css" href="../bcwastyle.css">



<style type="text/css">

h2 {

	letter-spacing:3px;
	text-transform: uppercase;
	text-align:center;
	font-family: 'helvetica neue',helvetica,sans-serif;
color:#B8B294

}
.text2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size:14px
}
.company {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px
}
.finalist {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	color:#B8B294;
	font-style:italic
}
</style>


<script type="text/javascript">

<!--



function newImage(arg) {

	if (document.images) {

		rslt = new Image();

		rslt.src = arg;

		return rslt;

	}

}



function changeImages() {

	if (document.images && (preloadFlag == true)) {

		for (var i=0; i<changeImages.arguments.length; i+=2) {

			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];

		}

	}

}



var preloadFlag = false;

function preloadImages() {

	if (document.images) {

		home_over = newImage("../menuimages/home-over.jpg");

		blog_over = newImage("../menuimages/blog-over.jpg");

		categories_over = newImage("../menuimages/categories-over.jpg");

		judging_over = newImage("../menuimages/judging-over.jpg");

		faq_over = newImage("../menuimages/faq-over.jpg");

		events_over = newImage("../menuimages/events-over.jpg");

		contact_over = newImage("../menuimages/contact-over.jpg");

		preloadFlag = true;

	}

}



// -->

</script>









</head>





<body onLoad="preloadImages();">

<center>

<table width="900" border="0" cellpadding="0" cellspacing="0">

	<tr>

	<td><br><a href="index.html"><img src="../siteimages/logo.png" align="left"/ hspace="10" border="0" ></a>

		<br><br><br><br><br><br><br><br><br><br><br>

	</td>



	<td align="right"><br><br><br><br><br><br><br><br><!--<button type="button" onClick="window.location.href='mailing.php';">Sign Up for Our Mailing List</button>
-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<br><br>
<img src="../siteimages/links.jpg" width="227" height="25" border="0" USEMAP="#links_Map">
&nbsp;&nbsp;&nbsp;&nbsp;

	</td>

	</tr>

</table>


<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">

	<tr>

		<td colspan="15">

			<img src="../menuimages/menu_01.jpg" width="879" height="4" alt="" /></td>

	</tr>

	<tr>

		<td>

			<img src="../menuimages/menu_02.jpg" width="38" height="21" alt="" /></td>

		<td>

			<a href="../index.php"

				onmouseover="changeImages('home', '../menuimages/home-over.jpg'); return true;"

				onmouseout="changeImages('home', '../menuimages/home.jpg'); return true;">

				<img name="home" src="../menuimages/home.jpg" width="68" height="21" border="0" alt="" /></a></td>

		<!--<td>

			<img src="../menuimages/menu_04.jpg" width="30" height="21" alt="" /></td>

		<td>

			<a href="http://bcweddingawards.com/blog/" target="_blank"

				onmouseover="changeImages('blog', '../menuimages/blog-over.jpg'); return true;"

				onmouseout="changeImages('blog', '../menuimages/blog.jpg'); return true;">

				<img name="blog" src="../menuimages/blog.jpg" width="67" height="21" border="0" alt="" /></a></td>-->

		<td>

			<img src="../menuimages/menu_06.jpg" width="25" height="21" alt="" /></td>

		<td>

			<a href="http://www.bcweddingawards.com/categories.php"

				onmouseover="changeImages('categories', '../menuimages/categories-over.jpg'); return true;"

				onmouseout="changeImages('categories', '../menuimages/categories.jpg'); return true;">

				<img name="categories" src="../menuimages/categories.jpg" width="162" height="21" border="0" alt="" /align="right"></a></td>

		<td>

			<img src="../menuimages/menu_08.jpg" width="40" height="21" alt="" /align="right"></td>

		<td>

			<a href="../judging.php"

				onmouseover="changeImages('judging', '../menuimages/judging-over.jpg'); return true;"

				onmouseout="changeImages('judging', '../menuimages/judging.jpg'); return true;">

				<img name="judging" src="../menuimages/judging.jpg" width="111" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_10.jpg" width="34" height="21" alt="" /></td>

		<td>

			<a href="../faq.php"

				onmouseover="changeImages('faq', '../menuimages/faq-over.jpg'); return true;"

				onmouseout="changeImages('faq', '../menuimages/faq.jpg'); return true;">

				<img name="faq" src="../menuimages/faq.jpg" width="51" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_12.jpg" width="35" height="21" alt="" /></td>

		<td>

			<a href="../events.php"

				onmouseover="changeImages('events', '../menuimages/events-over.jpg'); return true;"

				onmouseout="changeImages('events', '../menuimages/events.jpg'); return true;">

				<img name="events" src="../menuimages/events.jpg" width="91" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_14.jpg" width="31" height="21" alt="" /></td>

		<td>

			<a href="../contact.php"

				onmouseover="changeImages('contact', '../menuimages/contact-over.jpg'); return true;"

				onmouseout="changeImages('contact', '../menuimages/contact.jpg'); return true;">

				<img name="contact" src="../menuimages/contact.jpg" width="112" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_16.jpg" width="39" height="21" alt="" /></td>

	</tr>

	<tr>

		<td colspan="15">

			<img src="../menuimages/menu_17.jpg" width="879" height="4" alt="" /></td>

	</tr>

</table>







<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">

	<tr>

<td width="870">

<br><br>

<img src="http://www.bcweddingawards.com/siteimages/2013winners.png" width="250" height="75">



<p class="sansserif" align="justify">



<b>

Organizers of the 2013 Professional BC Wedding Awards handed out 26 trophies on November 27, 2013 at The Pinnacle Hotel at the Pier in North Vancouver.
The winners are those companies with the highest scoring entries. There is a maximum of 2 finalists per category, and they are listed here in alphabetical order. </b><br><br>
<small>Please note that in most cases, only a small part of the entry material is shown publicly. Photographer credit was not known for all entries, it has been provided wherever possible.<br>

<br><br>

<table width="870" border="0" align="center">
<tr><td width="50%" valign="top">
<a href='#1'><b><big>Best Wedding Cake</a></big></b><br><br>
<a href='#2'><b><big>Best Bridal Bouquet</a></big></b><br><br>
<a href='#3'><b><big>Best Wedding Florist - Overall</a></big></b><br><br>
<a href='#4'><b><big>Best Wedding Make-up</a></big></b><br><br>
<a href='#5'><b><big>Best Wedding Hair Style</a></big></b><br><br>
<a href='#6'><b><big>Best Wedding Decor</a></big></b><br><br>
<a href='#7'><b><big>Best Wedding Jewelry Design - Accessories</a></big></b><br><br>
<a href='#8'><b><big>Best Wedding Jewelry Design - Rings</a></big></b><br><br>
<a href='#9'><b><big>Best Wedding Transportation</a></big></b><br><br>
<a href="#10"><b><big>Best Wedding Stationery</a></big></b><br><br>
<a href='#11'><b><big>Best Wedding Reception Venue - Alternative Location</a></big></b><br><br>
<a href='#12'><b><big>Best Wedding Reception Venue - Hotel or Banquet Hall</a></big></b><br><br>
<a href='#13'><b><big>Best Wedding Ceremony Location</a></big></b><br><br>

</td><td width="50%" valign="top">

<a href='#113'><b><big>Best Wedding Officiant</a></big></b><br><br>
<a href='#14'><b><big>Best Wedding Event Planning</a></big></b><br><br>
<a href='#114'><b><big>Best Catered Wedding</a></big></b><br><br>
<a href='#115'><b><big>Best Live Wedding Entertainment</a></big></b><br><br>
<a href='#15'><b><big>Best Candid/Photojournalism Photograph</a></big></b><br><br>
<a href='#16'><b><big>Best Portrait: Bride and Groom together</a></big></b><br><br>
<a href='#17'><b><big>Best Wedding Detail Photograph</a></big></b><br><br>
<a href='#18'><b><big>Best Wedding Group Photograph</a></big></b><br><br>
<a href='#19'><b><big>Best Overall Wedding Photography</a></big></b><br><br>
<a href='#20'><b><big>Best Wedding Videographer/ Cinematographer</a></big></b><br><br>
<a href='#22'><b><big>2013 Tasters Choice - Best Hors D�Oeuvre</a></big></b><br><br>
<a href='#23'><b><big>2013 Judges Choice - Best Hors D�Oeuvre</a></big></b><br><br>

</td>

</tr>
<tr>
<td colspan="3" align="center">
<a href='#21'><b><big>2013 Industry Achievement Award Winner</a></big></b></td>

</tr>



<tr>
<td colspan="2" align="center">
</td></tr>
<tr>
<td colspan="2" align="center">
</td></tr>

</table>


</td></tr><a name="categories"></a>
<table width="870" border="0" align="center">
<tr><td align="center" bgcolor="#000000">
<p><strong>Winners:</strong> The winners are those companies with the highest score.</p>
<p><strong>Finalists:</strong> In order to qualify as a finalist the score needs to be within 10% of the winning score.  There is a maximum of 2 finalists per category.</p>


<h2><br><br><strong><a name="1"></a>Best Wedding Cake</strong></h2>
<p align="center">
<br />
<span class="company">Whisk Cake Company</span><br />

<a href="http://www.whiskcakes.com" target="_blank">www.whiskcakes.com</a><br><br />
<img src="../winners2013/Whisk_wc1_7913_3.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Aeyra Cakes</span><br />
  <a href="http://www.aeyracakes.ca" target="_blank">
www.aeyracakes.ca</a></p>

<p align="center"><img src="../winners2013/Aeyra_wc1_3584_3.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">When Pigs Fly Pastries</span><br />
<a href="http://www.annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a></p>

<p align="center"><img src="../winners2013/AnnaEliz2_wc1_4589_1.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>








<h2><br><br><strong><a name="2"></a>Best Bridal Bouquet</strong></h2>

<p align="center">
<br />
<span class="company">The Flower Factory</span><br />

<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br><br />
<img src="../winners2013/FlowerFactory_bb1_0003_1.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Just Fresh Concepts</span><br />
  <a href="http://www.justfreshconcepts.ca" target="_blank">
www.justfreshconcepts.ca
</a></p>

<p align="center"><img src="../winners2013/JustFresh_bb1_0357_1.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Sunflower Florist</span><br />
<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>

<p align="center"><img src="../winners2013/Sunflower_bb1_7677_2.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="3"></a>Best Wedding Florist - Overall</strong></h2>

<p align="center">
<br />
<span class="company">Verbena Floral Design</span><br />

<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a><br><br />
<img src="../winners2013/VerbenaOverall1.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Just Fresh Concepts</span><br />
  <a href="http://www.justfreshconcepts.ca" target="_blank">
www.justfreshconcepts.ca
</a></p>

<p align="center"><img src="../winners2013/JustFreshOverall.jpg" width="400" height="266" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Sunflower Florist</span><br />
<a href="http://www.sunflowerflorist.ca" target="_blank">www.sunflowerflorist.ca</a></p>

<p align="center"><img src="../winners2013/SunflowerOver1.jpg" width="400" height="266"border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br><strong><a name="4"></a>Best Wedding Make-up</strong></h2>

<p align="center">
<br />
<span class="company">Denise Elliott Makeup Artist</span><br />

<a href="http://www.makeupbydenise.ca" target="_blank">www.makeupbydenise.ca</a><br><br />
<img src="../winners2013/DeniseElliot2.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Pink Orchid Studio</span><br />
  <a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a></p>

<p align="center"><img src="../winners2013/PinkOrchidMakeup.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Studio 26 by Racquel Lacson</span><br />
<a href="http://www.26RL.com" target="_blank">www.26RL.com</a></p>

<p align="center"><img src="../winners2013/Studio26Makeup.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="5"></a>Best Wedding Hair Style</strong></h2>

<p align="center">
<br />
<span class="company">Pink Orchid Studio</span><br />

<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com</a><br><br />
<img src="../winners2013/PinkOrchid.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Sharon Rai Hair & Makeup Artistry</span><br />
  <a href="http://www.sharonrai.com" target="_blank">www.sharonrai.com</a></p>

<p align="center"><img src="../winners2013/SharonRai_hs1_8155_2.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Studio 26 by Racquel Lacson</span><br />
<a href="http://www.26RL.com" target="_blank">www.26RL.com</a></p>

<p align="center"><img src="../winners2013/Studio26_hs1_4884_3.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="6"></a>Best Wedding Decor</strong></h2>

<p align="center">
<br />
<span class="company">Vintage Origami</span><br />

<a href="http://www.vintageorigami.com" target="_blank">www.vintageorigami.com</a><br><br />
<img src="../winners2013/Vintage2.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">A Day to Remember Weddings & Events</span><br />
  <a href="http://www.adaytoremember.ca" target="_blank">www.adaytoremember.ca</a></p>

<p align="center"><img src="../winners2013/TV-DayToRemember_wd1_1153_3.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">ROA Floral and Event Designs Inc.</span><br />
<a href="http://www.RoaDesigns.com" target="_blank">www.RoaDesigns.com</a></p>

<p align="center"><img src="../winners2013/ROA_wd1_5465_1.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="7"></a>Best Wedding Jewelry Design - Accessories</strong></h2>

<p align="center">
<br />
<span class="company">Rebekah-Anne-Designs</span><br />

<a href="http://www.rebekah-anne-designs.com" target="_blank">www.rebekah-anne-designs.com</a><br><br />
<img src="../winners2013/Rebekah.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalist</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Whisper Jewels</span><br />
  <a href="http://www.whisperjewels.com" target="_blank">www.whisperjewels.com</a></p>

<p align="center"><img src="../winners2013/Whisper_jd2_6202_6.jpg" width="400" height="266" border="0">
<br />
</p></td>


  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="8"></a>Best Wedding Jewelry Design - Rings</strong></h2>

<p align="center">
<br />
<span class="company">The Sonja Picard Collection</span><br />

<a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com</a><br><br />
<img src="../winners2013/SonjaP.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalist</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Secret Sapphire Luxury Jewellery</span><br />
  <a href="http://secretsapphire.ca/" target="_blank">secretsapphire.ca</a></p>

<p align="center"><img src="../winners2013/SecretSaphireA.jpg" width="400" height="266" border="0">
<br />
</p></td>


  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong><a name="9"></a>Best Wedding Transportation</strong></h2>

<p align="center">
<br />
<span class="company">Ultimate Limousine</span><br />

<a href="http://www.ultimatelimo4you.com" target="_blank">www.ultimatelimo4you.com</a><br><br />
<img src="../winners2013/Ultimate.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalist</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Boss Limousine Service</span><br />
  <a href="http://www.bosslimos.ca/" target="_blank">www.bosslimos.ca</a></p>

<p align="center"><img src="../winners2013/Boss_wt1_2677_7.jpg" width="400" height="266" border="0">
<br />
</p></td>


  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br><strong><a name="10"></a>Best Wedding Stationery</strong></h2>

<p align="center">
<br />
<span class="company">Uniquity Invitations</span><br />

<a href="http://www.uniquityinvitations.com" target="_blank">www.uniquityinvitations.com</a><br><br />
<img src="../winners2013/Uniquity.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Sunlit Letterpress</span><br />
  <a href="http://www.sunlit-letterpress.com" target="_blank">www.sunlit-letterpress.com</a></p>

<p align="center"><img src="../winners2013/Sunlit_sd1_3143_2.jpg" width="400" height="266" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Yeates Creative</span><br />
<a href="http://www.yeatescreative.ca" target="_blank">www.yeatescreative.ca</a></p>

<p align="center"><img src="../winners2013/Yeates_sd1_4343_2.jpg" width="400" height="266" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="11"></a>Best Wedding Reception Venue - Alternative Location</strong></h2>

<p align="center">
<br />
<span class="company">Rockwater Secret Cove Resort</span><br />

<a href="http://www.rockwatersecretcoveresort.com" target="_blank">www.rockwatersecretcoveresort.com</a><br><br />
<img src="../winners2013/Rockwater.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Science World</span><br />
  <a href="http://www.scienceworld.ca/rentus" target="_blank">www.scienceworld.ca/rentus</a></p>

<p align="center"><img src="../winners2013/TV-Science2.jpg" width="400" height="266" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Vancouver Aquarium</span><br />
<a href="http://www.vanaqua.org/plan" target="_blank">www.vanaqua.org/plan</a></p>

<p align="center"><img src="../winners2013/TV-Aquarium.jpg" width="400" height="266" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="12"></a>Best Wedding Reception Venue - Hotel or Banquet Hall</strong></h2>

<p align="center">
<br />
<span class="company">The Vancouver Club</span><br />

<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a><br><br />
<img src="../winners2013/VanClub2.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">  <tr>
    <td><p align="center"><span class="company">Cecil Green Park House</span><br />
  <a href="http://www.cecilgreenpark.ubc.ca" target="_blank">www.cecilgreenpark.ubc.ca</a></p>

<p align="center"><img src="../winners2013/Cecil_rv1_6289_3.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Terminal City Club</span><br />
<a href="http://www.tcclub.com" target="_blank">www.tcclub.com/plan</a></p>

<p align="center"><img src="../winners2013/TerminalCity_rv1_8640_5.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="13"></a>Best Wedding Ceremony Location</strong></h2>

<p align="center">
<br />
<span class="company">The Wedding Yacht</span><br />

<a href="http://www.theweddingyacht.com" target="_blank">www.theweddingyacht.com</a><br><br />
<img src="../winners2013/WeddingYacht_cl1_2224_5.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">  <tr>
    <td><p align="center"><span class="company">John M.S. Lecky UBC Boathouse</span><br />
  <a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>

<p align="center"><img src="../winners2013/UBCBoathouse_cl1_2951_2.jpg" width="400" height="266"  border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">The Chapel Group � The Chapel at Minoru Park</span><br />
<a href="http://www.thechapels.ca" target="_blank">www.thechapels.ca</a></p>

<p align="center"><img src="../winners2013/Minerou_cl1_0770_6A.jpg" width="400" height="266"  border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="113"></a>Best Wedding Officiant</strong></h2>

<p align="center">
<br />
<span class="company">Michele Davidson - Modern Celebrant </span><br />

<a href="http://www.moderncelebrant.ca" target="_blank">www.moderncelebrant.ca</a><br><br />
<img src="../winners2013/MicheleD_wo1_4217_2.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Roxanne Thornton<br />MarryUs Custom Wedding Ceremonies</span><br />
  <a href="http://www.Marryus.ca" target="_blank">www.Marryus.ca</a></p>
<br />
</p></td>

    <td><p align="center"><span class="company">Shawn Miller<br />Young Hip & Married</span><br />
<a href="http://www.younghipandmarried.com" target="_blank">www.younghipandmarried.com</a></p>
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong><a name="14"></a>Best Wedding Event Planning</strong></h2>
<p align="center">
<br />
<span class="company">Filosophi Events</span><br />
<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a><br><br />
<img src="../winners2013/Filosophi_ep1_3746_7.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Petite Pearl Events</span><br />
  <a href="http://petitepearlevents.com" target="_blank">http://petitepearlevents.com</a></p>
<p align="center"><img src="../winners2013/Petite Pearl Events_ep1_9392_11.jpg" width="266" height="400" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Smitten Events</span><br />
<a href="http://smittenevents.ca" target="_blank">http://smittenevents.ca</a></p>
<p align="center"><img src="../winners2013/Smitten_ep1_7502_8.jpg" width="266" height="400" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong><a name="114"></a>Best Catered Wedding</strong></h2>
<p align="center">
<br />
<span class="company">Savoury City Catering & Events</span><br />
<a href="http://www.savourycity.com" target="_blank">www.savourycity.com</a><br><br />
<img src="../winners2013/tv-savoury.jpg" width="600" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Emelle's Catering</span><br />
  <a href="http://www.emelles.com" target="_blank">www.emelles.com</a></p>
<p align="center"><img src="../winners2013/Emelles_fc1_6551_5.jpg" width="266" height="400" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Louis Gervais Fine Foods & Catering</span><br />
<a href="http://www.louisgervais.com" target="_blank">www.louisgervais.com</a></p>
<p align="center"><img src="../winners2013/TV-Louis_fc1_7720_3.jpg" width="266" height="400" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="115"></a>Best Live Wedding Entertainment</strong></h2>
<p align="center">
<br />
<span class="company">SideOne</span><br />
<a href="http://www.sideone.ca" target="_blank">www.sideone.ca</a><br><br />
<img src="../winners2013/SideOne_le1_0902_2.jpg" width="700" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Musical Occasions</span><br />
  <a href="http://www.musicaloccasions.ca" target="_blank">www.musicaloccasions.ca</a></p>
<p align="center"><img src="../winners2013/Musical.jpg" width="400" height="266" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Butter Photobooth</span><br />
<a href="http://www.butterphotobooth.ca" target="_blank">www.butterphotobooth.ca</a></p>
<p align="center"><img src="../winners2013/ButterPhotobooth_le1_5333_4.jpg" width="400" height="266" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="15"></a>Best Candid/Photojournalism Photograph</strong></h2>
<p align="center">
<br />
<span class="company">Abby Photography</span><br />
<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a><br><br />
<img src="../winners2013/AbbyCandid.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">John Bello Photographer</span><br />
  <a href="http://www.bohnjello.com" target="_blank">www.bohnjello.com</a></p>
<p align="center"><img src="../winners2013/JohnBello_bp2_0487_1.jpg" width="400" height="266" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Raymond & Jessie Photography</span><br />
<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
<p align="center"><img src="../winners2013/Raymond_bp2_1909_1.jpg" width="400" height="266"  border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="16"></a>Best Portrait: Bride and Groom together</strong></h2>
<p align="center">
<br />
<span class="company">Abby Photography</span><br />
<a href="http://www.abbyphotography.ca" target="_blank">www.abbyphotography.ca</a><br><br />
<img src="../winners2013/AbbyBandG.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Dallas Kolotylo Photography</span><br />
  <a href="http://dallaskolotylo.com" target="_blank">http://dallaskolotylo.com</a></p>
<p align="center"><img src="../winners2013/Dallas.jpg" width="400" height="266"  border="0">
<br /></p></td>

    <td><p align="center"><span class="company">StonePhoto</span><br />
<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a></p>
<p align="center"><img src="../winners2013/StonPhoto_bp3_5447_1.jpg" width="400" height="266"  border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="17"></a>Best Wedding Detail Photograph</strong></h2>
<p align="center">
<br />
<span class="company">The Apartment Wedding Photography</span><br />
<a href="http://www.theapartmentphotography.com" target="_blank">www.theapartmentphotography.com</a><br><br />
<img src="../winners2013/Apartment.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Blush Wedding Photography</span><br />
  <a href="http://blushweddingphotography.com" target="_blank">http://blushweddingphotography.com</a></p>
<p align="center"><img src="../winners2013/Blush_bp5_1836_1.jpg" width="266" height="400" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Daniela Ciuffa Photography</span><br />
<a href="http://www.ciuffaphotography.com" target="_blank">www.ciuffaphotography.com</a></p>
<p align="center"><img src="../winners2013/Daniela_bp5_3202_1.jpg" width="400" height="266" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="18"></a>Best Wedding Group Photograph</strong></h2><p align="center">
<br />
<span class="company">Shari + Mike Photographers</span><br />
<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a><br><br />
<img src="../winners2013/ShariMikeGroup_bp4_6645_1.jpg" width="600" height="400" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Abby Photography</span><br />
  <a href="http://www.abbyphotography.ca" target="_blank">http://www.abbyphotography.ca</a></p>
<p align="center"><img src="../winners2013/AbbyGroup_bp4_0797_1.jpg" width="400" height="266" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Raymond & Jessie Photography</span><br />
<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
<p align="center"><img src="../winners2013/RaymondGroup.jpg" width="400" height="266" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2><br><br><strong><a name="19"></a>Best Overall Wedding Photography</strong></h2><p align="center">
<br />
<span class="company">JONETSU STUDIOS</span><br />
<a href="http://www.jonetsuphotography.com" target="_blank">www.jonetsuphotography.com</a><br><br />
<img src="../winners2013/JONETSU2.jpg" width="600" height="482" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>
<table width="900" border="0" align="center" cellpadding="10"><tr>
    <td><p align="center"><span class="company">Abby Photography</span><br />
  <a href="http://www.abbyphotography.ca" target="_blank">http://www.abbyphotography.ca</a></p>
<p align="center"><img src="../winners2013/AbbyOverall.jpg" width="400" height="266" border="0">
<br /></p></td>

    <td><p align="center"><span class="company">Leanne Pedersen Photographers</span><br />
<a href="http://www.leannepedersen.com" target="_blank">www.leannepedersen.com</a></p>
<p align="center"><img src="../winners2013/LeanneP.jpg" width="400" height="266" border="0">
<br /></p></td> </tr></table><br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>







<h2><br><br><strong><a name="20"></a>Best Wedding Videography/Cinematography</strong></h2>

 <p align="center"><span class="company">Hello Tomorrow Wedding Films</span><br />
<a href="http://www.hellotomorrow.ca" target="_blank">www.hellotomorrow.ca</a></p>

<p align="center"><iframe src="http://player.vimeo.com/video/76829145" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

  <br />

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>
<table width="1000" border="0" align="center">
  <tr>
    <td><p align="center"><span class="company">Brellow</span><br>
<a href="http://www.brellow.com" target="_blank">www.brellow.com</a></p>
<p align="center"><iframe src="http://player.vimeo.com/video/76778838" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

<br /></td>

    <td><p align="center"><span class="company">Solo Lite Productions</span><br />
<a href="http://mementofilms.ca" target="_blank">mementofilms.ca</a></p>
<p align="center"><iframe src="http://player.vimeo.com/video/76769006" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
<br /></td>
  </tr>
</table>

<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong><a name="21"></a>Industry Achievement Award</strong></h2>
Industry Achievement Award:



<p align="center"><a href="http://www.filosophi.com" target="_blank"><img src="../winners2013/erinbishop.jpg" border="0" ></a>
  <br />
  <span class="company">Erin Bishop - Filosophi Events</span><br />
<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a></p>

<p align="center" class="company">

<p align="center">&nbsp;</p>


<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<!--

<h2><br><br><strong><a name="22"></a>2013 Tasters Choice - Best Hors D�Oeuvre Award Winner</strong></h2>

  <p align="center"><span class="company">COMPANY
  </span><br />
<a href="http://www.website.com/" target="_blank">www.website.com/</a>
<br>DESCRIPTION</p>
<p align="center"><img src="../winners2013/photo.jpg" width="0" height="0" border="0" alt="photo credit">
  <br />

<h2><br><br><strong><a name="23"></a>2013 Judges Choice - Best Hors D�Oeuvre Award Winner</strong></h2>

  <p align="center"><span class="company">COMPANY
  </span><br />
<a href="http://www.website.com/" target="_blank">www.website.com/</a>
<br>DESCRIPTION</p>
<p align="center"><img src="../winners2013/photo.jpg" width="0" height="0" border="0" alt="photo credit">
  <br />

<!--

<h2><br><br><strong>PARTICIPANTS</strong></h2>
<center>
<table align="center" border="0"><tr><td align="center">
<font color="white" face="arial" size="2"><b>Kale and Nori</b><br />
<a href="http://www.kaleandnori.com/" target="_blank">www.kaleandnori.com/</a>
<br>Korean Beef �meatloaf�
Served with Crispy Bourbon Barrel Aged
Fish Sauce and Sweet and Hot
Okanagan Chili Pepper Kim Chi
<img src="../winners2013/kaleandnori.jpg" width="300" height="451" border="0" >
</td>

<td align="center"><span class="company"><font color="white" face="arial" size="2"><b>Edgeceptional Catering</b>
<br />
<a href="http://edgecatering.ca/" target="_blank">www.edgecatering.ca/</a>
<br>Edge Made Duck Proscuitto
on a Phyllo Spoon with Fig Compote
& Orange Glaze, served with microgreens
<img src="../winners2013/edgeceptional.jpg" width="300" height="451" border="0" >
</td>

<td align="center"><span class="company"><font color="white" face="arial" size="2"><b>River Rock Catering</b><br />
<a href="http://www.riverrock.com/" target="_blank">www.riverrock.com/</a>
<br>Qualicum Beach Scallop Tartar
with Spinach & Black Sesame Cone
- Wasabi Mayonnaise
Flying Fish Rose, Kaiware Sprouts
<img src="../winners2013/rr.jpg" width="300" height="451" border="0" >



</td></tr>
-->
<table width="1000" border="0" align="center">
<tr><td colspan="4"><center><span class="company"><font color="white" face="arial" size="2">
Our sincere thank-you to each of this years competing caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had. The bar has been raised once again!<br><img src="http://www.bcweddingawards.com/siteimages/catersponsorbar2013.jpg" width="879" height="125"border="0" align="center" ><br><b>Special Thanks also to our Judges</b><br>Mijune Pak of<a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br>David Roberston of<a href="http://www.dirtyapron.com" target="_blank"> The Dirty Apron</a>
</td></tr>
</table>
<font color="white" face="arial" size="2">
Congratulations to all of the Winners and Finalists!
<br><br><center>
<a href="http://www.bcweddingawards.com/pastwinners.html">
<img src="http://www.bcweddingawards.com/siteimages/pastwinnersheader.png" width="250" height="75" border="0"></a>
</center>


<br><br>
</p>
</div>

</td></tr>
<table>

</table>



<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>

<td><img src="../siteimages/dots.jpg" width="879" height="9" alt="" /></td>

	</tr>

	</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>



   <td style="padding-right:5px;padding-left:5px;" align="left">



     <a href="index.html" >HOME</a>


    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

    <a href="http://www.bcweddingawards.com/categories2012.php">CATEGORIES</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/judging.html">JUDGING</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/faq.html">FAQ</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/events.html">EVENTS</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/contact.php">CONTACT</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

    </td>





	</tr>

</table>





<MAP NAME="links_Map">

<AREA SHAPE="rect" ALT="" COORDS="202,0,224,25" HREF="http://twitter.com/BCweddingAwards" TARGET="_blank">

<AREA SHAPE="rect" ALT="" COORDS="177,0,200,25" HREF="http://www.facebook.com/pages/Professional-BC-Wedding-Awards/137667716271526?created" TARGET="_blank">

</MAP>

</body>

</html>

