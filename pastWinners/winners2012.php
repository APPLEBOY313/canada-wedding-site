<html>

<head>

<title>The Professional BC Wedding Awards</title>

<link rel=stylesheet type="text/css" href="../bcwastyle.css">



<style type="text/css">

h2 {

	letter-spacing:3px;
	text-transform: uppercase;
	text-align:center;
	font-family: 'helvetica neue',helvetica,sans-serif;
color:#B8B294

}
.text2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size:14px
}
.company {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px
}
.finalist {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	color:#B8B294;
	font-style:italic
}
</style>


<script type="text/javascript">

<!--



function newImage(arg) {

	if (document.images) {

		rslt = new Image();

		rslt.src = arg;

		return rslt;

	}

}



function changeImages() {

	if (document.images && (preloadFlag == true)) {

		for (var i=0; i<changeImages.arguments.length; i+=2) {

			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];

		}

	}

}



var preloadFlag = false;

function preloadImages() {

	if (document.images) {

		home_over = newImage("../menuimages/home-over.jpg");

		blog_over = newImage("../menuimages/blog-over.jpg");

		categories_over = newImage("../menuimages/categories-over.jpg");

		judging_over = newImage("../menuimages/judging-over.jpg");

		faq_over = newImage("../menuimages/faq-over.jpg");

		events_over = newImage("../menuimages/events-over.jpg");

		contact_over = newImage("../menuimages/contact-over.jpg");

		preloadFlag = true;

	}

}



// -->

</script>









</head>





<body onLoad="preloadImages();">

<center>

<table width="900" border="0" cellpadding="0" cellspacing="0">

	<tr>

	<td><br><a href="index.html"><img src="../siteimages/logo.png" align="left"/ hspace="10" border="0" ></a>

		<br><br><br><br><br><br><br><br><br><br><br>

	</td>



	<td align="right"><br><br><br><br><br><br><br><br><!--<button type="button" onClick="window.location.href='mailing.php';">Sign Up for Our Mailing List</button>
-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<br><br>
<img src="../siteimages/links.jpg" width="227" height="25" border="0" USEMAP="#links_Map">
&nbsp;&nbsp;&nbsp;&nbsp;

	</td>

	</tr>

</table>


<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">

	<tr>

		<td colspan="15">

			<img src="../menuimages/menu_01.jpg" width="879" height="4" alt="" /></td>

	</tr>

	<tr>

		<td>

			<img src="../menuimages/menu_02.jpg" width="38" height="21" alt="" /></td>

		<td>

			<a href="../index.php"

				onmouseover="changeImages('home', '../menuimages/home-over.jpg'); return true;"

				onmouseout="changeImages('home', '../menuimages/home.jpg'); return true;">

				<img name="home" src="../menuimages/home.jpg" width="68" height="21" border="0" alt="" /></a></td>

		<!--<td>

			<img src="../menuimages/menu_04.jpg" width="30" height="21" alt="" /></td>

		<td>

			<a href="http://bcweddingawards.com/blog/" target="_blank"

				onmouseover="changeImages('blog', '../menuimages/blog-over.jpg'); return true;"

				onmouseout="changeImages('blog', '../menuimages/blog.jpg'); return true;">

				<img name="blog" src="../menuimages/blog.jpg" width="67" height="21" border="0" alt="" /></a></td>-->

		<td>

			<img src="../menuimages/menu_06.jpg" width="25" height="21" alt="" /></td>

		<td>

			<a href="http://www.bcweddingawards.com/categories.php"

				onmouseover="changeImages('categories', '../menuimages/categories-over.jpg'); return true;"

				onmouseout="changeImages('categories', '../menuimages/categories.jpg'); return true;">

				<img name="categories" src="../menuimages/categories.jpg" width="162" height="21" border="0" alt="" /align="right"></a></td>

		<td>

			<img src="../menuimages/menu_08.jpg" width="40" height="21" alt="" /align="right"></td>

		<td>

			<a href="../judging.php"

				onmouseover="changeImages('judging', '../menuimages/judging-over.jpg'); return true;"

				onmouseout="changeImages('judging', '../menuimages/judging.jpg'); return true;">

				<img name="judging" src="../menuimages/judging.jpg" width="111" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_10.jpg" width="34" height="21" alt="" /></td>

		<td>

			<a href="../faq.php"

				onmouseover="changeImages('faq', '../menuimages/faq-over.jpg'); return true;"

				onmouseout="changeImages('faq', '../menuimages/faq.jpg'); return true;">

				<img name="faq" src="../menuimages/faq.jpg" width="51" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_12.jpg" width="35" height="21" alt="" /></td>

		<td>

			<a href="../events.php"

				onmouseover="changeImages('events', '../menuimages/events-over.jpg'); return true;"

				onmouseout="changeImages('events', '../menuimages/events.jpg'); return true;">

				<img name="events" src="../menuimages/events.jpg" width="91" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_14.jpg" width="31" height="21" alt="" /></td>

		<td>

			<a href="../contact.php"

				onmouseover="changeImages('contact', '../menuimages/contact-over.jpg'); return true;"

				onmouseout="changeImages('contact', '../menuimages/contact.jpg'); return true;">

				<img name="contact" src="../menuimages/contact.jpg" width="112" height="21" border="0" alt="" /></a></td>

		<td>

			<img src="../menuimages/menu_16.jpg" width="39" height="21" alt="" /></td>

	</tr>

	<tr>

		<td colspan="15">

			<img src="../menuimages/menu_17.jpg" width="879" height="4" alt="" /></td>

	</tr>

</table>







<table width="879" border="0" cellpadding="0" cellspacing="0" align="center">

	<tr>

<td width="870">

<br><br>

<img src="../siteimages/2012winnersheader.png" width="250" height="75">



<p class="sansserif" align="justify">



<b>

Organizers of the 2012 Professional BC Wedding Awards handed out 24 trophies on November 28, 2012 at the River Rock Theatre.
The winners are those companies with the highest scoring entries. There were a maximum of 2 finalists per category.</b><br><br>
<small>Please note that in some cases, only part of the entry material is shown. All entry text information has been omitted online. Photographer credit was not known for all entries, it has been provided wherever possible.<br>

<br><br>

<table width="870" border="0" align="center">
<tr><td width="50%" valign="top">
<a href='#1'><b><big>Best Wedding Cake</a></big></b><br><br>
<a href='#2'><b><big>Best Bridal Bouquet</a></big></b><br><br>
<a href='#3'><b><big>Best Wedding Florist - Overall</a></big></b><br><br>
<a href='#4'><b><big>Best Wedding Make-up</a></big></b><br><br>
<a href='#5'><b><big>Best Wedding Hair Style</a></big></b><br><br>
<a href='#6'><b><big>Best Wedding Decor</a></big></b><br><br>
<a href='#7'><b><big>Best Wedding Jewelry Design - Accessories</a></big></b><br><br>
<a href='#8'><b><big>Best Wedding Jewelry Design - Rings</a></big></b><br><br>
<a href='#9'><b><big>Best Wedding Transportation</a></big></b><br><br>
<a href="#10"><b><big>Best Wedding Stationery</a></big></b><br><br>
<a href='#11'><b><big>Best Wedding Reception Venue - Alternative Location</a></big></b><br><br>
<a href='#12'><b><big>Best Wedding Reception Venue - Hotel or Banquet Hall</a></big></b><br><br>

</td><td width="50%" valign="top">

<a href='#13'><b><big>Best Wedding Ceremony Location</a></big></b><br><br>
<a href='#14'><b><big>Best Wedding Event Planning</a></big></b><br><br>
<a href='#114'><b><big>Best Catered Wedding</a></big></b><br><br>
<a href='#15'><b><big>Best Candid/Photojournalism Photograph</a></big></b><br><br>
<a href='#16'><b><big>Best Portrait: Bride and Groom together</a></big></b><br><br>
<a href='#17'><b><big>Best Wedding Detail Photograph</a></big></b><br><br>
<a href='#18'><b><big>Best Wedding Group Photograph</a></big></b><br><br>
<a href='#19'><b><big>Best Overall Wedding Photography</a></big></b><br><br>
<a href='#20'><b><big>Best Wedding Videographer/ Cinematographer</a></big></b><br><br>
<a href='#22'><b><big>2012 Tasters Choice - Best Hors D�Oeuvre Award Winner</a></big></b><br><br>
<a href='#23'><b><big>2012 Judges Choice - Best Hors D�Oeuvre Award Winner</a></big></b><br><br>
<a href='#21'><b><big>2012 Industry Achievement Award Winner</a></big></b><br><br>

</td>
</tr><tr>
<td colspan="2" align="center">
</td></tr>
<tr>
<td colspan="2" align="center">
</td></tr>

</table>


</td></tr><a name="categories"></a>
<table width="870" border="0" align="center">
<tr><td align="center" bgcolor="#000000">
<p><strong>Winners:</strong> The winners are those companies with the highest score.</p>
<p><strong>Finalists:</strong> In order to qualify as a finalist the score needs to be within 10% of the winning score.  There is a maximum of 2 finalists per category.</p>



<h2><br><br><strong><a name="1"></a>Best Wedding Cake</strong></h2>
<p align="center">
<br />
<span class="company">Anna Elizabeth Cakes</span><br />

<a href="http://www.annaelizabethcakes.com" target="_blank">www.annaelizabethcakes.com</a><br><br />
<img src="http://www.bcweddingawards.com/winners2012/wc1.jpg" width="400" height="600" border="0">
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>
<!--<iframe src="http://www.bcweddingawards.com/winners2012/Cake/album" width="800" height="750" frameborder="0">
  <p>Your browser does not support iframes.</p>
</iframe>-->

<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Whisk Cake Company

</span><br />
  <a href="http://www.whiskcakes.com" target="_blank">
www.whiskcakes.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/wc2.jpg" width="266" height="400" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Luscious Creations

</span><br />
<a href="http://www.lusciouscreations.ca" target="_blank">www.lusciouscreations.ca</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/wc3.jpg" width="266" height="400" border="0">
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>






<h2><br><br><strong><a name="2"></a>Best Bridal Bouquet</strong></h4>
<p align="center"><span class="company"><br />
  The Flower Factory</span><br />
<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br><br />
<img src="http://www.bcweddingawards.com/winners2012/bbb1.jpg" width="600" height="400" border="0">
</p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg"></p>

<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Bliss Event + Design</span><br />
<a href="http://www.blissevent.ca/" target="_blank">www.blissevent.ca</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/bbb2.jpg" width="400" height="266" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">Verbena Floral Design

</span><br />
<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/bbb3.jpg"  width="400" height="266" border="0" >
<br />
</p></td>
  </tr>
</table>




<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong><a name="3"></a>Best Wedding Florist &#8211; Overall</strong></h4>
<p align="center">

<span class="company">The Flower Factory</span><br />
<a href="http://www.flowerfactory.ca" target="_blank">www.flowerfactory.ca</a><br><br />
<img src="http://www.bcweddingawards.com/winners2012/wf1.jpg" width="600" height="400" border="0">

<br /><br />
</p><p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist"><em>Finalists</em></p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>


    <td><p align="center"><span class="company">Verbena Floral Design

</span><br />
<a href="http://www.verbenafloraldesign.ca" target="_blank">www.verbenafloraldesign.ca</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/wf3.jpg"  width="400" height="266" border="0" >
<br />
</p></td>
<td><p align="center"><span class="company">

Sunflower Florist

</span><br />
<a href="http://www.sunflowerflorist.ca/" target="_blank">www.sunflowerflorist.ca</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/wf2.jpg"  width="400" height="266" border="0" >
<br />
</p></td>
  </tr>
</table>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>


<h2>
<br><br>
<strong><a name="4"></a>Best Wedding Make-up</strong></h4>
<p align="center">
<span class="company">Pink Orchid Studio
</span><br />
<a href="http://www.pinkorchidstudio.com" target="_blank">
www.pinkorchidstudio.com</a><br><br>
<img src="http://www.bcweddingawards.com/winners2012/ma1.jpg" width="458" height="600" border="0">

</p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>
<p align="center" class="finalist"><em>Finalists</em></p>



<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">ma-luxe</span><br />
  <a href="http://www.ma-luxe.com" target="_blank">www.ma-luxe.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/ma2.jpg" width="350" height="350" border="0">
<br />
</p></td>

    <td><p align="center"><span class="company">



Mink Makeup + Hair

</span><br />
<a href="http://www.minkmakeupart.com" target="_blank">www.minkmakeupart.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/ma3.jpg" width="350" height="350" border="0">
<br />
</p></td>
  </tr>
</table>




<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>







<h2><br><br>
<strong><a name="5"></a>Best Wedding Hair Style</strong></h4>
<p align="center">

<br /><br />
<span class="company">Mink Makeup + Hair</span><br />
<a href="http://www.minkmakeupart.com" target="_blank">www.minkmakeupart.com</a><br><br>
<img src="http://www.bcweddingawards.com/winners2012/hs1.jpg" width="400" height="600" border="0">




<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>
<p align="center" class="finalist"><em>Finalists</em></p>



<table width="900" border="0" align="center" cellpadding="10">
  <tr>


    <td><p align="center"><span class="company">Felicia Bromba</span><br />
  <a href="http://www.brides-by-felicia.com" target="_blank">www.brides-by-felicia.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/hs2.jpg" width="267" height="400" border="0">

<br />
</p></td>

    <td><p align="center"><span class="company">



Pink Orchid Studio

</span><br />
<a href="http://www.pinkorchidstudio.com" target="_blank">www.pinkorchidstudio.com

</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/hs3.jpg" width="267" height="400" border="0">

<br />
</p></td>
  </tr>
</table>






<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>







<h2><br><br>
<strong><a name="6"></a>Best Wedding Decor</strong></h4>

<p align="center">
<br /><br />
<span class="company">Art of The Party Design Inc.</span><br />
<a href="http://www.artoftheparty.ca" target="_blank">www.artoftheparty.ca</a><br>

<br>
<img src="http://www.bcweddingawards.com/winners2012/wd1.jpg" width="600" height="400" border="0">

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>


<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br>
<strong><a name="7"></a>Best Wedding Jewelry Design &#8211; Accessories</strong></h4>
<p align="center">

<br /><br />
<span class="company">Whisper Jewels</span><br />
<a href="http://www.whisperjewels.com" target="_blank">www.whisperjewels.com</a><br>
<br>
<img src="http://www.bcweddingawards.com/winners2012/jd1.jpg" width="600" height="400" border="0">


<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>
<p align="center" class="finalist"><em>Finalists</em></p>



<table width="900" border="0" align="center" cellpadding="10">
  <tr>


    <td><p align="center"><span class="company">Elsa Corsi  |  Beautiful Jewellery</span><br />
  <a href="http://www.elsacorsi.com" target="_blank">www.elsacorsi.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/jd2.jpg" width="400" height="224" border="0">

<br />
</p></td>

    <td><p align="center"><span class="company">

The Sonja Picard Collection
</span><br />
<a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com

</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/jd3.jpg" width="400" height="224" border="0">

<br />
</p></td>
  </tr>
</table>




<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br>
<strong><a name="8"></a>Best Wedding Jewelry Design - Rings</strong></h4>

<p align="center">
<span class="company">Jewellery Artists 3D</span><br />
<a href="http://www.ja3d.com" target="_blank">www.ja3d.com</a><br><br>


<img src="http://www.bcweddingawards.com/winners2012/jdr1.jpg" width="600" height="400" border="0">
<br /><br></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalist</p>
<p align="center"><span class="company">
The Sonja Picard Collection</span><br />
  <a href="http://www.sonjapicard.com" target="_blank">www.sonjapicard.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/jdr2.jpg" width="400" height="267" border="0">







<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>








<h2><br><br>
<strong><a name="9"></a>Best Wedding Transportation </strong></h4>
<p align="center"><br />
  <span class="company">Ultimate Limousine</span><br />

<a href="http://www.ultimatelimo4you.com" target="_blank">www.ultimatelimo4you.com</a><br><br>


<img src="http://www.bcweddingawards.com/winners2012/wt1.jpg" width="600" height="400" border="0">


<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><a name="10"></a>Best Wedding Stationery</h2>
<p align="center">
<br />
<span class="company">Love by Phoebe

</span><br />
<a href="http://www.lovebyphoebe.com" target="_blank">www.lovebyphoebe.com</a></span><br><br>


<img src="http://www.bcweddingawards.com/winners2012/sd1.jpg" width="600" height="400" border="0">



<p align="center" class="finalist"><em>Finalists</em></p>



<table width="900" border="0" align="center" cellpadding="10">
  <tr>


    <td><p align="center"><span class="company">Sunlit Letter Press

</span><br />
  <a href="http://www.sunlit-letterpress.com" target="_blank">www.sunlit-letterpress.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/sd2.jpg" width="400" height="267" border="0">

<br />
</p></td>

    <td><p align="center"><span class="company">


Uniquity Invitations



</span><br />
<a href="http://www.uniquityinvitations.com" target="_blank">www.uniquityinvitations.com

</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/sd3.jpg" width="400" height="267" border="0">

<br />
</p></td>
  </tr>
</table>



<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br>
<strong><a name="11"></a>Best Wedding Reception Venue &#8211; Alternative Location</strong></h4>
<p align="center">
<br />
<span class="company">Capilano Suspension Bridge

</span><br />
<a href="http://www.capbridge.com" target="_blank">www.capbridge.com</a></p><br><br>

<img src="http://www.bcweddingawards.com/winners2012/rva1.jpg" width="400" height="600" border="0">

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>



<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">The Wedding Yacht

</span><br />
<a href="http://www.theweddingyacht.com" target="_blank">www.theweddingyacht.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/rva3.jpg" width="400" height="267" border="0">

<br />
</td>
    <td><p align="center"><span class="company">John M.S. Lecky UBC Boathouse
</span><br />
<a href="http://www.ubcboathouse.com" target="_blank">www.ubcboathouse.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/rva2.jpg" width="400" height="267" border="0">

<br /></td>
  </tr>
</table>

<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br>
<strong><a name="12"></a>Best Wedding Reception Venue &#8211; Hotel or Banquet Hall</strong></h4>
<p align="center">
<span class="company"><br />
The Vancouver Club

</span><br />
<a href="http://www.vancouverclub.ca" target="_blank">www.vancouverclub.ca</a><br><br>

<img src="http://www.bcweddingawards.com/winners2012/rv1.jpg" width="600" height="386" border="0">


<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Pinnacle Hotel at the Pier

</span><br />
<a href="http://www.pinnaclepierhotel.com" target="_blank">www.pinnaclepierhotel.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/rv2.jpg" width="400" height="267" border="0">

<br />
</td>
    <td><p align="center"><span class="company">Terminal City Club

</span><br />
<a href="http://www.tcclub.com" target="_blank">www.tcclub.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/rv3.jpg" width="400" height="267" border="0">

<br /></td>
  </tr>
</table>





<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>





<h2><br><br><strong>
<br /><a name="13"></a>Best Wedding Ceremony Location</strong></h2>
<p align="center">
<br />
<span class="company">Rockwater Secret Cove Resort

</span><br />
<a href="http://www.rockwatersecretcoveresort.com" target="_blank">www.rockwatersecretcoveresort.com</a><br><br>

<img src="http://www.bcweddingawards.com/winners2012/cl1.jpg" width="600" height="400" border="0">
<br /><br /><br /><br />

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists<br />
</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><span class="company">Pinnacle Hotel at the Pier

</span><br />
<a href="http://www.pinnaclepierhotel.com" target="_blank">www.pinnaclepierhotel.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/cl2.jpg" width="400" height="267" border="0">
</td>
    <td>
<p align="center"><span class="company">The Chapel Group - Minoru Chapel



 </span><br />
<a href="http://www.thechapels.ca" target="_blank">www.thechapels.ca</a><br><br />
<img src="http://www.bcweddingawards.com/winners2012/cl3.jpg" width="400" height="267" border="0"></p></td>
  </tr>
</table>





<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br><strong>
<br /><a name="14"></a>Best Wedding Event Planning</strong></h2>
<p align="center">
<br />
<span class="company">White Dahlia Weddings</span></p>
<p align="center"><a href="http://www.whitedahliaweddings.com" target="_blank">www.whitedahliaweddings.com</a><br><br>
<img src="http://www.bcweddingawards.com/winners2012/ep1.jpg" width="400" height="600" border="0">


<p align="center" class="finalist">Finalists</p>
<br />
<br />

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td width="640" align="center" valign="top"><p align="center">
<span class="company">Filosophi Events

</span><br />
<a href="http://www.filosophi.com" target="_blank">www.filosophi.com</a></p>

<img src="http://www.bcweddingawards.com/winners2012/ep2.jpg" width="261" height="400" border="0"></p>
<br /></td>


<td width="664" valign="top"><p align="center">
<span class="company">Lori L Fraser Signature Events

</span><br />
<a href="http://www.lorilfrasersignatureevents.com" target="_blank">www.lorilfrasersignatureevents.com</a></p>

<p align="center"><img src="http://www.bcweddingawards.com/winners2012/ep3.jpg" width="267" height="400" border="0"></p></td>
  </tr>
</table>




<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>

<h2><br><br><strong>
<br /><a name="114"></a>Best Catered Wedding </strong></h2>
<p align="center">
<br />
<span class="company">Emelle's Catering Ltd.

</span></p>
<p align="center"><a href="http://www.emelles.com" target="_blank">www.emelles.com</a><br><br><br>

<img src="http://www.bcweddingawards.com/winners2012/cw.jpg" width="266" height="600" border="0">







<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>



<h2><br><br><strong><a name="15"></a>Photography &#8211; Best Candid/Photojournalism Photograph</strong></h2>



<p align="center"><span class="company"><br />
 Leanne Pedersen Photographers Ltd.</span><br />
<a href="http://www.leannepedersen.com" target="_blank">www.leannepedersen.com</a></p><br><br>

<img src="http://www.bcweddingawards.com/winners2012/pj1.jpg" width="600" height="400" border="0">
<br />
<br />

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>
<br />
<br />

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td width="640" align="center" valign="top"><p align="center">
<span class="company">StonePhoto</span><br />
<a href="http://www.stonephoto.ca" target="_blank">www.stonephoto.ca</a></p>

<img src="http://www.bcweddingawards.com/winners2012/pj2.jpg" width="400" height="266" border="0">
</p>
<br /></td>


<td width="664" valign="top"><p align="center">
<span class="company">Michael Wachniak Photographer</span><br />
<a href="http://www.michaelwachniak.com" target="_blank">www.michaelwachniak.com</a></p>

<img src="http://www.bcweddingawards.com/winners2012/pj3.jpg" width="400" height="400" border="0">
</p></td>
  </tr>
</table>


<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>




<h2><br><br><strong><a name="16"></a>Photography &#8211; Best portrait: Bride and Groom Together</strong></h2>

 <p align="center"> <br />
   <span class="company">Pardeep Singh Photography</span><br />
<a href="http://www.pardeepsingh.ca" target="_blank">www.pardeepsingh.ca</a></p><br><br>


<img src="http://www.bcweddingawards.com/winners2012/bg1.jpg" width="400" height="600" border="0">
<br />
<br />
<br />
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center">
<span class="company">Jen Steele Photography</span><br />
<a href="http://www.jensteele.com" target="_blank">www.jensteele.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/bg2.jpg" width="400" height="267" border="0">
</td>
    <td>
<p align="center"><span class="company">Randal Kurt Photography</span><br />
<a href="http://www.randalkurt.com" target="_blank">www.randalkurt.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/bg3.jpg" width="400" height="267" border="0">

</td>
  </tr>
</table>



<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>








<h2><br><br><strong><a name="17"></a>Photography &#8211; Best Wedding Detail Photograph</strong></h2>
<p align="center">
<br />

<span class="company">John Bello Photographer</span><br />
<a href="http://www.bohnjello.com" target="_blank">www.bohnjello.com</a></p><br><br>


<img src="http://www.bcweddingawards.com/winners2012/d1.jpg" width="600" height="400" border="0">

<br />
<br />
<br />
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center">
<span class="company">Butter Studios</span><br />
<a href="http://www.butterstudios.ca" target="_blank">www.butterstudios.ca</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/d2.jpg" width="300" height="400" border="0">
</td>
    <td>
<p align="center"><span class="company">Brellow Productions</span><br />
<a href="http://www.brellowproductions.com" target="_blank">www.brellowproductions.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/d3.jpg" width="267" height="400" border="0">

</td>
  </tr>
</table>


<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>









<h2><br><br><strong><a name="18"></a>Photography &#8211; Best Wedding Group Photograph</strong></h2>
<p align="center">
  <span class="company"><br />
  Erin Wallis Photography</span><br />
<a href="http://www.erinwallis.com" target="_blank">www.erinwallis.com</a><br><br>

<img src="http://www.bcweddingawards.com/winners2012/g1.jpg" width="600" height="400" border="0">


<br />
<br />
<br />
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><br />
  <span class="company">Abby Photography</span><br />
<a href="http://www.abbyphotography.ca/" target="_blank">www.abbyphotography.ca/</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/g2.jpg" width="400" height="267" border="0">

<br /></td>

    <td><p align="center"><br />
  <span class="company">Raymond & Jessie Photography</span><br />
<a href="http://www.raymondleung.ca" target="_blank">www.raymondleung.ca</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/g3.jpg" width="400" height="223" border="0">
</td>
  </tr>
</table>



<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>







<h2><br><br><strong><a name="19"></a>Photography &#8211; Best Overall Wedding Photography</strong></h2>
<p align="center">


  <span class="company"><br />
 JONETSU STUDIOS</span><br />
<a href="http://www.jonetsuphotography.com" target="_blank">www.jonetsuphotography.com</a><br><br>

<br>

<img src="http://www.bcweddingawards.com/winners2012/o1.jpg" width="529" height="600" border="0">

<br />
<br />
<br />
<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>

<table width="900" border="0" align="center" cellpadding="10">
  <tr>
    <td><p align="center"><br />
  <span class="company">Shari + Mike Photographers</span><br />
<a href="http://www.shariandmike.ca" target="_blank">www.shariandmike.ca</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/o2.jpg" width="400" height="267" border="0">

<br /></td>

    <td><p align="center"><br />
  <span class="company">Daniela Ciuffa Photography</span><br />
<a href="http://www.ciuffaphotography.com" target="_blank">www.ciuffaphotography.com</a></p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/o3.jpg" width="286" height="400" border="0">
</td>
  </tr>
</table>



<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>






<h2><br><br><strong><a name="20"></a>Best Wedding Videography/Cinematography</strong></h2>

 <p align="center"><span class="company">Modern Romance Productions</span><br />
<a href="http://www.modernromance.ca" target="_blank">www.modernromance.ca</a></p>

<p align="center"><iframe src="http://player.vimeo.com/video/50351368" width="500" height="282" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

  <br />

<p align="center"><img src="http://www.bcweddingawards.com/winners2011/divider.jpg" width="300" height="70"></p>

<p align="center" class="finalist">Finalists</p>
<table width="1121" border="0" align="center">
  <tr>
    <td><p align="center"><span class="company">Perfect Pair Media</span><br>
<a href="http://www.perfectpairmedia.ca" target="_blank">www.perfectpairmedia.ca</a></p>
<p align="center"><iframe src="http://player.vimeo.com/video/54001492" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

<br /></td>

    <td><p align="center"><span class="company">Glimmer Films Inc.</span><br />
<a href="http://www.glimmerfilms.ca" target="_blank">www.glimmerfilms.ca</a></p>
<p align="center"><iframe src="http://player.vimeo.com/video/51266853" width="500" height="213" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
<br /></td>
  </tr>
</table>








<p align="center">&nbsp;</p>
<br>
<p align="right"><a href="#categories" >return to category listing</a><img src="spacer.gif" width="120" height="5" border="0"></p>
<p align="center"><img src="http://www.bcweddingawards.com/siteimages/dots.jpg" align="center"></p>







<h2><br><br><strong><a name="21"></a>Industry Achievement Award</strong></h2>
Industry Achievement Award:



<p align="center"><a href="www.flower-z.com"><img src="http://www.bcweddingawards.com/winners2012/evan.jpg" border="0" ></a>
  <br />
  <span class="company">Evan Orion - Flowerz </span><br />
<a href="http://www.flower-z.com" target="_blank">www.flower-z.com</a></p>
<p align="center" class="company">Other Notable Individuals who were voted for an Industry Achievement Award<br />
(Random)</p>
<p align="center" class="company">
Alicia Keats<br>
Colin Upright<br>
Devon Hird<br>
Lisa Gratton<br>
Soha Lavin<br>
Erin Bishop<br>
Corrine Colledge
<p align="center">&nbsp;</p>


<br>
<p align="right"><a href="#categories" >return to category listing</a><br><img src="http://www.bcweddingawards.com/siteimages/dots.jpg"></p>



<h2><br><br><strong><a name="22"></a>2012 Tasters Choice - Best Hors D�Oeuvre Award Winner</strong></h2>

  <p align="center"><span class="company">Truffles Fine Foods
  </span><br />
<a href="http://www.trufflesfinefoods.com/" target="_blank">www.trufflesfinefoods.com/</a>
<br>Poached Quail Egg
Served on an Indian Candy Rosti
with Herb Foam and Caviar</p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/trufflesfinefoods.jpg" width="600" height="300" border="0" alt="photo credit">
  <br />

<h2><br><br><strong><a name="23"></a>2012 Judges Choice - Best Hors D�Oeuvre Award Winner</strong></h2>

  <p align="center"><span class="company">Emmelle's
  </span><br />
<a href="http://www.emelles.com/" target="_blank">www.emelles.com/</a>
<br>Blue Cheese & Walnut Dusted
Ruby & Golden Beet Duet
Nestled on a Parmesean Tuille
& Bed of Arugula Chiffonade.</p>
<p align="center"><img src="http://www.bcweddingawards.com/winners2012/emelles.jpg" width="453" height="600" border="0" alt="photo credit">
  <br />



<h2><br><br><strong>PARTICIPANTS</strong></h2>
<center>
<table align="center" border="0"><tr><td align="center">
<font color="white" face="arial" size="2"><b>Kale and Nori</b><br />
<a href="http://www.kaleandnori.com/" target="_blank">www.kaleandnori.com/</a>
<br>Korean Beef �meatloaf�
Served with Crispy Bourbon Barrel Aged
Fish Sauce and Sweet and Hot
Okanagan Chili Pepper Kim Chi
<img src="http://www.bcweddingawards.com/winners2012/kaleandnori.jpg" width="300" height="451" border="0" >
</td>

<td align="center"><span class="company"><font color="white" face="arial" size="2"><b>Edgeceptional Catering</b>
<br />
<a href="http://edgecatering.ca/" target="_blank">www.edgecatering.ca/</a>
<br>Edge Made Duck Proscuitto
on a Phyllo Spoon with Fig Compote
& Orange Glaze, served with microgreens
<img src="http://www.bcweddingawards.com/winners2012/edgeceptional.jpg" width="300" height="451" border="0" >
</td>

<td align="center"><span class="company"><font color="white" face="arial" size="2"><b>River Rock Catering</b><br />
<a href="http://www.riverrock.com/" target="_blank">www.riverrock.com/</a>
<br>Qualicum Beach Scallop Tartar
with Spinach & Black Sesame Cone
- Wasabi Mayonnaise
Flying Fish Rose, Kaiware Sprouts
<img src="http://www.bcweddingawards.com/winners2012/rr.jpg" width="300" height="451" border="0" >



</td></tr>

<tr><td colspan="4"><center><span class="company"><font color="white" face="arial" size="2">
Our sincere thanks to all of this years competing Caterers.<br />You all did an amazing job in not only the conception and execution of your dish but also the fantastic presentation that each of you had. <img src="http://www.bcweddingawards.com/winners2012/cateringjudges2012.jpg" width="600" height="357"border="0" align="center" ><br><b>Special Thanks also to our Judges - (l to r)</b><br>Mijune Pak of <a href="www.followmefoodie.com" target="_blank">Follow Me Foodie</a><br>Julian Bond of  <a href="www.picachef.com/" target="_blank"> Pacific Institute of Culinary Arts</a><br>Lindsay Anderson of <a href="http://www.365daysofdining.com/" target="_blank">365 Days of Dining </a>!
</td></tr>
</table>
<font color="white" face="arial" size="2">
Our most sincere congratulations to all of the winners and finalists, well done!
<br><br><center>
<a href="http://www.bcweddingawards.com/pastwinners.html">
<img src="http://www.bcweddingawards.com/siteimages/pastwinnersheader.png" width="250" height="75" border="0"></a>
</center>


<br><br></p>
</div>

</td></tr>
<table>

</table>



<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>

<td><img src="../siteimages/dots.jpg" width="879" height="9" alt="" /></td>

	</tr>

	</table>

<table width="879" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr>



   <td style="padding-right:5px;padding-left:5px;" align="left">



     <a href="index.html" >HOME</a>


    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

    <a href="http://www.bcweddingawards.com/categories2012.php">CATEGORIES</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/judging.html">JUDGING</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/faq.html">FAQ</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/events.html">EVENTS</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

   <a href="http://www.bcweddingawards.com/contact.php">CONTACT</a>

    &nbsp;&nbsp;  <!-- <img src="images/grey.gif"  width=1 height=7 border=0 vspace=0 hspace="5" alt="meta"align="bottom">-->

    </td>





	</tr>

</table>





<MAP NAME="links_Map">

<AREA SHAPE="rect" ALT="" COORDS="202,0,224,25" HREF="http://twitter.com/BCweddingAwards" TARGET="_blank">

<AREA SHAPE="rect" ALT="" COORDS="177,0,200,25" HREF="http://www.facebook.com/pages/Professional-BC-Wedding-Awards/137667716271526?created" TARGET="_blank">

</MAP>

<MAP NAME="galasponsorbar_tall_Map">
<AREA SHAPE="rect" ALT="" COORDS="339,86,527,124" HREF="http://www.perfectpairmedia.ca/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="540,87,657,123" HREF="http://www.pedersens.com/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="669,86,844,122" HREF="http://www.mcleanbartok.ca/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="669,16,845,75" HREF="http://www.audioedge.ca/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="521,14,661,74" HREF="http://www.savourycity.com/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="454,13,512,74" HREF="#">
<AREA SHAPE="rect" ALT="" COORDS="409,15,448,75" HREF="http://www.emelles.com/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="254,11,402,74" HREF="http://cheflaura.ca/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="117,23,249,62" HREF="http://www.pixstarphotobooth.com/" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="212,75,333,134" HREF="http://www.district319.com" TARGET="_blank">
<AREA SHAPE="rect" ALT="" COORDS="122,75,207,135" HREF="http://www.wishevents.ca/" TARGET="_blank">
</MAP>




</body>



</html>

