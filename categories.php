<html>
<head>
<title>The Professional BC Wedding Awards</title>
</head>
<body>
<?php include("top.inc"); ?>

<section class="slide fade kenBurns">
	<div class="content">
		<div class="container">
    		<div class="wrap cat_con">
				<h1>Categories</h1>
				<span>
					<p>Submissions now closed - thanks to all who submitted!</p>
					<br><br>
					A big THANK YOU to everyone who submitted this year - we are thrilled by the quality of work we have been receiving from all over BC.
					<br><br>
				    It's in the judges hands now -  Winners and Finalists will be announced live at the Professional BC Wedding Awards Night November 23rd, 2016 at the Hard Rock Casino.
					<br><br>
					RSVP information will be sent out in the next few weeks, and in the meantime,
					<a href="http://www.facebook.com/pages/Professional-BC-Wedding-Awards/137667716271526">like us on Facebook</a> to keep in touch with the most up-to-date information.
					<br><br>
					See you at the Professional BC Wedding Awards Night November 23rd at the Hard Rock Casino</b><br><br><br><br>
				</span>
				<span class="link_txt">
					<a href="winnerspackage.php" ><b><p>To check out what the winners receive, click here.</p></b></a>
				</span>
			</div>			
		</div>
	</div>
	<div class="background" style="background-image:url(http://bcweddingawards.com/pix/slides/awards.jpg)"></div>
	</section>
<?php include("bottom2.inc"); ?>